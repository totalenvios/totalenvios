<?php
	require './vendor/autoload.php';

	include 'includes/connection.class.php';
	include 'includes/packages.class.php';
    include 'includes/invoices.class.php';
	include 'includes/users.class.php';
    include 'includes/transport.class.php';

	use Spipu\Html2Pdf\Html2Pdf;

    $wh_id = $_REQUEST['id'];
    // echo $wh_id;
    $packages = new Packages;
    $transp = new Transport;
    $users = new Users;
    $allPackages = $packages->findPackageEnvPDF($wh_id);

   

    $codesDir = "./";   
    $codeFile = date('d-m-Y-h-i-s').'.png';
    // echo '<img class="img-thumbnail" src="'.$codesDir.$codeFile.'" />';
       


	
	// Variable para el conteo total de cajas para ese warehouse
	
		try {

	    
			$html2pdf = new Html2Pdf('p','A4','en',true,'UTF-8',array(20,20,50,5));
			$html2pdf->setTestTdInOnePage(false);
            $html2pdf->writeHTML('
            <div style="width:100%;">
               <table style="width: 100%; padding-top:-10px;">
                    <tr>
                        <td style="text-align: left;width=33%;">
                            <div style="font-size:8px; color:gray;"><img src="./logo-invoice.png"/><br>
                            <label>10625 nw 122st, Medley, Florida.33178. Estados Unidos</label><br><label>1 754-236-2131/58 4148898004</label><br>
                            <label>www.totalenvios.com</label><br><label></label>ICONOS<br>
                            <label>Recibo Nro: '.$wh_id.'</label></div>
                            
                        </td>
                    </tr>
                </table>
            </div><br>
              
            ');
            $html2pdf->writeHTML('<div style="width:100%;">
            <table cellpadding="0" cellspacing="0">
           
			<tr>
				<td style="width: 90px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div align="center"><label>NRO PAQUETE</label></div></td>
                <td style="width: 90px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: gray; font-size:10px;"><div align="center"><label>TRACKING</label></div></td>
				<td style="width: 90px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div align="center"><label>INGRESO</label></div></td>
                <td style="width: 90px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div align="center"><label>DESTINO</label></div></td>
				<td style="width: 90px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: gray; font-size:10px;"><div align="center"><label>PROVEEDOR</label></div></td>
                <td style="width: 90px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div align="center"><label>DESCRIPCIÓN</label></div></td>
				<td style="width: 90px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: gray; font-size:10px;"><div align="center"><label>OPERADOR</label></div></td>
				
			</tr>
			</table>
            </div>');

           foreach ($allPackages as $key => $value) {

            if ($value['tracking'] == null) {
                $tracking= "N/A";
            }else{
                $tracking = $value['tracking'];; 
            }

            $proveedor= $transp->findById($value['id_transp'])['name'];

            if ($value['operator'] == 0) {
                $operador= "No tiene operador";
            }else{
                $operador= $users->findById($value['operator'])['name']; 
            }

            $html2pdf->writeHTML('<div style="border-color: grey; width:90%">
            <table cellpadding="0" cellspacing="0">
			<tr>
            <td style="width: 90px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div align="center"><label>'.$value['code'].'</label></div></td>
            <td style="width: 90px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div align="center"><label>'.$tracking.'</label></div></td>
            <td style="width: 90px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div align="center"><label>'.$value['date_in'].'</label></div></td>
            <td style="width: 90px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div align="center"><label>'.$value['address_send'].'</label></div></td>
            <td style="width: 90px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div align="center"><label>'.$proveedor.'</label></div></td>  
            <td style="width: 90px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div align="center"><label>'.$value['description'].'</label></div></td>  
            <td style="width: 90px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div align="center"><label>'.$operador.'</label></div></td>  
            </tr>
			</table>
            </div>');
            };

            
           
           
			$html2pdf->Output('packages.pdf');
		} catch (PDOException $e) {
			echo $e;
		}
	
?>