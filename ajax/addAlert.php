<?php  

	include '../includes/connection.class.php';
	include '../includes/users.class.php';
	include '../includes/packages.class.php';

	$users = new Users;
	$packages = new Packages;

	session_start();

	$id_alert = $packages->addAlert($_POST['tracking'], $_POST['date_in'], $_POST['id_client'], $_POST['package_name'], $_POST['transp'], $_POST['date_in'], $_POST['content'], $_POST['cost'], $_POST['casillero'], $_POST['proveedor'], $_POST['status']);

	$target_dir = "assets/img/alert-invoices/";

	$target_file = $target_dir . basename($_FILES["alert-invoice"]["name"]);

	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	$check = getimagesize($_FILES["alert-invoice"]["tmp_name"]);

	if($check !== false) {

	  $uploadOk = 1;

	}else{
		
	  $uploadOk = 0;

	}

	if ($_FILES["alert-invoice"]["size"] > 5000000) {
	  $uploadOk = 0;
	}

	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" && $imageFileType != 'pdf') {
	  $uploadOk = 0;
	}

	if ($uploadOk == 0) {
	} else {
	  $name = rand().'-'.htmlspecialchars( basename( $_FILES["alert-invoice"]["name"]));
	  if (move_uploaded_file($_FILES["alert-invoice"]["tmp_name"], '../assets/img/alert-invoices/'.$name)) {
	    echo $name;
	    $packages->updateAlertImg($id_alert, $name);
	  }
	}

	echo $id_alert;

?>