<?php  
	
	ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);
	// echo "string1";
	include '../includes/connection.class.php';
	include '../includes/packages.class.php';
	include '../includes/warehouses.class.php';
	include '../includes/users.class.php';

	$users = new Packages;
	$destiner = new Users;
	$warehouses = new Warehouses;

	$wich = $_REQUEST['w'];
	$id = $_REQUEST['id'];
	$warehouse_id = $_REQUEST['wh_id'];
	$data = 0;

	

	if ($wich == 1) {
		$currier_id = $_REQUEST['currier_id'];
		$destiner_id = $_REQUEST['destiner'];
		$name = $_REQUEST['name'];
		$width = $_REQUEST['width'];
		$height = $_REQUEST['height'];
		$lenght = $_REQUEST['lenght'];
		$weight = $_REQUEST['weight'];
		$description = $_REQUEST['description'];
		$category = $_REQUEST['category'];
		$cost = $_REQUEST['cost'];
		$trip_id = $_REQUEST['trip_id'];

		// valores de medidas y costo

		$cubic_feet = $_REQUEST['cubic_feet'];
		$lb_vol = $_REQUEST['lb_vol'];
		$insurance = $_REQUEST['insurance'];
		$logistic = $_REQUEST['logistic'];
		$extra_cost = $_REQUEST['extra_cost'];
		$tarifa_currier = $_REQUEST['tarifa_currier'];
		$tarifa_te = $_REQUEST['tarifa_te'];
		$costo_currier = $_REQUEST['costo_currier'];
		$monto_te = $_REQUEST['monto_te'];
		$total = $_REQUEST['total'];
		$ganancia = $_REQUEST['ganancia'];
		$porc_ganancia = $_REQUEST['porc_ganancia']; 
		$operator = $_REQUEST['operator_id']; 

		$tarifa_interna_base=$_REQUEST['tarifa_interna_base'];
		$costo_interno_total=$_REQUEST['costo_interno_total'];

		//*********************************************************** */ 
		
		$code = $_REQUEST['id'];
		$active = 1;
		$kg_weight = number_format((($_REQUEST['weight']*0.4535)),2);
		// $cubic_feet = number_format(((($_REQUEST['width']*$_REQUEST['width']*$_REQUEST['lenght'])/1728)),2);
		$cubic_m = number_format(($cubic_feet*0.02831),2);
		// $lb_vol = number_format((($_REQUEST['width']*$_REQUEST['width']*$_REQUEST['lenght'])/166),2);
		$m_vol = number_format((($_REQUEST['width']*$_REQUEST['width']*$_REQUEST['lenght'])/355),2);

		$state_id = $destiner->findAddresseeById($destiner_id);
		$state = $state_id[0]['state_id'];
		$region = $destiner->findRegionByState($state);
		$region_id = $region['region_id'];

		
		
		$id = $users->addBoxes(
			$id, 
			$name, 
			$code, 
			$width, 
			$height, 
			$lenght, 
			$weight, 
			$description, 
			$cost, 
			$category, 
			$active, 
			$kg_weight,
			$cubic_feet, 
			$cubic_m, 
			$lb_vol, 
			$m_vol, 
			// valores a consultar
			$tarifa_currier, 
			$tarifa_te,
			$costo_currier, 
			$tarifa_interna_base,
			$costo_interno_total,
			$total,
			// valores a calcular
			$monto_te,
			
			$ganancia,
			$porc_ganancia, 
			$insurance,
			$logistic,
			$extra_cost,
			$operator
		);
		$create =  $users->addBoxesForWarehouse($id, $warehouse_id);
		$data = $warehouses->findBoxesWarehouse($warehouse_id);
		// print_r($data);
	}else{
		// echo $id.'---'.$warehouse_id;
		$create =  $users->addBoxesForWarehouse($id, $warehouse_id);
		$data = $warehouses->findBoxesWarehouse($warehouse_id);
	}
	
	// print_r($data);
	foreach ($data as $key => $value) { 
?>
		<tr>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['id']; ?></td>
			<td><?php echo $warehouses->findCategoryById($warehouses->findBoxesById($value['id_box'])['category_id'])['name']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['description']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cost']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['lenght']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['width']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['height']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['weight']; ?></td>
			
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cubic_feet']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['lb_vol']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cost_taf_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cost_inter_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['gastosExtra']; ?></td>
			
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['monto_calculo_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['tasa_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cost_total']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['ganancia_cost_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['porcentaje_ganancia_cost_type_trip']; ?></td>

			<td>
				<div class="btn-group" style="content-align:center; width:100%">
				<button onclick="deleteBox(<?php echo $value['id_box']; ?>, <?php echo $warehouse_id ?>)" class="btn btn-primary" style="margin-top:30px; background:#fc3c3d; color:white;"><i class="fas fa-trash"></i></button>
				<button onclick="editBoxForTable(<?php echo $value['id_box']; ?>, <?php echo $warehouse_id ?>)" class="btn btn-primary" style="margin-top:30px; background:#fc3c3d; color:white;"><i class="fas fa-edit"></i></button>
				</div>
			</td>
		</tr>
<?php
	}
?>