<?php  

include '../includes/connection.class.php';
include '../includes/invoices.class.php';
include '../includes/packages.class.php';
include '../includes/users.class.php';
include '../includes/warehouses.class.php';
include '../includes/packing.class.php';

$packing = new Packing;

$gastos_extras_id = $_REQUEST['gastos_extras_id'];
$gastos_extras = $_REQUEST['gastos_extras'];


$invoices = new Invoices;
$allInvoices = $invoices->addGastosExtras($gastos_extras_id,$gastos_extras,0); 
$extraCost = $packing->findExtraCost();

foreach ($extraCost as $key => $value) { 
    ?>
            <tr>
                <td><?php echo $packing->findExtraCostName($value['extracost_id'])[0]['name']; ?></td>
                <td><?php echo $value['value_extracost']; ?></td>
               
    
                <td>
                    <div class="btn-group" style="content-align:center; width:100%">
                    <button onclick="deleteExtraCost(<?php echo $value['id']; ?>)" class="btn btn-primary" style="margin-top:30px; background:#fc3c3d; color:white;"><i class="fas fa-trash"></i></button>
                    <button onclick="editExtraCost(<?php echo $value['id']; ?>)" class="btn btn-primary" style="margin-top:30px; background:#fc3c3d; color:white;"><i class="fas fa-edit"></i></button>
                    </div>
                </td>
            </tr>
    <?php
        }
    ?>
