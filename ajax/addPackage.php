<?php  

	include '../includes/connection.class.php';
	include '../includes/users.class.php';
	include '../includes/packages.class.php';

	$users = new Users;
	$packages = new Packages;

	session_start();

	$packages->code = $_POST['code'];
	$packages->user_to = $_POST['user_to'];
	$packages->user_from = $_POST['user_from'];
	$packages->date_in = $_POST['date_in'];
	$packages->date_out = $_POST['date_out'];
	$packages->alias = $_POST['alias'];
	$packages->operator = $_POST['operator'];
	$packages->tracking = $_POST['tracking'];
	$packages->description = $_POST['description'];
	$packages->address_send = $_POST['address_send'];
	$packages->state = $_POST['state'];
	$packages->height = $_POST['height'];
	$packages->lenght = $_POST['lenght'];
	$packages->width = $_POST['width'];
	$packages->weight = $_POST['weight'];
	$packages->headquarter = $_POST['headquarter'];
	$packages->stock = $_POST['stock'];
	$packages->transp = $_POST['transp'];
	$packages->cost = $_POST['cost'];
	$packages->alert_id = $_POST['alert_id'];

	$id_package = $packages->add();
	$code = 'TE-PK'.$id_package;
	$res = $packages->updatePackageCode($code, $id_package);

	echo $id_package;
	
	$target_dir = "assets/img/paquetes/";

	$target_file = $target_dir . basename($_FILES["paquete"]["name"]);

	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	$check = getimagesize($_FILES["paquete"]["tmp_name"]);

	if($check !== false) {

	  $uploadOk = 1;

	}else{
		
	  $uploadOk = 0;

	}

	if ($_FILES["paquete"]["size"] > 5000000) {
	  $uploadOk = 0;
	}

	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" && $imageFileType != 'pdf') {
	  $uploadOk = 0;
	}

	if ($uploadOk == 0) {
	} else {
	  $name = rand().'-'.htmlspecialchars( basename( $_FILES["paquete"]["name"]));
	  if (move_uploaded_file($_FILES["paquete"]["tmp_name"], '../assets/img/paquetes/'.$name)) {
	    echo $name;
	    $packages->updateImg($id_package, $name);
	  }
	}


?>