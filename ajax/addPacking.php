<?php  

	include '../includes/connection.class.php';
	include '../includes/users.class.php';
	include '../includes/packing.class.php';

	$packing = new Packing;

	$packing->code = $_REQUEST['code'];
	$packing->date_in = $_REQUEST['date_in'];
	$packing->date_out = $_REQUEST['date_out'];
	$packing->name = $_REQUEST['name'];
	$packing->note = $_REQUEST['note'];
	$packing->id_currier = $_REQUEST['id_currier'];
	$packing->id_trip = $_REQUEST['id_trip'];
	$packing->country = $_REQUEST['country'];
	$packing->state = $_REQUEST['state'];
	$packing->bulk = $_REQUEST['bulk'];
	
	if ($_REQUEST['bulk'] == 0) {
		$packing->id_warehouse = $_REQUEST['id_warehouse'];
	}else{
		$packing->id_warehouse = 0;
	}

	if ($packing->add()) {
		echo 1;
	}

?>