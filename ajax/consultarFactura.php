<?php
include '../includes/connection.class.php';
include '../includes/packages.class.php';
if(!empty($_GET['id'])){

    //DB details
    $dbHost = 'localhost';
    $dbUsername = 'root';
    $dbPassword = '';
    $dbName = 'total_envios_1108';
    
    //Create connection and select DB
    $db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);
    
    if ($db->connect_error) {
        die("Unable to connect database: " . $db->connect_error);
    }
    
    //get content from database
    $query = $db->query("SELECT * FROM alerts WHERE id = {$_GET['id']}");
    
    echo '<div style="height:100%; background-color:#005574;">
    <div class="row p-2">
        <div class="col-12">
            <img width="80" src="assets/img/logo.png" alt="">
        </div>
    </div>                         
 </div>';

 echo '<div class="mt-4 p-3">';

    if($query->num_rows > 0){
        $cmsData = $query->fetch_assoc();
        echo '<h3 style="color:#ff554dff; font-size:17px;"> Tracking: <span style="color: black; float:right;">'.$cmsData['tracking'].'</span></h3>';
        echo '<h3 style="color:#ff554dff; font-size:17px;"> Nro de Factura: <span style="color: black; float:right;">'.$cmsData['id'].'</span></h3>';
        $query2= $db->query("SELECT * FROM transp WHERE id = {$cmsData['transp']}");
        if($query2->num_rows > 0){
            $cmsData2 = $query2->fetch_assoc();
            // echo $cmsData2['name'];
            echo '<h3 style="color:#ff554dff; font-size:17px;"> Transportistas: <span style="color: black; float:right;">'.$cmsData2['name'].'</span></h3>';
        }
        // echo '<h3 style="color:#ff554dff; font-size:17px;"> Transporte: <span style="color: black; float:right;">'.$cmsData['transp'].'</span></h3>';
        echo '<h3 style="color:#ff554dff; font-size:17px;"> Contenido: <span style="color: black; float:right;">'.$cmsData['content'].'</span></h3>';
        echo '<h3 style="color:#ff554dff; font-size:17px;"> Valor USD: <span style="color: black; float:right;">'.$cmsData['cost'].'$</span></h3>';
    }else{
        echo 'Content not found....';
    }
echo '</div>';
}else{
    echo 'Content not found....';
}
?>