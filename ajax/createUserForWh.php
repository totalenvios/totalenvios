<?php  


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include '../includes/connection.class.php';
include '../includes/users.class.php';
include '../includes/addressees.class.php';

$users = new Users;
$addresse = new addressees;

$seleccion_venezuela = $_REQUEST['countryVenezuela'];
// consultar si debo usar id's o nombres de los estados y ciudades
if($seleccion_venezuela == 1)
{
	// si el pais es venezuela se toma el id 1 de la tabla countries_register para el usuario y el id correspondiente de la tabla countries en destinatarios
	//Para los estados y ciudades solo se consideran los id's

		$w = $_REQUEST['w']; 
		$users->name = $_REQUEST['name'];
		$users->lastname = $_REQUEST['lastname'];
		$users->email = $_REQUEST['email'];
		$users->password = password_hash($_REQUEST['password'], PASSWORD_DEFAULT);
		$users->phone = $_REQUEST['phone'];
		$users->phone_house = isset($_REQUEST['phone_house']) ? $_REQUEST['phone_house'] : '';
		$users->document = $_REQUEST['documentU'];
		

		$checkIfExists = $users->checkIfExists()['cuantos'];

		if ($checkIfExists > 0) {
			echo "Ya el usuario existe";
		}else{

			$users->country = "VENEZUELA";
			$users->user_type = $_REQUEST['user_type'];
			$users->headquarter = $_REQUEST['headquarter'];
			$users->code = $_REQUEST['code'];
			$users->born_date = $_REQUEST['born_date'];
			$users->state_id = $_REQUEST['address_state'];
			$users->city_id = $_REQUEST['address_city'];
			$users->address = $_REQUEST['address'];
			$users->state_name = $users->findRegionByState($_REQUEST['address_state'])['estado'];
			$users->city_name = $users->findcityByIdForClient($_REQUEST['address_city'])['name'];
			$users->country_id = $_REQUEST['country'];

			$user_id = $users->add();
			// asignar codigo de casillero
			$code = 'TE-'.$user_id;
			$user_code = $users->asignCode($user_id,$code);
			// registrar destinatario
			

			// registrar destinatario
			$addresse->country_id = $_REQUEST['country'];
			$addresse->state_id = $_REQUEST['address_state'];
			$addresse->city_id = $_REQUEST['address_city'];
			$addresse->address = $_REQUEST['address'];
			$addresse->country_name = "VENEZUELA";
			$addresse->state_name = $users->findRegionByState($_REQUEST['address_state'])['estado'];
			$addresse->city_name = $users->findcityByIdForClient($_REQUEST['address_city'])['name'];

			$addresse_id = $addresse->add($user_id,$_REQUEST['name'], $_REQUEST['lastname'],$_REQUEST['phone']);
			
		$mail = new PHPMailer(true);
		 // $mail->SMTPDebug = 3;                                  // Enable verbose debug output  
		 $mail->isSMTP();                                       // Set mailer to use SMTP  
		 $mail->Host = 'smtp.hostinger.es';                       // Specify main and backup SMTP servers  
		 $mail->SMTPAuth = true;                                // Enable SMTP authentication  
		 $mail->Username = 'notification@debartesting.online';               // your SMTP username  
		 $mail->Password = 'Zuleta99';                      // your SMTP password  
		 $mail->SMTPSecure = 'tls';                             // Enable TLS encryption, `ssl` also accepted  
		 $mail->Port = 587;                                     // TCP port to connect to  
		 $mail->setFrom('notification@debartesting.online', 'TOTAL ENVIOS');  
		 // $mail->addAddress('ramonbriceno12@gmail.com', 'Ramon');
		 $mail->addAddress($_REQUEST['email'], 'TOTAL ENVIOS');
		 $mail->isHTML(true);                                   // Set email format to HTML  
		 $mail->Subject = 'BIENVENIDO A TOTAL ENVIOS';
		 $mail->Body  = '
		 	<div class="container">
				<div class="card">
		 			<div class="card-body">
		 			    <h2 class="card-title">¡Hola! Bienvenido a Total Envios</h2>
		 			    <p class="card-text">Aqui puedes confirmar tu correo: http://192.241.240.138/totalenvios_bk/confirmemail.php?id='.$user_id.'</p>
		 			  	</div>
					</div> <br>
		 			<div class="text-center">
						TOTAL ENVIOS
		 			</div>
		 		</div>
		 	</div>';

		 if($mail->send()) {  
		    // echo 'Message has been sent';  
			} else {  
		    // echo 'Message could not be sent';  
	 	 }
			
			
			
			
			echo $user_id;
			// registrar destinatario
	 }


}else{
	// si el pais no es venezuela se toma el id correspondiente de la tabla countries_register para el usuario y el destinatario
	// Para el estado y la ciudad se considera el nombre del estado y de la ciudad para registrar el tabla state_register y ciudades.

	$w = $_REQUEST['w']; 
		$users->name = $_REQUEST['name'];
		$users->lastname = $_REQUEST['lastname'];
		$users->email = $_REQUEST['email'];
		$users->password = password_hash($_REQUEST['password'], PASSWORD_DEFAULT);
		$users->phone = $_REQUEST['phone'];
		$users->phone_house = isset($_REQUEST['phone_house']) ? $_REQUEST['phone_house'] : '';
		$users->document = $_REQUEST['documentU'];

	$checkIfExists = $users->checkIfExists()['cuantos'];

		if ($checkIfExists > 0) {
			echo "Ya el usuario existe";
		}else{

			$users->country = $users->findCountryRegister($_REQUEST['country'])['name'];;
			$users->user_type = $_REQUEST['user_type'];
			$users->headquarter = $_REQUEST['headquarter'];
			$users->code = $_REQUEST['code'];
			$users->born_date = $_REQUEST['born_date'];
			$users->state_id = 0;
			$users->city_id = 0;
			$users->address = $_REQUEST['address'];
			$users->state_name = $_REQUEST['address_state'];
			$users->city_name = $_REQUEST['address_city'];
			$users->country_id = $_REQUEST['country'];

			$user_id = $users->add();
			// asignar codigo de casillero
			$code = 'TE-'.$user_id;
			$user_code = $users->asignCode($user_id,$code);


			// registrar destinatario
			$addresse->country_id = $_REQUEST['country'];
			$addresse->state_id = 0;
			$addresse->city_id = 0;
			$addresse->address = $_REQUEST['address'];
			$addresse->country_name = $users->findCountryRegister($_REQUEST['country'])['name'];
			$addresse->state_name = $_REQUEST['address_state'];
			$addresse->city_name = $_REQUEST['address_city'];

			$addresse_id = $addresse->add($user_id,$_REQUEST['name'], $_REQUEST['lastname'],$_REQUEST['phone']);
			
			$mail = new PHPMailer(true);
			// $mail->SMTPDebug = 3;                                  // Enable verbose debug output  
			$mail->isSMTP();                                       // Set mailer to use SMTP  
			$mail->Host = 'smtp.hostinger.es';                       // Specify main and backup SMTP servers  
			$mail->SMTPAuth = true;                                // Enable SMTP authentication  
			$mail->Username = 'notification@debartesting.online';               // your SMTP username  
			$mail->Password = 'Zuleta99';                      // your SMTP password  
			$mail->SMTPSecure = 'tls';                             // Enable TLS encryption, `ssl` also accepted  
			$mail->Port = 587;                                     // TCP port to connect to  
			$mail->setFrom('notification@debartesting.online', 'TOTAL ENVIOS');  
			// $mail->addAddress('ramonbriceno12@gmail.com', 'Ramon');
			$mail->addAddress($_REQUEST['email'], 'TOTAL ENVIOS');
			$mail->isHTML(true);                                   // Set email format to HTML  
			$mail->Subject = 'BIENVENIDO A TOTAL ENVIOS';
			$mail->Body  = '
				<div class="container">
				   <div class="card">
						<div class="card-body">
							<h2 class="card-title">¡Hola! Bienvenido a Total Envios</h2>
							<p class="card-text">Aqui puedes confirmar tu correo: http://192.241.240.138/totalenvios_bk/confirmemail.php?id='.$user_id.'</p>
							  </div>
					   </div> <br>
						<div class="text-center">
						   TOTAL ENVIOS
						</div>
					</div>
				</div>';
   
			if($mail->send()) {  
			   // echo 'Message has been sent';  
	   } else {  
			   // echo 'Message could not be sent';  
			 }
			
			
			
			echo $user_id;
			// registrar destinatario
	
	}

}



?>