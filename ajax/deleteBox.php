<?php  


	ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);
	// echo "string1";
	include '../includes/connection.class.php';
	include '../includes/packages.class.php';
	include '../includes/warehouses.class.php';
	include '../includes/users.class.php';

	$warehouses = new Warehouses;
	$id = $_REQUEST['id'];
	$id_w = $_REQUEST['id_w'];

	$warehouses->deleteBox($id);

	$data = $warehouses->findBoxesWarehouse($id_w);

	foreach ($data as $key => $value) {   
	?>
			<tr>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['id']; ?></td>
			<td><?php echo $warehouses->findCategoryById($warehouses->findBoxesById($value['id_box'])['category_id'])['name']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['description']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cost']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['lenght']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['width']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['height']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['weight']; ?></td>
			
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cubic_feet']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['lb_vol']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cost_taf_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cost_inter_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['gastosExtra']; ?></td>
			
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['monto_calculo_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['tasa_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cost_total']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['ganancia_cost_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['porcentaje_ganancia_cost_type_trip']; ?></td>
			<td>
				<div class="btn-group" style="content-align:center; width:100%">
				<button onclick="deleteBox(<?php echo $value['id_box']; ?>, <?php echo $id_w ?>)" class="btn btn-primary" style="margin-top:30px; background:#fc3c3d; color:white;"><i class="fas fa-trash"></i></button>
				<button onclick="editBoxForTable(<?php echo $value['id_box']; ?>, <?php echo $id_w ?>)" class="btn btn-primary" style="margin-top:30px; background:#fc3c3d; color:white;"><i class="fas fa-edit"></i></button>
				</div>
			</td>
			</tr>
	<?php
		}
	?>