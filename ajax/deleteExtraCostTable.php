<?php  


	ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);
	// echo "string1";
	include '../includes/connection.class.php';
	include '../includes/packages.class.php';
	include '../includes/warehouses.class.php';
	include '../includes/users.class.php';
    include '../includes/packing.class.php';


	$warehouses = new Warehouses;
    $packing = new Packing;
	$id = $_REQUEST['id'];
	

	$warehouses->deleteExtraCostForTable($id);

	$extraCost = $packing->findExtraCost();

    foreach ($extraCost as $key => $value) { 
    ?>
            <tr>
                <td><?php echo $packing->findExtraCostName($value['extracost_id'])[0]['name']; ?></td>
                <td><?php echo $value['value_extracost']; ?></td>
               
    
                <td>
                    <div class="btn-group" style="content-align:center; width:100%">
                    <button onclick="deleteExtraCost(<?php echo $value['id']; ?>)" class="btn btn-primary" style="margin-top:30px; background:#fc3c3d; color:white;"><i class="fas fa-trash"></i></button>
                    <button onclick="editExtraCost(<?php echo $value['id']; ?>)" class="btn btn-primary" style="margin-top:30px; background:#fc3c3d; color:white;"><i class="fas fa-edit"></i></button>
                    </div>
                </td>
            </tr>
    <?php
        }
    ?>