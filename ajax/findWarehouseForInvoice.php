<?php  

    include '../includes/connection.class.php';
    include '../includes/invoices.class.php';
    include '../includes/packages.class.php';
    include '../includes/users.class.php';
    include '../includes/warehouses.class.php';

	$client = $_REQUEST['client_name'];
    $client_id = $_REQUEST['client_id'];
    $trip = $_REQUEST['trip_type'];
    $date_send = $_REQUEST['send_date'];
    $id_destiner_invoice = $_REQUEST['id_destiner_invoice'];
    $destiner_invoice = $_REQUEST['destiner_invoice'];

    $invoices = new Invoices;
    $allInvoices = $invoices->findWarehouseForClient($client,$trip,$date_send,$destiner_invoice);

    if(count($allInvoices) == 0){
        echo 0;
    }else{
        echo 1;
    }

?>