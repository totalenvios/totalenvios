<?php  

    include '../includes/connection.class.php';
    include '../includes/invoices.class.php';
    include '../includes/packages.class.php';
    include '../includes/users.class.php';
    include '../includes/warehouses.class.php';

	$departamento = $_REQUEST['departamento'];
    $tipo_envio = $_REQUEST['tipo_envio'];
    $courier = $_REQUEST['courier'];
    $region = $_REQUEST['region'];
    $fecha_de_salida = $_REQUEST['fecha_salida'];
    

    $invoices = new Invoices;
    $allInvoices = $invoices->findWarehouseForPacking($departamento,$tipo_envio,$courier,$region,$fecha_de_salida);

    if(count($allInvoices) == 0){
        echo 0;
    }else{
        echo 1;
    }

?>