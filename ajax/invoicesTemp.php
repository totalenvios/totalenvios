<?php  


	include '../includes/connection.class.php';
	include '../includes/packing.class.php';

	$packing = new Packing;

	$json = json_decode($_REQUEST['json']);
    $fecha_emision = date("Y-m-d");
    $fecha_vencimiento_suma = strtotime($fecha_emision."+ 2 months");
    $fecha_vencimiento = date("Y-m-d",$fecha_vencimiento_suma);
    $fecha_salida_wh = $_REQUEST['fecha_salida'];
    $user_id = $_REQUEST['user_id'];
    $total_invoice = $_REQUEST['total_invoice'];
    $type_trip_id = $_REQUEST['type_trip_id'];
    $destiner_id = $_REQUEST['destiner_id'];
    


    /**MODIFICAR FLAG EN WAREHOUSE INDICANDO QUE EL WAREHOUSE FUE FACTUIRADO CON EXITO */



    $invoice_id = $packing->addInvoice('FACT-', $user_id, $total_invoice, $type_trip_id, 0, $fecha_emision, $fecha_vencimiento, $fecha_salida_wh,$destiner_id,2);

    $extraCost = $packing->findExtraCost();

    if(count($extraCost) > 0){

        foreach ($extraCost as $key => $val) {
            
           $addExtraCost = $packing->updateExtraCostForInvoice($invoice_id, $val['id']);
        }  
    }
    

	$jsontxt = '';
	$jsoncount = count($json);
	$count = 0;

	foreach ($json as $key => $value) {
		$addInvoiceDetails = $packing->addInvoiceDetails($invoice_id,$value);
        $updateFLAG = $packing->updateFLAG(2, $value);
	}

    echo $invoice_id;
	

    
?>