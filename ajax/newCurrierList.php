<?php  

	include '../includes/connection.class.php';
	include '../includes/packages.class.php';

	$packages = new Packages;
	
	
	$allInternCurrier = $packages->findInternCouriers();
    ?>
    <div class="row">
        <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
            <div class="form-group">
                <label for="select_new_intern_currier" style="color:black;">COURIER INTERNO</label>
                    <select id="select_new_intern_currier" class="input-register-warehouse">
                    <option value="">Seleccione</option>
                        <?php  

                            foreach ($allInternCurrier as $key => $value) {
                        
                        ?>
                                
                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                        <?php
                            }

                        ?>
                    </select>
            </div>
        </div>
        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
            <button class="boton-total-envios-registro-config" style="margin-top:25px;" onclick="seleccionarInternCurrier();"><i class="fas fa-check fa-lg" aria-hidden="true"></i></button>
        </div>
        
    </div>
    
    <?php


	
?>