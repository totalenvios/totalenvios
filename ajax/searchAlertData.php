<?php  
	
	include '../includes/connection.class.php';
	include '../includes/packages.class.php';
	include '../includes/users.class.php';
	include '../includes/stock.class.php';
	include '../includes/transport.class.php';


	$packages = new Packages;
	$users = new Users;
	$stock = new Stock;

	$idUser = $_REQUEST['id'];
	$transports = new Transport;
	$allTransp = $packages->findAllTransp();
	$search = $users->findAlertsById($idUser);

    $transport = $search['transp'];

    $transport_name = $packages->findTransportForId($transport)['id'];
    $client_name = $users->findById($search['id_client'])['name'];
	$client_lastname = $users->findById($search['id_client'])['lastname'];

    echo $idUser.'-'.$search['tracking'].'-'.$search['id_client'].'-'.$client_name.'-'.$search['package_name'].'-'.$transport_name.'-'.$search['transp'].'-'.$search['cost'].'-'.$search['content'].'-'.$search['casillero'].'-'.$search['proveedor'].'-'.$client_lastname;
	
    ?>
