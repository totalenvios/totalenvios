<?php  
	
	include '../includes/connection.class.php';
	include '../includes/packages.class.php';
	include '../includes/users.class.php';
	include '../includes/stock.class.php';
	include '../includes/transport.class.php';


	$packages = new Packages;
	$users = new Users;
	$stock = new Stock;

	$idUser = $_REQUEST['idUser'];
	$date_in = $_REQUEST['date_in'];
	$date_out = $_REQUEST['date_out'];
	$transports = new Transport;
	$allTransp = $packages->findAllTransp();
	$search = $packages->findAlertSearch($idUser, $date_in, $date_out);
	// print_r($search);
	if (count($search) == 0) {
?>
		<div class="text-center">
			<h1>SIN RESULTADOS</h1>
		</div>
<?php
	}else{
?>
		<thead>
	        <!-- <th>#</th> -->
	        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Fecha Llegada</th>
	        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Transportista</th>
	        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Tracking</th>
	        <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Fecha Registro</th> -->
	        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Cliente</th>
	        
	        
	        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Contenido</th>
	        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Valor USD</th>
	        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Factura</th>

	        <!-- <th>Ultimo Paquete</th> -->
	        <!-- <th>Accion</th> -->
	    </thead>
	    <tbody>
	        <?php  

	            foreach ($search as $key => $value) {
	        ?>
	                <tr>
	                    <!-- <td><?php echo $value['id']; ?></td> -->
	                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['date_in']; ?></td>
	                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $transports->findById($value['transp'])['name']; ?></td>
	                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['tracking']; ?></td> 
	                    
	                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $users->findById($value['id_client'])['name'].' '.$users->findById($value['id_client'])['lastname']; ?></td>
	                    
	                    
	                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['content']; ?></td>
	                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['cost']; ?> $</td>
	                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
	                        <?php 
	                            if ($value['invoice'] == null) {
	                                echo "No tiene factura";
	                            }else{
	                                echo $value['invoice'];
	                            }
	                            
	                        ?>
	                    </td>
	                   
	                    <!-- <td><button class="btn btn-primary"><i class="fas fa-eye"></i></button></td> -->
	                </tr>
	        <?php
	            }
	        }
	        ?>
	    </tbody>
