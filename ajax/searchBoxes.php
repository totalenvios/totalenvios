<?php  

	include '../includes/connection.class.php';
	include '../includes/users.class.php';
	include '../includes/packages.class.php';

	$packages = new Packages;
	$q = $_REQUEST['q'];
	$allBoxes = $packages->findBoxesNames($q);
	if (count($allBoxes) == 0) {
?>
		<div class="text-center empty-search">
			<div class="container">
				No hay resultados
			</div>
		</div>
		
<?php
	}else{

		foreach ($allBoxes as $key => $value) {

	?>
			<li><a href="#" onclick="selectBox('<?php echo $value['id']; ?>','<?php echo $value['name']; ?>','<?php echo $value['width']; ?>', '<?php echo $value['height']; ?>', '<?php echo $value['weigth']; ?>', '<?php echo $value['lenght']; ?>','<?php echo $value['cubic_feet']; ?>','<?php echo $value['lb_vol']; ?>')"><?php echo $value['name']; ?></a></li>
	<?php
		}
	}
?>