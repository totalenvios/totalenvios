<?php  
	
	include '../includes/connection.class.php';
	include '../includes/packages.class.php';
	include '../includes/users.class.php';
	include '../includes/stock.class.php';
	include '../includes/transport.class.php';


	$packages = new Packages;
	$users = new Users;
	$stock = new Stock;
	$transp = new Transport;

	$idUser = $_REQUEST['idUser'];
	$date_in = $_REQUEST['date_in'];
	$date_out = $_REQUEST['date_out'];

	print_r($search = $packages->findPackSearch($idUser, $date_in, $date_out));

	if (count($search) == 0) {
?>
		<div class="text-center">
			<h1>SIN RESULTADOS</h1>

		</div>
	<?php
        echo $date_in;
	}else{
?>
		<thead>
            <!-- <th style="border: 2px solid #e3e3e3ff;"></th> -->
            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Estado</th>
            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Nro. paquete</th>       
            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Tracking</th>
            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Ingreso</th>
            <!-- <th>Casillero</th> -->
            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Destino</th>
            <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Región</th> -->
            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Proveedor</th>
            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Descripcion</th>
            <!-- <th>Almacen</th> -->
            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Operador</th>
            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Accion</th>
            <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Pre - Alerta</th> -->
            
            <!-- <th>Imagen</th> -->
            <!-- <th>Accion</th> -->
        </thead>
        <tbody>
            <?php  

                foreach ($search as $key => $value) {
            ?>
                    <tr>
                        <!-- <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><input onclick="countPChecks();" type="checkbox" class="form-check-input pcheck pcheck<?php echo $value['id'];?>" id="pcheck<?php echo $value['id'];?>" package="<?php echo $value['id'];?>"></td> -->
                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                            <?php 
                                echo $packages->findStateById($value['state'])['name'];
                            ?>
                        </td>
                        <!-- <td><?php echo $value['id']; ?></td> -->
                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['code']; ?></td>
                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                            <?php 
                                if ($value['tracking'] == null) {
                                    echo "N/A";
                                }else{
                                    echo $value['tracking'];; 
                                }
                                
                            ?>
                        </td>
                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                        <?php 
                            $fechaComoEntero = strtotime($value['date_in']);	
                            $fecha = date("m/d/Y", $fechaComoEntero);
                            echo $fecha; 
                        ?>
                        </td>
                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['address_send']; ?></td>
                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $transp->findById($value['id_transp'])['name']; ?></td>
                        <!-- <td><?php echo $value['code']; ?></td> -->
                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['description']; ?></td>
                        <!-- <td>
                            <?php 
                                if ($value['id_stock'] == 0) {
                                    echo "No tiene Almacen";
                                }else{
                                    echo $stock->findById($value['id_stock'])['name']; 
                                }
                                
                            ?>
                        </td> -->
                       
                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                            <?php 
                                if ($value['operator'] == 0) {
                                    echo "No tiene operador";
                                }else{
                                    echo $users->findById($value['operator'])['name']; 
                                }
                                
                            ?>
                        </td>
                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><button style="background-color:white; color:#005574; border:2px solid #005574;" onclick="window.open('package.php?id=<?php echo $value['id']; ?>', '_blank');" class="btn btn-primary"><i class="fas fa-edit"></i></button><button onclick="deletePackage(<?php echo $value['id']?>)" class="btn btn-primary delete-button" style="background-color:#fc3c3d; color:white; border:2px solid white; margin-left:10px;"><i class="fas fa-trash"></i></button></td>
                        
                        <!-- <td><a href="assets/img/paquetes/<?php echo $value['img'];?>"><img class="pack-img" src="assets/img/paquetes/<?php echo $value['img'];?>" alt=""></a> </td> -->
                        <!-- <td><button class="btn btn-primary"><i class="fas fa-eye"></i></button></td> -->
                    </tr>
            <?php
                }
            }
            ?>
        </tbody>
