<?php  
    
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

	include '../includes/connection.class.php';
    include '../includes/users.class.php';
    include '../includes/packages.class.php';
    include '../includes/headquarters.class.php';
    include '../includes/stock.class.php';
    include '../includes/packing.class.php';
    include '../includes/warehouses.class.php';

	$packing = new Packing;
	$warehouses = new Warehouses;
    $users = new Users;
    $packages = new Packages;
    $headquarters = new Headquarters;
    $stock = new Stock;
    $warehouses = new Warehouses;

    $currier = $_REQUEST['currier'];
    $date_in = $_REQUEST['date_in'];
    $date_out = $_REQUEST['date_out'];
	
	$allPacking = $packing->findAllByQuery($currier, $date_in, $date_out);

?>
	
    <thead>
        <th style="border: 2px solid #e3e3e3ff;"><button onclick="$('.wcheck').prop('checked', true); $('#sendPackingBtn').prop('disabled', false);" class="btn btn-primary" class="fw-normal">Todos</button></th>
        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Status WH</th>
        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Warehouse</th>
        <!-- <th>Casillero</th> -->
        
        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Destinatario</th>
        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Destino</th>
        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Tipo de envío</th>
        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Salida</th>
        <!-- <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Cliente</th>
        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Nro. Casillero</th> -->
        <!-- <th>Descripcion</th> -->
        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Currier</th>
    
        <!-- <th>Ancho</th>
        <th>Largo</th>
        <th>Alto</th>
        <th>Peso</th> -->
        <!-- <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Conf. usuario</th> -->
        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Status envío</th>
        <!-- <th style="width:12%; border: 2px solid #e3e3e3ff; background-color:#white;"></th>
        <th style="border: 2px solid #e3e3e3ff; background-color:#white;"></th> -->
    </thead>
    <tbody>
        <?php  

            foreach ($allPacking as $key => $value) {
        ?>
                <tr>
                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><input onclick="countChecks();" type="checkbox" class="form-check-input wcheck wcheck<?php echo $value['id'];?>" id="wcheck<?php echo $value['id'];?>" warehouse="<?php echo $value['id'];?>"></td>
                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                        <?php 
                            if( $value['state'] == 2){
                        ?> 
                        <i class="fas fa-lock fa-2x fa-lg" style="margin-top:10px;"> </i>
                        <?php 
                            }else{
                        ?>
                        <i class="fas fa-unlock-alt fa-2x fa-lg" style="margin-top:10px;"> </i>
                        <?php  
                            };
                        ?>
                    </td>
                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['code']; ?></td>
                    <!-- <td><?php echo $value['casillero']; ?></td> -->
                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $users->findAddresseeById($value['id_to'])[0]['name'].' '.$users->findAddresseeById($value['id_to'])[0]['lastname']; ?></td>
                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                        <?php echo $users->findcityById($users->findAddresseeById($value['id_to'])[0]['city_id'])['ciudad'];?></td>
                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $warehouses->findTripById($value['trip_id'])['name']; ?></td>
                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['date_send']; ?></td>
                    <!-- <td><?php echo $value['description']; ?></td> -->
                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $packages->findCurrierById($value['id_currier'])['name']; ?></td>
                
                    <!-- <td><?php echo $value['width']; ?></td>
                    <td ><?php echo $value['lenght']; ?></td>
                    <td><?php echo $value['height']; ?></td>
                    <td><?php echo $value['weight']; ?></td> -->
                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                        <?php 
                            echo $warehouses->findGeneralStateById($value['ship_state'])['name'];
                        ?>
                    </td>
                    <!-- <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><a target="_blank" href="invoiceFormat.php?id=<?php echo $value['id']; ?>" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-eye"></i></a></td>
                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white; text-layout:fixed;">
                        <div class="btn-group" style="content-align:center; width:100%">
                       
                         <div><a target="_blank" href="closeTicketFormat.php?id=<?php echo $value['id']; ?>" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-tags"></i></a></div>
                    
                        
                        <div><button onclick="$('#deleteModal').modal('show'); addDeleteText(<?php echo $value['id']; ?>)" class="btn btn-primary" style="background-color:#005574; color:white; border:2px solid #005574;"><i class="fas fa-trash"></i></button></div>
                        
                        <div><a target="_blank" href="invoiceForm.php?id=<?php echo $value['id']; ?>" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-file-invoice"></i></a></div>
                        </div>
                    </td> -->
                </tr>
        <?php
            }

        ?>
    </tbody>

    <!-- <thead>
        <th>#</th>
        <th>Name</th>
        <th>Fecha Entrada</th>
        <th>Fecha Salida</th>
        <th>Nota</th>
        <th>Currier</th>
        <th>Tipo de viaje</th>
        <th>Destino</th>
        <th>Accion</th>
    </thead>
    <tbody>
        <?php  

            foreach ($allPacking as $key => $value) {
        ?>
                <tr>
                    <td><?php echo $value['id']; ?></td>
                    <td><?php echo $value['name']; ?></td>
                    <td><?php echo $value['date_in']; ?></td>
                    <td><?php echo $value['date_out']; ?></td>
                    <td><?php echo $value['note']; ?></td>
                    <td><?php echo $packing->findCurrierById($value['id_currier'])['name']; ?></td>
                    <td><?php echo $packing->findTripById($value['id_trip'])['name']; ?></td>
                    <td><?php echo $warehouses->findCountryById($value['id_destiny'])['name']; ?></td>
                    <td><button onclick='window.open("pack.php?id=<?php echo $value['id']; ?>", "_blank");' class="btn btn-primary"><i class="fas fa-eye"></i></button><button onclick="$('#deleteModal').modal('show'); addDeleteTextP(<?php echo $value['id']; ?>)" class="btn btn-primary delete-button"><i class="fas fa-trash"></i></button></td>
                </tr>
        <?php
            }

        ?>
    </tbody> -->
