<?php  
	
	include '../includes/connection.class.php';
	include '../includes/users.class.php';


	$users = new Users;
	$q = $_REQUEST['q'];
	$search = $users->findUsersByNameOrPhone($q);
	if (count($search) == 0) {
?>
		<div class="text-center">
			<h1>SIN RESULTADOS</h1>
		</div>
	<?php
	}else{
?>
		<thead>
		    <th>#</th>
		    <th>Nombre</th>
		    <th>Email</th>
		    <th>Dirección</th>
		    <th>Teléfono</th>
		</thead>
		<tbody class="users-body">
<?php
			foreach ($search as $key => $value) {
		?>
				<tr>
		            <td><?php echo $value['id']; ?></td>
		            <td><?php echo $value['name'].' '.$value['lastname']; ?></td>
		            <td><?php echo $value['email']; ?></td>
		            <td><?php echo $value['address']; ?></td>
		            <td><?php echo $value['phone']; ?></td>
		        </tr>
		<?php
			}
	}

	?>
		</tbody>
