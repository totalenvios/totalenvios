<?php  
	
	include '../includes/connection.class.php';
	include '../includes/packages.class.php';
	include '../includes/warehouses.class.php';
	include '../includes/users.class.php';
	include '../includes/stock.class.php';


	$packages = new Packages;
	$warehouses = new Warehouses;
	$users = new Users;
	$stock = new Stock;

	$idUser = $_REQUEST['idUser'];
	$date_in = $_REQUEST['date_in'];
	$date_out = $_REQUEST['date_out'];

	$search = $warehouses->findWareSearch($idUser, $date_in, $date_out);
	if (count($search) == 0) {
?>
		<div class="text-center">
			<h1>SIN RESULTADOS</h1>
		</div>
	<?php
	}else{
?>
		<thead>
            <th></th>
            <th>Fecha Envio</th>
            <th>Warehouse</th>
            <th>Destino</th>
            <th>Descripcion</th>
            <th>Codigo Cliente</th>
            <th>Nombre</th>
            <th>Currier</th>
            <th>Tipo</th>
            <th>Ancho</th>
            <th>Largo</th>
            <th>Alto</th>
            <th>Peso</th>
            <th>Estado</th>
            <th>Accion</th>
        </thead>
		<tbody class="warehouses-body">
<?php
			foreach ($search as $key => $value) {
		?>
	            <tr>
                     <td><input onclick="countChecks();" type="checkbox" class="form-check-input wcheck wcheck<?php echo $value['id'];?>" id="wcheck<?php echo $value['id'];?>" warehouse="<?php echo $value['id'];?>"></td>
                    <td><?php echo $value['date_send']; ?></td>
                    <td><?php echo $value['code']; ?></td>
                    <!-- <td><?php echo $warehouses->findCountryById($value['id'])['name']; ?></td> -->
                    <td><?php echo $value['address_send']; ?></td>
                    <td><?php echo $value['description']; ?></td>
                    <td><?php echo $value['id_from']; ?></td>
                    <td><?php echo $users->findById($value['id_from'])['name'].' '.$users->findById($value['id_from'])['lastname']; ?></td>
                    <td><?php echo $packages->findCurrierById($value['id_currier'])['name']; ?></td>
                    <td><?php echo $packages->findBoxById($value['box'])['name']; ?></td>
                    <td><?php echo $value['width']; ?></td>
                    <td><?php echo $value['lenght']; ?></td>
                    <td><?php echo $value['height']; ?></td>
                    <td><?php echo $value['weight']; ?></td>
                    <td>
                        <?php 
                            echo $packages->findStateById($value['state'])['name'];
                        ?>
                    </td>
                    <td>
                        <button onclick="details(<?php echo $value['id']; ?>)" class="btn btn-primary"><i class="fas fa-eye"></i></button>
                        <button onclick="$('#addPackageModal').modal('show'); addWarehouseId(<?php echo $value['id']; ?>);" class="btn btn-primary"><i class="fas fa-box"></i></button>
                        <button onclick="$('#deleteModal').modal('show'); addDeleteText(<?php echo $value['id']; ?>)" class="btn btn-primary delete-button"><i class="fas fa-trash"></i></button>
                    </td>
                </tr>
		<?php
			}
	}

	?>
		</tbody>
