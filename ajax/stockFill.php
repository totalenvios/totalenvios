<?php  

    include '../includes/connection.class.php';
    include '../includes/stock.class.php';

    $stock = new Stock;
    $headquarter = $_REQUEST['headquarter'];
    $stockByHQ = $stock->findByHQ($headquarter);

?>

<label class="col-md-12 p-0">Almacen</label>
<div class="col-md-12 border-bottom p-0">
    <select id="stock" class="form-select shadow-none p-0 border-0 form-control-line">
        <option value="0">Seleccionar...</option>
        <?php  
        foreach ($stockByHQ as $key => $value) {
            ?>
                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
            <?php
        }
        ?>
    </select>
</div>