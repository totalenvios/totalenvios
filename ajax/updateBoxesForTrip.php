<?php  


	include '../includes/connection.class.php';
    include '../includes/warehouses.class.php';


    $packages = new Warehouses;
    $wh_id= $_REQUEST['wh_id'];
    $tarifa_currier= $_REQUEST['tarifa_currier'];
    $tarifa_te= $_REQUEST['tarifa_te'];
    $trip_id= $_REQUEST['trip_id'];
    $boxes = $packages->getBoxForWh($wh_id);
    $total_piecub = 0;
    $total_lbvol = 0;
    $total_peso = 0;
    $total_costo = 0;
    $total_ganancia= 0;
    $total_porc_ganancia = 0;

    

    if($trip_id == 1){
        // el nuevo tipo de envio es aereo
        foreach ($boxes as $key => $value) {

            $total_piecub = $total_piecub + $value['cubic_feet'];
            $total_lbvol = $total_lbvol + $value['lb_vol'];
            $total_peso = $total_peso + $value['weight'];

            $trip = 1;

            $pesolb = $value['weight'];
            $lbvol = $value['lb_vol'];
            $cost_inter_type_trip = $value['cost_inter_type_trip'];
            $costo_extra = $value['gastosExtra'];

         

            if($pesolb > $lbvol){
                $cost_type_trip = $pesolb*$tarifa_currier;
                $monto_te = $pesolb*$tarifa_te;

            }else{
                $cost_type_trip = $lbvol*$tarifa_currier;
                $monto_te = $lbvol*$tarifa_te;
            }
            
            $monto_calculo_type_trip = $cost_type_trip + $cost_inter_type_trip + $costo_extra;
            $cost_total = $monto_te;
            $ganancia = $cost_total - $monto_calculo_type_trip;
		    $porcGanancia = ($ganancia/$cost_total)*100;



            $total_costo = $total_costo + $cost_total;
            $total_ganancia = $total_ganancia +  $ganancia;
            $total_porc_ganancia = $total_porc_ganancia + $porcGanancia;

            $update = $packages->updateBoxForTrip($tarifa_currier, $tarifa_te, $cost_type_trip, $monto_calculo_type_trip, $cost_total, $ganancia, $porcGanancia, $value['id']);


        }    
    
       

    }else{
        //el nuevo tipo de envio es maritimo
        foreach ($boxes as $key => $value) {

            $total_piecub = $total_piecub + $value['cubic_feet'];
            $total_lbvol = $total_lbvol + $value['lb_vol'];
            $total_peso = $total_peso + $value['weight'];

            $trip = 2;
           
           

            $pesolb = $value['cubic_feet'];
            $cost_inter_type_trip = $value['cost_inter_type_trip'];
            $costo_extra = $value['gastosExtra'];


           
                $cost_type_trip = $pesolb*$tarifa_currier;
                $monto_te = $pesolb*$tarifa_te;



            
            $monto_calculo_type_trip = $cost_type_trip + $cost_inter_type_trip + $costo_extra;
            $cost_total = $monto_te;
            $ganancia = $cost_total - $monto_calculo_type_trip;
		    $porcGanancia = ($ganancia/$cost_total)*100;

          
         

            $total_costo = $total_costo + $cost_total;
            $total_ganancia = $total_ganancia +  $ganancia;
            $total_porc_ganancia = $total_porc_ganancia + $porcGanancia;
            

            $update = $packages->updateBoxForTrip($tarifa_currier, $tarifa_te, $cost_type_trip, $monto_calculo_type_trip, $cost_total, $ganancia, $porcGanancia, $value['id']);


        }    
    }

    $data = $packages->findBoxesWarehouse($wh_id);

    foreach ($data as $key => $value) { 
        ?>
               <tr>
			<td><?php echo $packages->findBoxesById($value['id_box'])['id']; ?></td>
			<td><?php echo $packages->findCategoryById($packages->findBoxesById($value['id_box'])['category_id'])['name']; ?></td>
			<td><?php echo $packages->findBoxesById($value['id_box'])['description']; ?></td>
			<td><?php echo $packages->findBoxesById($value['id_box'])['cost']; ?></td>
			<td><?php echo $packages->findBoxesById($value['id_box'])['lenght']; ?></td>
			<td><?php echo $packages->findBoxesById($value['id_box'])['width']; ?></td>
			<td><?php echo $packages->findBoxesById($value['id_box'])['height']; ?></td>
			<td><?php echo $packages->findBoxesById($value['id_box'])['weight']; ?></td>
			
			<td><?php echo $packages->findBoxesById($value['id_box'])['cubic_feet']; ?></td>
			<td><?php echo $packages->findBoxesById($value['id_box'])['lb_vol']; ?></td>
			<td><?php echo $packages->findBoxesById($value['id_box'])['cost_taf_type_trip']; ?></td>
			<td><?php echo $packages->findBoxesById($value['id_box'])['cost_inter_type_trip']; ?></td>
			<td><?php echo $packages->findBoxesById($value['id_box'])['gastosExtra']; ?></td>
			
			<td><?php echo $packages->findBoxesById($value['id_box'])['monto_calculo_type_trip']; ?></td>
			<td><?php echo $packages->findBoxesById($value['id_box'])['tasa_type_trip']; ?></td>
			<td><?php echo $packages->findBoxesById($value['id_box'])['cost_total']; ?></td>
			<td><?php echo $packages->findBoxesById($value['id_box'])['ganancia_cost_type_trip']; ?></td>
			<td><?php echo $packages->findBoxesById($value['id_box'])['porcentaje_ganancia_cost_type_trip']; ?></td>

			<td>
				<div class="btn-group" style="content-align:center; width:100%">
				<button onclick="deleteBox(<?php echo $value['id_box']; ?>, <?php echo $wh_id ?>)" class="btn btn-primary" style="margin-top:30px; background:#fc3c3d; color:white;"><i class="fas fa-trash"></i></button>
				<button onclick="editBoxForTable(<?php echo $value['id_box']; ?>, <?php echo $wh_id ?>)" class="btn btn-primary" style="margin-top:30px; background:#fc3c3d; color:white;"><i class="fas fa-edit"></i></button>
				</div>
			</td>
		</tr>
        <?php
            }
        ?>
        