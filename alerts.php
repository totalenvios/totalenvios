<?php  
        // error_reporting(0);
        // ini_set('display_errors', 0);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        
        include 'includes/connection.class.php';
        include 'includes/users.class.php';
        include 'includes/packages.class.php';
        include 'includes/headquarters.class.php';
        include 'includes/stock.class.php';
        include 'includes/transport.class.php';

        $users = new Users;
        $allUsers = $users->findAll();
        $packages = new Packages;
        $allAlerts = $packages->findAlerts();
        $allAlertsConfirmed = $packages->findAlertsConfirmed();
        $headquarters = new Headquarters;
        $transports = new Transport;
        $allHeadquarters = $headquarters->findAll();
        $allOperators = $users->findAllOperators();
        $allClients = $users->findAllClients();
        $states = $packages->findStates();
        $allTransp = $packages->findAllTransp();

        $stock = new Stock;

        session_start();
        if ($_SESSION['state'] != 1) {
            header('Location: login.php');
        }
        $userData = $users->findByEmail($_SESSION['email']);
        $date_in = date('2020-m-d');
        $date_out = date('Y-m-d');    
    ?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

            include 'head.php';

        ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body style="background:#e3e3e3ff !important;">

    
    <input id="id_client_alert" type="hidden" value="<?php echo $_SESSION['id_user']; ?>">
    <input id="id_user_form" type="hidden" value="0">
    <input id="idUser" type="hidden" value="0">
    <input id="idUserFor" type="hidden" value="0">
    <input id="idClient" type="hidden" value="<?php echo $userData['id']; ?>">

    <div class="modal fade" id="packageModal" tabindex="-1" role="dialog" aria-labelledby="packageModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="packageModalLabel">REGISTRAR PAQUETE</h5>
                    <button onclick="$('#packageModal').modal('hide');" type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="alert alert-danger register-error" role="alert">
                                </div>
                                <div class="alert alert-success register-success" role="alert">
                                </div>
                                <div class="card">
                                    <div class="card-body">

                                    
                                        <div class="row">

                                        <div class="col-lg-12 col-xlg-12 col-md-12">
                                                <div class="form-group">
                                                    <!-- <label class="col-md-12 p-0">Tracking</label> -->
                                                    <label class="col-sm-12" for="tracking">TRACKING</label>
                                                    <input onkeyup="searchForAlert();" id="tracking_pck" type="text"
                                                        placeholder="Tracking" class="form-control campos-input-login">
                                                    <ul id="myAlert">
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row"> 
                                            <div class="col-lg-5 col-xlg-5 col-md-5">
                                                <div class="form-group">
                                                    <!-- <label for="user_from" style="color:black;">Nombre/casillero</label> -->
                                                    <div class="input-group mb-3">
                                                        <input id="alert_id" hidden>
                                                        <input id="id_user_from" hidden>
                                                        <label class="col-sm-12" for="user_from">CLIENTE</label>
                                                        <input type="text" class="form-control" placeholder="Nombre"
                                                            aria-label="Recipient's username"
                                                            aria-describedby="basic-addon2"
                                                            onkeyup="searchUserPackage();" id="user_from">

                                                        <div class="input-group-append">
                                                            <button
                                                                onclick="$('#packageModal').modal('hide'); $('#newUserModal').modal('show');"
                                                                class="btn btn-outline-secondary"
                                                                style="background-color:#ff554dff; color:white;"><i
                                                                    style="color:white"
                                                                    class="fas fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                    <ul id="myUS">
                                                    </ul>

                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-xlg-5 col-md-5">
                                                <div class="form-group">
                                                    <!-- <label class="col-md-12 p-0">Alias del paquete</label> -->
                                                    <label class="col-sm-12" for="Nombre del paquete">NOMBRE DEL PAQUETE</label>
                                                    <input id="alias_pck" type="text" placeholder="Nombre del paquete"
                                                        class="form-control campos-input-login" value="">
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-xlg-2 col-md-2">
                                                <div class="form-group">
                                                    <!-- <label class="col-md-12 p-0">Alias del paquete</label> -->
                                                    <label class="col-sm-12" for="casillero">CASILLERO</label>
                                                    <input id="casillero_pck" type="text" placeholder="Casillero"
                                                        class="form-control campos-input-login" value="">
                                                </div>
                                            </div>
                                            
                                           
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                            <div class="form-group">
                                                    <label class="col-sm-12" for="transp">TRANSPORTISTA</label>

                                                    <select id="transp_pck" class="form-select campos-input-login">
                                                        <option value="0">Seleccionar...</option>
                                                        <?php  
                                                                foreach ($allTransp as $key => $value) {
                                                            ?>
                                                        <option value="<?php echo $value['id']; ?>">
                                                            <?php echo $value['name']; ?></option>
                                                        <?php
                                                                }
                                                            ?>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                            <div class="form-group">
                                                    <!-- <label class="col-md-12 p-0">Alias del paquete</label> -->
                                                    <label class="col-sm-12" for="proveedor">PROVEEDOR</label>
                                                    <input id="proveedor_pck" type="text" placeholder="Proveedor"
                                                        class="form-control campos-input-login" value="">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-xlg-4 col-md-4" hidden>
                                                <div class="form-group">
                                                    <label class="col-md-12 p-0">Operador_id</label>

                                                    <input id="operator_id" type="text" placeholder="Alias"
                                                        class="form-control campos-input-login"
                                                        value="<?php echo $users->findforCode($_SESSION['code'])[0]['id']; ?>"
                                                        readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                            
                                           
                                                <div class="form-group">
                                                    <label class="col-md-12 p-0">OPERADOR</label>

                                                    <input id="operator_pck" type="text" placeholder="Alias"
                                                        class="form-control campos-input-login"
                                                        value="<?php echo $_SESSION['name']; ?>" readonly>
                                                </div>
                                            
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <!-- <label for="example-email" class="col-md-12 p-0">Fecha Entrada *</label> -->
                                                    <label class="col-sm-12" for="date_in">FECHA DE REGISTRO</label>
                                                    <input readonly="readonly" type="text" placeholder="Fecha Entrada"
                                                        class="form-control campos-input-login" id="date_in"
                                                        value="<?php echo $date_out; ?>">

                                                    <input id="fecha_registro" value=" <?php echo $fechaActual; ?>"
                                                        hidden>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-4 col-xlg-4 col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-12">COSTO</label>

                                                    <input id="cost_pck" type="number" placeholder="Costo"
                                                        class="form-control campos-input-login">

                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-xlg-8 col-md-8">
                                            <div class="form-group">
                                                    <label class="col-md-12 p-0">CONTENIDO</label>

                                                    <input id="description" type="text" placeholder="Descripcion"
                                                        class="form-control campos-input-login"> </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <label class="col-sm-12">ALTO</label>

                                                    <input id="height" type="number" placeholder="Alto"
                                                        class="form-control campos-input-login">

                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <label class="col-sm-12">LARGO</label>

                                                    <input id="lenght" type="number" placeholder="Largo"
                                                        class="form-control campos-input-login">

                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <label class="col-sm-12">ANCHO</label>

                                                    <input id="width" type="number" placeholder="Ancho"
                                                        class="form-control campos-input-login">

                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <label class="col-sm-12">PESO</label>

                                                    <input id="weight" type="number" placeholder="LB"
                                                        class="form-control campos-input-login">

                                                </div>
                                            </div>
                                            
                                           



                                        </div>
                                        <div class=row>
                                           
                                            

                                            <div class="col-lg-4 col-xlg-4 col-md-4">
                                                <form enctype="multipart/form-data" id="formuploadajax" method="post"
                                                    class="upload-form">
                                                    <span class="upload-span-screenshot">ADJUNTA UNA IMAGEN:</span>
                                                    <span class="upload-span-screenshot">5 MB maximo. JPG, PNG,
                                                        JPEG:</span>
                                                    <input class="btn btn-primary" type="file"
                                                        style="width:100%; background:#005574;" id="paquete"
                                                        name="paquete" />
                                                </form>
                                            </div>
                                            <div class="col-lg-5 col-xlg-5 col-md-5" hidden>
                                                <div class="form-group">
                                                    <label class="col-md-12 p-0">Direccion - Entrega *</label>

                                                    <!-- <input id="address_send" type="text" placeholder="Entrega" class="form-control p-0 border-0" value="<?php echo $dataUser['address_send']; ?>">  -->
                                                    <input id="address_send" type="text" placeholder="Entrega"
                                                        class="form-control campos-input-login" value="">

                                                </div>
                                            </div>
                                        </div>









                                        <br>
                                        <div class="text-center">
                                            <div class="form-group mb-4">
                                                <div class="col-sm-12">
                                                    <button onclick="addPackageFromAlert();"
                                                        class="boton-total-envios-registro">Registrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="alertModalLabel" style="#005574;">Nueva prealerta</h5>
                    <button onclick="$('#alertModal').modal('hide');" type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="alert alert-danger error-warning" role="alert">
                                </div>
                                <div class="alert alert-success success-warning" role="alert">
                                </div>
                                <!-- <div class="card">
                            <div class="card-body"> -->
                                <div class="row">
                                <input id="user_id" class="form-control" type="text"
                                            value="" hidden>
                                    <div class="col-sm-10 col-md-10 col-lg-10"> 
                                        <div class="form-group" id="client_form">
                                        <label for="id_client" style="color:#005574;">NOMBRE DEL CLIENTE</label>
                                        <input id="id_client" class="form-control" type="text"
                                            value=""> 
                                        
                                    </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2 col-lg-2 pt-4"> 
                                        <button class="btn btn-primary" onclick="findUser();"
                                        style="background-color:#005574; color:white; border:2px solid #005574;"><i
                                            style="color:white;" class="fas fa-search"></i></button>
                                    </div>
                                </div>

                                <div id="resultado_usuario">
                                   
                                </div>
                                <div id="datos_cliente" style="display:none;">
                                    <hr>
                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-6"> 
                                                <div class="form-group" id="client_form">
                                                <label for="casillero" style="color:#005574;">CASILLERO</label>
                                                <input id="casillero" class="form-control" type="text"
                                                    value="">
                                                
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6"> 
                                                <div class="form-group">
                                                <label for="alias_paquete" style="color:#005574;">NOMBRE DEL PAQUETE</label>
                                                <input id="alias_paquete" class="form-control" type="text"
                                                    value="">
                                                
                                                </div>
                                                
                                            </div>
                                        </div>

                                    <hr>
                                </div>
                                
                                <div class="row">
                                    
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="transp" style="color:#005574;">TRANSPORTISTA</label>
                                            <select id="transp" class="form-select">
                                                <!-- <option value="0">Seleccionar...</option> -->
                                                <?php  
                                                            foreach ($allTransp as $key => $value) {
                                                        ?>
                                                <option value="<?php echo $value['id']; ?>">
                                                    <?php echo $value['name']; ?></option>
                                                <?php
                                                            }
                                                        ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="proveedor" style="color:#005574;">PROVEEDOR</label>
                                            <input id="proveedor" type="TEXT" placeholder="Proveedor" class="form-control">
                                        </div>
                                    </div>
                                    
                                   
                                </div>
                            
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="tracking" style="color:#005574;">TRACKING</label>

                                            <input id="tracking" type="text" placeholder="Tracking"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="cost" style="color:#005574;">VALOR USD</label>
                                            <input id="cost" type="number" placeholder="Costo" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6" style="display: none;">
                                        <div class="form-group">
                                            <label for="date_r" style="color:#005574;">Recibo</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <div class="col-md-12 border-bottom p-0">
                                                    <input readonly="readonly" id="date_r" type="text"
                                                        placeholder="Fecha de registro"
                                                        class="form-control p-0 border-0"
                                                        value="<?php echo date('Y-m-d h:m:s'); ?>"> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="date_in" style="color:#005574;">FECHA ESTIMADA</label>

                                            <input id="date_in" type="date" placeholder="Fecha de llegada"
                                                class="form-control"> </div>

                                    </div>
                                </div>



                                <div class="form-group">
                                    <label for="content" style="color:#005574;">CONTENIDO</label>

                                    <textarea class="form-control" id="content" rows="3"></textarea>

                                </div>
                                <div class="form-group">


                                    <form enctype="multipart/form-data" id="alert-invoice-form" method="post"
                                        class="upload-form">
                                        <span style="color:#005574;" class="upload-span-screenshot">CARGA TU FACTURA</span>

                                        <input type="file" id="alert-invoice" name="alert-invoice" />
                                    </form>

                                </div>

                                <hr>

                                <div align="right" class="form-group">
                                    <a onclick="addAlert();" class="btn btn-primary"
                                        style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i
                                            style="color:white;" class="fas fa-plus"></i> REGISTRAR</a>
                                    <!-- <div class="col-sm-12">
                                        <button onclick="addAlert();" class="btn btn-success">Registrar</button>
                                    </div> -->
                                </div>
                                <!-- </div>
                        </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-header-position="absolute" data-boxed-layout="full"
        style="background:#e3e3e3ff !important;">
        <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
                }

            ?>

        <div class="page-wrapper" style="background:#e3e3e3ff !important;">
            <div class="row">
                <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                    <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                </div>
                <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                    <h4 style="color:#005574;"> <strong>PREALERTAS</strong> </h4>
                </div>
            </div><br>
            <div class="alert alert-danger error-warning" role="alert">
            </div>
            <div class="alert alert-success register-state-success" role="alert">
            </div>
            <center>
                <div class="card">
                    <div class="card-body">
                        <div class="btn-group text-center" style="content-align:center; width:100%, padding:100px;"
                            align="center">

                            <button class="btn btn-primary" type="button" id="prealertPending"
                                onclick="showPrealertPending();"
                                style="background-color:#005574; color:white; border:2px solid #005574; width:300px;">PENDIENTES</button>


                            <button class="btn btn-primary" type="button" id="prealertConfirmed"
                                onclick="showPrealertConfirmed();"
                                style="background-color:white; color:#005574; border:2px solid #005574; width:300px;">HISTORIAL</button>




                        </div>
                    </div>
                </div>
            </center>
            <div class="card" id="editar-alerts" style="display:none; border:1px solid #e3e3e3ff;">
                <div class="card-body" style="border:1px solid #e3e3e3ff;">
                    <div class="row">

                        <input type="text" id="alert_id" hidden>
                        <br><br>
                        <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="name" style="color:#005574;"><strong>TRACKING</strong></label>
                                <input id="nroTracking" type="text" class="campos-input-login"> </div>
                        </div>

                        <div class="col-sm-12 col-md-1 col-lg-1 col-xl-1">
                            <div class="form-group">
                                <label for="document" style="color:#005574;"><strong>CASILLERO</strong></label>
                                <input id="id_client_user_type" type="text" class="campos-input-login">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-1 col-lg-1 col-xl-1">
                            <div class="form-group">
                                <label for="phone" style="color:#005574;"><strong>CLIENTE</strong></label>
                                <input id="client_name" type="text" class="campos-input-login">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="phone_house" style="color:#005574;"><strong>NOMBRE</strong></label>
                                <input id="package_name_user_type" type="text" class="campos-input-login">
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="email" style="color:#005574;"><strong>TRANSPORT</strong></label>
                                <select id="transport_id" class="form-select campos-input-login">

                                    <?php  
                                                foreach ($allTransp as $key => $value) {
                                            ?>
                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php
                                                }
                                            ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="email" style="color:#005574;"><strong>CONTENIDO</strong></label>
                                <input class="campos-input-login" type="text" id="content_user_type">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-1 col-lg-1 col-xl-1">
                            <div class="form-group">
                                <label for="email" style="color:#005574;"><strong>COSTO</strong></label>
                                <input type="text" class="campos-input-login" id="cost_user_type">
                            </div>
                        </div>
                        <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1">
                            <button class="boton-total-envios-registro-config" onclick="editAlert();"
                                style="margin-top:30px;"><i class="fas fa-check fa-lg" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    <div class="row">

                    </div>

                </div>
            </div>


            <div class="card" style="border:1px solid #e3e3e3ff;">
                <div class="card-body" style="border:1px solid #e3e3e3ff;">

                    <div class="row">
                        <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                            <div class="input-group mb-3">
                                <input id="myInput" onkeyup="searchFilter();" type="text"
                                    placeholder="¿Buscas un cliente?" class="form-control mt-0"><br><br>
                                <ul id="myUL">
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4"></div>
                        <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                            <input id="date_in_s" type="date" class="form-control" value="<?php echo $date_in; ?>">
                        </div>
                        <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                            <input id="date_out" type="date" class="form-control" value="<?php echo $date_out; ?>">
                        </div>
                        <div class="col-sm-12 col-md-1 col-lg-1 col-xl-1">
                            <button onclick="searchAlert();" class="btn btn-primary"
                                style="border:2px solid #005574; color:#005574; background-color:white;"><i
                                    class="fas fa-check"></i> Filtrar</button>
                        </div>
                    </div>
                    <hr style="background-color:#005574;">
                    <div class="row">

                        <div class="col-sm-9 col-md-9 col-lg-9">
                            <div align="left" style="background:white; color:#005574; width:100%; padding:8px;">
                                <h4>Pre-alertas</h4>
                            </div>
                        </div><br>
                        <div class="col-sm-3 col-md-3 col-lg-3" style="text-align:right; content-align:right;">
                            <div class="btn-group" align="right">
                                <a href="#" class="btn btn-primary" onclick="$('#alertModal').modal('show');"
                                    class="btn btn-primary"
                                    style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i
                                        style="color:white;" class="fas fa-plus"></i> Registrar</a>

                            </div>

                            <!-- <a align="right" href="registerWarehouse.php" class="btn" style="background-color:#ff554dff; color:white;"><i style="color:white;" class="fas fa-plus"></i> Crear</a> -->

                        </div>
                    </div>
                    <div id="alertPending_mod">
                    <div class="row">
                        <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                        <div class_="col-lg-10 col-xlg-10 col-md-10">
                            <table class="table packages-table"
                                style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                <thead>
                                    <!-- <th>#</th> -->
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Fecha Llegada</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Transportista</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Proveedor</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Tracking</th>
                                    <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Fecha Registro</th> -->
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Usuario</th>


                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Contenido</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Valor USD</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Factura</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Status</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Acción</th>



                                    <!-- <th>Ultimo Paquete</th> -->
                                    <!-- <th>Accion</th> -->
                                </thead>
                                <tbody>
                                    <?php  

                                    foreach ($allAlerts as $key => $value) {
                                ?>
                                    <tr>
                                        <!-- <td><?php echo $value['id']; ?></td> -->
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['date_in']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $transports->findById($value['transp'])['name']; ?></td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['proveedor']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['tracking']; ?></td>

                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $users->findById($value['id_client'])['name'].' '.$users->findById($value['id_client'])['lastname']; ?>
                                        </td>


                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['content']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['cost']; ?> $</td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php 
                                                    if ($value['invoice'] == null) {
                                                        echo "No tiene factura";
                                                    }else{
                                                        echo $value['invoice'];
                                                    }
                                                    
                                                ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#fc3c3d; background-color:white;"> <?php 
                                                    if ($value['alert_status'] == 1) {
                                                        echo "PENDIENTE";
                                                    }else{
                                                        echo "CONFIRMADA";
                                                    }
                                                    
                                                ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <div class="btn-group">
                                                <button class="btn btn-primary"
                                                    onclick="showEditAlert(<?php echo $value['id']; ?>);"
                                                    style="background-color:white; color:#005574; border:2px solid #005574;"
                                                    title="editar"><i class="fas fa-edit"></i></button>
                                                <button class="btn btn-primary"
                                                    onclick="sendAlert(<?php echo $value['id']; ?>);"
                                                    style="background-color:#005574;; color:white; border:2px solid #005574;"
                                                    title="enviar a almacén"><i class="fas fa-check"></i></button>
                                                <button class="btn btn-primary"
                                                    onclick="deleteAlert(<?php echo $value['id']; ?>);"
                                                    style="background-color:red; color:white; border:2px solid #005574;"
                                                    title="eliminar"><i class="fas fa-trash"></i></button>
                                            </div>

                                        </td>

                                    </tr>
                                    <?php
                                    }

                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                        </div>
                    </div>


                    <div id="alertConfirmed_mod" style="display:none;">
                    <div class="row">
                        <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                        <div class_="col-lg-10 col-xlg-10 col-md-10">
                            <table class="table packages-table"
                                style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                <thead>
                                    <!-- <th>#</th> -->
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Fecha Llegada</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Transportista</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Proveedor</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Tracking</th>
                                    <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Fecha Registro</th> -->
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Usuario</th>


                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Contenido</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Valor USD</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Factura</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Status</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        Acción</th>
                                   



                                    <!-- <th>Ultimo Paquete</th> -->
                                    <!-- <th>Accion</th> -->
                                </thead>
                                <tbody>
                                    <?php  

                                    foreach ($allAlertsConfirmed as $key => $value) {
                                ?>
                                    <tr>
                                        <!-- <td><?php echo $value['id']; ?></td> -->
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['date_in']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $transports->findById($value['transp'])['name']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $transports->findById($value['transp'])['name']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['tracking']; ?></td>

                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $users->findById($value['id_client'])['name'].' '.$users->findById($value['id_client'])['lastname']; ?>
                                        </td>


                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['content']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['cost']; ?> $</td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php 
                                                    if ($value['invoice'] == null) {
                                                        echo "No tiene factura";
                                                    }else{
                                                        echo $value['invoice'];
                                                    }
                                                    
                                                ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#fc3c3d; background-color:white;"> <?php 
                                                    if ($value['alert_status'] == 1) {
                                                        echo "PENDIENTE";
                                                    }else{
                                                        echo "CONFIRMADA";
                                                    }
                                                    
                                                ?></td>

                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <div class="btn-group">
                                               
                                                <button class="btn btn-primary"
                                                    onclick="deleteAlertHistory(<?php echo $value['id']; ?>);"
                                                    style="background-color:red; color:white; border:2px solid #005574;"
                                                    title="eliminar"><i class="fas fa-trash"></i></button>
                                            </div>

                                        </td>
                                        
                                    </tr>
                                    <?php
                                    }

                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                        </div>
                    </div>
                     
                    

                </div>

                <footer class="footer text-center"> 2021 © Total Envios
                </footer>
            </div>

        </div>
        <?php 

            include 'foot.php';

        ?>
</body>

</html>