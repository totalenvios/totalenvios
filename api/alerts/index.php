<?php  
	

	ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);
	header('Content-type: application/json');
	
	
	include '../../includes/connection.class.php';
	include '../../includes/headquarters.class.php';
	include '../../includes/packages.class.php';
	include '../../includes/packing.class.php';
	include '../../includes/stock.class.php';
	include '../../includes/users.class.php';
	include '../../includes/warehouses.class.php';

	$body = file_get_contents('php://input');

	$packages = new Packages;

	$url = $_SERVER['REQUEST_URI'];
	$parts = explode('/',$url);

	$parameter = str_replace('?id=', '', $parts[4]);

	if ($parameter == '') {

		$alerts = array();
		foreach ($packages->findAlerts() as $key => $value) {
			$alerts[] = array("id" => $value['id'], "tracking" => $value['tracking'], "date_r" => $value['date_r'], "id_client" => $value['id_client'], "transp" => $value['transp'], "date_in" => $value['date_in'], "content" => $value['content'], "last" => $value['last']);
		}

		echo json_encode($alerts);

	}else{

		$data = $packages->findAlertById($parameter);
		$alerts = array("id" => $data['id'], "tracking" => $data['tracking'], "date_r" => $data['date_r'], "id_client" => $data['id_client'], "transp" => $data['transp'], "date_in" => $data['date_in'], "content" => $data['content'], "last" => $data['last']);

		echo json_encode($alerts);

	}


?>