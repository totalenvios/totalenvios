<?php  
	

	// ini_set('display_errors', '1');
	// ini_set('display_startup_errors', '1');
	// error_reporting(E_ALL);

  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Headers: *');
  header('Access-Control-Allow-Methods: GET,POST,OPTIONS,DELETE,PUT');
	header('Content-type: application/json');
	
	include '../../includes/connection.class.php';
	include '../../includes/headquarters.class.php';
	include '../../includes/packages.class.php';
	include '../../includes/packing.class.php';
	include '../../includes/stock.class.php';
	include '../../includes/users.class.php';
	include '../../includes/warehouses.class.php';

	$body = file_get_contents('php://input');

	$users = new Users;

	$url = $_SERVER['REQUEST_URI'];
	$parts = explode('/',$url);

	$parameter = str_replace('?id=', '', $parts[4]);

	if ($parameter == '') {

		$clientsData = array();
		foreach ($users->findAll() as $key => $value) {
			$clientsData[] = array("id" => $value['id'], "name" => $value['name'], "lastname" => $value['lastname'], "email" => $value['email'], "document" => $value['document'], "address" => $value['address'], "country" => $value['country'], "phone" => $value['phone'], "user_type" => $value['user_type'], "born_date" => $value['born_date']);
		}

		echo json_encode($clientsData);

	}else{

		$data = $users->findById($parameter);
		$clientsData = array("id" => $data['id'], "name" => $data['name'], "lastname" => $data['lastname'], "email" => $data['email'], "document" => $data['document'], "address" => $data['address'], "country" => $data['country'], "phone" => $data['phone'], "user_type" => $data['user_type'], "born_date" => $data['born_date']);

		echo json_encode($clientsData);

	}


?>