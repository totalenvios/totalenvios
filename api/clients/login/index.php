<?php  
	
	ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);

	header('Content-type: application/json');
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Headers: Authorization,X-API-KEY,Origin,X-Rquested-Widht,Content-Type,Accept,Acces-Control-Allow-Request-Method');
	header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
  
	
	include '../../../includes/connection.class.php';
	include '../../../includes/users.class.php';


	$users = new Users;

	$token = bin2hex(random_bytes(64));

	$users->destroyToken();
	// echo $token;

	$data = json_decode(file_get_contents("php://input"), true);

	if ($data == '') {
		echo "Error: JSON VACIO";
	}
	else{
		try {
			$email = $data['email'];
			$password = $data['password'];

			$users->email = $email;
			$checkIfExists = $users->checkIfExists()['cuantos'];

			if ($checkIfExists > 0) {

				$password = $users->checkUserData()['password'];
				$userData = $users->checkUserData();
				$hash = $password;
				
				if (password_verify($data['password'], $hash)) {
					$id = $users->checkUserData()['id'];
					$users->addTempToken($id, $token);
					// consultar datos del usuario para el login
					// $userData = $users->findById($id);
					$datos = array(
						'token' => $token,
						'userData'=> $userData
						);
					echo json_encode($datos, JSON_FORCE_OBJECT);
					http_response_code(200);
				}else{
					echo "Error: PASSWORD INCORRECTO";
					http_response_code(400);
				}

			
			}else{
				echo "Error: USUARIO NO EXISTE";
				http_response_code(404);
			}
		} catch (PDOException $e) {
			echo $e;
		}
	

	}

?>