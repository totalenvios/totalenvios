<?php  
	
	ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);

	header('Content-type: application/json');
  	header('Access-Control-Allow-Origin: *');
  	header('Access-Control-Allow-Headers: Authorization,X-API-KEY,Origin,X-Rquested-Widht,Content-Type,Accept,Acces-Control-Allow-Request-Method');
	header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
  
	
	include '../../../includes/connection.class.php';
	include '../../../includes/users.class.php';


	$users = new Users;

	$token = bin2hex(random_bytes(64));

	$users->destroyToken();
	// echo $token;

	$data = json_decode(file_get_contents("php://input"), true);

	if ($data == '') {
		echo "Error: JSON VACIO";
	}
	else{
		$email = $data['email'];
		$password = $data['password'];

		$users->email = $email;
		$checkIfExists = $users->checkIfExists()['cuantos'];

		if ($checkIfExists > 0) {

			$password = $users->checkUserData()['password'];
		    $hash = $password;
			
			if (password_verify($data['password'], $hash)) {
				$id = $users->checkUserData()['id'];
				$users->addTempToken($id, $token);
				$datos = array(
					'token' => $token,
					);
				echo json_encode($datos, JSON_FORCE_OBJECT);
			}else{
				echo "Error: PASSWORD INCORRECTO";
			}

		// }else{

		// 	echo "Error: USUARIO NO EXISTE";

		}else{
			echo "Error: USUARIO NO EXISTE";
		}
		// $userData = $users->findByEmail($email);


	}

?>