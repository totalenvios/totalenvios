<?php  
	header('Content-type: application/json');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Authorization,X-API-KEY,Origin,X-Rquested-Widht,Content-Type,Accept,Acces-Control-Allow-Request-Method');
	header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
	
	include '../../../includes/connection.class.php';
	include '../../../includes/headquarters.class.php';
	include '../../../includes/packages.class.php';
	include '../../../includes/packing.class.php';
	include '../../../includes/stock.class.php';
	include '../../../includes/users.class.php';
	include '../../../includes/warehouses.class.php';

	$headers = apache_request_headers();

	$packages = new Packages;
	$users = new Users;


	$tracking = $_GET["tracking"];

	// $data = json_decode(file_get_contents("php://input"), true);

	$auth = 'TOKEN';
	
	foreach ($headers as $header => $value) {
	    if ($header == 'Authorization') {
	    	$auth = $value;
	    }
	}
	
	$continue = false;

	if ($users->findToken($auth)['cnt'] > 0) {
		$continue = true;
	}
	
	if ($continue == true) {
		
            // validar existencia de prealerta del paquete

            $found = $packages->findTracking($tracking);
                if($found['exist'] == 0){
                    $datos = array(
                        'found'=>0,
                    );
                }else{

                    $userData = $packages->findTrackingData($tracking);
                    $transpor = $packages->findTransportForId($userData['transp']);
                    $user = $packages->findUserForId($userData['id_client']);

                    
                    $datos = array(
                        'found'=>1,
                        'tracking'=>$tracking,
                        'userData'=>[ 
                            'user_name' => $user['name'],
                            
                            'user_code'=>$userData['id_client'],
                            'transport_id'=>$transpor['name'],
                            'description'=>$userData['content'],
                            'cost'=>$userData['value']
                        ]
                    );
                }

				echo json_encode($datos, JSON_FORCE_OBJECT);
				http_response_code(200);

			
		}else{
            echo "Error: Invalid Credentials";
			http_response_code(400);
        }

        ?>
