<?php  
	header('Content-type: application/json');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Authorization,X-API-KEY,Origin,X-Rquested-Widht,Content-Type,Accept,Acces-Control-Allow-Request-Method');
	header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
	
	include '../../../includes/connection.class.php';
	include '../../../includes/headquarters.class.php';
	include '../../../includes/packages.class.php';
	include '../../../includes/packing.class.php';
	include '../../../includes/stock.class.php';
	include '../../../includes/users.class.php';
	include '../../../includes/warehouses.class.php';

	$headers = apache_request_headers();

	$users = new Users;

	$data = json_decode(file_get_contents("php://input"), true);

	$auth = 'TOKEN';
	
	foreach ($headers as $header => $value) {
	    if ($header == 'Authorization') {
	    	$auth = $value;
	    }
	}
	
	$continue = false;

	if ($users->findToken($auth)['cnt'] > 0) {
		$continue = true;
	
	}
	if ($continue == true) {

			$data = array(
				'id'=>(int)$users->sesion($auth)['id'],
				'name'=>$users->sesion($auth)['name'],
				'lastname'=>$users->sesion($auth)['lastname'],
				'email'=>$users->sesion($auth)['email'],
				'user_type'=>3,
				'user_type_lastname'=>"Operador"
			);
		
			
			echo json_encode($data);
			http_response_code(200);
		
	}else{
		echo "Error: Invalid Credentials";
		http_response_code(200);
	}

	


?>