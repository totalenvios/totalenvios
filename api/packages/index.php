<?php  
	

	ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);
	header('Content-type: application/json');
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Headers: Authorization,X-API-KEY,Origin,X-Rquested-Widht,Content-Type,Accept,Acces-Control-Allow-Request-Method');
	header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
	
	include '../../includes/connection.class.php';
	include '../../includes/headquarters.class.php';
	include '../../includes/packages.class.php';
	include '../../includes/packing.class.php';
	include '../../includes/stock.class.php';
	include '../../includes/users.class.php';
	include '../../includes/warehouses.class.php';

	$body = file_get_contents('php://input');

	$packages = new Packages;

	$url = $_SERVER['REQUEST_URI'];
	$parts = explode('/',$url);

	$parameter = str_replace('?id=', '', $parts[4]);

	if ($parameter == '') {

		$packagesData = array();
		foreach ($packages->findAll() as $key => $value) {
			$packagesData[] = array("id" => $value['id'],"code" => $value['code'],"user_from" => $value['user_from'],"user_to" => $value['user_to'],"date_in" => $value['date_in'], "date_out" => $value['date_out'], "operator" => $value['operator'], "tracking" => $value['tracking'], "description" => $value['description'], "state" => $value['state'], "height" => $value['height'], "width" => $value['width'], "lenght" => $value['lenght'], "weight" => $value['weight'], "id_headquarter" => $value['id_headquarter'], "id_stock" => $value['id_stock'], "img" => $value['img']);
		}

		echo json_encode($packagesData);

	}else{

		$data = $packages->findById($parameter);
		$packagesData = array("id" => $data['id'],"code" => $data['code'],"user_from" => $data['user_from'],"user_to" => $data['user_to'],"date_in" => $data['date_in'], "date_out" => $data['date_out'], "operator" => $data['operator'], "tracking" => $data['tracking'], "description" => $data['description'], "state" => $data['state'], "height" => $data['height'], "width" => $data['width'], "lenght" => $data['lenght'], "weight" => $data['weight'], "id_headquarter" => $data['id_headquarter'], "id_stock" => $data['id_stock'], "img" => $data['img']);

		echo json_encode($packagesData);

	}


?>