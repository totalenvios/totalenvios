<?php  
	header('Access-Control-Allow-Origin: *');
	header('Content-type: application/json');
	
	include '../../../includes/connection.class.php';
	include '../../../includes/headquarters.class.php';
	include '../../../includes/packages.class.php';
	include '../../../includes/packing.class.php';
	include '../../../includes/stock.class.php';
	include '../../../includes/users.class.php';
	include '../../../includes/warehouses.class.php';

	$headers = apache_request_headers();

	$packages = new Packages;
	$users = new Users;

	$data = json_decode(file_get_contents("php://input"), true);

	$auth = 'TOKEN';
	
	foreach ($headers as $header => $value) {
	    if ($header == 'Authorization') {
	    	$auth = $value;
	    }
	}
	
	$continue = false;

	if ($users->findToken($auth)['cnt'] > 0) {
		$continue = true;
	}
	
	if ($continue == true) {
		if ($data == '') {
			echo "Error: JSON VACIO";
		}else{
			$date_in = $data['date_in'];
			$tracking = $data['tracking'];
			$description = $data['description'];
			$height = $data['height'];
			$width = $data['width'];
			$lenght = $data['lenght'];
			$weight = $data['weight'];

			if ($date_in == '') {
				echo "Error: INGRESA UNA FECHA DE ENTRADA";
			}else if($tracking == ''){
				echo "Error: INGRESA UN NUMERO DE TRACKING";
			}else if($height == ''){
				echo "Error: INGRESA LA ALTURA";
			}else if($width == ''){
				echo "Error: INGRESA EL ANCHO";
			}else if($lenght == ''){
				echo "Error: INGRESA EL LARGO";
			}else if($weight == ''){
				echo "Error: INGRESA EL PESO";
			}else{

				$packages->code = '';
				$packages->user_from = 50000;
				$packages->user_to = 50000;
				$packages->date_in = $date_in;
				$packages->operator = 0;
				$packages->tracking = $tracking;
				$packages->description = $description;
				$packages->state = 1;
				$packages->height = $height;
				$packages->width = $width;
				$packages->lenght = $lenght;
				$packages->weight = $weight;
				$packages->headquarter = 0;
				$packages->stock = 0;
				$id = $packages->add();
				echo json_encode("'Success':true, id:".$id."");

			}
		}
	}else{
		echo "Error: Invalid Credentials";
	}

	


?>