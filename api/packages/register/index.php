<?php  
	header('Content-type: application/json');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Authorization,X-API-KEY,Origin,X-Rquested-Widht,Content-Type,Accept,Acces-Control-Allow-Request-Method');
	header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
	
	include '../../../includes/connection.class.php';
	include '../../../includes/headquarters.class.php';
	include '../../../includes/packages.class.php';
	include '../../../includes/packing.class.php';
	include '../../../includes/stock.class.php';
	include '../../../includes/users.class.php';
	include '../../../includes/warehouses.class.php';

	$headers = apache_request_headers();

	$packages = new Packages;
	$users = new Users;

	$data = json_decode(file_get_contents("php://input"), true);

	$auth = 'TOKEN';
	
	foreach ($headers as $header => $value) {
	    if ($header == 'Authorization') {
	    	$auth = $value;
	    }
	}
	
	$continue = false;

	if ($users->findToken($auth)['cnt'] > 0) {
		$continue = true;
	}
	
	if ($continue == true) {
		
		if ($data == '') {
			echo "Error: JSON VACIO";
		}else{
			// if($data['user_code'] ==""){
			// 	$user_code = 0;
			// }else{
			// 	$user_code = (int)$data['user_code'];
			// };
		
			$user_code = (int)$data['code'];
			$alias = $data['alias'];
			$tracking = $data['tracking'];
			$operador_id = (int)$data['operator'];
			$date_in = $data['date'];;
			$description = $data['description'];
			$transport_id = (int)$data['transport'];
			$height = $data['height'];
			$width = $data['width'];
			$lenght = $data['lenght'];
			$weight = $data['weight'];
			$cost = $data['cost'];

			
				if($alias == "" && $data['code'] == ""){
					// paquete sin usuario
					$packages->code = "TE-PK0001";
					$packages->user_from = 0;
					$packages->user_to = 0;
					$packages->date_in = $date_in;
					$packages->alias = "";
					$packages->operator = $operador_id;
					$packages->tracking = $tracking;
					$packages->description = $description;
					$packages->state = 3;
					$packages->height = $height;
					$packages->width = $width;
					$packages->lenght = $lenght;
					$packages->weight = $weight;
					$packages->headquarter = 1;
					$packages->transp = $transport_id;
					$packages->address_send = "dirección";

					$id = $packages->add();
					$packagesData = $packages->findById($id);
				}else{
					if($data['code'] == ""){
						// paquete de usuario no registrado
						$packages->code = "TE-PK0000";
						$packages->user_from = 0;
						$packages->user_to = 0;
						$packages->date_in = $date_in;
						$packages->alias = $alias;
						$packages->operator = $operador_id;
						$packages->tracking = $tracking;
						$packages->description = $description;
						$packages->state = 2;
						$packages->height = $height;
						$packages->width = $width;
						$packages->lenght = $lenght;
						$packages->weight = $weight;
						$packages->headquarter = 1;
						$packages->transp = $transport_id;
						$packages->address_send = "dirección";

						$id = $packages->add();
						$packagesData = $packages->findById($id);

					}else{
						// paquete con usuario
					$packages->code = '';
					$packages->user_from = $user_code;
					$packages->user_to = $user_code;
					$packages->date_in = $date_in;
					$packages->alias = "";
					$packages->operator = $operador_id;
					$packages->tracking = $tracking;
					$packages->description = $description;
					$packages->state = 1;
					$packages->height = $height;
					$packages->width = $width;
					$packages->lenght = $lenght;
					$packages->weight = $weight;
					$packages->headquarter = 1;
					$packages->transp = $transport_id;
					$packages->address_send = "dirección";

					$id = $packages->add();
					$code = 'TE-PK'.$id;
					$packagesCode = $users->generateCodePackage($code,$id);
					$packagesData = $packages->findById($id);

					}
					
				}
				
				

				$dataPackage = array(
					"id" => (int)$packagesData['id'] ,
					"code" =>$packagesData['code'],
					"alias" => $packagesData['alias'],
					"user_from_id" => $packagesData['user_from'],
					"user_to_id" => $packagesData['user_to'],
					"date_in" => $packagesData['date_in'],
					"operator" => $packagesData['operator'],
					"tracking" => $packagesData['tracking'],
					"description" => $packagesData['description'],
					"height" => $packagesData['height'],
					"width" => $packagesData['width'],
					"lenght" => $packagesData['lenght'],
					"weight" => $packagesData['weight'],
					"cost" =>$data['cost']
				);
				
				$response = array(
					"message" => "registro exitoso",
					"packageData" => $dataPackage
				);
				echo json_encode($response);
				http_response_code(200);
			

		
		}
	}else{
		echo "Error: Invalid Credentials";
		http_response_code(200);
	}

	


?>