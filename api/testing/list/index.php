<?php  
	header('Content-type: application/json');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Authorization,X-API-KEY,Origin,X-Rquested-Widht,Content-Type,Accept,Acces-Control-Allow-Request-Method');
	header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
	
	include '../../../includes/connection.class.php';
	include '../../../includes/headquarters.class.php';
	include '../../../includes/packages.class.php';
	include '../../../includes/packing.class.php';
	include '../../../includes/stock.class.php';
	include '../../../includes/users.class.php';
	include '../../../includes/warehouses.class.php';

	$headers = apache_request_headers();

	$wh_id = 5000;

	$transport = new Warehouses;
    $count = $transport->countwarehousesBox(30);
	$extra_cost = $transport->findExtraCostForWarehouseBox(8801);
	$val = ((int)$count['cnt'])-1;

	$whData = $transport->listWarehouseData($wh_id);

	$userData = $transport->listUserDataToReceive($whData[0]['id_from']);
	$addresseeData = $transport->listAddresseeDataToReceive($whData[0]['id_to']);
	$tripData = $transport->findTripName($whData[0]['trip_id']);
	$cityData = $transport->findcityName($addresseeData[0]['city_id']);
	$stateData = $transport->findstateName($addresseeData[0]['state_id']);
	$countryData = $transport->findcountryName($addresseeData[0]['country_id']);

    if( $whData[0]['trip_id'] == 1){
        $transport_list = $transport->listBoxWarehouseIdForAir($wh_id);
    }else{
        $transport_list = $transport->listBoxWarehouseIdForOcean($wh_id);
    }
	

		$response = array(
			'warehouse'=> $whData[0],
			'cliente'=> $userData[0],
			'destinatario'=> $addresseeData[0],
			'envio'=> $tripData[0],
			'ciudad' => $cityData[0],
			'estado' => $stateData[0],
			'pais' => $countryData[0],
			'boxes' => $transport_list
		);
		
		echo json_encode($response);
		http_response_code(200);	
?>