<?php  
	header('Content-type: application/json');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Authorization,X-API-KEY,Origin,X-Rquested-Widht,Content-Type,Accept,Acces-Control-Allow-Request-Method');
	header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
	
	include '../../../includes/connection.class.php';
	include '../../../includes/headquarters.class.php';
	include '../../../includes/packages.class.php';
	include '../../../includes/packing.class.php';
	include '../../../includes/stock.class.php';
	include '../../../includes/users.class.php';
	include '../../../includes/warehouses.class.php';

	$headers = apache_request_headers();

	$transport = new Users;

	$auth = 'TOKEN';
	
	foreach ($headers as $header => $value) {
	    if ($header == 'Authorization') {
	    	$auth = $value;
	    }
	}
	
	$continue = false;

	if ($transport->findToken($auth)['cnt'] > 0) {
		$continue = true;
	}
	if ($continue == true){

			$transport_list = $transport->getTransport();
			echo json_encode($transport_list);
			http_response_code(200);		
		
	}else{
		echo "Error: Invalid Credentials";
		http_response_code(200);
	}

	


?>