<?php  
    // error_reporting(0);
    // ini_set('display_errors', 0);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    include 'includes/connection.class.php';
    include 'includes/users.class.php';
    include 'includes/packages.class.php';
    include 'includes/headquarters.class.php';
    include 'includes/stock.class.php';
    include 'includes/transport.class.php';
    session_start();

    $users = new Users;

    $user_id = $users->findByEmail($_SESSION['email'])['id'];
    // echo $user_id;
    
    
    $transp = new Transport;
    $allUsers = $users->findAll();
    $packages = new Packages;
    $allPackages = $packages->findPackageEnv($_GET['id']);
    $headquarters = new Headquarters;
    $allHeadquarters = $headquarters->findAll();
    $allOperators = $users->findAllOperators();
    $allClients = $users->findAllClients();
    $states = $packages->findStates();

    $stock = new Stock;


    if ($_SESSION['state'] != 1) {
        header('Location: login.php');
    }

    $dataUser = $users->findByEmail($_SESSION['email']);
    $date_in = date('2020-m-d');
    $date_out = date('Y-m-d h:m:s');
    $allTransp = $packages->findAllTransp();

    $digits = 5;
    $fecha = new DateTime();
    $dataFecha = $fecha->getTimestamp();
    $code = 'TE-PK'.substr($dataFecha, -5);


?>
        <html dir="ltr" lang="en">

        <head>
            <?php 

                include 'head.php';

            ?>

            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
            <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        </head>
        <body style="background:#e3e3e3ff;">
        <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
            data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
            <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
                }

            ?>
            <?php include 'sidebarca.php'; ?>

            <div class="page-wrapper" style="background:#e3e3e3ff; margin-bottom:40px;"><br>
            <div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                                <h4 style="color:#005574;">PAQUETES ENVIADOS</h4>
                            </div>
                        </div><br>

                        <div class="row m-3">
                            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2"></div>
                            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2"></div>
                            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2"></div>
                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3"></div> 
                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3 text-right">
                            <a href="./casillero-pack.php"><button class="btn btn-primary" style="border:2px solid #005574; color:#005574; background-color:white;"><i class="fas fa-arrow-left"></i> Atras</button></a>
                            </div> 
                        </div>

                        <div class="card">
                            <div class="card-body">
                               
                            <table class="table packages-table" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                            <thead>
                                <!-- <th style="border: 2px solid #e3e3e3ff;"></th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Estado</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Nro. paquete</th>       
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Tracking</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Ingreso</th>
                                <!-- <th>Casillero</th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Destino</th>
                                <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Región</th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Proveedor</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Descripcion</th>
                                <!-- <th>Almacen</th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Operador</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Consultar Recibo</th>
                            </thead>
                            <tbody>
                                <?php  

                                    foreach ($allPackages as $key => $value) {
                                        // echo $value;
                                ?>
                                        <tr>
                                            <!-- <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><input onclick="countPChecks();" type="checkbox" class="form-check-input pcheck pcheck<?php echo $value['id'];?>" id="pcheck<?php echo $value['id'];?>" package="<?php echo $value['id'];?>"></td> -->
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            
                                                <?php 
                                                    echo $packages->findStateById($value['state'])['name'];
                                                ?>
                                            </td>
                                            <!-- <td><?php echo $value['id']; ?></td> -->
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['code']; ?></td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php 
                                                    if ($value['tracking'] == null) {
                                                        echo "N/A";
                                                    }else{
                                                        echo $value['tracking'];; 
                                                    }
                                                    
                                                ?>
                                            </td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php 
                                                $fechaComoEntero = strtotime($value['date_in']);    
                                                $fecha = date("m/d/Y", $fechaComoEntero);
                                                echo $fecha; 
                                            ?>
                                            </td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['address_send']; ?></td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $transp->findById($value['id_transp'])['name']; ?></td>
                                            <!-- <td><?php echo $value['code']; ?></td> -->
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['description']; ?></td>
                                            <!-- <td>
                                                <?php 
                                                    if ($value['id_stock'] == 0) {
                                                        echo "No tiene Almacen";
                                                    }else{
                                                        echo $stock->findById($value['id_stock'])['name']; 
                                                    }
                                                    
                                                ?>
                                            </td> -->
                                           
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php 
                                                    if ($value['operator'] == 0) {
                                                        echo "No tiene operador";
                                                    }else{
                                                        echo $users->findById($value['operator'])['name']; 
                                                    }
                                                    
                                                ?>
                                            </td>
                                            <td>
                                                <div style="margin:auto;">
                                                    <a target="_blank" href="PackageReceive.php?id=<?php echo $value['id'];?>" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;">
                                                        <i class="fas fa-file"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                <?php
                                    }

                                ?>
                            </tbody>
                        </table>
                            </div>
                        </div>
              
                        

                            
                        
        </body>
        <!-- <footer class="footer text-center"> 2021 © Total Envios
        </footer> -->
        <?php 

        include 'foot.php';

        ?>