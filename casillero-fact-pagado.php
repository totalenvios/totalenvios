<?php  
    // error_reporting(0);
    // ini_set('display_errors', 0);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    include 'includes/connection.class.php';
    include 'includes/users.class.php';
    // include 'includes/packages.class.php';
    include 'includes/headquarters.class.php';
    include 'includes/stock.class.php';
    include 'includes/transport.class.php';
    include 'includes/invoices.class.php';
    include 'includes/warehouses.class.php';
    session_start();

    $users = new Users;

    $user_id = $users->findByEmail($_SESSION['email'])['id'];
    // echo $user_id;
    
    
    $transp = new Transport;
    $allUsers = $users->findAll();
    $invoices = new Invoices;
    $wareHouse = new Warehouses;
    $allwareHouse = $wareHouse -> notaWarehouse();
    $allInvoices = $invoices->findByIdPag($user_id);
    $headquarters = new Headquarters;
    $allHeadquarters = $headquarters->findAll();
    $allOperators = $users->findAllOperators();
    $allClients = $users->findAllClients();
    // $states = $packages->findStates();

    $stock = new Stock;


    if ($_SESSION['state'] != 1) {
        header('Location: login.php');
    }

    $dataUser = $users->findByEmail($_SESSION['email']);
    $date_in = date('2020-m-d');
    $date_out = date('Y-m-d'); 
    // $allTransp = $packages->findAllTransp();

    $digits = 5;
    $fecha = new DateTime();
    $dataFecha = $fecha->getTimestamp();
    $code = 'TE-PK'.substr($dataFecha, -5);
    // echo $code;


?>
        <html dir="ltr" lang="es">

        <head>
            <?php 

                include 'head.php';

            ?>

            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
            <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        </head>
        <body style="background:#e3e3e3ff;">
        <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
            data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
            <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
                }

            ?>
            <?php include 'sidebarca.php'; ?>

            <div class="page-wrapper" style="background:#e3e3e3ff; margin-bottom:40px;"><br>
            <div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                                <h4 style="color:#005574;">FACTURACIONES CANCELADAS</h4>
                            </div>
                        </div><br>

                        <div class="row m-3">
                            <input id="idUser" type="text" class="form-control" value="<?php echo $user_id; ?>" style="display:none;">
                            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <input id="date_in_s" type="date" class="form-control" value="<?php echo $date_in; ?>">
                            </div>
                            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <input id="date_out" type="date" class="form-control" value="<?php echo $date_out; ?>">
                            </div>
                            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <button onclick="searchFactPag();" class="btn btn-primary" style="border:2px solid #005574; color:#005574; background-color:white;"><i class="fas fa-check"></i> Filtrar</button>
                            </div>
                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3"></div> 
                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3 text-right">
                            <a href="./casillero-fact.php"><button class="btn btn-primary" style="border:2px solid #005574; color:#005574; background-color:white;"><i class="fas fa-arrow-left"></i> Atras</button></a>
                            </div> 
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <table class="table packages-table" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                    <thead>
                                        <!-- <th style="border: 2px solid #e3e3e3ff;"></th> -->
                                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Code Invoice</th>
                                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Type Trip</th>       
                                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Fecha Emisión</th>
                                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Fecha Vencimiento</th>
                                        <!-- <th>Casillero</th> -->
                                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Status Id</th>
                                        <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Región</th> -->
                                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Total Invoice</th>
                                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Nota</th>
                                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">User</th>
                                    </thead>
                                    <tbody>
                                            <?php 
                                                 foreach ($allInvoices as $key => $value) {
                                                    ?>
                                                    <tr>
                                                         <td><?php echo $value['code_invoce']?></td>
                                                         <td><?php echo $transp->findAllTripbyId($value['type_trip_id'])['name'];?></td>
                                                         <td><?php echo $value['fecha_emision']?></td>
                                                         <td><?php echo $value['fecha_vencimiento'];?></td>
                                                         <td><?php echo $invoices->generalStatus($value['status_id'])['name'];?></td>
                                                         <td><?php echo $value['total_invoce'];?></td>
                                                         <td><?php 
                                                            foreach ($allwareHouse as $key => $value2) {;
                                                                if($value['code_invoce'] == $value2['code']){
                                                                    echo $value2['nota_wh'];
                                                                }
                                                            }
                                                         
                                                         ?></td>  
                                                         <td><?php echo $users->findByEmail($_SESSION['email'])['name'];?></td>
                                                        </tr>
                                                    <?php
                                                 }
                                            
                                            ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
              
                        

                            
                        
        </body>
        <!-- <footer class="footer text-center"> 2021 © Total Envios
        </footer> -->
        <?php 

        include 'foot.php';

        ?>