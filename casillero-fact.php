<?php
// error_reporting(0);
// ini_set('display_errors', 0);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'includes/connection.class.php';
include 'includes/users.class.php';
// include 'includes/packages.class.php';
include 'includes/headquarters.class.php';
include 'includes/stock.class.php';
include 'includes/transport.class.php';
include 'includes/invoices.class.php';
include 'includes/warehouses.class.php';
session_start();

$users = new Users;

$user_id = $users->findByEmail($_SESSION['email'])['id'];


$transp = new Transport;
$allUsers = $users->findAll();
$invoices = new Invoices;
$wareHouse = new Warehouses;
$allwareHouse = $wareHouse->notaWarehouse();
$allInvoices = $invoices->findById($user_id);
$headquarters = new Headquarters;
$allHeadquarters = $headquarters->findAll();
$allOperators = $users->findAllOperators();
$allClients = $users->findAllClients();
//$states = $packages->findStates();

$stock = new Stock;


if ($_SESSION['state'] != 1) {
    header('Location: login.php');
}

$dataUser = $users->findByEmail($_SESSION['email']);
$date_in = date('2020-m-d');
$date_out = date('Y-m-d h:m:s');
//$allTransp = $packages->findAllTransp();

$digits = 5;
$fecha = new DateTime();
$dataFecha = $fecha->getTimestamp();
$code = 'TE-PK' . substr($dataFecha, -5);


?>
<html dir="ltr" lang="es">

<head>
    <?php

    include 'head.php';

    ?>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body style="background:#e3e3e3ff;">
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
        <?php

        include 'navbar.php';
        if ($_SESSION['state'] != 1) {
            header('Location: login.php');
        }

        ?>
        <?php include 'sidebarca.php'; ?>

        <div class="page-wrapper" style="background:#e3e3e3ff; margin-bottom:40px;"><br>
            <div class="row">
                <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                    <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                </div>
                <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                    <h4 style="color:#005574;">FACTURACIÓN PENDIENTES</h4>
                </div>
            </div><br>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12 text-right p-3">
                            <button class="pagado btn btn-primary" style="background-color: white; color: #005574; border: 2px solid #005574;" onclick="iraPagado()">Facturas Canceladas</button>
                            <!-- <a href="./casillero-fact-pagado.php" target="_blank" class=""></a> -->
                        </div>
                    </div>
                    <table class="table packages-table" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                        <thead>
                            <!-- <th style="border: 2px solid #e3e3e3ff;"></th> -->
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Code Invoice</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Type Trip</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Fecha Emisión</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Fecha Vencimiento</th>
                            <!-- <th>Casillero</th> -->
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Status Id</th>
                            <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Región</th> -->
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Total Invoice</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Nota</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">User</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Acción</th>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                foreach ($allInvoices as $key => $value) {
                                ?>
                                    <td><?php echo $value['code_invoce'] ?></td>
                                    <td><?php echo $transp->findAllTripbyId($value['type_trip_id'])['name']; ?></td>
                                    <td><?php echo $value['fecha_emision'] ?></td>
                                    <td><?php echo $value['fecha_vencimiento']; ?></td>
                                    <td><?php echo $invoices->generalStatus($value['status_id'])['name']; ?></td>
                                    <td><?php echo $value['total_invoce']; ?></td>
                                    <td><?php
                                        foreach ($allwareHouse as $key => $value2) {

                                            if ($value['code_invoce'] == $value2['code']) {
                                                echo $value2['nota_wh'];
                                                break;
                                            }
                                        }

                                        ?></td>
                                    <td><?php echo $users->findByEmail($_SESSION['email'])['name']; ?></td>
                                    <td> <button data-bs-toggle="modal" data-bs-target="#exampleModal" class="pagado btn btn-primary" style="background-color: white; color: #005574; border: 2px solid #005574;">Pagar</button>
                                    </td>
                                <?php
                                }
                                ?>

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>




            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="pagoP('cerrar')"></button>
                        </div>
                        <div class="modal-body">
                            <div class="row text-center">
                                <div class="p-4">
                                    <button class="pagado btn btn-primary" onclick="pagoP('paypal')" style="background-color: white; color: #005574; border: 2px solid #005574;">Paypal</button>
                                    <button class="pagado btn btn-primary" onclick="pagoP('zelle')" style="background-color: white; color: #005574; border: 2px solid #005574;">Zelle o Transferencias</button>
                                </div>

                                <div class="col-12">


                                    <div class="my-3 form-group" id="smart-button-container" style="display:none; width:100%;">
                                        <div style="text-align: center">
                                            <label for="description">Descripción </label><br>
                                            <input type="text" class="form-control" name="descriptionInput" id="description" maxlength="127" value="">
                                        </div>
                                        <p id="descriptionError" style="visibility: hidden; color:red; text-align: center;">Debe introducir una descripción</p>
                                        <div style="text-align: center">
                                            <label for="amount">Monto: </label><br>
                                            <input name="amountInput" class="form-control" type="number" placeholder="USD O BS" id="amount" value=""><span></span>
                                        </div>
                                        <p id="priceLabelError" style="visibility: hidden; color:red; text-align: center;">Debe introducir un monto</p>
                                        <div id="invoiceidDiv" style="text-align: center; display: none;">
                                            <label for="invoiceid"> </label>
                                            <input name="invoiceid" class="form-control" maxlength="127" type="text" id="invoiceid" value="">
                                        </div>
                                        <p id="invoiceidError" style="visibility: hidden; color:red; text-align: center;">Please enter an Invoice ID</p>
                                        <div style="text-align: center; margin-top: 0.625rem;" class="form-control" id="paypal-button-container"></div>
                                    </div>

                                </div>
                                <div class="col-12">

                                    <div id="pagoTransf" class="form-group" style="display:none; width:100%;">
                                        <div class="form-group my-3">
                                            <label for="transp" style="color:#005574;">Tipo de Banco </label>
                                            <select id="banco" class="form-select">
                                                <option value="nacional">Nacional</option>
                                                <option value="internacional">Internacional</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="montoP" style="color:#005574;">Monto Pagado</label>
                                            <input id="montoP" type="number" placeholder="Monto pagado" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="nombreT" style="color:#005574;">Nombre del Titular</label>
                                            <input id="nombreT" type="text" placeholder="Nombre del Titular" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="nombreT" style="color:#005574;">Nombre del Titular</label>
                                            <input id="nombreT" type="text" placeholder="Nombre del Titular" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="numIdent" style="color:#005574;">Número de Identificación</label>
                                            <input id="numIdent" type="text" placeholder="Número de Identificación" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="fechaTransaccion" style="color:#005574;">Fecha de la Transacción</label>
                                            <input id="fechaTransaccion" type="date" placeholder="Fecha de la Transacción" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="numeroTransaccion" style="color:#005574;">Número de Transacción</label>
                                            <input id="numeroTransaccion" type="text" placeholder="Número de Transacción" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="nota" style="color:#005574;">Nota</label>
                                            <input id="nota" type="text" placeholder="Nota" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" onclick="pagoP('cerrar')">Cancelar</button>
                            <button type="button" class="btn btn-success">Pagar</button>
                        </div>
                    </div>
                </div>
            </div>
</body>
<script>
    function pagoP(pago) {
        if (pago == 'paypal') {
            document.getElementById('smart-button-container').style.display = 'block';
            document.getElementById('pagoTransf').style.display = 'none';
        } else if (pago == 'zelle') {
            document.getElementById('smart-button-container').style.display = 'none';
            document.getElementById('pagoTransf').style.display = 'block';
        } else {
            document.getElementById('pagoTransf').style.display = 'none';
            document.getElementById('smart-button-container').style.display = 'none';
        }
    }

    function iraPagado() {
        // $('.enviado').load('./ajax/consultarFactura.php?id='+id,function(){ });
        window.location.href = "./casillero-fact-pagado.php";
    }
</script>
<script src="https://www.paypal.com/sdk/js?client-id=sb&enable-funding=venmo&currency=USD" data-sdk-integration-source="button-factory"></script>
<script>
    function initPayPalButton() {
        var description = document.querySelector('#smart-button-container #description');
        var amount = document.querySelector('#smart-button-container #amount');
        var descriptionError = document.querySelector('#smart-button-container #descriptionError');
        var priceError = document.querySelector('#smart-button-container #priceLabelError');
        var invoiceid = document.querySelector('#smart-button-container #invoiceid');
        var invoiceidError = document.querySelector('#smart-button-container #invoiceidError');
        var invoiceidDiv = document.querySelector('#smart-button-container #invoiceidDiv');

        var elArr = [description, amount];

        if (invoiceidDiv.firstChild.innerHTML.length > 1) {
            invoiceidDiv.style.display = "block";
        }

        var purchase_units = [];
        purchase_units[0] = {};
        purchase_units[0].amount = {};

        function validate(event) {
            return event.value.length > 0;
        }

        paypal.Buttons({
            style: {
                color: 'gold',
                shape: 'rect',
                label: 'paypal',
                layout: 'vertical',

            },

            onInit: function(data, actions) {
                actions.disable();

                if (invoiceidDiv.style.display === "block") {
                    elArr.push(invoiceid);
                }

                elArr.forEach(function(item) {
                    item.addEventListener('keyup', function(event) {
                        var result = elArr.every(validate);
                        if (result) {
                            actions.enable();
                        } else {
                            actions.disable();
                        }
                    });
                });
            },

            onClick: function() {
                if (description.value.length < 1) {
                    descriptionError.style.visibility = "visible";
                } else {
                    descriptionError.style.visibility = "hidden";
                }

                if (amount.value.length < 1) {
                    priceError.style.visibility = "visible";
                } else {
                    priceError.style.visibility = "hidden";
                }

                if (invoiceid.value.length < 1 && invoiceidDiv.style.display === "block") {
                    invoiceidError.style.visibility = "visible";
                } else {
                    invoiceidError.style.visibility = "hidden";
                }

                purchase_units[0].description = description.value;
                purchase_units[0].amount.value = amount.value;

                if (invoiceid.value !== '') {
                    purchase_units[0].invoice_id = invoiceid.value;
                }
            },

            createOrder: function(data, actions) {
                return actions.order.create({
                    purchase_units: purchase_units,
                });
            },

            onApprove: function(data, actions) {
                return actions.order.capture().then(function(orderData) {

                    // Full available details
                    console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));

                    // Show a success message within this page, e.g.
                    const element = document.getElementById('paypal-button-container');
                    element.innerHTML = '';
                    element.innerHTML = '<h3>Thank you for your payment!</h3>';

                    // Or go to another URL:  actions.redirect('thank_you.html');

                });
            },

            onError: function(err) {
                console.log(err);
            }
        }).render('#paypal-button-container');
    }
    initPayPalButton();
</script>


<!-- <footer class="footer text-center"> 2021 © Total Envios
        </footer> -->
<?php

include 'foot.php';

?>