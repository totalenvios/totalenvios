<?php  
    // error_reporting(0);
    // ini_set('display_errors', 0);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    include 'includes/connection.class.php';
    include 'includes/users.class.php';
    include 'includes/packages.class.php';
    include 'includes/headquarters.class.php';
    include 'includes/stock.class.php';
    include 'includes/transport.class.php';
    session_start();

    $users = new Users;

    $user_id = $users->findByEmail($_SESSION['email'])['id'];
    // echo $user_id;
    
    
    $transp = new Transport;
    $allUsers = $users->findAll();
    $packages = new Packages;
    $allPackages = $packages->findAllByUser($user_id);
    $headquarters = new Headquarters;
    $allHeadquarters = $headquarters->findAll();
    $allOperators = $users->findAllOperators();
    $allClients = $users->findAllClients();
    $states = $packages->findStates();

    $stock = new Stock;


    if ($_SESSION['state'] != 1) {
        header('Location: login.php');
    }

    $dataUser = $users->findByEmail($_SESSION['email']);
    $date_in = date('2020-m-d');
    $date_out = date('Y-m-d h:m:s');
    $allTransp = $packages->findAllTransp();

    $digits = 5;
    $fecha = new DateTime();
    $dataFecha = $fecha->getTimestamp();
    $code = 'TE-PK'.substr($dataFecha, -5);


?>
        <html dir="ltr" lang="en">

        <head>
            <?php 

                include 'head.php';

            ?>

            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
            <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        </head>
        <body style="background:#e3e3e3ff;">
        <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
            data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
            <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
                }

            ?>
            <?php include 'sidebarca.php'; ?>

            <div class="page-wrapper" style="background:#e3e3e3ff; margin-bottom:40px;"><br>
            <div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                                <h4 style="color:#005574;">PAQUETES</h4>
                            </div>
                        </div><br>

                        <div class="card">
                            <div class="card-body">
                                <div class="m-3" style="float:right;">  
                                    <button class="enviado btn btn-primary" 
                                    style="background-color: white; color: #005574; border: 2px solid #005574;" 
                                    onclick="paquetesEnviados('2')">Paquetes Enviados</button>
                                 
                                </div>
                               
                            <table class="table packages-table" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                            <thead>
                                <!-- <th style="border: 2px solid #e3e3e3ff;"></th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Estado</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Nro. paquete</th>       
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Tracking</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Ingreso</th>
                                <!-- <th>Casillero</th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Destino</th>
                                <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Región</th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Proveedor</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Descripcion</th>
                                <!-- <th>Almacen</th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Operador</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Confirmar Envío</th>
                                <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Accion</th> -->
                                <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Pre - Alerta</th> -->
                                
                                <!-- <th>Imagen</th> -->
                                <!-- <th>Accion</th> -->
                            </thead>
                            <tbody>
                                <?php  

                                    foreach ($allPackages as $key => $value) {
                                        // echo $value;
                                ?>
                                        <tr>
                                            <!-- <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><input onclick="countPChecks();" type="checkbox" class="form-check-input pcheck pcheck<?php echo $value['id'];?>" id="pcheck<?php echo $value['id'];?>" package="<?php echo $value['id'];?>"></td> -->
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            
                                                <?php 
                                                    echo $packages->findStateById($value['state'])['name'];
                                                ?>
                                            </td>
                                            <!-- <td><?php echo $value['id']; ?></td> -->
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['code']; ?></td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php 
                                                    if ($value['tracking'] == null) {
                                                        echo "N/A";
                                                    }else{
                                                        echo $value['tracking'];; 
                                                    }
                                                    
                                                ?>
                                            </td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php 
                                                $fechaComoEntero = strtotime($value['date_in']);    
                                                $fecha = date("m/d/Y", $fechaComoEntero);
                                                echo $fecha; 
                                            ?>
                                            </td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['address_send']; ?></td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $transp->findById($value['id_transp'])['name']; ?></td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['description']; ?></td>
                                           
                                           
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php 
                                                    if ($value['operator'] == 0) {
                                                        echo "No tiene operador";
                                                    }else{
                                                        echo $users->findById($value['operator'])['name']; 
                                                    }
                                                    
                                                ?>
                                            </td>
                                            <td> 
                                               
                                            <?php 
                                                    if ($value['state'] != '1') {
                                                        echo "";
                                                    }else{
                                                        ?>
                                                        <button class="btn btn-success" 
                                                        type="button" 
                                                        data-bs-toggle="modal" 
                                                    data-bs-target="#exampleModal"><i style="color:white;" class="fas fa-check"></i></button></td>
                                                <?php    
                                                }
                                                    
                                                ?>
                                            
                                            
                                        </tr>
                                <?php
                                    }

                                ?>
                            </tbody>
                        </table>
                            </div>
                        </div>
              
                        

                            
                        
        </body>

        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
          <label for="">Tipo de Envío</label>
            <select class="form-select" aria-label="Default select example">
                <option selected>Escoger envío</option>
                <option value="1">Marítimo</option>
                <option value="2">Aéreo</option>
            </select>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked>
                <label class="form-check-label" for="flexCheckDefault">
                Paquete Asegurado
            </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                <label class="form-check-label" for="flexCheckChecked">
                   Confirmar Proceso de Reempaque y Consolidado
                </label>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" style="background-color: white; color: #005574; border: 2px solid #005574;" data-bs-dismiss="modal" >Confirmar</button>
      </div>
    </div>
  </div>
</div>

        <script>
            function paquetesEnviados(id){
                console.log("le dio click" + id);
                // $('.enviado').load('./ajax/consultarFactura.php?id='+id,function(){ });
                window.location.href="./casillero-env.php?id="+id;
            }
        </script>
        <!-- <footer class="footer text-center"> 2021 © Total Envios
        </footer> -->
        <?php 

        include 'foot.php';

        ?>