<?php  
    // error_reporting(0);
    // ini_set('display_errors', 0);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    include 'includes/connection.class.php';
    include 'includes/users.class.php';
    include 'includes/packages.class.php';
    include 'includes/headquarters.class.php';
    include 'includes/stock.class.php';
    include 'includes/warehouses.class.php';
    include 'includes/depart.class.php';
    session_start();
    $users = new Users;

    $user_id = $users->findByEmail($_SESSION['email'])['id'];

    
    $allUsers = $users->findAll();
    $packages = new Packages;
    $allPackages = $packages->findAll();
    $depart = new Depart;
    $allDeparts = $depart->findAll();
    $headquarters = new Headquarters;
    $allHeadquarters = $headquarters->findAll(); 
    $allOperators = $users->findAllOperators();
    $allClients = $users->findAllClients();
    $states = $packages->findStates();

    $stock = new Stock;
    $warehouses = new Warehouses;
    $warehouseStates = $warehouses->findWStates();
    $allWarehouses = $warehouses->findAllByUser($user_id);
    $findLast = $warehouses->findLast();
    echo $user_id;
    $boxes = $packages->findBoxes();
    $trips = $warehouses->findTrips();
    $categories = $warehouses->findCategories();
    $countries = $warehouses->findCountries();

    $regions = $users->findRegions();

    $userDestiny = $users->findByEmail($_SESSION['email'])['name'].' '.$users->findByEmail($_SESSION['email'])['lastname'];
    $userDestinyId = $users->findByEmail($_SESSION['email'])['id'];
    $dataUser = $users->findByEmail($_SESSION['email']);
    $destiny = $users->findAddressById($dataUser['country']);
    $curriers = $packages->findCurriers();
    $regionCurrier = $users->findRegionByZone($dataUser['country']);

    if ($_SESSION['state'] != 1) {
        header('Location: login.php');
    }

    $date_in = date('2020-m-d');
    $date_out = date('Y-m-d');
    $digits = 5;
    $fecha = new DateTime();
    $dataFecha = $fecha->getTimestamp();
    $boxCode = substr($dataFecha, -5);
    $bulk = isset($_REQUEST['bulk']) ? $_REQUEST['bulk'] : 0;

    $nextFriday = strtotime('next friday');
    $nextFriday = date('Y-m-d', $nextFriday);


?>
        <html dir="ltr" lang="en">

        <head>
            <?php 

                include 'head.php';

            ?>

            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
            <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        </head>
        <body style="background:#e3e3e3ff;">
        <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
            data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
            <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
                }

            ?>
            <?php include 'sidebarca.php'; ?>

            <div class="page-wrapper" style="background:#e3e3e3ff; margin-bottom:40px;"><br>
            <div class="row">
                    <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                        <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                    </div>
                    <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                        <h4 style="color:#005574;">WAREHOUSES</h4>
                    </div>
                </div><br>
                        <div class="card">
                            <div class="card-body">
                            <div class="m-3" style="float:right;">  
                            <button class="enviado btn btn-primary" 
                                    style="background-color: white; color: #005574; border: 2px solid #005574;" 
                                    onclick="wharehouseEnviados(<?php echo $user_id ?>)">Wharehouse Enviados</button>
                                </div>
                             <table class="table table-warehouse" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                        <thead>
                                            <!-- <th style="border: 2px solid #e3e3e3ff;"></th> -->
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Status WH</th>
                                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Warehouse</th>
                                            <!-- <th>Casillero</th> -->
                                            
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Destinatario</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Destino</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Tipo de envío</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Salida</th>
                                            <!-- <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Cliente</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Nro. Casillero</th> -->
                                            <!-- <th>Descripcion</th> -->
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Currier</th>
                                        
                                            <!-- <th>Ancho</th>
                                            <th>Largo</th>
                                            <th>Alto</th>
                                            <th>Peso</th> -->
                                            <!-- <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Conf. usuario</th> -->
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Status envío</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Consultar PDF</th>
                                            <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#white;"></th> -->
                                        </thead>
                                        <tbody>
                                            <?php  

                                                foreach ($allWarehouses as $key => $value) {
                                            ?>
                                                    <tr>
                                                        <!-- <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><input onclick="countChecks();" type="checkbox" class="form-check-input wcheck wcheck<?php echo $value['id'];?>" id="wcheck<?php echo $value['id'];?>" warehouse="<?php echo $value['id'];?>"></td> -->
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                            <?php 
                                                                if( $value['state'] == 2){
                                                            ?> 
                                                            <i class="fas fa-lock fa-2x fa-lg" style="margin-top:10px;"> </i>
                                                            <?php 
                                                                }else{
                                                            ?>
                                                            <i class="fas fa-unlock-alt fa-2x fa-lg" style="margin-top:10px;"> </i>
                                                            <?php  
                                                                };
                                                            ?>
                                                        </td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['code']; ?></td>
                                                        <!-- <td><?php echo $value['casillero']; ?></td> -->
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $users->findAddresseeById($value['id_to'])[0]['name'].' '.$users->findAddresseeById($value['id_to'])[0]['lastname']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $users->findAddresseeById($value['id_to'])[0]['city_name']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $warehouses->findTripById($value['trip_id'])['name']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['date_send']; ?></td>
                                                        <!-- <td><?php echo $value['description']; ?></td> -->
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $packages->findCurrierById($value['id_currier'])['name']; ?></td>
                                                    
                                                        <!-- <td><?php echo $value['width']; ?></td>
                                                        <td ><?php echo $value['lenght']; ?></td>
                                                        <td><?php echo $value['height']; ?></td>
                                                        <td><?php echo $value['weight']; ?></td> -->
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                            <?php 
                                                                echo $warehouses->findGeneralStateById($value['ship_state'])['name'];
                                                            ?>
                                                        </td>
                                                        <!-- <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><a target="_blank" href="invoiceFormat.php?id=<?php echo $value['id']; ?>" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-eye"></i></a></td> -->
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white; text-align:center;">
                                                            <div class="btn-group text-center" style="content-align:center; width:100%">
                                                           
                                                            <div style="margin:auto;"><a target="_blank" href="invoceReceive.php?id=<?php echo $value['id'];?>" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-file"></i></a></div>
                                                        
                                                            
                                                            <!-- <div><button onclick="$('#deleteModal').modal('show'); addDeleteText(<?php echo $value['id']; ?>)" class="btn btn-primary" style="background-color:#005574; color:white; border:2px solid #005574;"><i class="fas fa-trash"></i></button></div> -->
                                                            
                                                            <!-- <div><a target="_blank" href="invoiceForm.php?id=<?php echo $value['id']; ?>" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-file-invoice"></i></a></div> -->
                                                            </div>
                                                        </td>
                                                    </tr>
                                            <?php
                                                }

                                            ?>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
              
                        

                            
                        
        </body>
        <script>
            function wharehouseEnviados(id){
                console.log("le dio click" + id);
                // $('.enviado').load('./ajax/consultarFactura.php?id='+id,function(){ });
                window.location.href="./casillero-whe.php?id="+id;
            }
       </script>
        <!-- <footer class="footer text-center"> 2021 © Total Envios
        </footer> -->
        <?php 

        include 'foot.php';

        ?>