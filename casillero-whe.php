<?php  
    // error_reporting(0);
    // ini_set('display_errors', 0);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    include 'includes/connection.class.php';
    include 'includes/users.class.php';
    include 'includes/packages.class.php';
    include 'includes/headquarters.class.php';
    include 'includes/stock.class.php';
    include 'includes/transport.class.php';
    include 'includes/warehouses.class.php';
    session_start();

    $users = new Users;

    $user_id = $users->findByEmail($_SESSION['email'])['id'];
    echo $user_id;
    
    
    $transp = new Transport;
    $allUsers = $users->findAll();
    $packages = new Packages;
    $allPackages = $packages->findAll();
    $warehouses = new Warehouses;
    $warehouseStates = $warehouses->findWStates();
    $allWarehouses = $warehouses->findAllByUserWE($_GET['id']);
    $allCurriers = $warehouses->findAllCurriers();
    $allDeparments= $warehouses ->findAllDepartments();
    $allTrip= $warehouses ->findAllTrips();
    $headquarters = new Headquarters;
    $allHeadquarters = $headquarters->findAll();
    $allOperators = $users->findAllOperators();
    $allClients = $users->findAllClients();
    $states = $packages->findStates();

    $stock = new Stock;


    if ($_SESSION['state'] != 1) {
        header('Location: login.php');
    }

    $dataUser = $users->findByEmail($_SESSION['email']);
    $date_in = date('2020-m-d');
    $date_out = date('Y-m-d h:m:s');
    $allTransp = $packages->findAllTransp();

    $digits = 5;
    $fecha = new DateTime();
    $dataFecha = $fecha->getTimestamp();
    $code = 'TE-PK'.substr($dataFecha, -5);


?>
        <html dir="ltr" lang="en">

        <head>
            <?php 

                include 'head.php';

            ?>

            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
            <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        </head>
        <body style="background:#e3e3e3ff;">
        <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
            data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
            <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
               }

            ?>
            <?php include 'sidebarca.php'; ?>

            <div class="page-wrapper" style="background:#e3e3e3ff; margin-bottom:40px;"><br>
            <div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                                <h4 style="color:#005574;">PAQUETES ENVIADOS</h4>
                            </div>
                        </div><br>

                        <div class="card">
                            <div class="card-body">
                               
                            <table class="table packages-table" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                            <thead>
                                <!-- <th style="border: 2px solid #e3e3e3ff;"></th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Nro Wharehouse</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Fecha de Salida</th>       
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Courier</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Tipo de Envío</th>
                                <!-- <th>Casillero</th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Nº Cajas</th>
                                <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Región</th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Departamento</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Fecha de Llegada</th>
                                <!-- <th>Almacen</th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Costo</th>
                            </thead>
                            <tbody>
                                <?php  

                                    foreach ($allWarehouses as $key => $value) {
                                        // echo $value;
                                ?>
                                        <tr>
                                            <!-- <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><input onclick="countPChecks();" type="checkbox" class="form-check-input pcheck pcheck<?php echo $value['id'];?>" id="pcheck<?php echo $value['id'];?>" package="<?php echo $value['id'];?>"></td> -->
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            
                                                <?php 
                                                    echo $value['code'];
                                                ?>
                                            </td>
                                            <!-- <td><?php echo $value['id']; ?></td> -->
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php 
                                                    echo $value['date_send'];
                                                    
                                                ?>
                                            </td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php 
                                            // $user_Currier = $allCurriers->findAllCurriers($value['id_currier'])['name'];
                                            // echo $allCurriers;
                                            foreach ($allCurriers as $key => $value2) {
                                                if($value['id_currier'] == $value2['id']){
                                                    echo $value2['name'];
                                                    break;
                                                }
                                            }
                                            ?>
                                            </td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            
                                            <?php 
                                                foreach ($allTrip as $key => $value4) {
                                                    if($value['ship_state'] == $value4['id']){
                                                        echo $value4['name'];
                                                        break;
                                                    }
                                                }
                                            ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            
                                            <?php 
                                                echo $value['box'];
                                            ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            
                                            <?php 
                                                // echo $value['box'];
                                                foreach ($allDeparments as $key => $value3) {
                                                    if($value['id_depart'] == $value3['id']){
                                                        echo $value3['nombre'];
                                                        break;
                                                    }
                                                }
                                            ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            
                                            <?php 
                                                echo $value['date_complete'];
                                                
                                            ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            
                                            <?php 
                                                echo $value['cost'];
                                                echo '$';
                                            ?>
                                        </td>
                                        </tr>
                                <?php
                                    }

                                ?>
                            </tbody>
                        </table>
                            </div>
                        </div>
              
                        

                            
                        
        </body>
        <!-- <footer class="footer text-center"> 2021 © Total Envios
        </footer> -->
        <?php 

        include 'foot.php';

        ?>