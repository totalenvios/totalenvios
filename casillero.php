<?php


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'includes/connection.class.php';
include 'includes/users.class.php';
include 'includes/packages.class.php';
include 'includes/headquarters.class.php';
include 'includes/stock.class.php';
include 'includes/transport.class.php';
session_start();

$users = new Users;
$allUsers = $users->findAll();
$packages = new Packages;
$allAlertsByUser = $packages->findAlertsByUser($users->findByEmail($_SESSION['email'])['id']);
$headquarters = new Headquarters;
$transports = new Transport;
$allHeadquarters = $headquarters->findAll();
$allOperators = $users->findAllOperators();
$allClients = $users->findAllClients();
$states = $packages->findStates();
$allTransp = $packages->findAllTransp();



$stock = new Stock;



if ($_SESSION['state'] != 1) {
    header('Location: login.php');
}

$userData = $users->findByEmail($_SESSION['email']);
$date_in = date('2020-m-d');
$date_out = date('Y-m-d');


?>
<html dir="ltr" lang="en">

<head>
    <?php

    include 'head.php';

    ?>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body style="background:#e3e3e3ff;">
    <input id="id_client_alert" type="hidden" value="<?php echo $_SESSION['id_user']; ?>">
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
        <?php

        include 'navbar.php';
        if ($_SESSION['state'] != 1) {
            header('Location: login.php');
        }

        ?>
        <?php include 'sidebarca.php'; ?>

        <div class="page-wrapper" style="background:#e3e3e3ff; margin-bottom:40px;"><br>
            <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="alertModalLabel" style="#005574;">Nueva prealerta</h5>
                            <button onclick="$('#alertModal').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-12 col-xlg-12 col-md-12">
                                        <div class="alert alert-danger error-warning" role="alert">
                                        </div>
                                        <div class="alert alert-success success-warning" role="alert">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label for="transp" style="color:#005574;">Empresa de transporte *</label>
                                                    <select id="transp" class="form-select">
                                                        <?php
                                                        foreach ($allTransp as $key => $value) {
                                                        ?>
                                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label for="cost" style="color:#005574;">Valor USD</label>
                                                    <input id="cost" type="number" placeholder="Costo" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group" id="client_form">
                                    <label for="id_client" style="color:#005574;">Cliente/Nro casillero</label>
                                        <input id="id_client" class="form-control" type="text" value="<?php echo $userData['name'] . ' ' . $userData['lastname']; ?>">
                                        <div class="form-check">
                                            <input  class="check_user" type="checkbox" value="" id="check_user_form">
                                            <label class="form-check-label" for="check_user_form">
                                            &nbsp;No soy cliente
                                            </label>
                                        </div>    
                                </div> -->

                                        <input id="client_id" class="form-control" type="text" value="<?php echo $users->findByEmail($_SESSION['email'])['id']; ?>" hidden>

                                        <div class="form-group" id="name_form">
                                            <label for="package_name" style="color:#005574;">Nombre del paquete</label>
                                            <input id="package_name" class="form-control" type="text" value="<?php echo $users->findByEmail($_SESSION['email'])['name'] . ' ' . $users->findByEmail($_SESSION['email'])['lastname'] ?>">
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label for="tracking" style="color:#005574;">Tracking</label>

                                                    <input id="tracking" type="text" placeholder="Tracking" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6" style="display: none;">
                                                <div class="form-group">
                                                    <label for="date_r" style="color:#005574;">Fecha de Registro *</label>
                                                    <div class="col-md-12 border-bottom p-0">
                                                        <div class="col-md-12 border-bottom p-0">
                                                            <input readonly="readonly" id="date_r" type="text" placeholder="Fecha de registro" class="form-control p-0 border-0" value="<?php echo date('Y-m-d h:m:s'); ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label for="date_in" style="color:#005574;">Fecha de llegada</label>

                                                    <input id="date_in" type="date" placeholder="Fecha de llegada" class="form-control">
                                                </div>

                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label for="content" style="color:#005574;">Contenido *</label>

                                            <textarea class="form-control" id="content" rows="3"></textarea>

                                        </div>
                                        <div class="form-group">


                                            <form enctype="multipart/form-data" id="alert-invoice-form" method="post" class="upload-form">
                                                <span style="color:#005574;" class="upload-span-screenshot">Adjunta una factura:</span>

                                                <input type="file" id="alert-invoice" name="alert-invoice" />
                                            </form>

                                        </div>

                                        <hr>

                                        <div align="right" class="form-group">
                                            <a onclick="addAlertforUser();" class="btn btn-primary" style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i style="color:white;" class="fas fa-plus"></i> Registrar</a>
                                            <!-- <div class="col-sm-12">
                                        <button onclick="addAlert();" class="btn btn-success">Registrar</button>
                                    </div> -->
                                        </div>
                                        <!-- </div>
                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div align="left" style="color:#fc3c3d;">
                    <h4><strong>MIS PREALERTAS</strong></h4>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <hr style="background-color:#005574;">
                </div>
            </div>

            <div class="row">

                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                    <input id="date_in_s" type="date" class="form-control" value="<?php echo $date_in; ?>">
                </div>
                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                    <input id="date_out" type="date" class="form-control" value="<?php echo $date_out; ?>">
                </div>
                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                    <button onclick="searchAlert();" class="btn btn-primary" style="border:2px solid #005574; color:#005574; background-color:white;"><i class="fas fa-check"></i> Filtrar</button>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3"></div>

                <div class="col-sm-3 col-md-3 col-lg-3" style="text-align:right; content-align:right;">
                    <div class="btn-group" align="right">
                        <a href="#" class="btn btn-primary" onclick="$('#alertModal').modal('show');" class="btn btn-primary" style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i style="color:white;" class="fas fa-plus"></i> Registrar</a>

                    </div>

                    <!-- <a align="right" href="registerWarehouse.php" class="btn" style="background-color:#ff554dff; color:white;"><i style="color:white;" class="fas fa-plus"></i> Crear</a> -->

                </div>
            </div><br>

            <div class="card">
                <div class="card-body">
                    <table class="table packages-table" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                        <thead>
                            <!-- <th>#</th> -->
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">FECHA DE LLEGADA</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">TRANSPORTISTA</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">TRACKING</th>
                            <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Fecha Registro</th> -->



                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">CONTENIDO</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">VALOR USD</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">FACTURA</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">CONSULTAR FACTURA</th>

                            <!-- <th>Ultimo Paquete</th> -->
                            <!-- <th>Accion</th> -->
                        </thead>
                        <tbody>
                            <?php

                            foreach ($allAlertsByUser as $key => $value) {
                            ?>
                                <tr>
                                    <!-- <td><?php echo $value['id']; ?></td> -->
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['date_in']; ?></td>
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $transports->findById($value['transp'])['name']; ?></td>
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['tracking']; ?></td>




                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['content']; ?></td>
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['cost']; ?> $</td>
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        <?php
                                        if ($value['invoice'] == null) {
                                            echo "No tiene factura";
                                        } else {
                                            //echo $value['invoice'];
                                            echo '<img src="data:image/jpeg;base64,' . base64_encode($value['invoice']) . '"/>';
                                        }

                                        ?>
                                    </td>

                                    <td><button type="button" class="btn btn-primary" style="background-color: white; color: #005574; border: 2px solid #005574; padding:5px; margin:0;" data-bs-toggle="modal" data-bs-target="#exampleModal2" onclick="consultarFact(<?php echo $value['id']; ?>)"><i class="fas fa-eye"></i> </button></td>
                                </tr>
                            <?php
                            }

                            ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body2">
                            <!-- <div style="height:100%; background-color:#005574;">
          <div class="row p-2">
               <div class="col-12">
                   <img width="80" src="assets/img/logo.png" alt="">
               </div>
           </div>                         
        </div>
        <div class="row p-2">
               <div class="col-12">
                    <h3 style="color:#ff554dff;">Tracking: <span style="color: black;">50gf5895</span></h3>
                    <h3 style="color:#ff554dff;">Nro de Factura: <span style="color: black;">44</span></h3>
                   <h3 style="color:#ff554dff;">Transporte: <span style="color: black;">DHL</span></h3>
                   <h3 style="color:#ff554dff;">Contenido: <span style="color: black;">videojuego</span></h3>
                    <h3 class="text-right" style="color:#ff554dff; text-decoration:underline;">Valor USD: <span style="color: black;">50$</span></h3>
               </div>
        </div>   -->
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" style="background-color: white; color: #005574; border: 2px solid #005574;">X</button>
                        </div>
                    </div>
                </div>
            </div>





</body>

<script>
    function consultarFact(id) {
        console.log(id);
        $('.modal-body2').load('./ajax/consultarFactura.php?id=' + id, function() {
            $('#myModal').modal({
                show: true
            });
        });

    }
</script>

<!-- <footer class="footer text-center"> 2021 © Total Envios
        </footer> -->
<?php

include 'foot.php';

?>