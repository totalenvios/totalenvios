
<?php  

    include 'includes/connection.class.php';
    include 'includes/warehouses.class.php';

    $warehouses = new Warehouses;
    $categories = $warehouses->findCategories();

    session_start();
    if ($_SESSION['state'] != 1) {
        header('Location: login.php');
    }
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

        include 'head.php';

    ?>
</head>

<body>
    <!-- Modal -->
    <div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="addCategoryLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="addCategoryLabel">Datos:</h5>
            <button onclick="$('#addCategory').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="alert alert-danger register-error" role="alert">
                        </div>
                        <div class="alert alert-success register-success" role="alert">
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Nombre *</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input id="name" type="text" placeholder="Nombre"
                                            class="form-control p-0 border-0"> </div>
                                </div>
                            </div>
                                <div class="form-group mb-4">
                                    <div class="col-sm-12">
                                        <button onclick="addCategory();" class="btn btn-success">Registrar</button>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-header-position="absolute" data-boxed-layout="full">
        <?php 

            include 'navbar.php'; 
            

        ?>
        
        <div class="page-wrapper">
            <div class="page-breadcrumb bg-white">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><a href="#" class="btn btn-primary" onclick="$('#addCategory').modal('show');" class="fw-normal">Nueva Categoria</a></h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <div class="d-md-flex">
                            <ol class="breadcrumb ms-auto">
                                <li><a href="logout.php" class="fw-normal">Logout</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <!-- <span class="input-group-text search-span" id="basic-addon1"><i class="fas fa-search"></i></span> -->
                      </div>
                    </div>
                    <table class="table users-table-a">
                        <thead>
                            <th>#</th>
                            <th>Nombre</th>
                        </thead>
                        <tbody class="users-body">
                            <?php  

                                foreach ($categories as $key => $value) {
                            ?>
                                    <tr>
                                        <td><?php echo $value['id']; ?></td>
                                        <td><?php echo $value['name']; ?></td>
                                    </tr>
                            <?php
                                }

                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="footer text-center"> 2021 © Total Envios
            </footer>
        </div>
        
    </div>
    <?php 

        include 'foot.php';

    ?>
</body>

</html>