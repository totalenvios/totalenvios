<?php
	require './vendor/autoload.php';

	include 'includes/connection.class.php';
	include 'includes/warehouses.class.php';
	include 'includes/packages.class.php';
	include 'includes/users.class.php';

	use Spipu\Html2Pdf\Html2Pdf;

	$id = $_REQUEST['id'];
	$warehouses = new Warehouses;
	$users = new Users;
	$warehouseData = $warehouses->findById($id);
	
	$userData = $users->findById($warehouseData['id_from']);
	
	$userDataTo = $users->findAddresseeById($warehouseData['id_to']);
	
	$tipo = $warehouses->findTripById($warehouseData['trip_id'])['name'];
	$totalBox = $warehouses->countBoxwarehouses($id);
	$total = $totalBox['total'];
	$boxes = $warehouses->findAllBoxesW($id);

	$addresseeData = $warehouses->listAddresseeDataToReceive($warehouseData['id_to']);
	$cityData = $warehouses->findcityDestiny($id)[0]['city_id'];
	$cityDestiny = $warehouses->findcityNameForwh($cityData)[0]['name'];
	$stateData = $warehouses->findstateName($addresseeData[0]['state_id']);
	

	// Variable para el conteo total de cajas para ese warehouse
	// $n = $total;
	$i=0;
	

		try {
			$width_in_mm = 104.3;
			$height_in_mm = 156.4;
	    
			$html2pdf = new Html2Pdf('p',array($width_in_mm,$height_in_mm),'en',true,'UTF-8',array(3,5,2,2));
			$html2pdf->setTestTdInOnePage(false);
			
			foreach ($boxes as $key => $value) {
				$n = $total;
				$i= $i+1;
		        $box = $warehouses->findBoxesById($value['id_box']);
		        $pc = number_format((($box['width']*$box['height']*$box['lenght'])/1728),2);
		        $lb = number_format((($box['width']*$box['height']*$box['lenght'])/166),2);

				$nombre = strtoupper($userData["name"]);
				$apellido = strtoupper($userData["lastname"]);

				$nombre_desti = strtoupper($userDataTo[0]["name"]);
				$apellido_destin = strtoupper($userDataTo[0]["lastname"]);

				$tipo_envio = strtoupper($tipo);

				$fechaComoEntero = strtotime($warehouseData["date_send"]);	
				$fecha = date("m/d/Y", $fechaComoEntero);
			$html2pdf->writeHTML('<div style="border-width: 1px; border-style: solid; border-color: black; width:330px; height:315px;">
			<table class="default" cellpadding="0" cellspacing="0">
			<tr>
				<td style="border-width: 1px; border-style: solid; border-color: black;"><div align="center" style="width:156px; height:60px;"><img style="margin-top:12px;" src="./logo-factura.png"></div></td>
				<td style="border-width: 1px; border-style: solid; border-color: black;"><div style="width:80px; height:60px; font-weight:bold;">FECHA:<br>'.$fecha.'</div></td>
				<td style="border-width: 1px; border-style: solid; border-color: black; background-color:black;"><div align="center" style="width:90px; height:60px;color:white; font-weight:bold; font-size:25px;"><label style="margin-top:10px;">'.$value['id_box'].'</label></div></td>
			</tr>
			</table>
			<table class="default" cellpadding="0" cellspacing="0">
			<tr>
				<td style="border-width: 1px; border-style: solid; border-color: black;"><div style="width:148px; height:49px; " >&nbsp;<label style="font-weight:bold;">ORIGEN:</label><br>&nbsp;'.$nombre.' '.$apellido.'</div></td>
				<td style="border-width: 1px; border-style: solid; border-color: black;"><div style="width:182px; height:49px; font-weight: bold; font-size:18px;">&nbsp;DESTINO:<br>&nbsp;'.$nombre_desti.' '.$apellido_destin.'</div></td>
			</tr>
			</table>
			<table class="default" cellpadding="0" cellspacing="0">
			<tr>
				<td style="border-width: 1px; border-style: solid; border-color: black;"><div style="width:231px; height:100px; " class="float"><Label style="font-weight:bold;">&nbsp;DIRECCIÓN:</label><br>&nbsp;'.$addresseeData[0]['address'].'</div></td>
				<td style="border-width: 1px; border-style: solid; border-color: black;"><div style="width:99px; height:100px; " class="float"><Label style="font-weight:bold;">&nbsp;TELÉFONOS:</label><br><br>&nbsp;'.$userData["phone"].'</div></td>
			</tr>
			</table>
			<table class="default" cellpadding="0" cellspacing="0">
			<tr>
				<td style="border-width: 1px; border-style: solid; border-color: white; background-color:black;"><div align="center" style="width:65px; height:32px; color:white;  font-weight: bold; font-size:14px;"  >PESO/L<br>B </div></td>
				<td style="border-width: 1px; border-style: solid; border-color: white; background-color:black;"><div align="center" style="width:119px; height:32px; color:white;  font-weight: bold; font-size:14px;"  >MEDIDAS</div></td>
				<td style="border-width: 1px; border-style: solid; border-color: white; background-color:black;"><div align="center" style="width:69px; height:32px; color:white;  font-weight: bold; font-size:14px;" >FT3</div></td>
				<td style="border-width: 1px; border-style: solid; border-color: white; background-color:black;"><div align="center" style="width:69px; height:32px; color:white;  font-weight: bold; font-size:14px;"  >VL/LB</div></td>
			</tr>
			</table>
			<table class="default" cellpadding="0" cellspacing="0">
			<tr>
				<td style="border-width: 1px; border-style: solid; border-color: black;"><div align="center" style="width:65px; height:50px; font-size:20px; font-weight: bold;"  class="float"><br>&nbsp;'.$box['weight'].'</div></td>
				<td style="border-width: 1px; border-style: solid; border-color: black;"><div align="center" style="width:119px; height:50px; font-size:20px; font-weight: bold;"  class="float"><br>&nbsp;'.$box['width'].' x '.$box['height'].' x '.$box['lenght'].'</div></td>
				<td style="border-width: 1px; border-style: solid; border-color: black;"><div align="center" style="width:69px; height:50px; font-size:20px; font-weight: bold;" class="float"><br>&nbsp;'.$pc.'</div></td>
				<td style="border-width: 1px; border-style: solid; border-color: black;"><div align="center" style="width:69px; height:50px; font-size:20px; font-weight: bold;"  class="float"><br>&nbsp;'.$lb.'</div></td>
			</tr>
			</table>

			</div>
		    <div style="margin-top: 5px; border-width:1px; border-style: solid; border-color: black; width:330px; height:100px;">
		            <table class="default" cellpadding="0" cellspacing="0">
		            <tr>
		                <td style="border-width: 1px; border-style: solid; border-color: black;"><div align="center" style="width:148px; height:38px; "><label style="font-weight: bold; font-size:28px;">'.$tipo_envio.'</label></div></td>
		                <td style="border-width: 1px; border-style: solid; border-color: black;"><div align="center" style="width:182px; height:38px;"><label style="font-weight: bold; font-size:22px;">'.$cityDestiny.'</label></div></td>
		            </tr>
		            </table>
		            <table class="default" cellpadding="0" cellspacing="0">
		            <tr>
		                <td style="border-width: 1px; border-style: solid; border-color: black; background-color:black;"><div align="center"style="width:231px; height:50px;"><label style="color:white; font-weight: bold; text-decoration: none; font-size:40px;">'.$warehouseData["code"].'</label></div></td>
		                <td style="border-width: 1px; border-style: solid; border-color: black;"><div align="center" style="width:99px; height:50px; font-weight: bold;"><a style=" color:black; text-decoration: none">BULTO</a><br><label style=" color:black; text-decoration: none; font-weight: bold; font-size:30px;">'.$i.'-'.$n.'</label></div></td>
		            </tr>
		            </table>
		            <table class="default" cellpadding="0" cellspacing="0">
		            <tr>
		                <td style=" border-style: solid; border-color: black;" ><div align="center" style="width:66px; height:70px;" class="float"> <img src="./qrL.png" /></div></td>
		                <td style="border-style: solid; border-color: black;" ><div style="width:254px; height:70px; margin-left:10px;" class="float"><br><a style="color:blue; text-decoration: underline; font-size:21px;">www.totalenvios.com</a><br><a style=" color:black; text-decoration: none;">¡Gracias por preferirnos!</a></div></td>
		            </tr>
		            </table>
		            </div>
		            ');
		    }
			$html2pdf->output();
		} catch (PDOException $e) {
			// echo $e;
		}
	
?>