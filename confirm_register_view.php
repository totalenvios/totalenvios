<?php  


include 'includes/connection.class.php';
include 'includes/headquarters.class.php';
include 'includes/users.class.php';


$users = new Users;


$id = $_REQUEST['id'];
//buscar usuario para ese id

$userById = $users->findById($id);

?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

        include 'head.php';
        

    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        #myMap {
           height: 350px;
           width: 680px;
        }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCnFc9MHexw438xRR2JF6yP044jDeZeF3U&sensor=false">
    </script>
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>

   
</head>
<body class="m-0 vh-100 row justify-content-center align-items-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

        <div class = "card text-center" style="border-color:white;">
            <div class="card-body text-center" style="border-color:white;">
                <img src="icons/logo-te.png" alt="" width="300" height="80">
                <div class="alert alert-danger register-error" role="alert">
                </div>
                <div class="alert alert-success register-success" role="alert">
                </div>
                <br><br>
                <h2 style="color:#005574;">TE ENVIAMOS UN CORREO ELECTRÓNICO</h2><br>  
                <h6 style="color:grey;">Se envió un correo electrónico a<strong> <?php echo $userById['email']; ?></strong></h6>
                <h6 style="color:grey;">Asegúrese de revisar su carpeta de correo no deseado</h6><br>
                <hr style="width:50%; margin: auto;">
                <br>
                <h4 style="color:grey;"><strong>¿Email incorrecto?</strong></h4>
                <h5 style="color:grey; text-decoration: underline;"> <a href="index.php" style="color:grey;">Regrese e ingrese nuevamente su correo electrónico</a></h5>
                <br>
                <h4 style="color:grey;"><strong>¿No recibiste el correo?</strong></h4>
                <h5 style="color:grey;"><button onclick="sendEmail(<?php echo $id; ?>);">Reenviar email</button></h5>
                <h5 style="color:grey;">Aun tienes problemas con el registro. <a href="contact.php" style="text-decoration: underline; color:grey;">contáctanos</a></h5>
            </div>
        </div>
  
      
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- Bootstrap tether Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="js/app-style-switcher.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
