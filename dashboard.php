
<?php  
    
    include 'includes/connection.class.php';
    include 'includes/users.class.php';
    include 'includes/packages.class.php';

    $users = new Users;
    $packages = new Packages;
    $allClients = $users->findAllClients();
    $countUsers = $users->countUsers()['cnt'];
    $countPackages = $packages->countPackages()['cnt'];
    $usersRegistered = $users->countClients()['cuantos'];
    $userForConfirmed = $users->countClientsForConfirmed()['forConfirmed'];
    $countpackageConfirmed = $packages->countConfirmed()['total_paquetes'];
    $countCloseWH = $packages->countCloseWarehouse()['warehouse'];
    $countOpenWH = $packages->countOpenWarehouse()['warehouse'];
    $countOceanWH = $packages->countOceanWarehouse()['warehouse'];
    $countAirWH = $packages->countAirWarehouse()['warehouse'];
    $countpackageAlert = $packages->countAlerts()['alerts'];
    session_start();
    if ($_SESSION['state'] != 1) {
        header('Location: login.php');
    }



?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

        include 'head.php';

    ?>
    
</head>

<body style="background:#e3e3e3ff;">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-navbarbg="skin5" 
         data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
        <?php 

            include 'navbar.php';

        ?>
       
        <div class="page-wrapper" style="background:#e3e3e3ff;">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
          
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" style="background:#e3e3e3ff !important;">

                
                    <div class="row">
                    <div class="col-lg-2 col-xlg-2 col-md-2">
                        <div class="card" style="border: solid 5px #005574; border-radius: 25px; height:160px;">
                            <div class="card-body">
                                <div class="row">
                                   
                                    
                                        <h5 style="color:#005574;"><strong>CLIENTES</strong></h5>
                                        <h1 style="color:#005574;"><?php echo $usersRegistered; ?></h1>
                                       
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-xlg-2 col-md-2">
                        <div class="card" style="border: solid 5px #ff554dff; border-radius: 25px; height:160px;">
                            <div class="card-body">
                                <div class="row">
                                    
                                    
                                        <h5 style="color:#005574;"><strong>CUMPLEAÑOS</strong></h5>
                                        <h1 style="color:#005574;"><?php echo $userForConfirmed; ?></h1>
                                      
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-2 col-xlg-2 col-md-2">
                        <div class="card" style="border: solid 5px #005574; border-radius: 25px; height:160px;">
                            <div class="card-body">
                                <div class="row">
                                     
                                    
                                        <h5 style="color:#005574;"><strong>PAQUETES ENVIADOS</strong></h5>
                                        <h1 style="color:#005574;"><?php echo $countpackageConfirmed; ?></h1>
                                      
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-2 col-xlg-2 col-md-2">
                        <div class="card" style="border: solid 5px #ff554dff; border-radius: 25px; height:160px;">
                            <div class="card-body">
                                <div class="row">
                                    
                                    
                                        <h5 style="color:#005574;"><strong>ENVIOS MARÍTIMOS</strong></h5>
                                        <h1 style="color:#005574;"><?php echo $countOceanWH; ?></h1>
                                       
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-2 col-xlg-2 col-md-2">
                        <div class="card" style="border: solid 5px #005574; border-radius: 25px; height:160px;">
                            <div class="card-body">
                                <div class="row">
                                     
                                   
                                        <h5 style="color:#005574;"><strong>ENVÍOS AÉREOS</strong></h5>
                                        <h1 style="color:#005574;"><?php echo $countAirWH; ?></h1>
                                       
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-2 col-xlg-2 col-md-2">
                        <div class="card" style="border: solid 5px #ff554dff; border-radius: 25px; height:160px;">
                            <div class="card-body">
                                <div class="row">
                                     
                                    
                                        <h5 style="color:#005574;"><strong>PREALERTAS</strong></h5>
                                        <h1 style="color:#005574;"><?php echo $countpackageAlert; ?></h1>
                                       
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                

               
                


                
                <!-- ============================================================== -->
                <!-- PRODUCTS YEARLY SALES -->
                <!-- ============================================================== -->
                <div class="row" style="border-radius: 25px !important;">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="white-box" style="border-radius: 25px !important;">
                            <h3 class="box-title">ENVIOS POR ESTADOS DE VENEZUELA</h3>
                            <div class="d-md-flex">
                                <ul class="list-inline d-flex ms-auto">
                                    <li class="ps-3">
                                        <h5><i class="fa fa-circle me-1 text-info"></i>AÉREO</h5>
                                    </li>
                                    <li class="ps-3">
                                        <h5><i class="fa fa-circle me-1 text-inverse"></i>MARÍTIMO</h5>
                                    </li>
                                </ul>
                            </div>
                            <div id="ct-visits" style="height: 405px;">
                                <div class="chartist-tooltip" style="top: -17px; left: -12px;"><span
                                        class="chartist-tooltip-value">6</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            
                <!-- ============================================================== -->
                <!-- RECENT SALES -->
                <!-- ============================================================== -->
                <div class="row" style="border-radius: 25px !important;">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="white-box" style="border-radius: 25px !important;">
                            <div class="d-md-flex mb-3">
                                <h3 class="box-title mb-0">TOP 20 - CLIENTES</h3>
                                <div class="col-md-3 col-sm-4 col-xs-6 ms-auto">
                                    <select class="form-select shadow-none row border-top">
                                        <option>March 2021</option>
                                        <option>April 2021</option>
                                        <option>May 2021</option>
                                        <option>June 2021</option>
                                        <option>July 2021</option>
                                    </select>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <strong><table class="table text-center" style="font-weight:600 !important; table-layout: fixed; text-layout:fixed;">
                                    <thead>
                                        <tr>
                                            <th class="border-top-0" style="border: solid 1px white; background:#ff554dff; color:white;">CASILLERO</th>
                                            <th class="border-top-0" style="border: solid 1px white; background:#ff554dff; color:white;">NOMBRE</th>
                                            <th class="border-top-0" style="border: solid 1px white; background:#ff554dff; color:white;">TELÉFONO</th>
                                            <th class="border-top-0" style="border: solid 1px white; background:#ff554dff; color:white;">IDENTIFICACIÓN</th>
                                            <th class="border-top-0" style="border: solid 1px white; background:#ff554dff; color:white;">PAÍS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php  

                                                    foreach ($allClients as $key => $value) {
                                                    ?>
                                                    <tr>

                                                    
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['code']; ?></td>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['name']; ?></td>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['phone']; ?></td>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['document']; ?></td>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['country']; ?></td>
                                                    </tr>
                                                    <?php 



                                                    }

                                        ?>
                                    </tbody>
                                </table></strong>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Recent Comments -->
                <!-- ============================================================== -->
                
                       
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center"> 2021 © Total Envios
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php 

        include 'foot.php';

    ?>
</body>

</html>