<?php


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'includes/connection.class.php';
include 'includes/users.class.php';
include 'includes/packages.class.php';
include 'includes/headquarters.class.php';
include 'includes/stock.class.php';
include 'includes/transport.class.php';
include 'includes/destinatarios.php';
session_start();

$users = new Users;
$destin = new Destinatarios;

$allCountries = $users->findAddressee();
$allAddressStates = $users->findAddressStates();
$allAddressCities = $users->findAddressCities();

$user_id = $users->findByEmail($_SESSION['email'])['id'];
// echo $user_id;

$allDestinUser = $destin->findDestinById($user_id);


$stock = new Stock;



if ($_SESSION['state'] != 1) {
    header('Location: login.php');
}

$userData = $users->findByEmail($_SESSION['email']);
$date_in = date('2020-m-d');
$date_out = date('Y-m-d');


?>
<html dir="ltr" lang="en">

<head>
    <?php

    include 'head.php';

    ?>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        #myMap {
            height: 350px;
            width: 680px;
        }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCnFc9MHexw438xRR2JF6yP044jDeZeF3U&sensor=false">
    </script>
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>

    <style>
        label,
        ::placeholder {
            font-size: 11px;
        }
    </style>
</head>

<body style="background:#e3e3e3ff;">
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
        <?php

        include 'navbar.php';
        if ($_SESSION['state'] != 1) {
            header('Location: login.php');
        }

        ?>
        <?php include 'sidebarca.php'; ?>

        <div class="page-wrapper" style="background:#e3e3e3ff; margin-bottom:40px;"><br>

            <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="alertModalLabel" style="color:#005574;">Nuevo Destinatario</h5>
                            <button onclick="$('#alertModal').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-12 col-xlg-12 col-md-12">
                                        <div class="alert alert-danger error-warning" role="alert"></div>
                                        <div class="alert alert-success success-warning" role="alert"></div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <input id="user_id" type="hidden" value="<?php echo $user_id ?>">
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="transp" style="color:#005574;">Nombre *</label>
                                                            <input type="text" id="name" class="form-control" placeholder="Nombre y Apellido" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="transp" style="color:#005574;">Apellido *</label>
                                                            <input type="text" id="apellido" class="form-control" placeholder="Apellido" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="alias" style="color:#005574;">Alias *</label>
                                                            <input id="alias" type="text" placeholder="Alias" class="form-control" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="cedula" style="color:#005574;">Cèdula o Número de Identificación *</label>
                                                            <input id="cedula" type="text" placeholder="Cédula o número de identificación" class="form-control" required>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group" id="name_form">
                                                    <label for="correo" style="color:#005574;">Correo Electrónico *</label>
                                                    <input id="correo" class="form-control" type="email" placeholder="Correo Electrónico" required>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group" id="name_form">
                                                    <label for="direccion" style="color:#005574;">Dirección de Entrega *</label>
                                                    <textarea class="form-control" name="" id="direccion" placeholder="direccion de entrega"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group" id="name_form">
                                                    <label for="telefono" style="color:#005574;">Teléfono de Contacto *</label>
                                                    <input id="telefono" class="form-control" type="text" placeholder="Teléfono de Contacto" required>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group" id="name_form">
                                                    <label for="pais" style="color:#005574;">País de Entrega *</label>
                                                    <input id="pais" class="form-control" type="text" placeholder="País de Entrega" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group" id="name_form">
                                                    <label for="estado" style="color:#005574;">Estado de Entrega *</label>
                                                    <input id="estado" class="form-control" type="text" placeholder="Estado de Entrega" required>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group" id="name_form">
                                                    <label for="ciudad" style="color:#005574;">Ciudad de Entrega *</label>
                                                    <input id="ciudad" class="form-control" type="text" placeholder="Ciudad de Entrega" required>
                                                </div>
                                            </div>
                                        </div>


                                        <div align="center" class="form-group">
                                            <a onclick="addDestinforUser();" class="btn btn-primary" style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i style="color:white;" class="fas fa-plus"></i> Registrar</a>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div align="left" style="color:#fc3c3d;">
                    <h4><strong>DESTINATARIOS</strong></h4>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <hr style="background-color:#005574;">
                </div>
            </div>

            <div class="row">

                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2"></div>
                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2"></div>
                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2"></div>
                <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3"></div>

                <div class="col-sm-3 col-md-3 col-lg-3" style="text-align:right; content-align:right;">
                    <div class="btn-group" align="right">
                        <a href="./register-destiner.php" class="btn btn-primary" style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;">
                            <i style="color:white;" class="fas fa-plus"></i> Registrar
                        </a>
                    </div>
                </div>
            </div><br>

            <div class="card">
                <div class="card-body">
                    <table class="table packages-table" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                        <thead>
                            <!-- <th>#</th> -->
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Nombre y Apellido</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Alias</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Cédula o Num de Identificacin</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Correo</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Teléfono</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">País</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Estado</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Ciudad</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Dirección</th>
                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Acción</th>
                        </thead>
                        <tbody>
                            <?php

                            foreach ($allDestinUser as $key => $value) {
                            ?>
                                <tr>
                                    <td id="nameDestin" style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['name']; ?> <?php echo $value['lastname']; ?></td>
                                    <td id="aliasDestin" style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['alias']; ?> <button type="button" onclick="showEditAlias('<?php echo $value['id']; ?>', '<?php echo $value['alias']; ?>')" class="btn btn-primary" style="background-color: white; color: #005574; border: 2px solid #005574; padding:5px; margin:0;"><i class="fas fa-edit"></i></button></td>
                                    <td id="cedulaDestin" style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['cedula']; ?></td>
                                    <td id="correoDestin" style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['correo']; ?></td>
                                    <td id="telefonoDestin" style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['contact_phone']; ?></td>
                                    <td id="paisDestin" style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['country_name']; ?></td>
                                    <td id="estadoDestin" style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['state_name']; ?></td>
                                    <td id="ciudadDestin" style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['city_name']; ?></td>
                                    <td id="direccionDestin" style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['address']; ?></td>
                                    <td>
                                        <button type="button" class="btn btn-primary" style="background-color: white; color: #005574; border: 2px solid #005574; padding:5px; margin:0;" onclick="showeditDestin(
                                            '<?php echo $value['id']; ?>','<?php echo $value['name']; ?>', 
                                            '<?php echo $value['lastname']; ?>','<?php echo $value['alias']; ?>',
                                            '<?php echo $value['cedula']; ?>','<?php echo $value['correo']; ?>',
                                            '<?php echo $value['contact_phone']; ?>','<?php echo $value['country_name']; ?>',
                                            '<?php echo $value['state_name']; ?>','<?php echo $value['city_name']; ?>',
                                            '<?php echo $value['address']; ?>')"><i class="fas fa-edit"></i> </button>
                                        <button type="button" class="btn btn-danger" style="background-color: white; color: #005574; border: 2px solid #005574; padding:5px; margin:0;" onclick="deleteDestiner('<?php echo $value['id']; ?>')"><i class="fas fa-trash"></i> </button>
                                    </td>
                                </tr>
                            <?php
                            }

                            ?>
                        </tbody>
                    </table>
                </div>
            </div>



            <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="alertModalLabel" style="color:#005574;">Editar Destinatario</h5>
                            <button onclick="$('#exampleModal2').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="card-registro-usuarios-wh">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                        <input id="id" type="hidden">
                                        <input id="user_id" type="hidden" value="<?php echo $user_id ?>">
                                        <input id="countryVenezuela" type="hidden" value="0">
                                    </div>
                                    <div class="col-lg-12 col-xlg-12 col-md-12">
                                        <div class="alert alert-danger error-warning" role="alert"></div>
                                        <div class="alert alert-success success-warning" role="alert"></div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="transp" style="color:#005574;">Nombre</label>
                                                            <input type="text" id="nameDestinatario" class="form-control" placeholder="Nombre">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="transp" style="color:#005574;">Apellido</label>
                                                            <input type="text" id="apeDestinatario" class="form-control" placeholder="Apellido">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="transp" style="color:#005574;">Alias</label>
                                                            <input type="text" id="aliDestinatario" class="form-control" placeholder="Alias" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="cedula" style="color:#005574;">Cèdula o Número de Identificación</label>
                                                            <input id="cedDestinatario" type="text" placeholder="Cédula o número de identificación" class="form-control" required>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group" id="name_form">
                                            <label for="correo" style="color:#005574;">Correo Electrónico</label>
                                            <input id="corDestinatario" class="form-control" type="email" placeholder="Correo Electrónico" required>
                                        </div>

                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group" id="name_form">
                                                    <label for="telefono" style="color:#005574;">Teléfono de Contacto</label>
                                                    <input id="telDestinatario" class="form-control" type="text" placeholder="Teléfono de Contacto" required>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group" id="name_form">
                                                    <label for="address_destiny_destiner_dos">País destino</label>
                                                    <select id="address_destiny_destiner_dos" class="form-select campos-input-login">
                                                        <option value="0">¿Cuál es destino?</option>
                                                        <?php
                                                        foreach ($allCountries as $key => $value) {
                                                        ?>
                                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-6 address_state_destiner_dos">
                                                <div class="form-group">
                                                    <label for="address_state_destiner_dos">Estado</label>
                                                    <input id="address_state_destiner_dos" type="text" placeholder="Estado" class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-6  select_state_destiner_dos" style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_state_destiner_dos">Estado</label>
                                                    <select id="select_state_destiner_dos" class="form-select campos-input-login">
                                                        <option value="0">¿Cuál estado?</option>
                                                        <?php
                                                        foreach ($allAddressStates as $key => $value) {
                                                        ?>
                                                            <option value="<?php echo $value['id_estado']; ?>"><?php echo $value['estado']; ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-6 address_city_destiner_dos">
                                                <div class="form-group">
                                                    <label for="address_city_destiner_dos">Ciudad</label>
                                                    <input id="address_city_destiner_dos" type="text" placeholder="Ciudad" class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-6 select_city_destiner_dos" style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_city_destiner_dos">Ciudad</label>
                                                    <select id="select_city_destiner_dos" class="form-select campos-input-login">
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group" id="name_form">
                                                    <label for="direccion" style="color:#005574;">Dirección de Entrega</label>
                                                    <textarea class="form-control" name="" id="dirDestinatario" placeholder="direccion de entrega"></textarea>
                                                </div>
                                            </div>
                                        </div>


                                        <div align="center" class="form-group">
                                            <a onclick="ediDestin();" class="btn btn-warning" style="background-color: white; color: #005574; border: 2px solid #005574; padding:5px; margin:0;"><i class="fas fa-edit"></i> Editar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <!-- Modal editar alias -->
            <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Editar Alias</h5>
                            <button type="button" class="close" onclick="$('#exampleModal3 ').modal('hide');" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <input class="form-control" type="hidden" id="aliasId">
                                <input class="form-control" type="text" id="aliasD">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button"  class="btn btn-danger mr-2" onclick="$('#exampleModal3').modal('hide');"  data-dismiss="modal" style="background-color: white; color: #005574; border: 2px solid #005574; padding:5px; margin:0;">Cancelar</button>
                            <button type="button"  class="btn btn-success" onclick="editAlias()" style="background-color: white; color: #005574; border: 2px solid #005574; padding:5px; margin:0;">Guardar Cambios</button>
                        </div>
                    </div>
                </div>
            </div>
</body>

<script src="bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="js/app-style-switcher.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!--Menu sidebar -->
<script src="js/sidebarmenu.js"></script>
<!--Custom JavaScript -->
<script src="js/custom.js"></script>
<script src="js/main.js"></script>
<!-- <footer class="footer text-center"> 2021 © Total Envios
        </footer> -->
<?php

include 'foot.php';

?>