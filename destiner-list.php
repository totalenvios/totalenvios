<?php  
    
    include 'includes/connection.class.php';
    include 'includes/headquarters.class.php';
    include 'includes/users.class.php';
    
    
    $users = new Users;
    $headquarters = new Headquarters;
    $id = $_REQUEST['id'];
    //buscar usuario para ese id
    
    $userById = $users->findById($id);
    $allDestiner = $users->findAddresseesById($id);

    $allcountriesRegister = $users->findCountryForRegister();
    $allCountries = $users->findAddressee();
    $allAddressStates = $users->findAddressStates();
    $allAddressCities = $users->findCities();
    $allHeadQ = $headquarters->findAll();
    $userTypes = $users->findRegisterTypes();
    $digits = 5;
    $fecha = new DateTime();
    $dataFecha = $fecha->getTimestamp();
    $code = 'TE -'.substr($dataFecha, -5);


?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

        include 'head.php';
        

    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        #myMap {
           height: 350px;
           width: 680px;
        }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCnFc9MHexw438xRR2JF6yP044jDeZeF3U&sensor=false">
    </script>
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>

   
</head>
<body style="background:#e3e3e3ff;">
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper"  data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
        <?php 

            include 'navbar.php'; 
            

        ?>
        <div class="page-wrapper" style="background:#e3e3e3ff;">
        <div class="alert alert-danger register-error" role="alert">
        </div>
        <div class="alert alert-success register-success" role="alert">
        </div>

        <br><h3 style="color:grey;"> <strong> <?php echo$userById['code'].'-'.$userById['name'].' '.$userById['lastname'];?></strong></h3><br>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <h4>Registra un destinatario</h4>
                <input id="countryVenezuela" type="hidden" value="0">
                <input id="user_id" type="hidden" value="<?php echo $userById['id'];?>">
                <input id="destiner_id"  type="hidden">
                    <div class="col-lg-4 col-xlg-4 col-md-4">
                        <div class="form-group">
                            <label for="name_destiner">Nombre *</label>
                                                    
                            <input id="name_destiner" type="text" placeholder="Nombre Completo"
                                class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-xlg-4 col-md-4">
                    <div class="form-group">
                                                    <label for="lastname_destiner">Apellido *</label>
                                                    
                                                        <input id="lastname_destiner" type="text" placeholder="Apellido Completo"
                                                            class="form-control"> 
                                                </div>
                    </div>  
                    <div class="col-lg-4 col-xlg-4 col-md-4">
                    <div class="form-group">
                                                    <label for="contact_phone_destiner">Telefono de contacto</label>
                                                    
                                                        <input type="text" placeholder="123 456 7890"
                                                            class="form-control"
                                                            id="contact_phone_destiner">
                                                    
                                                </div>
                    </div>     
                </div>
                <div class="row">
                    <div class="col-lg-2 col-xlg-2 col-md-2">
                    <div class="form-group">
                                                                            <label for="address_destiny_destiner_dos">Pais destino</label>
                                                                                <select id="address_destiny_destiner_for_client" class="form-select campos-input-login">
                                                                                    <option value="0">¿Cuál es destino?</option>
                                                                                    <?php  
                                                                                        foreach ($allCountries as $key => $value) {
                                                                                    ?>
                                                                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                                                    <?php
                                                                                        }
                                                                                    ?>
                                                                                </select>
                                                                                </div>
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2 address_state_destiner_for_client">
                                                                                <div class="form-group">
                                                                                <label for="address_state_destiner_for_client">Estado</label>
                                                                                    <input id="address_state_destiner_dos" type="text" placeholder="Estado" class="form-control campos-input-login">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2 select_state_destiner_for_client" style="display: none;">
                                                                                <div class="form-group">
                                                                                <label for="select_state_destiner_for_client">Estado</label>
                                                                                    <select id="select_state_destiner_for_client" class="form-select campos-input-login">
                                                                                    <option value="0">¿Cuál estado?</option>
                                                                                    <?php  
                                                                                        foreach ($allAddressStates as $key => $value) {
                                                                                    ?>
                                                                                            <option value="<?php echo $value['id_estado']; ?>"><?php echo $value['estado']; ?></option>
                                                                                    <?php
                                                                                        }
                                                                                    ?>
                                                                                    </select>
                                                                                </div>    
                                                                            </div>
                   
                    <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2 address_city_destiner_for_client">
                                                                                <div class="form-group">
                                                                                <label for="address_city_destiner_for_client">Ciudad</label>
                                                                                    <input id="address_city_destiner_for_client" type="text" placeholder="Ciudad" class="form-control campos-input-login">
                                                                                </div>    
                                                                            </div>
                                                                            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2 select_city_destiner_for_client" style="display: none;">
                                                                                <div class="form-group">
                                                                                <label for="select_city_destiner_for_client">Ciudad</label>
                                                                                    <select id="select_city_destiner_for_client" class="form-select campos-input-login">
                                                                                    <option value="0">¿Cuál ciudad?</option>
                                                                                    <?php  
                                                                                        foreach ($allAddressCities as $key => $value) {
                                                                                    ?>
                                                                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                                                    <?php
                                                                                        }
                                                                                    ?>
                                                                                    </select>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4 address_address_destiner_for_client">
                                                                                    <div class="form-group">
                                                                                    <label for="address_Destiner_for_client">Dirección</label>
                                                                                        <input id="address_destiner_for_client" type="text" placeholder="Dirección" class="form-control campos-input-login">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4 select_address_destiner_for_client" style="display: none;">
                                                                                    <div class="form-group">
                                                                                    <label for="select_address_destiner_for_client">Dirección</label>
                                                                                        <input id="select_address_destiner_for_client" type="text" placeholder="Dirección" class="form-control campos-input-login">
                                                                                    </div>
                                                                                </div>
                    <div class="col-lg-2 col-xlg-2 col-md-2">
                        <button id="button-register-destiner-for-client" onclick="registerDestinerForClient();" class="btn btn-primary" style="margin-top:30px; background:#fc3c3d; color:white;"><strong>REGISTRAR</strong></button>
                        <button id="button-edit-destiner-for-client" onclick="editDestinerForClient();" class="btn btn-primary" style="margin-top:30px; background:#fc3c3d; color:white; display:none;"><strong>EDITAR</strong></button>
                    </div>      
                </div>

            </div>
        </div>
        <hr>

       
            
                        <div class="row"> 
                            

            <?php  

                foreach ($allDestiner as $key => $value) {
                ?>

                   
                            <div class="col-lg-4 col-xlg-4 col-md-4">
                                <div class="card">
                                    <div class="card-body">
               
                                <h2 style="color:#005574;"> <strong> <?php echo $value['name'].' '.$value['lastname'];?> </strong></h2>
                                <hr>
                                <h4 style="color:#005574;"><i class="fas fa-map-marker-alt"></i> <?php echo $value['country_name'].', '.$value['state_name'].', '.$value['city_name'];?></h4> 
                                <h5 style="color:#005574;"><i class="fas fa-address-book"></i> <?php echo $value['address'];?> </h5>
                                <h5 style="color:#005574;"><i class="fas fa-phone-square-alt"></i> <?php echo $value['contact_phone'];?></h5><br>
                                <div align="right">
                                    <a class="button" style="color:#005574;" type="button" onclick="deleteDestiner(<?php echo $value['id'];?>);">Eliminar</a>&nbsp;&nbsp;
                                    <a class="button" style="color:#005574;" type="button" onclick="editarDestiner(<?php echo $value['id'];?>);">Editar</a>
                                </div>
                                </div> 
                            </div>
                       
                       </div>
                <?php  
                }
            ?>
           
                </div>
                        
                                            
        </div>
    </div>
    <script src="bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="js/app-style-switcher.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.js"></script>
    <script src="js/main.js"></script>
</body>

</html>