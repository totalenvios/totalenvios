<?php  


    include 'includes/connection.class.php';
    include 'includes/headquarters.class.php';
    include 'includes/users.class.php';
    
    
    $users = new Users;
    $headquarters = new Headquarters;
    
    
    $id = $_REQUEST['id'];
    //buscar usuario para ese id
    
    $userById = $users->findById($id);

    $userById = $users->findById($id);
    $allDestiner = $users->findAddresseesById($id);

    $allcountriesRegister = $users->findCountryForRegister();
    $allCountries = $users->findAddressee();
    $allAddressStates = $users->findAddressStates();
    $allAddressCities = $users->findCities();
    $allHeadQ = $headquarters->findAll();
    $userTypes = $users->findRegisterTypes();

   
    $digits = 5;
    $fecha = new DateTime();
    $dataFecha = $fecha->getTimestamp();
    $code = 'TE -'.substr($dataFecha, -5);

    if($userById['country_id'] == 1){
        //El pais de registro es venezuela
        $value_venezuela = 1;
        $cityForState = $users->cityForSate($userById['state_id']);
    }else{
        $value_venezuela = 0;
    }

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

        include 'head.php';
        

    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        #myMap {
           height: 350px;
           width: 680px;
        }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCnFc9MHexw438xRR2JF6yP044jDeZeF3U&sensor=false">
    </script>
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>

   
</head>
<body style="background:#e3e3e3ff;">
<div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper"  data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
        <?php 

            include 'navbar.php'; 
            

        ?>
        <div class="page-wrapper" style="background:#e3e3e3ff;">

    <!-- Modal de confirmación de registro************************************************ -->

   
                                    
    
        <input id="countryVenezuela_edit"  type="hidden" value="<?php echo $value_venezuela; ?>">
        <input id="user_id_client" type="hidden" value="<?php 

echo $id;


?>">
            <div class="card">
                <div class="card-body" style="padding:20px;">
                <div class="row" style="padding:50px;">
                <div class="col-lg-12 col-xlg-12 col-md-12">
                    
                    
                        <!-- <div class="encabezado"><img id="imag" src="./logo-home-te.png"/></div> -->
            
                        <div class="alert alert-danger register-error" role="alert">
                        </div>
                        <div class="alert alert-success register-success" role="alert">
                        </div>
                        <div>
                        <div class="row">
                            <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1">
                                <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                            </div>
                            <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                <h2 style="color:#005574;"><strong>EDICIÓN DE DATOS DEL CLIENTE</strong></h2>
                            </div>
                        </div><br><br>

                            <div> 
                           
                                
                            <div class="row">
                                <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                    <div class="form-group">
                                    <!-- <label for="name" style="color:#005574;"><strong>Nombre completo</strong></label> -->
                                        <input id="name" type="text" placeholder="Nombre Completo" value="<?php echo $userById['name']; ?>"
                                        class="campos-input-login" style="background: url('icons/name.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;"> </div>
                                </div>
                                <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
            
                                        <div class="form-group">
                                        <!-- <label for="lastname"style="color:#005574;"><strong>Apellido completo</strong></label> -->
                                            <input id="lastname" type="text" placeholder="Apellido Completo" value="<?php echo $userById['lastname']; ?>"
                                                class="campos-input-login" style="background: url('icons/name.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;"> </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                        
                                            <div class="form-group">
                                            <!-- <label for="document"style="color:#005574;"><strong>Identificación</strong></label> -->
                                                <input id="document" type="text" placeholder="# Identificación" value="<?php echo $userById['document']; ?>" class="campos-input-login" style="background: url('icons/identification.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;"> 
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                    
                                    <div class="form-group">
                                    <!-- <label for="phone"style="color:#005574;"><strong>Teléfono celular</strong></label> -->
                                        <input id="phone" type="text" placeholder="Telf. celular" value="<?php echo $userById['phone']; ?>" class="campos-input-login" style="background: url('icons/phone.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>
                                
                            </div>
                            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                
                                    <div class="form-group">
                                    <!-- <label for="phone_house"style="color:#005574;"><strong>Teléfono de casa</strong></label> -->
                                        <input id="phone_house" type="text" placeholder="Telf. Fijo" value="<?php echo $userById['phone_house']; ?>" class="campos-input-login" style="background: url('icons/phone-house.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>
                                
                            </div> 
                                                </div>  
                                    
                                    
                                
                                <div class="row">
                                    <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                            <div class="form-group">
                                            <!-- <label for="email"style="color:#005574;"><strong>Correo electrónico</strong></label> -->
                                                <input type="email" placeholder="Correo electrónico" value="<?php echo $userById['email']; ?>"
                                                    class="campos-input-login" name="example-email"
                                                    id="email" style="background: url('icons/email-blue.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                            <div class="form-group">
                                            <!-- <label for="email"style="color:#005574;"><strong>Confirma tu correo electrónico</strong></label> -->
                                                <input type="email" placeholder="confirmación" value="<?php echo $userById['email']; ?>"
                                                    class="campos-input-login has-error" name="example-email"
                                                    id="confirm-email" style="background: url('icons/email-blue.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                                
                                            </div>
                                        </div>
                                        
                                    <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                        
                                        <div class="form-group">
                                        <!-- <label for="born_date"style="color:#005574;"><strong>Fecha de nacimiento</strong></label> -->
                                            <input type="date" placeholder="1990-01-01" value="<?php echo $userById['born_date']; ?>"
                                                class="campos-input-login"
                                                id="born_date" style="background: url('icons/cumpleaños.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                        </div>
                                    </div>
                            </div>
                        
                                
                            
                            <!-- <hr> -->
                            <div class="row">
                                
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <div class="form-group">
                                <!-- <label for="address_destiny"style="color:#005574;"><strong>Pais</strong></label> -->
                                    <select id="address_destiny" class="campos-input-login country_edit_change" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                                 <option value="<?php echo $userById['country_id']; ?>"><?php echo $userById['country']; ?></option>
                                        
                                        <?php  
                                            foreach ($allcountriesRegister as $key => $value) {
                                                if($value['id'] == $userById['country_id']){
                                                    $key = $key+1;
                                                }else{
                                        ?>      
                                               
                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                        <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                    </div>
                                </div>

                                
                                    <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 select_state">
                                        <div class="form-group">
                                        <!-- <label for="select_state"style="color:#005574;"><strong>Estado</strong></label> -->
                                            <select id="select_state" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                    background-position: 15px center;
                                                    background-size: 20px;
                                                    display: flex;
                                                    align-items: center; padding-left: 45px;">
                                            <option value="<?php echo $userById['state_id']; ?>"><?php echo $userById['state_name']; ?></option>
                                            <?php  
                                                foreach ($allAddressStates as $key => $value) {
                                                    if($value['id'] == $userById['state_id']){
                                                        $key = $key+1;
                                                    }else{
                                            ?>
                                                    <option value="<?php echo $value['id_estado']; ?>"><?php echo $value['estado']; ?></option>
                                            <?php
                                             }
                                                }
                                            ?>
                                            </select>
                                        </div>    
                                    </div>

                                   

                                    <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 select_city">
                                    <div class="form-group">
                                    <!-- <label for="select_city"style="color:#005574;"><strong>Ciudad</strong></label> -->
                                        <select id="select_city" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                                
                                                <option value="<?php echo $userById['city_id']; ?>"><?php echo $userById['city_name']; ?></option>
                                            <?php  
                                                foreach ($cityForState as $key => $value) {
                                                    if($value['id'] == $userById['city_id']){
                                                        $key = $key+1;
                                                    }else{
                                            ?>
                                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                            <?php
                                             }
                                                }
                                            ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 select_address">
                                        <div class="form-group">
                                        <!-- <label for="select_address"style="color:#005574;"><strong>Dirección</strong></label> -->
                                            <input id="select_address" type="text" placeholder="Dirección" value="<?php echo $userById['address']; ?>" class="campos-input-login" style="background: url('icons/direccion.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                        </div>
                                    </div>
                                
                                    
                                    
                                     <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 address_state">
                                    <div class="form-group">
                                    <!-- <label for="address_state"style="color:#005574;"><strong>Estado</strong></label> -->
                                        <input id="address_state" type="text" placeholder="Estado" value="<?php echo $userById['state_name']; ?>" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>
                                </div>
                               
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 address_city">
                                    <div class="form-group">
                                    <!-- <label for="address_city"style="color:#005574;"><strong>Ciudad</strong></label> -->
                                        <input id="address_city" type="text" placeholder="Ciudad" value="<?php echo $userById['city_name']; ?>" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>    
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 address_address">
                                        <div class="form-group">
                                        <!-- <label for="address"style="color:#005574;"><strong>Dirección</strong></label> -->
                                            <input id="address" type="text" placeholder="Dirección" value="<?php echo $userById['address']; ?>" class="campos-input-login" style="background: url('icons/direccion.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                        </div>
                                    </div>

                                    
                                    

                              

                                   
                               
                               </div>
                                <!-- </div> -->
                                <!-- <div class="row"> -->
                                   
                                   

                            <!-- <hr> -->
                            
                           
                                
                            <!-- <hr> -->
                        
                            <!-- <div id="myMap"></div> <br> -->
                            
                            
                           
                            <hr>
                            
                            <div class="form-group mb-4 text-center">
                                <div class="col-sm-12">
                                    <button onclick="editClientForMod();" class="boton-total-envios-registro"><strong>EDITAR</strong></button>
                                </div>
                                
                            </div>
                            </div>

                        </div>
                    </div>

                   
                                                                        <!-- ******************************************************************************** -->
                                                                  
                                  
            
                                                      </div>
                                                </div>
                                                        
                                                    <!-- </div>
                                                </div>
                                            </div>
                                                                                   
                                            </div> -->
            <!-- <div class="col-sm-12 col-md-4 col-lg-2 col-xl-2"></div>
        </div>    -->
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <!-- <footer class="footer text-center"> 2021 © Total Envios <a
                    href="https://www.wrappixel.com/"></a>
            </footer> -->
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- Bootstrap tether Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="js/app-style-switcher.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.js"></script>
    <script src="js/main.js"></script>
</body>

</html>