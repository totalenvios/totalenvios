/* fetch.php */
<?php
    /* 
        Modified only to filter supplied variables to help avoid nasty surprises 
    */
    $options=array( 'flags' => FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_ENCODE_HIGH );
    $lat=filter_input( INPUT_GET, 'lat', FILTER_SANITIZE_ENCODED, $options );
    $lng=filter_input( INPUT_GET, 'lng', FILTER_SANITIZE_ENCODED, $options );

    $url="http://maps.googleapis.com/maps/api/geocode/key=AIzaSyCnFc9MHexw438xRR2JF6yP044jDeZeF3U&latlng={$lat},{$lng}&sensor=false";

    $curl=curl_init( $url );
    /* 
        If CURLOPT_RETURNTRANSFER is set to FALSE 
        there is no need to echo the response 
        at the end
    */
    curl_setopt( $curl, CURLOPT_RETURNTRANSFER, TRUE );
    curl_setopt( $curl, CURLOPT_AUTOREFERER, TRUE );
    curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, TRUE );
    curl_setopt( $curl, CURLOPT_FRESH_CONNECT, TRUE );
    curl_setopt( $curl, CURLOPT_FORBID_REUSE, TRUE );
    curl_setopt( $curl, CURLINFO_HEADER_OUT, FALSE );
    curl_setopt( $curl, CURLOPT_USERAGENT, 'curl-geolocation' );
    curl_setopt( $curl, CURLOPT_CONNECTTIMEOUT, 15 );
    curl_setopt( $curl, CURLOPT_TIMEOUT, 90 );
    curl_setopt( $curl, CURLOPT_HEADER, FALSE );
    $response=curl_exec( $curl );

    echo $response;

?>