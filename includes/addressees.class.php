<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Include Connection
// include('connection.class.php');
////////////////////////////////////////////////////////////////////////////////////////////////////AdressesClass////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *
 */
class Addressees
{

  ////////////////////////////////////////////////////////////////////////////////////////////////////Properties////////////////////////////////////////////////////////////////////////////////////////////////////
  public $conn;
  public $name;
  public $lastname;
  public $alias;
  public $cedula;
  public $correo;
  public $country_id;
  public $state_id;
  public $city_id;
  public $address;
  public $contact_phone;
  public $user_id;
  public $state_name;
  public $city_name;
  public $country_name;

  public function __construct()
  {
    $this->conn = new Connection;
    $this->conn = $this->conn->connect_mysql();
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////Methods////////////////////////////////////////////////////////////////////////////////////////////////////
  public function add($id, $name, $lastname, $contact_phone)
  {
    $gbd = $this->conn;
    $sql = 'INSERT INTO addressee (name, lastname, country_id, state_id, city_id, address, state_name, city_name, country_name, contact_phone, user_id) VALUES (?,?,?,?,?,?,?,?,?,?,?)';
    $stmt = $gbd->prepare($sql);
    $stmt->execute([$name, $lastname, $this->country_id, $this->state_id, $this->city_id, $this->address, $this->state_name, $this->city_name, $this->country_name, $contact_phone, $id]);

    $last_id = $gbd->lastInsertId();

    return $last_id;
  }

  public function addDestinerForWh()
  {
    $gbd = $this->conn;
    $sql = 'INSERT INTO addressee (name, lastname, alias, cedula,correo,country_id, state_id, city_id, address, contact_phone,user_id, country_name,state_name, city_name  ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
    $stmt = $gbd->prepare($sql);
    $stmt->execute([$this->name, $this->lastname, $this->alias, $this->cedula, $this->correo, $this->country_id, $this->state_id, $this->city_id, $this->address, $this->contact_phone, $this->user_id, $this->country_name, $this->state_name, $this->city_name,]);

    $last_id = $gbd->lastInsertId();

    echo $last_id;

    return $last_id;
  }
}
