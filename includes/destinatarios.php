<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  //Include Connection
  // include('connection.class.php');
  ////////////////////////////////////////////////////////////////////////////////////////////////////WarehousesClass////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *
   */
  class Destinatarios{

  ////////////////////////////////////////////////////////////////////////////////////////////////////Properties////////////////////////////////////////////////////////////////////////////////////////////////////
    public $conn;
    public $name;
    public $lastname;
    public $alias; 
    public $cedula; 
    public $correo; 
    public $address; 
    public $contact_phone; 
    public $user_id; 
    public $country_name; 
    public $state_name;
    public $city_name;


    public function __construct(){
      $this->conn = new Connection;
      $this->conn = $this->conn->connect_mysql();
    }

  ////////////////////////////////////////////////////////////////////////////////////////////////////Methods////////////////////////////////////////////////////////////////////////////////////////////////////

    public function addDestin(){
      $gbd = $this->conn;
      $sql = 'INSERT INTO addressee (`name`, `lastname`, `alias`, `cedula`, `correo`, `address`, `contact_phone`, `user_id`, `country_name`, `state_name`, `city_name`) VALUES (?,?,?,?,?,?,?,?,?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->name, $this->lastname, $this->alias, 
          $this->cedula, $this->correo, $this->address, $this->contact_phone, 
          $this->user_id, $this->country_name, $this->state_name, $this->city_name]);
      $last_id = $gbd->lastInsertId();
      return $last_id;
    }
    

    public function findDestinById($id){
      // echo $id;
      $gbd = $this->conn;
      $sql = 'SELECT * FROM addressee WHERE user_id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function updateDestin(){
      $gbd = $this->conn;
      $sql = "UPDATE user_destinatario SET name = ?, alias = ?, telefono=?, direccion=? WHERE id_user = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->name, $this->alias, $this->telefono, $this->direccion]);
      return true;
    }

    // public function deleteDestin($id){
    //   $gbd = $this->conn;
    //   $sql = "DELETE FROM `currier_region_cost` WHERE id = ?";
    //   $stmt = $gbd->prepare($sql);
    //   $stmt->execute([$id]);
    //   return true;
    // }

  }

?>
