<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  //Include Connection
  // include('connection.class.php');
  ////////////////////////////////////////////////////////////////////////////////////////////////////InvoicesClass////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *
   */
  class Invoices{

  ////////////////////////////////////////////////////////////////////////////////////////////////////Properties////////////////////////////////////////////////////////////////////////////////////////////////////
    public $conn;
    public $id;
    public $number;
    public $date_i;
    public $client;
    public $id_warehouse;
    public $value;


    public function __construct(){
      $this->conn = new Connection;
      $this->conn = $this->conn->connect_mysql();
    }

  ////////////////////////////////////////////////////////////////////////////////////////////////////Methods////////////////////////////////////////////////////////////////////////////////////////////////////

    public function add(){
      $gbd = $this->conn;
      $sql = 'INSERT INTO invoices (date_i, id_warehouse, id_client, value) VALUES (?,?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->date_i, $this->id_warehouse, $this->client, $this->value]);
      return true;
    }

    public function addGastosExtras($gastos_extras_id,$gastos_extras,$invoice_id){
      $gbd = $this->conn;
      $sql = 'INSERT INTO invoice_extracost (extracost_id, value_extracost,invoice_id) VALUES (?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$gastos_extras_id,$gastos_extras,$invoice_id]);
      return true;
    }

    public function findAll(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM invoices ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM invoices WHERE user_id = ? AND status_id=2';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findByIdPag($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM invoices WHERE user_id = ? AND status_id=3';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findLastId(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM invoices ORDER BY id DESC limit 1';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }
    
    public function findByHQ($headquarter){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM invoices WHERE id_headquarter = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$headquarter]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;

    }

    public function findAlertSearchPag($idUser, $date_in, $date_out){
      $gbd = $this->conn;
      $txt = '';
      if ($idUser != '0') {
        $txt = ' and u.id = '.$idUser.'';
      }
      $sql = "SELECT * FROM invoices a INNER JOIN users u ON a.user_id = u.id WHERE fecha_emision >= ? AND fecha_salida_wh<= ? ".$txt." LIMIT 50";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$date_in, $date_out]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findWarehouseForClient($client_name,$trip_type,$date_send,$destiner_name){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses WHERE client_name = ? and trip_id = ? and date_send = ? and destiner_name= ? and FLAG = 0';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$client_name,$trip_type,$date_send,$destiner_name]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }


    public function findWarehouseForPacking($departamento,$tipo_envio,$courier,$region,$fecha_de_salida){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses,warehouse_destiny WHERE warehouses.id = warehouse_destiny.warehouse_id and id_depart = ? and trip_id = ? and id_currier = ? and city_id= ?  and date_send = ? and FLAG = 2';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$departamento,$tipo_envio,$courier,$region,$fecha_de_salida]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function delete($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM invoices WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function generalStatus($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM general_status WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

  

    public function update($id){
      $gbd = $this->conn;
    }

  }

?>
