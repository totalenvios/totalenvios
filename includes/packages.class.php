<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  //Include Connection
  // include('connection.class.php');
  ////////////////////////////////////////////////////////////////////////////////////////////////////PackagesClass////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *
   */
  class Packages{

  ////////////////////////////////////////////////////////////////////////////////////////////////////Properties////////////////////////////////////////////////////////////////////////////////////////////////////
    public $conn;
    public $id;
    public $code;
    public $user_from;
    public $user_to;
    public $date_in;
    public $date_out;
    public $alias;
    public $operator;
    public $tracking;
    public $description;
    public $state;
    public $height;
    public $width;
    public $lenght;
    public $weight;
    public $headquarter;
    public $stock;
    public $transp;
    public $address_send;
    public $cost;


    public function __construct(){
      $this->conn = new Connection;
      $this->conn = $this->conn->connect_mysql();
    }

  ////////////////////////////////////////////////////////////////////////////////////////////////////Methods////////////////////////////////////////////////////////////////////////////////////////////////////

    public function add(){
      $gbd = $this->conn;
      $sql = 'INSERT INTO packages (code, user_from, user_to, date_in, date_out, alias, operator, tracking, description, state, height, width, lenght, weight, id_headquarter, id_stock, id_transp, address_send, cost) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->code, $this->user_from, $this->user_to, $this->date_in, $this->date_out, $this->alias, $this->operator, $this->tracking, $this->description, $this->state, $this->height, $this->width, $this->lenght, $this->weight, $this->headquarter, $this->stock, $this->transp, $this->address_send, $this->cost]);

      $last_id = $gbd->lastInsertId();

      return $last_id;
    }

    public function addAlert($tracking, $date_r, $id_client, $package_name, $transp, $date_in, $content,$cost){
      $gbd = $this->conn;
      $sql = 'INSERT INTO alerts (tracking, date_r, id_client, package_name, transp, date_in, content, cost) VALUES (?,?,?,?,?,?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$tracking, $date_r, $id_client, $package_name, $transp, $date_in, $content, $cost]);
      
      $last_id = $gbd->lastInsertId();

      return $last_id;
    }

    public function consultarFact($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM alerts WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }


    /***
     $id, 
			$name, 
			$code, 
			$width, 
			$height, 
			$lenght, 
			$weight, 
			$description, 
			$cost, 
			$category, 
			$active, 
			$kg_weight,
			$cubic_feet, 
			$cubic_m, 
			$lb_vol, 
			$m_vol, 
			// valores a consultar
			$tarifa_currier, 
			$tarifa_te,
			$costo_currier, 
			// valores a calcular
			$monto_te,
			$total,
			$ganancia,
			$porc_ganancia, 
			$insurance,
			$logistic,
			$extra_cost
     */

    public function addBoxes($id, $name, $code, $width, $height, $lenght, $weight, $description, $cost, $category, $active, $kg_weight,$cubic_feet, $cubic_m, $lb_vol, $m_vol, $cost_taf_type_trip, $tasa_type_trip, $cost_type_trip, $tarifaInterna, $costo_interno, $monto_te, $total_cost_box, $ganancia, $porc_ganancia, $insurance,$logistic,$extra_cost){
      $gbd = $this->conn;
      echo $porc_ganancia;
      $sql = "INSERT INTO boxes (id,name, code, width, height, lenght, weight, description, cost, category_id, active, kg_weight, cubic_feet, cubic_m, lb_vol, m_vol, cost_taf_type_trip, tasa_type_trip, cost_type_trip, taf_inter_type_trip, cost_inter_type_trip, cost_total, monto_calculo_type_trip, ganancia_cost_type_trip, porcentaje_ganancia_cost_type_trip, insurance, logistica, gastosExtra) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id, $name, $code, $width, $height, $lenght, $weight, $description, $cost, $category, $active, $kg_weight,$cubic_feet, $cubic_m, $lb_vol, $m_vol, $cost_taf_type_trip, $tasa_type_trip, $cost_type_trip, $tarifaInterna, $costo_interno, $monto_te, $total_cost_box, $ganancia, $porc_ganancia, $insurance,$logistic,$extra_cost]);

      $last_id = $gbd->lastInsertId();

      return $last_id;
    }

    public function findAllByUser($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packages WHERE user_from = ? AND state = 1 ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findPackageEnv($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packages WHERE state = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findPackageEnvPDF($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packages WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }


    public function addBoxesForWarehouse($id_box, $id_warehouse){
      $gbd = $this->conn;
      $sql = "INSERT INTO box_warehouse(id_box, id_warehouse) VALUES (?,?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id_box, $id_warehouse]);

      $last_id = $gbd->lastInsertId();

      return $last_id;
    }
    public function updateImg($id_package, $img){
      $gbd = $this->conn;
      $sql = 'UPDATE packages SET img = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$img, $id_package]);
      return true;
    }

    public function updateAlertImg($id_alert, $img){
      $gbd = $this->conn;
      $sql = 'UPDATE alerts SET invoice = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$img, $id_alert]);
      return true;
    }

    public function findAll(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packages WHERE state = 1';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllStatusShip(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM general_status ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllConfirmed(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packages WHERE state = 2';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function countAlerts(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as alerts FROM alerts';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function countConfirmed(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as total_paquetes FROM packages WHERE state = 2';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function countIncomplete(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as total_paquetes FROM packages WHERE state = 3';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function countAll(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as total_paquetes FROM packages WHERE state = 1';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    //**************************************************************** */
    /**FUNCIONES DE REPORTES RAPIDOS EN MODULO DE WAREHOUSE */

    public function countCloseWarehouse(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as warehouse FROM warehouses WHERE state = 2 AND FLAG = 0 OR FLAG = 2 AND state = 2';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function countOpenWarehouse(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as warehouse FROM warehouses WHERE state = 1 AND FLAG = 0 OR FLAG = 2 AND state = 1';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function countOceanWarehouse(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as warehouse FROM warehouses WHERE trip_id = 2 AND FLAG = 0 OR FLAG = 2 AND trip_id = 2';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function countAirWarehouse(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as warehouse FROM warehouses WHERE trip_id = 1 AND FLAG = 0 OR FLAG = 2 AND trip_id = 1';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }
    /*************************************************************** */

    public function findAllByW($id){
      $gbd = $this->conn;
      $sql = 'SELECT wp.id as wid, p.* FROM packages p INNER JOIN warehouse_packages wp ON p.id = wp.id_package WHERE id_warehouse = ? ORDER BY p.id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    //******************************************************** */
    public function findDestinerDataById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM addressee WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function existCountry($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM countries WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function existState($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM estados WHERE id_estado = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function existCity($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cities  WHERE id = ? ';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }


    /**************************************************** */

    public function findLastId(){
      $gbd = $this->conn;
      $sql = 'SELECT id FROM packages ORDER by id DESC limit 1';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }
    public function findById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packages WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findBoxForWh($wh_id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM box_warehouse WHERE id_warehouse = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$wh_id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findDepartById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM departamento WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCostDataForTypeId($trip_id, $city_id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM currier_region_cost WHERE trip_id = ? AND city_id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$trip_id, $city_id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCostDataForTypeIdIntern($id_currier, $trip_id, $city_id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM intern_currier_cost WHERE intern_currier_id = ? AND trip_id = ? AND city_id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id_currier, $trip_id, $city_id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllTransp(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM transp ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findByAlert($tracking){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM alerts WHERE tracking = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$tracking]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }
    
    public function findByEmail($email){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packages WHERE email = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$email]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findStates(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packages_states ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findGeneralStatus(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM general_status ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findInternCouriers(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM intern_curriers ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findInternCouriersById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM intern_curriers WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    public function findCityCurrierById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cities WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCountryCurrierById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM countries WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findStateCurrierById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM estados WHERE id_estado = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findInternCouriersCost(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM intern_currier_cost ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCityForCurrier(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cities ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findStateById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packages_states WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findSearch($q){
      $gbd = $this->conn;
      $sql = "SELECT * FROM packages WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$q]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findPackSearch($idUser, $date_in, $date_out){
      $gbd = $this->conn;
      $sql = "SELECT * FROM packages p INNER JOIN users u ON p.user_from = u.id WHERE date_in >= ? AND date_in <= ? AND u.id = ? LIMIT 50";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$date_in, $date_out, $idUser]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

     public function findAlertSearch($idUser, $date_in, $date_out){
      $gbd = $this->conn;
      $txt = '';
      if ($idUser != '0') {
        $txt = ' and u.id = '.$idUser.'';
      }
      $sql = "SELECT * FROM alerts a INNER JOIN users u ON a.id_client = u.id WHERE date_in >= ? AND date_in <= ? ".$txt." LIMIT 50";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$date_in, $date_out]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findLast(){
      $gbd = $this->conn;
      $sql = "SELECT id FROM packages ORDER BY id DESC limit 1";
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findBoxes(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM box ORDER BY id ASC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findBoxForName($box_name){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM box WHERE name= ? ';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$box_name]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCostForBoxById($box_id,$trip_id,$region_id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM box_region_cost WHERE box_id = ? AND trip_id = ? AND region_id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$box_id,$trip_id,$region_id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findBoxById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM box WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findBoxesNames($q){
      $gbd = $this->conn;
      $sql = "SELECT * FROM box WHERE name LIKE '%".$q."%' ORDER BY id ASC LIMIT 10";
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAlerts(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM alerts ORDER BY id ASC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAlertsByUser($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM alerts WHERE id_client = ? ORDER BY id ASC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAlert($q){
      $gbd = $this->conn;
      $sql = "SELECT count(*) as cuantos FROM packages WHERE tracking = ? ORDER BY id ASC";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$q]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;

    }

    public function findAlertById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM alerts WHERE id =?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCurriers(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM curriers ORDER BY id ASC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCurrierById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM curriers WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function countPackages(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as cnt FROM packages';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function delete($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM packages WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteWP($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM warehouse_packages WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function update($id){
      $gbd = $this->conn;
    }

    public function checkIfExists(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as cuantos FROM packages WHERE email = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->email]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function checkUserData(){
      $gbd = $this->conn;
      $sql = 'SELECT id, email, name, password, user_type FROM packages WHERE email = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->email]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function changePassword($email, $password){
      $gbd = $this->conn;
      $sql = 'UPDATE packages SET password = ? WHERE email = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$password, $email]);
      return true;
    }

    public function findTracking($tracking){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as exist FROM alerts where tracking = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$tracking]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
     
    }

    public function findTrackingData($tracking){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM alerts where tracking = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$tracking]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
     
    }

    public function findTransportForId($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM transp where id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
     
    }

    public function whListForSend(){
      $gbd = $this->conn;
      $sql = 'SELECT users.name, users.lastname, users.code, COUNT(*) as cantidad_wh FROM users, warehouses WHERE users.id = warehouses.id_from AND warehouses.state = 2 AND warehouses.ship_state > 2 GROUP BY warehouses.id_from';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findBoxForWarehouseById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM boxes where id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
     
    }


    public function findUserForId($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM users where id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    // ********************************************************************************
    // Funciones de edición para configuración del sistema
    // ********************************************************************************

    public function updateParameters($name, $description, $cost_value,$id){
      $gbd = $this->conn;
      $sql = 'UPDATE parametres SET name = ?, description = ?, cost_value = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $description, $cost_value,$id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function updateBox($name, $width, $height, $lenght, $weigth, $cubic_feet, $lb_vol, $box_cost, $id){
      $gbd = $this->conn;
      $sql = 'UPDATE box SET name = ?, width = ?, height = ?, lenght = ?, weigth = ?, cubic_feet = ?, lb_vol = ?, box_cost = ?  WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $width, $height, $lenght, $weigth, $cubic_feet, $lb_vol, $box_cost, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function updateCategory($name, $active, $id){
      $gbd = $this->conn;
      $sql = 'UPDATE categories SET name = ?, active = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $active, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function updateDepart($nombre, $descripcion, $id){
      $gbd = $this->conn;
      $sql = 'UPDATE departamento SET nombre = ?, descripcion = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$nombre, $descripcion, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function updateRegion($name, $active, $id){
      $gbd = $this->conn;
      $sql = 'UPDATE regions SET name = ?, active = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $active, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }


    public function updateGeneralStatus($name, $description, $siglas, $id){
      $gbd = $this->conn;
      $sql = 'UPDATE general_status SET name = ?, descripcion = ?, siglas= ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $description, $siglas, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function updateCurrier($currier, $ciudad, $trip, $region_id, $tarifa, $tarifaTE, $id){
      $gbd = $this->conn;
      $sql = 'UPDATE currier_region_cost SET currier_id = ?, city_id = ?, trip_id = ?, region_id = ?, tarifa = ?, tarifaTE = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$currier, $ciudad, $trip, $region_id, $tarifa, $tarifaTE, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function updateInternCurrier($currier, $ciudad, $trip, $tarifa, $tarifaTE, $id){
      $gbd = $this->conn;
      $sql = 'UPDATE intern_currier_cost SET intern_currier_id = ?, city_id = ?, trip_id = ?, tarifa_intern = ?, tarifaTe_intern = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$currier, $ciudad, $trip, $tarifa, $tarifaTE, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    

    public function updateCitiesForConfig($city_name, $region_id, $state_id, $country, $id){
      $gbd = $this->conn;
      $sql = 'UPDATE cities SET name = ?, region_id = ?, state_id = ?, country = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$city_name, $region_id, $state_id, $country, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function updateAlert($tracking, $id_client, $package_name, $transport_id, $cost, $content, $id){
      $gbd = $this->conn;
      $sql = 'UPDATE alerts SET tracking = ?, id_client = ?, package_name = ?, transp = ?, cost = ?, content = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$tracking, $id_client, $package_name, $transport_id, $cost, $content, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function updateCountryForConfig($name, $region, $active, $id){
      $gbd = $this->conn;
      $sql = 'UPDATE countries SET name = ?, id_region = ?, active = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $region, $active, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function updateWarehouse($user_from, $user_to, $trip_id, $currier_id, $depart_id, $name_user_from, $name_user_to, $status_wh, $ship_status, $num_guia, $nota_wh, $id){
      $gbd = $this->conn;
      $sql = 'UPDATE warehouses SET id_from = ?, id_to = ?, trip_id = ?, id_currier = ?, id_depart = ?, client_name = ?, destiner_name = ?, state = ?, ship_state = ?, num_guia = ?, nota_wh = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$user_from, $user_to, $trip_id, $currier_id, $depart_id, $name_user_from, $name_user_to, $status_wh, $ship_status, $num_guia, $nota_wh, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function updateWarehouseDestiny($country_id, $state_id, $city_id, $country_name, $state_name, $city_name, $address, $id_destiny){
      $gbd = $this->conn;
      $sql = 'UPDATE warehouse_destiny SET country_id = ?, state_id = ?, city_id = ?, country_name = ?, state_name = ?, city_name = ?, address_destiner = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$country_id, $state_id, $city_id, $country_name, $state_name, $city_name, $address, $id_destiny]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function updateWarehouseBox($height, $width, $lenght,$weight,$cost, $category_id, $description, $id)
    {
      $gbd = $this->conn;
      $sql = 'UPDATE boxes SET height = ?, width = ?, lenght = ?, weight = ?, cost = ?, category_id = ?, description = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$height, $width, $lenght,$weight,$cost, $category_id, $description, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function updateWarehouseBoxForTable($name, $height, $width, $lenght,$weight,$cost, $category_id, $description, $piecubico, $lbvol, $pesokg, $metrocubico, $kgvol, $total, $ganancia, $porcGanancia, $tarifaCurrier, $tarifa_te, $costo_currier, $monto_te, $tarifa_interna, $costo_interno, $gasto_extra, $id)
    {
      $gbd = $this->conn;
      $sql = 'UPDATE boxes SET name = ?, height = ?, width = ?, lenght = ?, weight = ?, cost = ?, category_id = ?, description = ?, cubic_feet = ?, lb_vol = ?, kg_weight = ?, cubic_m = ?, m_vol = ?, monto_calculo_type_trip = ?, ganancia_cost_type_trip = ?, porcentaje_ganancia_cost_type_trip = ?, cost_taf_type_trip = ?, tasa_type_trip = ?, cost_type_trip = ?, cost_total = ?, taf_inter_type_trip = ?, cost_inter_type_trip = ?, gastosExtra = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $height, $width, $lenght,$weight,$cost, $category_id, $description, $piecubico, $lbvol, $pesokg, $metrocubico, $kgvol, $total, $ganancia, $porcGanancia, $tarifaCurrier, $tarifa_te, $costo_currier, $monto_te, $tarifa_interna, $costo_interno, $gasto_extra, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function updateShipStatus($shipStatus, $id)
    {
      $gbd = $this->conn;
      $sql = 'UPDATE warehouses SET ship_state = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$shipStatus, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    // ********************************************************************************

    /******************************************************************************* */
    /*************** AREA DE FUNCIONES DEL MODULO DE PAQUETES **************************/

    /******************************************************************************** */

  }

?>
