<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  //Include Connection
  // include('connection.class.php');
  ////////////////////////////////////////////////////////////////////////////////////////////////////PackingClass////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *
   */
  class Packing{

  ////////////////////////////////////////////////////////////////////////////////////////////////////Properties////////////////////////////////////////////////////////////////////////////////////////////////////
    public $conn;
    public $id;
    public $code;
    public $date_in;
    public $date_out;
    public $name;
    public $note;
    public $state;
    public $id_currier;
    public $id_trip;
    public $country;
    public $id_warehouse;
    public $bulk;


    public function __construct(){
      $this->conn = new Connection;
      $this->conn = $this->conn->connect_mysql();
    }

  ////////////////////////////////////////////////////////////////////////////////////////////////////Methods////////////////////////////////////////////////////////////////////////////////////////////////////

    public function addInvoice($code_invoice, $user_id, $total_invoice, $type_trip_id, $pay_method_id, $fecha_emision, $fecha_vencimiento, $fecha_salida_wh, $destiner_id, $status_id){
      $gbd = $this->conn;
      $sql = "INSERT INTO invoices (code_invoce, user_id, total_invoce, type_trip_id, pay_method_id, fecha_emision, fecha_vencimiento, fecha_salida_wh, addresse_id, status_id) VALUES (?,?,?,?,?,?,?,?,?,?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$code_invoice, $user_id, $total_invoice, $type_trip_id, $pay_method_id, $fecha_emision, $fecha_vencimiento, $fecha_salida_wh, $destiner_id, $status_id]);
      $last_id = $gbd->lastInsertId();
      return $last_id;
    }

    public function addInvoiceDetails($invoice_id, $warehouse_id){
      $gbd = $this->conn;
      $sql = "INSERT INTO invoice_details (invoice_id, wh_id) VALUES (?,?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$invoice_id, $warehouse_id]);
      $last_id = $gbd->lastInsertId();
      return $last_id;

    }



    public function add(){
      $gbd = $this->conn;
      $sql = "INSERT INTO packing (date_in, name, note, id_currier, date_out, id_trip, id_destiny, state) VALUES (?,?,?,?,?,?,?,?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->date_in, $this->name, $this->note, $this->id_currier, $this->date_out, $this->id_trip, $this->country, $this->state]);
      $last_id = $gbd->lastInsertId();

      if ($this->bulk == 0) {
        $sql = "INSERT INTO warehouse_packing (id_packing, id_warehouse) VALUES (?,?)";
        $stmt = $gbd->prepare($sql);
        $stmt->execute([$last_id, $this->id_warehouse]);
      }else{
        $wtemp = $this->findBulk($this->bulk)['json'];
        $tempwarehouses = explode(",", $wtemp);
        foreach($tempwarehouses as $w) {
            $w = trim($w);
            $sql = "INSERT INTO warehouse_packing (id_packing, id_warehouse) VALUES (?,?)";
            $stmt = $gbd->prepare($sql);
            $stmt->execute([$last_id, $w]);
        }
      }
      return true;

    }


    public function addTemp($json){
      $gbd = $this->conn;
      $sql = "INSERT INTO packing_temp (json) VALUES (?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$json]);
      $last_id = $gbd->lastInsertId();
      return $last_id;
    }

    public function findAll(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packing ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findExtraCostName($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cost_concept where id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllByQuery($id, $date_in, $date_out){
      $gbd = $this->conn;
      $sql = "SELECT * FROM packing WHERE id like ? AND date_in >= ? AND date_in <= ? ORDER BY id";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id, $date_in, $date_out]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findLastId(){
      $gbd = $this->conn;
      $sql = 'SELECT id FROM packing ORDER by id DESC limit 1';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }
    public function findById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packing WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findDataWarehouses($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouse_packing WHERE id_packing = ? ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    
    public function findStates(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packing_states ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findStateById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packing_states WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCurriers(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM curriers ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCurrierById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM curriers WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findTripById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM trip WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCountryById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM countries WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findBoxes(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM box ORDER BY id ASC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findBoxById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM box WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findBulk($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packing_temp WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findExtraCost(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM invoice_extracost WHERE invoice_id = 0';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findExtraCostForId($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM invoice_extracost WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }


    public function borrarPackage($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM packages WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
    }

    public function delete($id){
      $gbd = $this->conn;

      $sql = 'DELETE FROM packing WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);

      $sql = 'DELETE FROM warehouse_packing WHERE id_packing = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);

      return true;
    }

    public function deleteState($id){
      $gbd = $this->conn;

      $sql = 'DELETE FROM packing_states WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);

      return true;
    }

    public function deleteWP($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM warehouse_packing WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function updateFLAG($flag, $id_warehouse){
      $gbd = $this->conn;

      $sql = 'UPDATE warehouses SET FLAG = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$flag, $id_warehouse]);

      return true;
    }

    public function updateExtraCostForInvoice($invoice_id, $extracost_invoice_id){
      $gbd = $this->conn;

      $sql = 'UPDATE invoice_extracost SET invoice_id = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$invoice_id, $extracost_invoice_id]);

      return true;
    }

    public function updateExtraCost($name_cost,$costo,$id){
      $gbd = $this->conn;

      $sql = 'UPDATE invoice_extracost SET extracost_id = ?, value_extracost = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name_cost,$costo,$id]);

      return true;
    }

    public function update($id, $name, $note, $pack_w, $currier, $fecha, $trip, $country, $state){
      $gbd = $this->conn;

      $sql = 'UPDATE packing SET name = ?, note = ?, id_currier = ?, date_out = ?, id_trip = ?, id_destiny = ?, state = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $note, $currier, $fecha, $trip, $country, $state, $id]);

      $sql = 'UPDATE warehouse_packing SET id_warehouse = ? WHERE id_packing = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$pack_w, $id]);

      return true;
    }

  }

?>
