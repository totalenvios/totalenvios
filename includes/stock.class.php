<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  //Include Connection
  // include('connection.class.php');
  ////////////////////////////////////////////////////////////////////////////////////////////////////StockClass////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *
   */
  class Stock{

  ////////////////////////////////////////////////////////////////////////////////////////////////////Properties////////////////////////////////////////////////////////////////////////////////////////////////////
    public $conn;
    public $id;
    public $name;
    public $active;
    public $headquarter;


    public function __construct(){
      $this->conn = new Connection;
      $this->conn = $this->conn->connect_mysql();
    }

  ////////////////////////////////////////////////////////////////////////////////////////////////////Methods////////////////////////////////////////////////////////////////////////////////////////////////////

    public function add(){
      $gbd = $this->conn;
      $sql = 'INSERT INTO stock (name, id_headquarter) VALUES (?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->name, $this->headquarter]);
      return true;
    }

    public function findAll(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM stock ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM stock WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }
    
    public function findByHQ($headquarter){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM stock WHERE id_headquarter = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$headquarter]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function delete($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM stock WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function update($id){
      $gbd = $this->conn;
    }

  }

?>
