<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  //Include Connection
  // include('connection.class.php');
  ////////////////////////////////////////////////////////////////////////////////////////////////////HeadquartersClass////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *
   */
  class Transport{

  ////////////////////////////////////////////////////////////////////////////////////////////////////Properties////////////////////////////////////////////////////////////////////////////////////////////////////
    public $conn;
    public $id;
    public $name;
    public $active;

    public function __construct(){
      $this->conn = new Connection;
      $this->conn = $this->conn->connect_mysql();
    }

  ////////////////////////////////////////////////////////////////////////////////////////////////////Methods////////////////////////////////////////////////////////////////////////////////////////////////////

    
    public function findAll(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM transp ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM transp WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllTripbyId($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM trip WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }
    

   
  }

?>