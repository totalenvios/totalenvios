<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  // Include Connection;
  // include('connection.class.php');
  ////////////////////////////////////////////////////////////////////////////////////////////////////UsersClass////////////////////////////////////////////////////////////////////////////////////////////////////

  /**
   *
   */
  class Users{

  ////////////////////////////////////////////////////////////////////////////////////////////////////Properties////////////////////////////////////////////////////////////////////////////////////////////////////
    public $conn;
    public $id;
    public $name;
    public $lastname;
    public $email;
    public $cedula;
    public $alias;
    public $password;
    public $country;
    public $document;
    public $phone;
    public $phone_house;
    public $user_type;
    public $headquarter;
    public $code;
    public $born_date;
    public $state_id;
    public $city_id;
    public $address;
    public $state_name;
    public $city_name;
    public $country_id;

    public function __construct(){
      $this->conn = new Connection;
      $this->conn = $this->conn->connect_mysql();
    }

  ////////////////////////////////////////////////////////////////////////////////////////////////////Methods////////////////////////////////////////////////////////////////////////////////////////////////////

    public function add(){
      $gbd = $this->conn;
      $sql = 'INSERT INTO users (name, lastname, email, password, phone, phone_house, document, country, user_type, id_headquarter, code, born_date, state_id, city_id, address, state_name, city_name, country_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->name, $this->lastname, $this->email, $this->password, $this->phone, $this->phone_house, $this->document, $this->country, $this->user_type, $this->headquarter, $this->code, $this->born_date, $this->state_id, $this->city_id, $this->address,$this->state_name, $this->city_name, $this->country_id]);

      $last_id = $gbd->lastInsertId();

      return $last_id;
    }

    public function add_user($nombre, $apellido, $email, $documento, $celular, $casa, $tipo_usuario, $contraseña, $agencia, $fecha_nacimiento, $codigo, $confirmado, $country, $country_id, $state_id, $state_name, $city_id, $city_name, $address){
      $gbd = $this->conn;
      $sql = 'INSERT INTO users (name, lastname, email, document, phone, phone_house, user_type, password, id_headquarter, born_date, code, confirmed, country, country_id, state_id, state_name, city_id, city_name, address) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$nombre, $apellido, $email, $documento, $celular, $casa, $tipo_usuario, $contraseña, $agencia, $fecha_nacimiento, $codigo, $confirmado, $country, $country_id, $state_id, $state_name, $city_id, $city_name, $address]);

      $last_id = $gbd->lastInsertId();

      return $last_id;
    }

    public function add_destiner($nombre, $apellido, $country_id, $country, $state_id, $state_name, $city_id, $city_name, $address, $celular, $user_id){
      $gbd = $this->conn;
      $sql = 'INSERT INTO addressee (name, lastname, country_id, country_name, state_id, state_name, city_id, city_name, address, contact_phone, user_id) VALUES (?,?,?,?,?,?,?,?,?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$nombre, $apellido, $country_id, $country, $state_id, $state_name, $city_id, $city_name, $address, $celular, $user_id]);

      $last_id = $gbd->lastInsertId();

      return $last_id;
    }

    public function add_state_register($state_name, $country_id){
      $gbd = $this->conn;
      $sql = 'INSERT INTO state_register (state_name, country_id) VALUES (?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$state_name, $country_id]);

      $last_id = $gbd->lastInsertId();

      return $last_id;
    }

    public function add_city_register($id_state, $ciudad, $capital, $region_id){
      $gbd = $this->conn;
      $sql = 'INSERT INTO ciudades (id_estado, ciudad, capital, region_id) VALUES (?,?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id_state, $ciudad, $capital, $region_id]);

      $last_id = $gbd->lastInsertId();

      return $last_id;
    }

    public function exist_state_register($state_name){
      $gbd = $this->conn;
      $sql = "SELECT * FROM state_register where state_name like '%".$state_name."%'";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$state_name]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function exist_city_register($city_name){
      $gbd = $this->conn;
      $sql = "SELECT * FROM ciudades where ciudad like '%".$city_name."%'";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$city_name]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function updateEmailUser($id,$email){
      $gbd = $this->conn;
      $sql = 'UPDATE users SET email = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id,$email]);
      return true;
    }

    public function confirm($id){
      $gbd = $this->conn;
      $sql = 'UPDATE users SET confirmed = 1 WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }
    

    public function asignCode($id,$code){
      $gbd = $this->conn;
      $sql = "UPDATE users SET code = ? WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$code, $id]);
      return true;

    }

    public function findConfirmed($email){
      $gbd = $this->conn;
      $sql = 'SELECT confirmed FROM users WHERE email = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$email]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAll(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM users ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findDestiner(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM addressee ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }


    

    public function findAllClients(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM users WHERE user_type = 2 ORDER BY id limit 20';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findRegisterTypes(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM users WHERE user_type in(2,5) ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAddressee(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM countries ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCountryForRegister(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM countries_register ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAddressById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM countries WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAddresseesById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM addressee WHERE user_id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAddresseeById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM addressee WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
   
    public function findcityById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM ciudades WHERE id_ciudad = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findcityByIdForClient($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cities WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }
    public function cityForSate($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cities WHERE state_id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findstateAdById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM estados WHERE id_estado = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCountryAdById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM countries WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findTripTypeById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM trip WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCurrierByTrip($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM curriers WHERE type_id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAddressStates(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM estados ORDER BY id_estado';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAddressCities(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM ciudades ORDER BY id_ciudad';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCities(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cities ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCitiesByState($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM ciudades WHERE id_estado = ? ORDER BY id_ciudad';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCitiesByStateForDestiner($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cities WHERE state_id = ? ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllOperators(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM users WHERE user_type = 3 ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM users WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findUserTypes(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM user_types ORDER by id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    
    public function findByEmail($email){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM users WHERE email = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$email]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findUsersByNameOrPhone($dato){
      $gbd = $this->conn;
      $sql = "SELECT * FROM users WHERE concat_ws(' ',name,lastname) like '%".$dato."%' OR phone like '%".$dato."%' LIMIT 20";
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findUsersByNameOrPhoneA($dato){
      $gbd = $this->conn;
      $sql = "SELECT * FROM users WHERE user_type = 1 AND (concat_ws(' ',name,lastname) like '%".$dato."%' OR phone like '%".$dato."%') LIMIT 20";
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    public function findforCode($q){
      $gbd = $this->conn;
      $sql = "SELECT id, name, lastname, phone, code FROM users WHERE code=?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$q]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
  
    public function findSearch($q){
      $gbd = $this->conn;
      $sql = "SELECT * FROM users WHERE (concat_ws(' ',name,lastname) like '%".$q."%') LIMIT 5";
      // echo $sql;
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      // print_r($data);
      return $data;
    }
    public function findAdressForUser($id){
      $gbd = $this->conn;
      $sql = "SELECT * FROM addressee WHERE user_id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    public function findDestinyById($id){
      $gbd = $this->conn;
      $sql = "SELECT * FROM countries WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllDestiny(){
      $gbd = $this->conn;
      $sql = "SELECT * FROM countries ORDER BY id";
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCurriers(){
      $gbd = $this->conn;
      $sql = "SELECT * FROM curriers ORDER BY id";
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCurrierById($id){
      $gbd = $this->conn;
      $sql = "SELECT * FROM curriers WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findRegions(){
      $gbd = $this->conn;
      $sql = "SELECT * FROM regions ORDER BY id";
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findRegionById($id){
      $gbd = $this->conn;
      $sql = "SELECT * FROM regions WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCountryRegister($id){
      $gbd = $this->conn;
      $sql = "SELECT * FROM countries_register WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCountryForClient($id){
      $gbd = $this->conn;
      $sql = "SELECT * FROM countries_register WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCountryForClientDestiner($id){
      $gbd = $this->conn;
      $sql = "SELECT * FROM countries WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findRegionByState($id){
      $gbd = $this->conn;
      $sql = "SELECT * FROM estados WHERE id_estado = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function addCurrier($name, $region, $type, $cost, $tasa, $cost_inter){

      $gbd = $this->conn;

      $sql = "INSERT INTO curriers (name) VALUES (?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name]);

      $last_id = $gbd->lastInsertId();

      $sql = "INSERT INTO `currier_region_cost` (currier_id, region_id, cost, tasa, trip_id, cost_taf_intern) VALUES (?,?,?,?,?,?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$last_id, $region, $cost, $tasa, $type, $cost_inter]);

      return true;

    }

    public function addCurrierCost($currier_id, $region, $type, $cost, $tasa, $cost_inter){

      $gbd = $this->conn;

      $sql = "INSERT INTO `currier_region_cost` (currier_id, region_id, trip_id, cost, tasa, cost_taf_intern) VALUES (?,?,?,?,?,?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$currier_id, $region, $type, $cost, $tasa, $cost_inter]);

      return true;

    }

    // public function findCurrierByZone($country){
    //   $gbd = $this->conn;
    //   $sql = "SELECT * FROM curriers WHERE id_destiny = ? ORDER BY id DESC LIMIT 1";
    //   $stmt = $gbd->prepare($sql);
    //   $stmt->execute([$country]);
    //   $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    //   return $data;
    // }

    public function findRegionByZone($country){
      $gbd = $this->conn;
      $sql = "SELECT r.* FROM regions r INNER JOIN countries c ON c.id_region = r.id WHERE c.id = ? ORDER BY r.id DESC LIMIT 1";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$country]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function addCountry($name, $region){
      $gbd = $this->conn;
      $sql = "INSERT INTO countries (name, id_region) VALUES (?,?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $region]);
      return true;
    }

    public function countUsers(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as cnt FROM users';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function delete($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM users WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteClient($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM users WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function findDestinerForClient($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM addressee WHERE user_id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteDestinerForClient($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM addressee WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    // public function update($id){
    //   $gbd = $this->conn;
    // }

    public function addTempToken($id, $token){
      $gbd = $this->conn;
      $sql = "UPDATE users SET temp_token = ? WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$token, $id]);
      return true;
    }

    public function checkIfExists(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as cuantos FROM users WHERE email = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->email]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function countClients(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as cuantos FROM users';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->email]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function countClientsForConfirmed(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as forConfirmed FROM users WHERE confirmed = 0';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->email]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }



    public function findToken($token){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as cnt FROM users WHERE temp_token = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$token]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function sesion($token){
      $gbd = $this->conn;
      $sql = 'SELECT id,name,lastname,email FROM users WHERE temp_token = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$token]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function getTransport(){
      $gbd = $this->conn;
      $sql = 'SELECT id,name FROM transp ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function generateCodePackage($code,$id){
      $gbd = $this->conn;
      $sql = 'UPDATE packages SET code = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$code,$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function destroyToken(){
      $gbd = $this->conn;
      $sql = "UPDATE users SET temp_token = ''";
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      return true;
    }

    public function checkUserData(){
      $gbd = $this->conn;
      $sql = 'SELECT id, email, name, lastname, password, user_type, code,confirmed FROM users WHERE email = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->email]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function update_destiner_for_client($name, $lastname, $alias, $cedula, $correo, $country_id, $state_id, $city_id, $address, $contact_phone, $user_id, $country_name, $state_name, $city_name, $id){
      $gbd = $this->conn;
      $sql = "UPDATE addressee SET name = ?, lastname = ?, alias = ?, cedula = ?, correo = ?, country_id = ?, state_id = ?, city_id = ?, address = ?, contact_phone = ?, user_id = ?, country_name = ?, state_name = ?, city_name = ? WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $lastname, $alias, $cedula, $correo, $country_id, $state_id, $city_id, $address, $contact_phone, $user_id, $country_name, $state_name, $city_name, $id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function editAlias($alias, $id_a){
      $gbd = $this->conn;
      $sql = "UPDATE addressee SET alias = ? WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id_a, $alias]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function update_clienteForMod($nombre, $apellido, $email, $documento, $celular, $phone_house, $country, $country_id, $state_id, $state_name, $city_id, $city_name, $address, $id){
      $gbd = $this->conn;
      $sql = 'UPDATE users SET name = ?, lastname = ?, email = ?, document = ?, phone = ?, phone_house= ?, country = ?, country_id = ?, state_id = ?, state_name = ?, city_id = ?, city_name = ?, address = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$nombre, $apellido, $email, $documento, $celular, $phone_house,$country, $country_id, $state_id, $state_name, $city_id, $city_name, $address, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function findAlertsById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM alerts WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function update_userType($user_type, $id){
      $gbd = $this->conn;
      $sql = 'UPDATE users SET user_type = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$user_type, $id]);
      // $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return true;
    }

    public function changePassword($email, $password){
      $gbd = $this->conn;
      $sql = 'UPDATE users SET password = ? WHERE email = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$password, $email]);
      return true;
    }

  }

?>
