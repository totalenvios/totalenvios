<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  //Include Connection
  // include('connection.class.php');
  ////////////////////////////////////////////////////////////////////////////////////////////////////WarehousesClass////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *
   */
  class Warehouses{

  ////////////////////////////////////////////////////////////////////////////////////////////////////Properties////////////////////////////////////////////////////////////////////////////////////////////////////
    public $conn;
    public $id;
    public $code;
    public $id_to;
    public $id_from;
    public $date_in;
    public $date_out;
    public $country;
    public $package;
    public $state;
    public $description;
    public $cost;
    public $box;
    public $id_currier;
    public $trip_id;
    public $casillero;
    public $address_send;
    public $depart;
    public $insurance;
    public $ship_state;
    public $state_user;


    public function __construct(){
      $this->conn = new Connection;
      $this->conn = $this->conn->connect_mysql();
    }

  ////////////////////////////////////////////////////////////////////////////////////////////////////Methods////////////////////////////////////////////////////////////////////////////////////////////////////

    public function add(){
      $gbd = $this->conn;
      $sql = 'INSERT INTO `warehouses`(`id_from`, `id_to`, `address`, `date_in`, `date_send`, `description`, `price`, `id_currier`, `code`, `state`, `cost`, `trip_id`, `country_id`, `box`, `casillero`, `address_send`, `id_depart`, `ship_state`, `user_confirm_state`, `insurance`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->id_from, $this->id_to, $this->address, $this->date_in, $this->date_out, $this->description, $this->price, $this->id_currier, $this->code, $this->state, $this->cost, $this->trip_id, $this->country, $this->box, $this->casillero, $this->address_send, $this->depart,$this->ship_state, $this->state_user,$this->insurance]);
      $last_id = $gbd->lastInsertId();
      return $last_id;
    }

    public function updateBoxForTrip($cost_taf_type_trip, $tasa_type_trip, $cost_type_trip, $monto_te, $total_cost_box, $ganancia, $porc_ganancia, $id){
      $gbd = $this->conn;
      $sql = "UPDATE boxes SET cost_taf_type_trip = ?, tasa_type_trip = ?, cost_type_trip = ?, monto_calculo_type_trip = ?, cost_total = ?, ganancia_cost_type_trip = ?, porcentaje_ganancia_cost_type_trip = ?  WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$cost_taf_type_trip, $tasa_type_trip, $cost_type_trip, $monto_te, $total_cost_box, $ganancia, $porc_ganancia, $id]);
      return true;
    }

    public function addWarehouseDetails($warehose_id, $country_id, $state_id, $city_id, $destiner_id, $country_name, $state_name, $city_name, $address){
      $gbd = $this->conn;
      $sql = 'INSERT INTO `warehouse_destiny`(`warehouse_id`, `country_id`, `state_id`, `city_id`, `destiner_id`,`country_name`,`state_name`,`city_name`,`address_destiner`) VALUES (?,?,?,?,?,?,?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$warehose_id, $country_id, $state_id, $city_id, $destiner_id,$country_name, $state_name, $city_name, $address]);
      $last_id = $gbd->lastInsertId();
      return $last_id;
    }

    public function updateWarehouseForRegister($id_from, $id_to, $address, $date_in, $date_out, $description, $price, $id_currier, $code, $state, $cost, $trip_id, $country, $box, $casillero, $address_send, $depart,$ship_state, $state_user,$insurance, $client_name, $destiner_name, $flag, $nota,$num_guia, $id){
      $gbd = $this->conn;
      $sql = "UPDATE `warehouses` SET id_from = ?, id_to = ?, address = ?, date_in = ?, date_send = ?, description = ?, price = ?, id_currier = ?, code = ?, state = ?, cost = ?, trip_id = ?, country_id  = ?, box = ? , casillero = ?, address_send = ?, id_depart = ? , ship_state = ?, user_confirm_state = ? , insurance = ?, client_name = ?, destiner_name = ?, FLAG = ?, nota_wh = ?, num_guia = ? WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id_from, $id_to, $address, $date_in, $date_out, $description, $price, $id_currier, $code, $state, $cost, $trip_id, $country, $box, $casillero, $address_send, $depart,$ship_state, $state_user,$insurance, $client_name, $destiner_name, $flag, $nota,$num_guia, $id]);
      return true;
    }

    

    public function generateIdForWarehouse($id_from, $id_to, $address, $date_in, $price, $id_currier, $code, $cost, $trip_id, $country_id, $box, $flag){
      $gbd = $this->conn;
      $sql = 'INSERT INTO `warehouses`(`id_from`, `id_to`, `address`, `date_in`, `price`, `id_currier`, `code`, `cost`, `trip_id`, `country_id`, `box`, `FLAG`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id_from, $id_to, $address, $date_in, $price, $id_currier, $code, $cost, $trip_id, $country_id, $box, $flag]);
      $last_id = $gbd->lastInsertId();
      return $last_id;
    }

    public function addExtraCost($cost_id,$box_id,$value_cost){
      $gbd = $this->conn;
      $sql = 'INSERT INTO `box_extra_cost`(`extra_cost_id`, `box_id`, `cost_extra`) VALUES (?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$cost_id,$box_id,$value_cost]);
      $last_id = $gbd->lastInsertId();
      return $last_id;
    }

    public function getBoxForWh($wh_id){
      $gbd = $this->conn;
      $sql = 'SELECT boxes.* FROM boxes,box_warehouse WHERE boxes.id = box_warehouse.id_box AND box_warehouse.id_warehouse = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$wh_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllByUser($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses WHERE FLAG = 0 AND id_from = ? ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function notaWarehouse(){
      // echo $code;
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

  

    public function findAllCurriers(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM curriers';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllDepartments(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM departamento';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllTrips(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM trip';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    

    public function findAllByUserWE($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses WHERE state = 8 AND id_from = ? ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCostMinPiecub(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM parametres WHERE id = 7';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCiudades(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cities ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findWarehouseDestiny($wh_id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouse_destiny WHERE warehouse_id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$wh_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
     
    /**status para editar wh */
    public function findStatusForId($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM general_status WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findStatusFor(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM general_status';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findShipStateFor(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses_states';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findShipStateForId($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses_states WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    /**status para editar wh */
    public function findcityDestiny($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouse_destiny WHERE warehouse_id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findcityNameForwh($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cities WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function addInternCurrier($currier_id, $city_id, $trip_id, $tarifa, $tarifaTE){
      $gbd = $this->conn;
      $sql = 'INSERT INTO `intern_currier_cost`(`intern_currier_id`, `city_id`, `trip_id`, `tarifa_intern`, `tarifaTe_intern`) VALUES (?,?,?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$currier_id, $city_id, $trip_id, $tarifa, $tarifaTE]);
      return true;
    }

    public function addCurrier($currier_id, $region_id, $trip_id, $city_id, $tarifa, $tarifaTE){
      $gbd = $this->conn;
      $sql = 'INSERT INTO `currier_region_cost`(`currier_id`, `region_id`,`trip_id`, `city_id`, `tarifa`, `tarifaTE`) VALUES (?,?,?,?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$currier_id, $region_id, $trip_id, $city_id, $tarifa, $tarifaTE]);
      return true;
    }

    public function addNewInternCurrier($currier_name){
      $gbd = $this->conn;
      $sql = 'INSERT INTO `intern_curriers`(`name`) VALUES (?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$currier_name]);
      return true;
    }

    public function addNewCurrier($currier_name){
      $gbd = $this->conn;
      $sql = 'INSERT INTO `curriers`(`name`) VALUES (?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$currier_name]);
      return true;
    }

    // fUNCIONES DE CONSULTAS PARA GENERAR RECEIVE E INVOCE
    // *****************************************************************************************************
    public function listWarehouseData($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    
    public function getWarehouseParameter(){
      $gbd = $this->conn;
      $sql = 'SELECT cost_value FROM parametres WHERE name = warehouse_cant';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findParametresById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM parametres WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCurriersById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM curriers WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findGeneralStatusById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM general_status WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findInternCurrierById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM intern_currier_cost WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCurrierById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM currier_region_cost WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findRegionById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM regions WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findRegionByCityId($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cities WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }


    public function findAllParameters(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM `parametres` ORDER by id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function getExtraGastos(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cost_concept ORDER BY id ASC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function getCostForInvoice($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cost_concept where id = ? ';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function listUserDataToReceive($user_id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM users WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$user_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    } 

    public function listAddresseeDataToReceive($addreese_id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM addressee WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$addreese_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    } 

    public function listWarehouseForPacking($trip_id, $currier_id, $send_date){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses WHERE trip_id = ? AND id_currier = ? AND date_send = ? AND FLAG = 0';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$trip_id, $currier_id, $send_date]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function boxbywarehouse($id){
      $gbd = $this->conn;
      $sql = 'Select box_warehouse.id_box from boxes,box_warehouse where box_warehouse.id_warehouse= ? LIMIT 1 ';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function listBoxbyId($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM boxes WHERE code = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function boxesForInvoice($invoice_id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM boxes,box_warehouse,invoice_details WHERE boxes.id = box_warehouse.id_box and box_warehouse.id_warehouse = invoice_details.wh_id and invoice_id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$invoice_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function extraCostForInvoice($invoice_id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cost_concept, invoice_extracost WHERE cost_concept.id = invoice_extracost.extracost_id and invoice_extracost.invoice_id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$invoice_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    
    public function findTripName($trip_id){
      $gbd = $this->conn;
      $sql = 'SELECT trip.id, trip.name FROM trip WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$trip_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    } 

    public function regionForName($name){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM regions WHERE name = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    } 

    public function cityForName($name){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cities WHERE name = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    } 

    public function findcityName($city_id){
      $gbd = $this->conn;
      $sql = 'SELECT ciudades.id_ciudad, ciudades.ciudad FROM ciudades WHERE id_ciudad = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$city_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    } 

    public function findstateName($state_id){
      $gbd = $this->conn;
      $sql = 'SELECT estados.id_estado, estados.estado FROM estados WHERE id_estado = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$state_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    } 

    public function findcountryName($country_id){
      $gbd = $this->conn;
      $sql = 'SELECT countries.id, countries.name FROM countries WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$country_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    } 
    public function listBoxWarehouseIdForAir($id){
      $gbd = $this->conn;
      $sql = 'SELECT boxes.id, boxes.code,boxes.height,boxes.width,boxes.weight,boxes.lenght,boxes.cubic_feet as vol,boxes.description,box_warehouse.id_warehouse, boxes.cost_total 
      FROM boxes,box_warehouse 
      WHERE  box_warehouse.id_box = boxes.id AND box_warehouse.id_warehouse = ? ORDER BY box_warehouse.id_warehouse';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function listBoxWarehouseIdForOcean($id){
      $gbd = $this->conn;
      $sql = 'SELECT boxes.id, boxes.code,boxes.height,boxes.width,boxes.weight,boxes.lenght,boxes.lb_vol as vol,boxes.description,box_warehouse.id_warehouse,boxes.cost_total 
      FROM boxes,box_warehouse 
      WHERE  box_warehouse.id_box = boxes.id AND box_warehouse.id_warehouse = ? ORDER BY box_warehouse.id_warehouse';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function countwarehousesBox($id){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as cnt FROM box_warehouse WHERE id_warehouse = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findBoxesWarehouse($id_warehouse){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM box_warehouse WHERE id_warehouse = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id_warehouse]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function deleteCategoryToConfig($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM categories WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteDestinerForUser($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM addressee WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function findCitiesById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM cities WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function deleteCountryById($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM countries WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteCitiesById($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM cities WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }


    public function deleteInternCurrierToConfig($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM intern_currier_cost WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteCurrierToConfig($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM currier_region_cost WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteDepartToConfig($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM departamento WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteGeneralStatus($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM general_status WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteParametresToConfig($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM parametres WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteRegionToConfig($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM regions WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteCourierToConfig($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM curriers WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function findCourierCostToConfig($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM currier_region_cost WHERE currier_id = ? ORDER BY id ASC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function deleteCourierCostToConfig($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM currier_region_cost WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteBox($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM box_warehouse WHERE id_box = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;

      $gbd = $this->conn;
      $sql = 'DELETE FROM boxes WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteBoxToConfig($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM box WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function findGeneralStateById($id){
     $gbd = $this->conn;
      $sql = 'SELECT * FROM general_status WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findExtraCostForWarehouseBox($id){
      $gbd = $this->conn;
      $sql = 'SELECT cost_concept.name,box_extra_cost.cost_extra FROM box_extra_cost,cost_concept WHERE cost_concept.id = box_extra_cost.extra_cost_id AND box_id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }


    // **************************************************************************************************

    public function addWarehousePackage($w, $p){
      $gbd = $this->conn;
      $sql = 'INSERT INTO warehouse_packages (id_warehouse, id_package) VALUES (?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$w, $p]);
      return true;
    }

    public function addBoxes($w, $p){
      $gbd = $this->conn;
      $sql = 'INSERT INTO `box_warehouse` (id_warehouse, id_box) VALUES (?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$w, $p]);
      return true;
    }

    public function boxesForWarehouse($warehouse_id){
      $gbd = $this->conn;
      $sql = 'SELECT count(id) as countBox FROM box_warehouse WHERE id_warehouse = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$warehouse_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function calculateCostBox($warehouse_id){
      $gbd = $this->conn;
      $sql = 'SELECT SUM(boxes.cost_total) AS SUMTOTAL FROM boxes,box_warehouse WHERE boxes.id = box_warehouse.id_box AND box_warehouse.id_warehouse = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$warehouse_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    /************************************************************* */
    public function calculatepie3($warehouse_id){
      $gbd = $this->conn;
      $sql = 'SELECT SUM(boxes.cubic_feet) AS TOTALPIE FROM boxes,box_warehouse WHERE boxes.id = box_warehouse.id_box AND box_warehouse.id_warehouse = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$warehouse_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    public function calculatelbvol($warehouse_id){
      $gbd = $this->conn;
      $sql = 'SELECT SUM(boxes.lb_vol) AS TOTALLBVOL FROM boxes,box_warehouse WHERE boxes.id = box_warehouse.id_box AND box_warehouse.id_warehouse = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$warehouse_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    public function calculateancho($warehouse_id){
      $gbd = $this->conn;
      $sql = 'SELECT SUM(boxes.height) AS TOTALANCHO FROM boxes,box_warehouse WHERE boxes.id = box_warehouse.id_box AND box_warehouse.id_warehouse = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$warehouse_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    public function calculatealto($warehouse_id){
      $gbd = $this->conn;
      $sql = 'SELECT SUM(boxes.width) AS TOTALALTO FROM boxes,box_warehouse WHERE boxes.id = box_warehouse.id_box AND box_warehouse.id_warehouse = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$warehouse_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    public function calculatelargo($warehouse_id){
      $gbd = $this->conn;
      $sql = 'SELECT SUM(boxes.lenght) AS TOTALLARGO FROM boxes,box_warehouse WHERE boxes.id = box_warehouse.id_box AND box_warehouse.id_warehouse = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$warehouse_id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    /********************************************************** */

    public function addIBox($name, $width, $height, $weight, $lenght){
      $gbd = $this->conn;
      $sql = 'INSERT INTO `box` (name, width, height, weigth, lenght) VALUES (?,?,?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $width, $height, $weight, $lenght]);
      return true;
    }

    public function addGeneralStatus($name, $description, $siglas){
      $gbd = $this->conn;
      $sql = 'INSERT INTO `general_status` (name, descripcion, siglas) VALUES (?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $description, $siglas]);
      return true;
    }

    public function findAll(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses WHERE FLAG = 0 AND state = 1 AND ship_state < 3 ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllComplete(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses WHERE FLAG = 0 OR FLAG = 2 ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllInvoices(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM invoices ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllInvoicesForConfirm(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM invoices WHERE status_id = 1 ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllInvoicesForPay(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM invoices WHERE status_id = 2 ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllInvoicesPay(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM invoices WHERE status_id = 3 ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }


    // add data of configuration
  // -----------------------------------------------------------------------------------------------------------
    public function addParameters($name, $description, $cost){
      $gbd = $this->conn;
      $sql = 'INSERT INTO `parametres`(`name`, `description`, `cost_value`) VALUES (?,?,?)';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $description, $cost]);
      return true;
    }

    

    public function findAllBoxesW($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM box_warehouse WHERE id_warehouse = ? ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllBoxes(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM boxes ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function boxPaginator($inicio,$fin){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM box LIMIT :inicio, :fin';
      $stmt = $gbd->prepare($sql);
      $stmt->bindParam(':inicio', $inicio, PDO::PARAM_INT);
      $stmt->bindParam(':fin', $fin, PDO::PARAM_INT);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function countBoxPaginator(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as boxes FROM box';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findAllBox(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM box ORDER BY name ASC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findBoxesById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM boxes WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findDepartById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM departamento WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    

    public function findDepart(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM departamento ORDER BY id';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }
    
    public function findLastId(){
      $gbd = $this->conn;
      $sql = 'SELECT id FROM warehouses ORDER by id DESC limit 1';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findLastIdBox(){
      $gbd = $this->conn;
      $sql = 'SELECT id FROM boxes ORDER by id DESC limit 1';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findByEmail($email){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses WHERE email = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$email]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findWStates(){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses_states ORDER BY id DESC';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findWStateById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM warehouses_states WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findStateById($id){
      $gbd = $this->conn;
      $sql = 'SELECT * FROM packages_states WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findWareSearch($idUser, $date_in, $date_out){
      $gbd = $this->conn;
      $txt = '';
      if ($idUser != '0') {
        $txt = ' and u.id = '.$idUser.'';
      }
      $sql = "SELECT * FROM warehouses p INNER JOIN users u ON p.id_from = u.id WHERE date_in >= ? AND date_in <= ? ".$txt." LIMIT 50";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$date_in.' 00:00:00', $date_out.' 23:59:59']);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findLast(){
      $gbd = $this->conn;
      $sql = "SELECT * FROM warehouses ORDER BY id DESC limit 1";
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCRCost(){
      $gbd = $this->conn;
      $sql = "SELECT * FROM `currier_region_cost` ORDER BY city_id DESC";
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function updateCR($id, $trip){
      $gbd = $this->conn;
      $sql = "UPDATE `currier_region_cost` SET trip_id = ? WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$trip, $id]);
      return true;
    }

    public function updateCRCost($id, $cost){
      $gbd = $this->conn;
      $sql = "UPDATE `currier_region_cost` SET cost = ? WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$cost, $id]);
      return true;
    }

    public function updateCRCostA($id, $cost){
      $gbd = $this->conn;
      $sql = "UPDATE `currier_region_cost` SET cost_taf_intern = ? WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$cost, $id]);
      return true;
    }

    public function deleteCR($id){
      $gbd = $this->conn;
      $sql = "DELETE FROM `currier_region_cost` WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function updateCRTasa($id, $tasa){
      $gbd = $this->conn;
      $sql = "UPDATE `currier_region_cost` SET tasa = ? WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$tasa, $id]);
      return true;
    }

    public function findCategories(){
      $gbd = $this->conn;
      $sql = "SELECT * FROM categories ORDER BY id ASC";
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCategoryById($id){
      $gbd = $this->conn;
      $sql = "SELECT * FROM categories WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function addCategory($name){
      $gbd = $this->conn;
      $sql = "INSERT INTO categories (name) VALUES (?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name]);
      return true;
    }

    public function addState($name){
      $gbd = $this->conn;
      $sql = "INSERT INTO `warehouses_states` (name) VALUES (?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name]);
      return true;
    }

    public function deleteState($id){
      $gbd = $this->conn;
      $sql = "DELETE FROM `warehouses_states` WHERE id =  ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function updateState($name, $id){
      $gbd = $this->conn;
      echo $name.'-'.$id;
      $sql = "UPDATE `warehouses_states` SET name = ? WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $id]);
      return true;
    }

    public function addRegion($name){
      $gbd = $this->conn;
      $sql = "INSERT INTO regions (name) VALUES (?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name]);
      return true;
    }

    public function addDepart($name, $description){
      $gbd = $this->conn;
      $sql = "INSERT INTO departamento (nombre, descripcion) VALUES (?,?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $description]);
      return true;
    }

    public function addBOX($name, $width, $height, $weight, $lenght, $cubic_feet, $lb_vol, $box_cost){
      $gbd = $this->conn;
      $sql = "INSERT INTO box (name, width, height, weigth, lenght, cubic_feet, lb_vol, box_cost) VALUES (?,?,?,?,?,?,?,?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $width, $height, $weight, $lenght, $cubic_feet, $lb_vol, $box_cost]);
      return true;
    }


    public function findTrips(){
      $gbd = $this->conn;
      $sql = "SELECT * FROM trip ORDER BY id";
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findTripById($id){
      $gbd = $this->conn;
      $sql = "SELECT * FROM trip WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCountries(){
      $gbd = $this->conn;
      $sql = "SELECT * FROM countries ORDER BY id";
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function addNewCity($name, $region_id, $state_id, $country){
      $gbd = $this->conn;
      $sql = "INSERT INTO cities (name, region_id, state_id, country) VALUES (?,?,?,?)";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$name, $region_id, $state_id, $country]);
      return true;
    }


    public function findEstados(){
      $gbd = $this->conn;
      $sql = "SELECT * FROM estados ORDER BY id_estado";
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findEstadosById($id){
      $gbd = $this->conn;
      $sql = "SELECT * FROM estados WHERE id_estado = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data;
    }

    public function findCountryById($id){
      $gbd = $this->conn;
      $sql = "SELECT * FROM countries WHERE id = ?";
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function countwarehouses(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as cnt FROM warehouses';
      $stmt = $gbd->prepare($sql);
      $stmt->execute();
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function countBoxwarehouses($id){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as total FROM box_warehouse WHERE id_warehouse = ?' ;
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function update($id){
      $gbd = $this->conn;
      $sql = 'UPDATE `warehouses` SET `id_from` = ?, `id_to` = ?, `address` = ?, `date_in` = ?, `date_send` = ?, `height` = ?, `width` = ?, `weight` = ?, `lenght` = ?, `description` = ?, `price` = ?, `id_currier` = ?, `code` = ?, `state` = ?, `cost` = ?, `trip_id` = ?, `country_id` = ?, `box` = ? WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->id_from, $this->id_to, $this->address, $this->date_in, $this->date_out, $this->height, $this->width, $this->weight, $this->lenght, $this->description, $this->price, $this->id_currier, 0, $this->state, $this->cost, $this->trip_id, $this->country, $this->box, $id]);
      return true;
    }

    public function checkIfExists(){
      $gbd = $this->conn;
      $sql = 'SELECT count(*) as cuantos FROM warehouses WHERE email = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->email]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function checkUserData(){
      $gbd = $this->conn;
      $sql = 'SELECT id, email, name, password, user_type FROM warehouses WHERE email = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$this->email]);
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }

    public function changePassword($email, $password){
      $gbd = $this->conn;
      $sql = 'UPDATE warehouses SET password = ? WHERE email = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$password, $email]);
      return true;
    }

    public function delete($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM warehouses WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteInternCurrier($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM intern_curriers WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    public function deleteNewCurrier($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM curriers WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

    
    public function deleteExtraCostForTable($id){
      $gbd = $this->conn;
      $sql = 'DELETE FROM invoice_extracost WHERE id = ?';
      $stmt = $gbd->prepare($sql);
      $stmt->execute([$id]);
      return true;
    }

  }

?>
