<?php

include 'includes/connection.class.php';
include 'includes/headquarters.class.php';
include 'includes/users.class.php';

$headquarters = new Headquarters;
$usersc = new Users;
$allcountriesRegister = $usersc->findCountryForRegister();
$allCountries = $usersc->findAddressee();
$allAddressStates = $usersc->findAddressStates();
$allAddressCities = $usersc->findAddressCities();
$allHeadQ = $headquarters->findAll();
$userTypes = $usersc->findRegisterTypes();
$digits = 5;
$fecha = new DateTime();
$dataFecha = $fecha->getTimestamp();
$code = 'TE -' . substr($dataFecha, -5);

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php

    include 'head.php';


    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        #myMap {
            height: 350px;
            width: 680px;
        }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCnFc9MHexw438xRR2JF6yP044jDeZeF3U&sensor=false">
    </script>
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>


</head>

<body class="m-0 vh-100 row justify-content-center align-items-center" style="background:#005574 !important;">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->

    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <!-- Modal de confirmación de registro************************************************ -->

    <div class="modal fade" id="confirmRegisterModal" tabindex="-1" role="dialog" aria-labelledby="confirmRegisterModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <!-- <div class="modal-header">
                    <h5 class="modal-title" id="confirmRegisterModalLabel">Pre - Alerta:</h5>
                    <button onclick="$('#alertModal').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div> -->
                <div class="modal-body">
                    <div class="container-fluid">
                        <h3 id="id_register_user" hidden></h3>
                        <div align="center"><br>
                            <div align="center"><img src="./confirmRegister.png"></div><br>
                            <h2 style="color:#005574;">Registro exitoso</h2>
                        </div>
                        <hr style="background-color:#ff554dff; height: 2px; opacity: 1.0;">
                        <div class="row">

                            <div style="padding:3%;">
                                <h6 style="color:#005574;">¡Bienvenido! te hemos enviado un correo de confirmacion, sino
                                    lo ves, revisa tu bandeja de SPAM.</h6>
                            </div>

                        </div>

                        <div class="row">

                            <div style="padding:3%;">
                                <h6 style="color:#005574; padding-bottom:1%;"><strong>¿No recibiste el correo de
                                        confirmación?</strong></h6>
                                <div class="form-group">
                                    <input id="new_email" style="padding-bottom:1%;" type="text" class="input-register-warehouse" placeholder="Confirma tu correo electronico aquí">
                                    <button onclick="rsendEmail();" style="background:#ff554dff; color:white;"><i class="fas fa-register"></i> Reenviar</button>
                                </div>
                            </div>

                        </div>
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ********************************************************************************** -->
    <div class="col-lg-10 col-xlg-10 col-md-10">

        <div id="main-wrapper">

            <input id="countryVenezuela" type="hidden" value="0">
            <div class="card" style="border-radius: 50px;">
                <div class="card-body">
                    <div style="padding-top:10px; padding-left:30px; display:flex;">

                        <div>
                            <h2 style="color:#005574; width:330px; padding-right:10px; font-weight:1000 !important; font-size:35px; letter-spacing: -0.5px !important;">
                                <strong>Registro de clientes</strong>
                            </h2>
                        </div>
                        <div>
                            <hr style="background-color:#ff554dff; height: 8px; opacity: 1.0; border-radius: 50px; width:1500px;  margin-right:0px !important;">
                        </div>
                    </div>
                    <div class="row" style="padding:40px;">
                        <div class="col-lg-12 col-xlg-12 col-md-12">


                            <!-- <div class="encabezado"><img id="imag" src="./logo-home-te.png"/></div> -->

                            <div class="alert alert-danger register-error" role="alert">
                            </div>
                            <div class="alert alert-success register-success" role="alert">
                            </div>
                            <div>

                                <div class="row">
                                    <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2"></div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">

                                                <div class="form-group">
                                                    <!-- <label for="user_type" style="color:#005574;"><strong>Tipo de usuario</strong></label> -->
                                                    <select id="user_type" class="campos-input-login-REGISTER" style="background: url('icons/register/usuario.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                        <option value="0">TIPO DE USUARIO</option>
                                                        <option value="2">PERSONA</option>
                                                        <option value="5">EMPRESA</option>
                                                    </select>
                                                </div>

                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <!-- <label for="headquarter" style="color:#005574;"><strong>Agencia</strong></label> -->
                                                    <select id="headquarter" class="campos-input-login-REGISTER" style="background: url('icons/register/agencia.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                        <!-- <option value="0">Seleccionar...</option> -->
                                                        <?php
                                                        foreach ($allHeadQ as $key => $value) {
                                                        ?>
                                                            <option value="<?php echo $value['id']; ?>">
                                                                <?php echo $value['name']; ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2"></div>
                                        </div>

                                        <div id="user-register" style="display:block;">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group">
                                                        <!-- <label for="name" style="color:#005574;"><strong>Nombre completo</strong></label> -->
                                                        <input id="name" type="text" placeholder="NOMBRES" class="campos-input-login-REGISTER" style="background: url('icons/register/nombre.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px; color:red !important;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">

                                                    <div class="form-group">
                                                        <!-- <label for="lastname"style="color:#005574;"><strong>Apellido completo</strong></label> -->
                                                        <input id="lastname" type="text" placeholder="APELLIDOS" class="campos-input-login-REGISTER" style="background: url('icons/register/nombre.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group">
                                                        <!-- <label for="email"style="color:#005574;"><strong>Correo electrónico</strong></label> -->
                                                        <input type="email" placeholder="CORREO ELECTRÓNICO" class="campos-input-login-REGISTER" name="example-email" id="email" style="background: url('icons/register/email.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group">
                                                        <!-- <label for="email"style="color:#005574;"><strong>Confirma tu correo electrónico</strong></label> -->
                                                        <input type="email" placeholder="CONFIRMACIÓN" class="campos-input-login-REGISTER" name="example-email" id="email" style="background: url('icons/register/email.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                    <div class="form-group">
                                                        <!-- <label for="address_destiny"style="color:#005574;"><strong>Pais</strong></label> -->
                                                        <select id="address_destiny" class="campos-input-login-REGISTER" style="background: url('icons/register/tierra.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                            <option value="0">PAÍS</option>
                                                            <?php
                                                            foreach ($allcountriesRegister as $key => $value) {
                                                            ?>
                                                                <option value="<?php echo $value['id']; ?>">
                                                                    <?php echo $value['name']; ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 address_state">
                                                    <div class="form-group">
                                                        <!-- <label for="address_state"style="color:#005574;"><strong>Estado</strong></label> -->
                                                        <input id="address_state" type="text" placeholder="ESTADO" class="campos-input-login-REGISTER" style="background: url('icons/register/tierra.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 select_state" style="display: none;">
                                                    <div class="form-group">
                                                        <!-- <label for="select_state"style="color:#005574;"><strong>Estado</strong></label> -->
                                                        <select id="select_state" class="campos-input-login-REGISTER" style="background: url('icons/register/tierra.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                            <option value="0">ESTADO</option>
                                                            <?php
                                                            foreach ($allAddressStates as $key => $value) {
                                                            ?>
                                                                <option value="<?php echo $value['id_estado']; ?>">
                                                                    <?php echo $value['estado']; ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 address_city">
                                                    <div class="form-group">
                                                        <!-- <label for="address_city"style="color:#005574;"><strong>Ciudad</strong></label> -->
                                                        <input id="address_city" type="text" placeholder="CIUDAD" class="campos-input-login-REGISTER" style="background: url('icons/register/tierra.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 select_city" style="display: none;">
                                                    <div class="form-group">
                                                        <!-- <label for="select_city"style="color:#005574;"><strong>Ciudad</strong></label> -->
                                                        <select id="select_city" class="campos-input-login-REGISTER" style="background: url('icons/register/tierra.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 address_address">
                                                    <div class="form-group">
                                                        <!-- <label for="address"style="color:#005574;"><strong>Dirección</strong></label> -->
                                                        <input id="address" type="text" placeholder="DIRECCIÓN" class="campos-input-login-REGISTER" style="background: url('icons/register/ubicacion.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 select_address" style="display: none;">
                                                    <div class="form-group">
                                                        <!-- <label for="select_address"style="color:#005574;"><strong>Dirección</strong></label> -->
                                                        <input id="select_address" type="text" placeholder="DIRECCIÓN" class="campos-input-login-REGISTER" style="background: url('icons/register/ubicacion.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                    <div class="form-group">
                                                        <!-- <label for="document"style="color:#005574;"><strong>Identificación</strong></label> -->
                                                        <input id="document" type="text" placeholder="IDENTIFICACIÓN" class="campos-input-login-REGISTER" style="background: url('icons/register/identificador.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                    <div class="form-group">
                                                        <!-- <label for="phone"style="color:#005574;"><strong>Teléfono celular</strong></label> -->
                                                        <input id="phone" type="text" placeholder="TELF. CELULAR" class="campos-input-login-REGISTER" style="background: url('icons/register/celular.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                    <div class="form-group">
                                                        <!-- <label for="phone_house"style="color:#005574;"><strong>Teléfono de casa</strong></label> -->
                                                        <input id="phone_house" type="text" placeholder="TELF. FIJO" class="campos-input-login-REGISTER" style="background: url('icons/register/celular.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group">
                                                        <!-- <label for="password"style="color:#005574;"><strong>Contraseña</strong></label> -->
                                                        <input id="password" placeholder="CONTRASEÑA" class="campos-input-login-REGISTER" style="background: url('icons/register/password.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group">
                                                        <!-- <label for="confirm_password"style="color:#005574;"><strong>Confirmar contraseña</strong></label> -->
                                                        <input id="confirm_password" placeholder="CONFIRMA TU CONTRASEÑA" class="campos-input-login-REGISTER" style="background: url('icons/register/password.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>



                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group">
                                                        <!-- <label for="born_date"style="color:#005574;"><strong>Fecha de nacimiento</strong></label> -->
                                                        <input type="date" placeholder="1990-01-01" class="campos-input-login-REGISTER" id="born_date" style="background: url('icons/cumpleaños.png');background-repeat: no-repeat;
                                                        background-position: 15px center;
                                                        background-size: 20px;
                                                        display: flex;
                                                        align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group">
                                                        <!-- <label for="contact"style="color:#005574;"><strong>Como supiste de nosotros</strong></label> -->
                                                        <select id="contact" class="campos-input-login-REGISTER" style="
                                                        display: flex;
                                                        align-items: center; padding-left: 20px;">
                                                            <option value="0">COMO SUPISTE DE NOSOTROS</option>
                                                            <option value="2">Facebook</option>
                                                            <option value="3">Instagram</option>
                                                            <option value="4">Twitter</option>
                                                            <option value="5">Radio/Televisión</option>
                                                            <option value="6">Referido</option>
                                                            <option value="7">Otros</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                                                <label class="form-check-label" for="flexCheckChecked">
                                                    Acepto terminos y condiciones
                                                </label>
                                            </div>

                                            <div class="form-group mb-4 text-center">
                                                <div class="col-sm-12">
                                                    <button onclick="register(1);" class="boton-total-envios-registro"><strong>REGISTRAR</strong></button>
                                                </div>
                                                <div align="center"><a href="login.php" class="footer-text" style="color:#005574;">¿Ya tienes una cuenta? Ingresa aquí</a>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- AREA DE REGISTRO DE EMPRESAS -->
                                        <!-- ******************************************************************************* -->
                                        <div id="company-register" style="display:none;">

                                            <div class="row">
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group">
                                                        <!-- <label for="company_name">Nombre de la empresa</label> -->
                                                        <input id="company_name" type="text" placeholder="EMPRESA" class="campos-input-login-REGISTER" style="background: url('icons/register/agencia.png');background-repeat: no-repeat;
                                                                background-position: 15px center;
                                                                background-size: 20px;
                                                                display: flex;
                                                                align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group">
                                                        <!-- <label for="company_document"># Identificación</label> -->
                                                        <input id="company_document" type="text" placeholder="IDENTIFICACIÓN" class="campos-input-login-REGISTER" style="background: url('icons/register/identificador.png');background-repeat: no-repeat;
                                                                background-position: 15px center;
                                                                background-size: 20px;
                                                                display: flex;
                                                                align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group">
                                                        <!-- <label for="company_email">Correo electrónico</label> -->
                                                        <input type="email" placeholder="CORREO ELECTRÓNICO" class="campos-input-login-REGISTER" name="example-email" id="company_email" style="background: url('icons/register/email.png');background-repeat: no-repeat;
                                                                background-position: 15px center;
                                                                background-size: 20px;
                                                                display: flex;
                                                                align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group">
                                                        <!-- <label for="confirm_email">Confirmar correo electrónico</label> -->
                                                        <input type="email" placeholder="CONFIRMACIÓN" class="campos-input-login-REGISTER" name="example-email" id="confirm_email" style="background: url('icons/register/email.png');background-repeat: no-repeat;
                                                                background-position: 15px center;
                                                                background-size: 20px;
                                                                display: flex;
                                                                align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                    <div class="form-group">
                                                        <!-- <label for="address_destiny_company">Pais</label> -->
                                                        <select id="address_destiny_company" class="campos-input-login-REGISTER" style="background: url('icons/register/tierra.png');background-repeat: no-repeat;
                                                                background-position: 15px center;
                                                                background-size: 20px;
                                                                display: flex;
                                                                align-items: center; padding-left: 45px;">
                                                            <option value="0">PAÍS</option>
                                                            <?php
                                                            foreach ($allcountriesRegister as $key => $value) {
                                                            ?>
                                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?>
                                                                </option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 address_state_company">
                                                    <div class="form-group">
                                                        <!-- <label for="address_state_company">Estado</label> -->
                                                        <input id="address_state_company" type="text" placeholder="ESTADO" class="campos-input-login-REGISTER" style="background: url('icons/register/tierra.png');background-repeat: no-repeat;
                                                                background-position: 15px center;
                                                                background-size: 20px;
                                                                display: flex;
                                                                align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 select_state_company" style="display: none;">
                                                    <div class="form-group">
                                                        <!-- <label for="select_state_company">Estado</label> -->
                                                        <select id="select_state_company" class="campos-input-login-REGISTER" style="background: url('icons/register/tierra.png');background-repeat: no-repeat;
                                                                background-position: 15px center;
                                                                background-size: 20px;
                                                                display: flex;
                                                                align-items: center; padding-left: 45px;">
                                                            <option value="0">ESTADO</option>
                                                            <?php
                                                            foreach ($allAddressStates as $key => $value) {
                                                            ?>
                                                                <option value="<?php echo $value['id_estado']; ?>">
                                                                    <?php echo $value['estado']; ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 address_city_company">
                                                    <div class="form-group">
                                                        <!-- <label for="address_city_company">Ciudad</label> -->
                                                        <input id="address_city_company" type="text" placeholder="CIUDAD" class="campos-input-login-REGISTER" style="background: url('icons/register/tierra.png');background-repeat: no-repeat;
                                                                background-position: 15px center;
                                                                background-size: 20px;
                                                                display: flex;
                                                                align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 select_city_company" style="display: none;">
                                                    <div class="form-group">
                                                        <!-- <label for="select_city_company">Ciudad</label> -->
                                                        <select id="select_city_company" class="campos-input-login-REGISTER" style="background: url('icons/register/tierra.png');background-repeat: no-repeat;
                                                                background-position: 15px center;
                                                                background-size: 20px;
                                                                display: flex;
                                                                align-items: center; padding-left: 45px;">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 address_address_company">
                                                    <div class="form-group">
                                                        <!-- <label for="address_company">Dirección</label> -->
                                                        <input id="address_company" type="text" placeholder="DIRECCIÓN" class="campos-input-login-REGISTER" style="background: url('icons/register/ubicacion.png');background-repeat: no-repeat;
                                                                background-position: 15px center;
                                                                background-size: 20px;
                                                                display: flex;
                                                                align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 select_address_company" style="display: none;">
                                                    <div class="form-group">
                                                        <!-- <label for="select_address_company">Dirección</label> -->
                                                        <input id="select_address_company" type="text" placeholder="DIRECCIÓN" class="campos-input-login-REGISTER" style="background: url('icons/register/ubicacion.png');background-repeat: no-repeat;
                                                                background-position: 15px center;
                                                                background-size: 20px;
                                                                display: flex;
                                                                align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group">
                                                        <input id="company_password" placeholder="CONTRASEÑA" class="campos-input-login-REGISTER" style="background: url('icons/register/password.png');background-repeat: no-repeat;
                                                            background-position: 15px center;
                                                            background-size: 20px;
                                                            display: flex;
                                                            align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <div class="form-group">
                                                        <input id="company_confirm_password" placeholder="CONFIRMA TU CONTRASEÑA" class="campos-input-login-REGISTER" style="background: url('icons/register/password.png');background-repeat: no-repeat;
                                                                background-position: 15px center;
                                                                background-size: 20px;
                                                                display: flex;
                                                                align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2"></div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                    <div class="form-group">
                                                        <!-- <label for="company_phone">Teléfono de Contacto</label> -->
                                                        <input id="company_phone" type="text" placeholder="TELF. CONTACTO" class="campos-input-login-REGISTER" style="background: url('icons/register/celular.png');background-repeat: no-repeat;
                                                                background-position: 15px center;
                                                                background-size: 20px;
                                                                display: flex;
                                                                align-items: center; padding-left: 45px;">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                    <div class="form-group">
                                                        <!-- <label for="contact">Como supiste de nosotros</label> -->
                                                        <select id="contact" class="campos-input-login-REGISTER" style="
                                                                display: flex;
                                                                align-items: center; padding-left:20px;">
                                                            <option value="0">COMO SUPISTE DE NOSOTROS</option>
                                                            <option value="2">Facebook</option>
                                                            <option value="3">Instagram</option>
                                                            <option value="4">Twitter</option>
                                                            <option value="5">Radio/Televisión</option>
                                                            <option value="6">Referido</option>
                                                            <option value="7">Otros</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2"></div>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                                                <label class="form-check-label" for="flexCheckChecked">
                                                    Acepto terminos y condiciones
                                                </label>
                                            </div>
                                            <br>
                                            <div class="form-group mb-4 text-center">
                                                <div class="col-sm-12">
                                                    <button onclick="register(1);" class="boton-total-envios-registro"><strong>REGISTRAR</strong></button>
                                                </div>
                                                <div align="center"><a href="login.php" class="footer-text" style="color:black;">¿Ya
                                                        tienes una cuenta? Ingresa aquí</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                        <div style="padding-top:150px;"><img src="icons/register/IMAGEN-REGISTER.png" style="width:500px; height:auto;" /></div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- </div>
                                                </div>
                                            </div>
                                                                                   
                                            </div> -->
                        <!-- <div class="col-sm-12 col-md-4 col-lg-2 col-xl-2"></div>
        </div>    -->
                        <!-- ============================================================== -->
                        <!-- End Container fluid  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- footer -->
                        <!-- ============================================================== -->
                        <!-- <footer class="footer text-center"> 2021 © Total Envios <a
                    href="https://www.wrappixel.com/"></a>
            </footer> -->
                        <!-- ============================================================== -->
                        <!-- End footer -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- End Page wrapper  -->
                        <!-- ============================================================== -->
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Wrapper -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- All Jquery -->
                    <!-- ============================================================== -->
                    <!-- Bootstrap tether Core JavaScript -->
                    <script src="bootstrap/dist/js/bootstrap.bundle.min.js"></script>
                    <script src="js/app-style-switcher.js"></script>
                    <!--Wave Effects -->
                    <script src="js/waves.js"></script>
                    <!--Menu sidebar -->
                    <script src="js/sidebarmenu.js"></script>
                    <!--Custom JavaScript -->
                    <script src="js/custom.js"></script>
                    <script src="js/main.js"></script>


                   
</body>

</html>