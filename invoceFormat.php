<?php
	require './vendor/autoload.php';

	include 'includes/connection.class.php';
	include 'includes/warehouses.class.php';
	include 'includes/packages.class.php';
    include 'includes/invoices.class.php';
	include 'includes/users.class.php';
    include 'libraries/phpqrcode/qrlib.php';

	use Spipu\Html2Pdf\Html2Pdf;

    $invoices = new Invoices;
    $warehouse = new Warehouses;
    $invoice_id = $_REQUEST['id'];
    /**CONSULTAR DATOS DE ENCABEZADO */ 
    $invoice_data = $invoices->findById($invoice_id);
    $tripData = $warehouse->findTripName($invoice_data['type_trip_id']);


    $userData = $warehouse->listUserDataToReceive($invoice_data['user_id']);
	$addresseeData = $warehouse->listAddresseeDataToReceive($invoice_data['addresse_id']);
	$cityData =$warehouse->findcityName($addresseeData[0]['city_id']);
	$stateData = $warehouse->findstateName($addresseeData[0]['state_id']);
	$countryData = $warehouse->findcountryName($addresseeData[0]['country_id']);
    $countryData =$warehouse->findcountryName($addresseeData[0]['address']);

    $boxesForInvoices = $warehouse->boxesForInvoice($invoice_id);

    $extracost = $warehouse->extraCostForInvoice($invoice_id);

    if(count($extracost) > 0){
        $extracost = $extracost;
        $existcost = 1;

    }else{
        $existcost = 0;
    }

   

    /*$codesDir = "./";   
    $codeFile = date('d-m-Y-h-i-s').'.png';
    QRcode::png("FAC-0010", $codesDir.$codeFile,"H", 2.5); */
    // echo '<img class="img-thumbnail" src="'.$codesDir.$codeFile.'" />';
       


	
	// Variable para el conteo total de cajas para ese warehouse
	
		try {

	    
			$html2pdf = new Html2Pdf('p','A4','en',true,'UTF-8',array(20,20,50,5));
			$html2pdf->setTestTdInOnePage(false);
			$html2pdf->writeHTML('
         
            <div style=width:100%;">
			<table class="default">
			<tr>
				<td style="width:200px;"><div style="font-size:8px; color:gray;"><img src="./logo-invoice.png"/><br><label>10625 nw 122st, Medley, Florida.33178. Estados Unidos</label><br><label>1 754-236-2131/58 4148898004</label><br><label>www.totalenvios.com</label><br><label></label>ICONOS</div></td>
				<td style="width:190px;"><div></div></td>
               
				<td style="width:130px;"><div><label style="font-size:16px;"><div style="width:136px; height:50px; background:Gainsboro;"><br>FACTURA</div></label><label style="color:red; font-size:18px;">FAC-'.$invoice_id.'</label></div></td>
			</tr>
			</table>
            <table class="default">
			<tr>
				<td><div style="width:390px;"></div></td>
                <td><div style="width:215px;">
                    <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:100px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>Tipo</div></td>
                        <td style="width:100px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>'.$tripData[0]['name'].'</div></td>
                    </tr>
                    <tr>
                        <td style="width:100px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>Fecha de salida:</div></td>
                        <td style="width:100px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>'.$invoice_data['fecha_salida_wh'].'</div></td>
                    </tr>
                    <tr>
                        <td style="width:100px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>Cliente</div></td>
                        <td style="width:100px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>'.$userData[0]['code'].'</div></td>
                    </tr>
                    <tr>
                        <td style="width:100px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>Fecha de vencimiento</div></td>
                        <td style="width:100px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>'.$invoice_data['fecha_vencimiento'].'</div></td>
                    </tr>
                    <tr>
                        <td style="width:100px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>#BL</div></td>
                        <td style="width:100px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>5151</div></td>
                    </tr>
                    <tr>
                        <td style="width:100px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>Agencia</div></td>
                        <td style="width:100px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div >Total envios USA</div></td>
                    </tr>
                    </table>
                    </div>
                </td>
				
			</tr>
			</table>
            
            <table class="default">
			<tr>
            <td><div style="width:204px;">REMITENTE<br>'.$userData[0]['name'].' '.$userData[0]['lastname'].'<br>'.$userData[0]['address'].','.$userData[0]['country'].','.$userData[0]['state_name'].','.$userData[0]['city_name'].'<br>'.$userData[0]['phone'].'<br>'.$userData[0]['email'].'</div></td>
            <td><div style="width:180px;"></div></td>
            <td><div style="width:215px;">DESTINATARIO<br>'.$addresseeData[0]['name'].' '.$addresseeData[0]['lastname'].'<br>'.$addresseeData[0]['address'].','.$addresseeData[0]['country_name'].','.$addresseeData[0]['state_name'].','.$addresseeData[0]['city_name'].'<br>'.$addresseeData[0]['contact_phone'].'</div></td>
			</tr>
			</table>
            </div><br><br>');
            // Encabezado del recibo
            $html2pdf->writeHTML('<div style="width:100%;">
            <table cellpadding="0" cellspacing="0">
			<tr>
				<td valign="middle" style="width: 30px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div align="center"><label>ITEM</label></div></td>
                <td valign="middle" style="width: 36px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: gray; font-size:10px;"><div align="center"><label># BOX</label></div></td>
				<td valign="middle" style="width: 100px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div align="center"><label>DESCRIPCION</label></div></td>
                <td valign="middle" style="width: 38px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div><label>LARGO</label></div></td>
				<td valign="middle" style="width: 37px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: gray; font-size:10px;"><div align="center"><label>ANCHO</label></div></td>
                <td valign="middle" style="width: 32px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div><label>ALTO</label></div></td>
				<td valign="middle" style="width: 37px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: gray; font-size:10px;"><div align="center"><label>PESOLB</label></div></td>
                <td valign="middle" style="width: 40px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div><label>LBVOL</label></div></td>
				<td valign="middle" style="width: 42px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div><label>PIE3</label></div></td>
				<td valign="middle" style="width: 29px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div><label>CANT.</label></div></td>
                <td valign="middle" style="width: 53px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div align="center" ><label>PRECIO</label></div></td>
                <td valign="middle" style="width: 54px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div align="center" ><label>TOTAL</label></div></td>
				
			</tr>
			</table>
            </div>');
            // Generando contenido de la factura

            $total_largo = 0;
            $total_ancho = 0;
            $total_alto = 0;
            $total_peso = 0;
            $total_lib_vol = 0;
            $total_pie_cub = 0;

            foreach ($boxesForInvoices as $key => $value) {
            $item = $key + 1; 
            
            $total_largo = $total_largo + $value['lenght'];
            $total_ancho = $total_ancho + $value['width'];
            $total_alto = $total_alto + $value['height'];
            $total_peso = $total_peso + $value['weight'];
            $total_lib_vol = $total_lib_vol + $value['lb_vol'];
            $total_pie_cub = $total_pie_cub + $value['cubic_feet'];

            $html2pdf->writeHTML('<div style="border-color: grey; width:90%">
            <table cellpadding="0" cellspacing="0">
			<tr>
            <td valign="middle" style="width: 30px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div align="center"><label>'.$item.'</label></div></td>
            <td valign="middle" style="width: 36px; height:20px; border-width: 1px; border-style: solid; border-color: gray; font-size:8px;"><div align="center"><label>'.$value['id_box'].'</label></div></td>
            <td valign="middle" style="width: 100px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div align="center"><label>WH-'.$value['id_warehouse'].'<br>'.$value['description'].'</label></div></td>
            <td valign="middle" style="width: 38px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$value['lenght'].'</label></div></td>
            <td valign="middle" style="width: 37px; height:20px; border-width: 1px; border-style: solid; border-color: gray; font-size:8px;"><div align="center"><label>'.$value['height'].'</label></div></td>
            <td valign="middle" style="width: 32px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$value['width'].'</label></div></td>
            <td valign="middle" style="width: 37px; height:20px; border-width: 1px; border-style: solid; border-color: gray; font-size:8px;"><div align="center"><label>'.$value['weight'].'</label></div></td>
            <td valign="middle" style="width: 40px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$value['lb_vol'].'</label></div></td>
            <td valign="middle" style="width: 42px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$value['cubic_feet'].'</label></div></td>
            <td valign="middle" style="width: 29px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>1</label></div></td>
            <td valign="middle" style="width: 53px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div align="center" ><label>'.$value['cost'].'</label></div></td>
            <td valign="middle" style="width: 54px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div align="center" ><label>'.$value['cost_total'].'</label></div></td>
			</tr>
			</table>
            </div>');
            };
            $html2pdf->writeHTML('<div style="border-color: grey; width:90%">
            <table cellpadding="0" cellspacing="0">
			<tr>
            <td style="width: 182px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div align="center"><label>TOTAL GENERAL</label></div></td>
       
            <td style="width: 38px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$total_largo.'</label></div></td>
            <td style="width: 37px; height:20px; border-width: 1px; border-style: solid; border-color: gray; font-size:8px;"><div align="center"><label>'.$total_alto.'</label></div></td>
            <td style="width: 32px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$total_ancho.'</label></div></td>
            <td style="width: 37px; height:20px; border-width: 1px; border-style: solid; border-color: gray; font-size:8px;"><div align="center"><label>'.$total_peso.'</label></div></td>
            <td style="width: 40px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$total_lib_vol.'</label></div></td>
            <td style="width: 42px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$total_pie_cub.'</label></div></td>
            
			</tr>
			</table>
            </div>');

            if($existcost == 1) {
                $html2pdf->writeHTML('<br><br><div style="width:100%;">
                <table cellpadding="0" cellspacing="0">
                <tr>
                   
                    <td style="width: 100px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: gray; font-size:10px;"><div align="center"><label>SERVICIO(S) EXTRA</label></div></td>
                    <td style="width: 50px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div align="center"><label>COSTO</label></div></td>
                   
                    
                </tr>
                </table>
                </div>');

                foreach ($extracost as $key => $value) {
                 
                $html2pdf->writeHTML('<div style="border-color: grey; width:90%">
                <table cellpadding="0" cellspacing="0">
                <tr>
                
                <td style="width: 100px; height:20px; border-width: 1px; border-style: solid; border-color: gray; font-size:8px;"><div align="center"><label>'.$value['name'].'</label></div></td>
                <td style="width: 50px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div align="center"><label>'.$value['value_extracost'].'</label></div></td>
                
                </tr>
                </table>
                </div>');
                };

                $html2pdf->writeHTML('
            <page_footer><div style="border-color: grey; width:70%">
            <table class="default">
            <tr>
                <td style="width: 336px; height:30px;">Términos<hr style="width: 336px;></td>
                <td></td>
                
            </tr>
            </table>
            <table class="default">
            <tr>
                <td><div style="width: 445px; height:20px;"><label style="color:red; font-size:8px;"> - Total Envios se responsabiliza  hasta un valor de USD $200.00 sobre los envíos no asegurados. Con el envío de<br> esta
                carga el cliente entiende la situación aduanera y de seguridad de envíos de carga a Venezuela es muy inestable<br> y, por
                consiguiente, la mercancía puede estar en riesgo.</label><br>
                <label style="color:black; font-size:8px;">- Costo seguro 5% de valor declarado.</label><br>
                <label style="color:red; font-size:8px;">- La mercancia debe ser pagada antes de ser enviada a destino.</label><br>
                <label style="color:black; font-size:8px;">- Los pesos promedio por caja es de un máximo de 80 Lbs. Cajas que sobrepase los pesos permitidos, la empresa no<br> se hace
                responsable por daños en el manejo de su carga.</label><br>
                <label style="color:black; font-size:8px;">- En el flete viene incluido el despacho en a Venezuela, por lo que advertimos los choferes no están autorizados a cobrar<br>ningún costo adicional.</label><br>
                <label style="color:black; font-size:8px;">- La empresa no se hace responsable por articulos golpeados y maltratados enviados por vía marítima.</label><br>
                <label style="color:black; font-size:8px;">- Carga mínima marítima 40$ y aerea 30$.</label>
                </div></td>
                <td style="width: 184px;><div>
                    <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:90px; font-size:8px"><div style="text-align:right;">SubTotal</div></td>
                        <td style="width:54px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>'.$invoice_data['total_invoce'].'</div></td>
                    </tr>
                    <tr>
                        <td style="width:90px; font-size:8px"><div style="text-align:right;">Descuento</div></td>
                        <td style="width:54px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>0</div></td>
                    </tr>
                    <tr>
                        <td style="width:90px; font-size:8px"><div style="text-align:right;">Subtotal/descuento</div></td>
                        <td style="width:54px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>'.$invoice_data['total_invoce'].'</div></td>
                    </tr>
                    <tr>
                        <td style="width:90px; font-size:8px"><div style="text-align:right;">TAX</div></td>
                        <td style="width:54px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>0</div></td>
                    </tr>
                    <tr>
                        <td style="width:90px; font-size:8px"><div style="text-align:right;">TIPO DE PAGO</div></td>
                        <td style="width:54px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>0</div></td>
                    </tr>
                    <tr>
                        <td style="width:90px; font-size:8px"><div style="text-align:right;">TOTAL</div></td>
                        <td style="width:54px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div >'.$invoice_data['total_invoce'].'</div></td>
                    </tr>
                    </table>
                
                </div></td>
                
            </tr>
            </table>
            </div></page_footer>
            ');
        }else{

            $html2pdf->writeHTML('
            <page_footer><div style="border-color: grey; width:70%">
            <table class="default">
            <tr>
                <td style="width: 336px; height:30px;">Términos<hr style="width: 336px;></td>
                <td></td>
                
            </tr>
            </table>
            <table class="default">
            <tr>
                <td><div style="width: 445px; height:20px;"><label style="color:red; font-size:8px;"> - Total Envios se responsabiliza  hasta un valor de USD $200.00 sobre los envíos no asegurados. Con el envío de<br> esta
                carga el cliente entiende la situación aduanera y de seguridad de envíos de carga a Venezuela es muy inestable<br> y, por
                consiguiente, la mercancía puede estar en riesgo.</label><br>
                <label style="color:black; font-size:8px;">- Costo seguro 5% de valor declarado.</label><br>
                <label style="color:red; font-size:8px;">- La mercancia debe ser pagada antes de ser enviada a destino.</label><br>
                <label style="color:black; font-size:8px;">- Los pesos promedio por caja es de un máximo de 80 Lbs. Cajas que sobrepase los pesos permitidos, la empresa no<br> se hace
                responsable por daños en el manejo de su carga.</label><br>
                <label style="color:black; font-size:8px;">- En el flete viene incluido el despacho en a Venezuela, por lo que advertimos los choferes no están autorizados a cobrar<br>ningún costo adicional.</label><br>
                <label style="color:black; font-size:8px;">- La empresa no se hace responsable por articulos golpeados y maltratados enviados por vía marítima.</label><br>
                <label style="color:black; font-size:8px;">- Carga mínima marítima 40$ y aerea 30$.</label>
                </div></td>
                <td style="width: 184px;><div>
                    <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:90px; font-size:8px"><div style="text-align:right;">SubTotal</div></td>
                        <td style="width:54px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>'.$invoice_data['total_invoce'].'</div></td>
                    </tr>
                    <tr>
                        <td style="width:90px; font-size:8px"><div style="text-align:right;">Descuento</div></td>
                        <td style="width:54px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>0</div></td>
                    </tr>
                    <tr>
                        <td style="width:90px; font-size:8px"><div style="text-align:right;">Subtotal/descuento</div></td>
                        <td style="width:54px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>'.$invoice_data['total_invoce'].'</div></td>
                    </tr>
                    <tr>
                        <td style="width:90px; font-size:8px"><div style="text-align:right;">TAX</div></td>
                        <td style="width:54px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>0</div></td>
                    </tr>
                    <tr>
                        <td style="width:90px; font-size:8px"><div style="text-align:right;">TIPO DE PAGO</div></td>
                        <td style="width:54px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>0</div></td>
                    </tr>
                    <tr>
                        <td style="width:90px; font-size:8px"><div style="text-align:right;">TOTAL</div></td>
                        <td style="width:54px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px"><div>'.$invoice_data['total_invoce'].'</div></td>
                    </tr>
                    </table>
                
                </div></td>
                
            </tr>
            </table>
            </div></page_footer>
            ');
        };
           
           
			$html2pdf->Output('invoice.pdf');
		} catch (PDOException $e) {
			echo $e;
		}
	
?>