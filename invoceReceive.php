<?php
	require './vendor/autoload.php';

	include 'includes/connection.class.php';
	include 'includes/warehouses.class.php';
	include 'includes/packages.class.php';
    include 'includes/invoices.class.php';
	include 'includes/users.class.php';
    include 'libraries/phpqrcode/qrlib.php';

	use Spipu\Html2Pdf\Html2Pdf;

    $wh_id = $_REQUEST['id'];
    // echo $wh_id;
    $transport = new Warehouses;
    $count = $transport->countwarehousesBox(30);
	$val = ((int)$count['cnt'])-1;

	$whData = $transport->listWarehouseData($wh_id);
    $boxid = $transport->boxbywarehouse($wh_id)['id_box'];

    $boxesForInvoices= $transport->listBoxbyId($boxid);
   
    foreach ($whData as $key => $value) {
        $paquete= $value['code'];
        $salida= $value['date_send'];
        $cliente= $value['id_from'];
        $remitente=$value['client_name'];
        // echo $value['code'];
    }

	$userData = $transport->listUserDataToReceive($whData[0]['id_from']);
	$addresseeData = $transport->listAddresseeDataToReceive($whData[0]['id_to']);
	$tripData = $transport->findTripName($whData[0]['trip_id']);
	$cityData = $transport->findcityName($addresseeData[0]['city_id']);
	$stateData = $transport->findstateName($addresseeData[0]['state_id']);
	$countryData = $transport->findcountryName($addresseeData[0]['country_id']);
	$total = 0;

    if( $whData[0]['trip_id'] == 1){
        $transport_list = $transport->listBoxWarehouseIdForAir($wh_id);
    }else{
        $transport_list = $transport->listBoxWarehouseIdForOcean($wh_id);
    }
	

		$response = array(
			'warehouse'=> $whData[0],
			'cliente'=> $userData[0],
			'destinatario'=> $addresseeData[0],
			'envio'=> $tripData[0],
			'ciudad' => $cityData[0],
			'estado' => $stateData[0],
			'pais' => $countryData[0],
			'boxes' => $transport_list
		);

        // foreach ($response as $key => $value) {
        //     echo $value['warehouse'];
        // }
		
		$trip_type = strtoupper($tripData[0]['name']);

   

    $codesDir = "./";   
    $codeFile = date('d-m-Y-h-i-s').'.png';
    QRcode::png("FAC-0010", $codesDir.$codeFile,"H", 2.5); 
    // echo '<img class="img-thumbnail" src="'.$codesDir.$codeFile.'" />';
       


	
	// Variable para el conteo total de cajas para ese warehouse
	
		try {

	    
			$html2pdf = new Html2Pdf('p','A4','en',true,'UTF-8',array(20,20,50,5));
			$html2pdf->setTestTdInOnePage(false);
            $html2pdf->writeHTML('
            <div style="width:100%;">
               <table style="width: 100%; padding-top:-10px;">
                    <tr>
                        <td style="text-align: left;width=33%;">
                            <div style="font-size:8px; color:gray;"><img src="./logo-invoice.png"/><br>
                            <label>10625 nw 122st, Medley, Florida.33178. Estados Unidos</label><br><label>1 754-236-2131/58 4148898004</label><br>
                            <label>www.totalenvios.com</label><br><label></label>ICONOS</div>
                        </td>
                        <td style="text-align: center;width=33%; padding-top:-5px;"></td>
                        <td style="text-align: left;width=33%;">
                            <div style="background-color:lightgrey; width:100px;"><h5 style="font-size:15px; margin-left:5px;">PAQUETE</h5></div> 
                            <p style="margin:0; color:red; font-weight:bold;">'.$paquete.'</p>
                        </td>
                    </tr>
                    <tr>
                        <td style=" width:33%;"></td>
                        <td style=" width:33%;"></td>
                        <td style="width:33%;">
                        <span style="border:2px solid lightgrey; ">Salida: </span>
                        <span style="border:2px solid lightgrey; font-size:10px;">'.$salida.'</span>
                        </td>
                    </tr>
                    <tr>
                    <td style=" width:33%;"></td>
                    <td style=" width:33%;"></td>
                    <td style="width:33%;">
                        <span style="border:2px solid lightgrey;">Cliente: </span>
                        <span style="border:2px solid lightgrey; font-size:10px;">'.$cliente.'</span>
                    </td>
                    </tr>
                    <tr>
                    <td style=" width:33%;"></td>
                    <td style=" width:33%;"></td>
                    <td style="width:33%;">
                        <span style="border:2px solid lightgrey;">Agencia: </span>
                        <span style="border:2px solid lightgrey; font-size:12px;">TOTAL ENVIO USA</span>
                    </td>
                    </tr>
                    <tr>
                        <td>REMITENTE: </td>
                    </tr>
                    <tr>
                        <td>'.$remitente.'</td>
                    </tr>
                </table>
            </div>
              
            ');
            $html2pdf->writeHTML('<div style="width:100%;">
            <table cellpadding="0" cellspacing="0">
			<tr>
				<td style="width: 30px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div align="center"><label>ITEM</label></div></td>
                <td style="width: 36px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: gray; font-size:10px;"><div align="center"><label># BOX</label></div></td>
				<td style="width: 100px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div align="center"><label>DESCRIPCION</label></div></td>
                <td style="width: 38px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div><label>LARGO</label></div></td>
				<td style="width: 37px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: gray; font-size:10px;"><div align="center"><label>ANCHO</label></div></td>
                <td style="width: 32px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div><label>ALTO</label></div></td>
				<td style="width: 37px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: gray; font-size:10px;"><div align="center"><label>PESOLB</label></div></td>
                <td style="width: 40px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div><label>LBVOL</label></div></td>
				<td style="width: 42px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div><label>PIE3</label></div></td>
				<td style="width: 29px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div><label>CANT.</label></div></td>
                <td style="width: 53px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div align="center" ><label>PRECIO</label></div></td>
                <td style="width: 54px; height:30px; background-color:lightgrey; border-width: 1px; border-style: solid; border-color: grey; font-size:10px;"><div align="center" ><label>TOTAL</label></div></td>
				
			</tr>
			</table>
            </div>');
            // Generando contenido de la factura

            $total_largo = 0;
            $total_ancho = 0;
            $total_alto = 0;
            $total_peso = 0;
            $total_lib_vol = 0;
            $total_pie_cub = 0;
            $cost_Total=0;
            $total=1;

            foreach($boxesForInvoices as $key => $value){
                $item = $key + 1; 

                $total_largo = $total_largo + $value['lenght'];
                $total_ancho = $total_ancho + $value['width'];
                $total_alto = $total_alto + $value['height'];
                $total_peso = $total_peso + $value['weight'];
                $total_lib_vol = $total_lib_vol + $value['lb_vol'];
                $total_pie_cub = $total_pie_cub + $value['cubic_feet'];
                $cost_Total = $cost_Total + $value['cost_total'];
         

            $html2pdf->writeHTML('<div style="border-color: grey; width:90%">
            <table cellpadding="0" cellspacing="0">
			<tr>
                <td style="width: 30px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;">'.$item.'</td>
                <td style="width: 36px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;">'.$boxid.'</td>
                <td style="width: 100px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;">'.$value['description'].'</td>
                <td style="width: 38px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;">'.$value['lenght'].'</td>
                <td style="width: 37px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;">'.$value['width'].'</td>
                <td style="width: 32px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;">'.$value['height'].'</td>
                <td style="width: 37px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;">'.$value['weight'].'</td>
                <td style="width: 40px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;">'.$value['lb_vol'].'</td>
                <td style="width: 42px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;">'.$value['cubic_feet'].'</td>
                <td style="width: 29px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;">'.$item.'</td>
                <td style="width: 53px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;">'.$value['cost_total'].'</td>
                <td style="width: 54px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;">'.$value['cost_total'] * $item.'</td>
            </tr>
			</table>
            </div>');
            
            $html2pdf->writeHTML('<div style="border-color: grey; width:90%">
            <table cellpadding="0" cellspacing="0">
			<tr>
            <td style="width: 182px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div align="center"><label>TOTAL GENERAL</label></div></td>
       
            <td style="width: 38px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$total_largo.'</label></div></td>
            <td style="width: 37px; height:20px; border-width: 1px; border-style: solid; border-color: gray; font-size:8px;"><div align="center"><label>'.$total_ancho.'</label></div></td>
            <td style="width: 32px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$total_alto.'</label></div></td>
            <td style="width: 37px; height:20px; border-width: 1px; border-style: solid; border-color: gray; font-size:8px;"><div align="center"><label>'.$total_peso.'</label></div></td>
            <td style="width: 40px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$total_lib_vol.'</label></div></td>
            <td style="width: 42px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$total_pie_cub.'</label></div></td>
            <td style="width: 29px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$item.'</label></div></td>
            <td style="width: 53px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$cost_Total.'</label></div></td>
			<td style="width: 54px; height:20px; border-width: 1px; border-style: solid; border-color: grey; font-size:8px;"><div><label>'.$cost_Total * $item.'</label></div></td>
            </tr>
			</table>
            </div>');

        }
           
           
			$html2pdf->Output('invoice.pdf');
		} catch (PDOException $e) {
			echo $e;
		}
	
?>