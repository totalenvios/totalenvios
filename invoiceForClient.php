<?php  

include 'includes/connection.class.php';
include 'includes/invoices.class.php';
include 'includes/packages.class.php';
include 'includes/users.class.php';
include 'includes/warehouses.class.php';

$client = $_REQUEST['client_name'];
$client_id = $_REQUEST['client_id'];
$trip = $_REQUEST['trip_type'];
$date_send = $_REQUEST['send_date'];
$id_destiner_invoice = $_REQUEST['id_destiner_invoice'];
$destiner_invoice = $_REQUEST['destiner_invoice'];

    $invoices = new Invoices;
    $allInvoices = $invoices->findWarehouseForClient($client,$trip,$date_send,$destiner_invoice);




$warehouses = new Warehouses;
$allCostExtra =  $warehouses->getExtraGastos();

session_start();
if ($_SESSION['state'] != 1) {
    header('Location: login.php');
}
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

    include 'head.php';

?>
</head>

<body>
    <!-- Modal -->
    <div class="modal fade" id="addInvoice" tabindex="-1" role="dialog" aria-labelledby="addInvoiceLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addInvoiceLabel">Datos:</h5>
                    <button onclick="$('#addInvoice').modal('hide');" type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="alert alert-danger addInvoice-error" role="alert">
                                </div>
                                <div class="alert alert-success register-success" role="alert">
                            </div>
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0"># Factura *</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <input id="fact" readonly="readonly" type="text" placeholder="# Factura"
                                                    class="form-control p-0 border-0"
                                                    value="<?php echo ($invoices->findLastId()['id']+1); ?>">
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0">Warehouse</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <select id="warehouse"
                                                    class="form-select shadow-none p-0 border-0 form-control-line">
                                                    <option value="0">Seleccionar...</option>
                                                    <?php  
                                        foreach ($allWarehouses as $key => $value) {
                                            ?>
                                                    <option value="<?php echo $value['id']; ?>">
                                                        <?php echo $value['id']; ?></option>
                                                    <?php
                                        }
                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0">Cliente</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <select id="client"
                                                    class="form-select shadow-none p-0 border-0 form-control-line">
                                                    <option value="0">Seleccionar...</option>
                                                    <?php  
                                        foreach ($allClients as $key => $value) {
                                            ?>
                                                    <option value="<?php echo $value['id']; ?>">
                                                        <?php echo $value['name']; ?></option>
                                                    <?php
                                        }
                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0">Fecha *</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <input id="date_i" type="date" placeholder="Fecha"
                                                    class="form-control p-0 border-0"
                                                    value="<?php echo date('Y-m-d 00:00:00'); ?>">
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <div class="col-sm-12">
                                                <button onclick="addInvoice();" class="btn btn-success">Agregar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
        <?php 
        include 'navbar.php'; 
    ?>

        <div class="page-wrapper" style="background:#e3e3e3ff;">




            <div class="row">

            </div>


            <div class="container-fluid">
                <h5 style="color:#005574; font-weight:600;">WAREHOUSE A FACTURAR</h5>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <h6 style="color:#005574;">Cliente:</h6>
                                <p><?php echo $client; ?></p><br>
                            </div>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <h6 style="color:#005574;">Tipo de viaje:</h6>
                                <p><?php if($trip==1){
                        ?>
                                    AÉREO
                                    <?php
                        }else{
                        ?>
                                    MARÍTIMO
                                    <?php

                        } ?> </p>

                            </div>
                        </div>

                    </div>
                </div>



                <div class="card">
                    <div class="card-body">





                        


                    
                    <br>

                    <div class="row">
                        <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                        <div class_="col-lg-10 col-xlg-10 col-md-10">
                            <table class="table table-warehouse"
                                style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                <thead>
                                    <th style="border: 2px solid #e3e3e3ff;">SELECCIONAR</th>
                                    <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                        WAREHOUSE</th>
                                    <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                        CASILLERO</th>
                                    <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                        DESTINATARIO</th>
                                    <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                        DESTINO</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                        CANTIDAD DE CAJAS</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                        COSTO CAJAS</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                        PIE CÚBICO</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                        LB VOL</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                        ANCHO</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                        ALTO</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                        LARGO</th>





                                </thead>
                                <tbody>
                                    <?php  

                                            foreach ($allInvoices as $key => $value) {
                                        ?>
                                    <tr>

                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><input onclick="countChecksInvoices();" type="checkbox" class="form-check-input wcheck wcheck<?php echo $value['id'];?>" id="wcheck<?php echo $value['id'];?>" warehouse="<?php echo $value['id'];?>"></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['code']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            TE-<?php echo $value['id_from']; ?></td>



                                        <!-- <td><?php echo $value['casillero']; ?></td> -->
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['destiner_name']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $warehouses->findcityNameForwh($warehouses->findcityDestiny($value['id'])[0]['city_id'])[0]['name']; ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $warehouses->boxesForWarehouse($value['id'])[0]['countBox']; ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;" class="subtotal-box">
                                            <?php echo $warehouses->calculateCostBox($value['id'])[0]['SUMTOTAL']; ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $warehouses->calculatepie3($value['id'])[0]['TOTALPIE']; ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $warehouses->calculatelbvol($value['id'])[0]['TOTALLBVOL']; ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $warehouses->calculateancho($value['id'])[0]['TOTALANCHO']; ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $warehouses->calculatealto($value['id'])[0]['TOTALALTO']; ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $warehouses->calculatelargo($value['id'])[0]['TOTALLARGO']; ?>
                                        </td>










                                    </tr>
                                    <?php
                                            }

                                        ?>
                                </tbody>
                            </table>
                            <div class="row">


                            <div class="col-sm-12 col-md-12 col-lg-12" style="text-align:right; content-align:right;">
                                <div class="btn-group" align="right">

                                    <a onclick="bulkinvoices();" class="btn btn-primary"
                                        style="background-color:white; color:#ff554dff; border:2px solid #ff554dff;"><i
                                            style="color:#ff554dff;" class="fas fa-check"></i> Generar factura</a>
                                </div>

                                <!-- <a align="right" href="registerWarehouse.php" class="btn" style="background-color:#ff554dff; color:white;"><i style="color:white;" class="fas fa-plus"></i> Crear</a> -->

                            </div>
                        </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-4 col-xlg-4 col-md-4">
                                    <div class="form-group">
                                        <label for="costo_extra_select" style="font-size:12px;"> <strong>COSTO EXTRA</strong> </label>
                                                <select id="costo_extra_select" class="form-select campos-input-login" style="height:40px !important;"> 
                                                <option value="0">Selecciona</option>
                                                    <?php  
                                                        foreach ($allCostExtra as $key => $value) {
                                                    ?>
                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                    <?php
                                                        }
                                                    ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-xlg-4 col-md-4">
                                    <div class="form-group">
                                        <label for="costo_extra_value" style="font-size:12px;"> <strong>COSTO</strong> </label>
                                            <input type="number" class="form-control campos-input-login" id="costo_extra_value">
                                            <input class="form-control campos-input-login" id="invoice_temporal" hidden>
                                            <input class="form-control campos-input-login" id="trip_type_id" value=" <?php echo $trip; ?>" hidden>
                                            <input class="form-control campos-input-login" id="client_name" value=" <?php echo $client; ?>" hidden>
                                            <input class="form-control campos-input-login" id="date_send" value=" <?php echo $date_send; ?>" hidden>
                                            <input class="form-control campos-input-login" id="client_id" value=" <?php echo $client_id; ?>" hidden>
                                            <input class="form-control campos-input-login" id="destiner_id" value=" <?php echo $id_destiner_invoice; ?>" hidden>
                                            <input class="form-control campos-input-login" id="destiner_name" value=" <?php echo $destiner_invoice; ?>" hidden>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-xlg-4 col-md-4 pt-4">
                                    <button class="btn btn-primary" id="agregar_costo_extra" onclick="cargarGastosExtras();"
                                        style="background-color:white; color:#ff554dff; border:2px solid #ff554dff;"><i
                                            style="color:#ff554dff;" class="fas fa-plus"></i>AGREGAR</button>
                                </div>
                                
                                    <button class="btn btn-primary" style="display:none;" id="editar_gastos_extras" onclick="editGastosExtras();"
                                        style="background-color:white; color:#ff554dff; border:2px solid #ff554dff;"><i
                                            style="color:#ff554dff;" class="fas fa-check"></i>EDITAR</button>
                                
                            </div>
                            <div class="row">
                            <input id="id_extra_cost" hidden>
                                <div class="col-lg-4 col-xlg-4 col-md-4 pt-4">
                                <table class="table table-extra-cost"
                                style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                
                                <th>SERVICIO</th>
                                <th>COSTO</th>
                                <th>ACCIÓN</th>
                                <!-- <th>% GANAN</th> -->
                                <!-- <th>Eliminar</th> -->
                                <tbody id="extra-cost-table">
                                    <tr></tr>
                                </tbody>
                            </table>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-10 col-xlg-10 col-md-10">
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                <div class="form-group">
                                        <label for="costo_extra_value" style="font-size:12px;"> <strong>SUBTOTAL CAJAS</strong> </label>
                                            <input type="number" class="form-control campos-input-login" value="0" id="subtotal_boxes">
                                    </div>
                                    <div class="form-group">
                                        <label for="subtotal_costo_extra_value" style="font-size:12px;"> <strong>SUBTOTAL COSTO EXTRA</strong> </label>
                                            <input type="number" class="form-control campos-input-login" value = "0" id="subtotal_costo_extra_value">
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label for="total_factura" style="font-size:12px;"> <strong>TOTAL A FACTURAR</strong> </label>
                                            <input type="number" class="form-control campos-input-login" id="total_factura" value = "0">
                                    </div>
                                </div>
                            </div>
                            

                            

                                                                                
                        </div>
                        <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                    </div>
                </div>
            </div>

            <footer class="footer text-center"> 2021 © Total Envios
            </footer>
        </div>

    </div>
    <?php 

    include 'foot.php';

?>
</body>

</html>