<?php
	require './vendor/autoload.php';

	include 'includes/connection.class.php';
	include 'includes/warehouses.class.php';
	include 'includes/packages.class.php';
	include 'includes/users.class.php';

	use Spipu\Html2Pdf\Html2Pdf;

    $subtotal = 0;
    $total_cost = 0;
    $wh_id = $_REQUEST['id'];
    $transport = new Warehouses;
    $count = $transport->countwarehousesBox(30);
    $allCostExtra =  $transport->getExtraGastos();
	$val = ((int)$count['cnt'])-1;

	$whData = $transport->listWarehouseData($wh_id);

	$userData = $transport->listUserDataToReceive($whData[0]['id_from']);
	$addresseeData = $transport->listAddresseeDataToReceive($whData[0]['id_to']);
	$tripData = $transport->findTripName($whData[0]['trip_id']);
	$cityData = $transport->findcityName($addresseeData[0]['city_id']);
	$stateData = $transport->findstateName($addresseeData[0]['state_id']);
	$countryData = $transport->findcountryName($addresseeData[0]['country_id']);

    if( $whData[0]['trip_id'] == 1){
        $transport_list = $transport->listBoxWarehouseIdForAir($wh_id);
        
    }else{
        $transport_list = $transport->listBoxWarehouseIdForOcean($wh_id);
    }
	// print_r($transport_list);
    // $test = 0;

    // foreach ($transport_list as $key => $value) {
    //     echo $value['cost_total'];

    // }




		$response = array(
			'warehouse'=> $whData[0],
			'cliente'=> $userData[0],
			'destinatario'=> $addresseeData[0],
			'envio'=> $tripData[0],
			'ciudad' => $cityData[0],
			'estado' => $stateData[0],
			'pais' => $countryData[0],
			'boxes' => $transport_list
		);


?>

<!DOCTYPE html>
    <html dir="ltr" lang="en">

    <head>
        <?php 

            include 'head.php';

        ?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    </head>
    <body>

<div id="main-wrapper" data-boxed-layout="full">
            <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
                }

            ?>
            
    
           <div style="width:100%;">
			<table class="default">
			<tr>
				<td><div style="width:302px; font-size:20px; margin-left:20px; font-weight:bold; color:grey; margin-top:40px;">Invoice</div></td>
			</tr>
			</table>
            <table class="default">
			<tr>
				<td><div style="width:400px; margin-top:20px; margin-left:20px; font-size:13px; color:grey;">10625 nw 122st, Medley, Florida.33178.<br>Estados Unidos<br>1 754-236-2131/58 4148898004<br><a>www.totalenvios.com</a><br>ICONOS</div></td>
                <td><div style="width:120px;"></div></td>
				<td><div style="width:302px; font-weight:bold; margin-top:10px;"><label style="font-size:25px; font-weight:bold;"> <?php echo $whData[0]['code']; ?></label><br> DATE:<?php echo $whData[0]['date_send']; ?><br>CUSTOMER ID: <?php echo $userData[0]['code']; ?><br>#BK:</div></td>
			</tr>
			</table>
            
            <table class="default">
			<tr>
				<td><div style="width:400px; margin-left:40px; margin-top:15px;"><label style="font-size:12px;">TYPE:<?php echo $tripData[0]['name'] ?></label><br><br><label style="font-size:15px; color:grey; text-decoration:underline; font-weight:bold;">BILL TO</label><br><br><label style="font-size:13px;"> <?php echo $userData[0]['name']; ?> , <?php echo $userData[0]['lastname']; ?></label><br><label style="font-size:13px; color:grey;"><?php echo $addresseeData[0]['address']; ?></label><br><label style="font-size:13px; color:grey;"><?php echo $countryData[0]['name'];?>,<?php echo $stateData[0]['estado']; ?> ,<?php echo $cityData[0]['ciudad']; ?></label></div></td>
                <td><div style="width:100px;"></div></td>
				<td><div style="width:302px; margin-top:45px;"><label style="font-size:15px; color:grey; text-decoration:underline; font-weight:bold;">SHIP TO</label><br><br><label style="font-size:13px;"> <?php echo $addresseeData[0]['name']; ?>,<?php echo $addresseeData[0]['lastname']; ?> </label><br><label style="font-size:13px; color:grey;"><?php echo $addresseeData[0]['address']; ?></label><br><label style="font-size:13px; color:grey;"><?php echo $countryData[0]['name']; ?>,<?php echo $stateData[0]['estado']; ?>,<?php echo $cityData[0]['ciudad'];?></label></div></td>
			</tr>
			</table>
            </div>
            <br><div style="border-width: 1px; margin-left:20px; width:70%">
            <table class="default">
			<tr>
				<td><div align="center" style="width: 50px; height:30px; border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px;">ITEM</label></div></td>
				<td><div align="center" style="width: 140px; height:30px; border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px;"># BOX</label></div></td>
				<td><div align="center" style="width: 200px; height:30px;border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px;">DESCRIPTION</label></div></td>
                <td><div align="center" style="width: 240px; height:30px; border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px;">CONTENIDO</label></div></td>
				<td><div align="center" style="width: 50px; height:30px; border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px;">QTY</label></div></td>
                <td><div align="center" style="width: 240px; height:30px; border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px;">UNIT PRICE</label></div></td>
				<td><div align="center" style="width: 240px; height:30px; border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px;">SUB - TOTAL</label></div></td>
                <td><div align="center" style="width: 240px; height:30px; border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px;">AGREGAR GASTOS</label></div></td>
			</tr>
			</table>
            </div>
            <?php
            foreach ($transport_list as $key => $value) {
             $subtotal = number_format(($subtotal + $value['cost_total']),2);    
             $item = $key+1; 
             $total = $key+1;
            ?>   
               <div style="border-width: 1px; margin-left:20px; width:70%">

                <table class="default">
                <tr>
                    <td><div align="center" style="width: 50px; height:50px; border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px; color:grey;"> <?php echo $item; ?></label></div></td>
                    <td><div align="center" style="width: 140px; height:50px; border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px; color:grey;"> <?php echo $value['id'];?></label></div></td>
                    <td><div align="center" style="width: 200px; height:50px;border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px; color:grey;"> <?php echo $value['height']; ?> x  <?php echo $value['lenght']; ?> x  <?php echo $value['width'];?>  <?php echo $value['weight'];?>LB  <?php echo $value['vol'];?>ft3</label></div></td>
                    <td><div align="center" style="width: 240px; height:50px; border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px; color:grey;"> <?php echo $value['description'];?> </label></div></td>
                    <td><div align="center" style="width: 50px; height:50px; border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px; color:grey;">1</label></div></td>
                    <td><div align="center" style="width: 240px; height:50px; border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px; color:grey;"><?php echo $value['cost_total'];?> USD</label></div></td>
                    <td><div align="center" style="width: 240px; height:50px; border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px; color:grey;"><?php echo $value['cost_total'];?> USD</label></div></td>
                    <td style="display:none"><input id="id_whr" value="<?php echo $value['id'];?>"></td>
                    <td><div align="center" style="width: 240px; height:50px; border-width: 1px; border-style: solid; border-color: grey;"><div><button onclick="openExtraCost(<?php echo $value['id']; ?>)" class="btn btn-primary" style="margin-top:1%; background-color:#005574; color:white; border:2px solid #005574;"><i class="fas fa-plus"></i> Agregar gasto</button></div></div></td>
                    
                    
                    </tr>
                </table>
                    
                </div>
                <!-- area de extra costos -->
                <br><h6 style="margin-left:20px;"><strong>Gastos extras</strong></h6>
                             
                                <div style="border-width: 1px; margin-left:20px; width:70%;" >
                               

                                    <table class="default">
                                    <tr>
                                    <?php
                                         $extra_cost = $transport->findExtraCostForWarehouseBox($value['id']);
                                            foreach ($extra_cost as $k => $val) {
                                                $subtotal = number_format(($subtotal + $val['cost_extra']),2); 
                                            
                                    ?>  
                                    <td><div align="center" style="width: 100%; height:50px; border-width: 1px; border-style: solid; border-color: grey;"><?php echo $val['name']?></div></td>
                                    <td><div align="center" style="width: 100%; height:50px; border-width: 1px; border-style: solid; border-color: grey;"><?php echo $val['cost_extra']?></div></td>
                                    
                                    <?php
                                        };
                                    ?> 
                                    </tr>
                                    </table>
                                    

                                </div>

                                <div style="margin-left:20px; margin-top:20px; width:70%;" class="card">
                                <div class="card-body">
                                  
                                        <div class="row">
                                            <div class="col-sm-2 col-md-3 col-lg-2" hidden>
                                            <input type="text" class="form-control box_id" placeholder="First name" id="box_id" value="<?php echo $value['id'];?>">
                                            </div>
                                            <div class="col-sm-2 col-md-3 col-lg-2">
                                            <label for="extra_cost">Gastos extras</label>
                                                                                <select id="extra_cost" class="extra_cost form-select campos-input-login">
                                                                                <option value=""> Selecciona...</option>
                                                                                    <?php  
                                                                                        foreach ($allCostExtra as $key => $value) {
                                                                                    ?>
                                                                                        
                                                                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                                                    <?php
                                                                                        }
                                                                                    ?>
                                                                                </select>
                                            </div>
                                            <div class="col-sm-2 col-md-2 col-lg-2">
                                            <label for="costo">Costo</label>
                                            <input type="text" class="costo form-control" id="costo" placeholder="Last name">
                                            </div>
                                            <div class="col-sm-2 col-md-2 col-lg-2">
                                            <button class="btn btn-primary" onclick="addnewCost(<?php echo $value['id'];?>,<?php echo $whData['id'];?>);" style="margin-top:1%; background-color:#005574; color:white; border:2px solid #005574;"><i class="fas fa-check"></i> OK</button>
                                            </div>
                                        </div>
                                    
                                    </div>    
                                </div>    
                               
                <!-- <div style="border-width: 1px; margin-left:20px; width:70%;" >

                    <table class="default">
                    <tr>
                    <td><input id="nombre"></td>
                    
                    </tr>
                </table>

                </div> -->

            <?php
                };
                
                // echo number_format(($total_cost),4);
            ?>
            <div class="row">
                <div class="col-3"></div>
                <div class="col-3"></div>
                <div class="col-3"></div>
                <div class="col-3">
                    <div class="form_group">
                        <label for="total_pagar">Total a pagar:</label>
                        <input style="padding:20px;" type="number" value="<?php echo $subtotal;?>" id="total_pagar" class="form-control">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-3"></div>
                <div class="col-3"></div>
                <div class="col-3"></div>
                <div class="col-3">
                    <div class="form_group">
                    <a class="btn btn-primary" href="invoceReceive.php?id=5000" style="margin-top:1%; background-color:#005574; color:white; border:2px solid #005574;"><i class="fas fa-check"></i> Generar factura</a>
                    </div>
                </div>
            </div>
           

            <!-- <div style="margin-left:20px; border-color: grey; width:70%">
                <table class="default">
                <tr>
                    <td><div align="center" style="width: 566px; height:30px;"></div></td>
                    <td><div align="center" style="width: 70px; height:30px; "><label style="margin-top:5px; color:black;"> TOTAL</label></div></td>
                    <td><div align="center" style="width: 50px; height:30px; border-width: 1px; border-style: solid; border-color: grey;"><label style="margin-top:5px; color:grey;"> <?php echo $total; ?></label></div></td>
                </tr>
                </table>
            </div> -->
            </div>
            <h1> <?php
                
            
            
        ?> </h1>
			
            <!-- <footer class="footer text-center"> 2021 © Total Envios
                </footer> -->
                <?php 

            include 'foot.php';

        ?>
        <?php 

            if ($bulk != '0') {
            ?>
                <input id="packagesForBulk" type="hidden" value="1">
            <?php
                echo '<script type="text/javascript">'
                . '$( document ).ready(function() {'
                . '$("#warehouseModal").modal("show");'
                . 'getBulkPackages();'
                . '});'
                . '</script>';
            }else{
            ?>
                <input id="packagesForBulk" type="hidden" value="0">
            <?php
            }
        ?>
        <script>
            $(document).ready(function () {
                localStorage.removeItem('boxes');
            })
        </script>

            </body>

    </html>