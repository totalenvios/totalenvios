<?php  

    include 'includes/connection.class.php';
    include 'includes/invoices.class.php';
    include 'includes/packages.class.php';
    include 'includes/users.class.php';
    include 'includes/warehouses.class.php';

    $invoices = new Invoices;
    $allInvoices = $invoices->findAll();

    $clients = new Users;
    $allClients = $clients->findAll();
    $warehouses = new Warehouses;
    $allWarehouses = $warehouses->findAll();
    $trips = $warehouses->findTrips();
    $allInvoicesForConfirm = $warehouses->findAllInvoicesForConfirm();
    $allInvoicesForPay = $warehouses->findAllInvoicesForPay();
    $allInvoicesPay = $warehouses->findAllInvoicesPay();

    session_start();
    if ($_SESSION['state'] != 1) {
        header('Location: login.php');
    }
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

        include 'head.php';

    ?>
</head>

<body>
    <!-- Modal -->
    <div class="modal fade" id="addInvoice" tabindex="-1" role="dialog" aria-labelledby="addInvoiceLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addInvoiceLabel">Datos:</h5>
                    <button onclick="$('#addInvoice').modal('hide');" type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="alert alert-danger addInvoice-error" role="alert">
                                </div>
                                <div class="alert alert-success addInvoice-success" role="alert">
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0"># Factura *</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <input id="fact" readonly="readonly" type="text" placeholder="# Factura"
                                                    class="form-control p-0 border-0"
                                                    value="<?php echo ($invoices->findLastId()['id']+1); ?>">
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0">Warehouse</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <select id="warehouse"
                                                    class="form-select shadow-none p-0 border-0 form-control-line">
                                                    <option value="0">Seleccionar...</option>
                                                    <?php  
                                            foreach ($allWarehouses as $key => $value) {
                                                ?>
                                                    <option value="<?php echo $value['id']; ?>">
                                                        <?php echo $value['id']; ?></option>
                                                    <?php
                                            }
                                            ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0">Cliente</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <select id="client"
                                                    class="form-select shadow-none p-0 border-0 form-control-line">
                                                    <option value="0">Seleccionar...</option>
                                                    <?php  
                                            foreach ($allClients as $key => $value) {
                                                ?>
                                                    <option value="<?php echo $value['id']; ?>">
                                                        <?php echo $value['name']; ?></option>
                                                    <?php
                                            }
                                            ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0">Fecha *</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <input id="date_i" type="date" placeholder="Fecha"
                                                    class="form-control p-0 border-0"
                                                    value="<?php echo date('Y-m-d 00:00:00'); ?>">
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <div class="col-sm-12">
                                                <button onclick="addInvoice();" class="btn btn-success">Agregar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
        <?php 
            include 'navbar.php'; 
        ?>

        <div class="page-wrapper" style="background:#e3e3e3ff;">

            <div class="row">

                <div class="col-lg-3 col-xlg-3 col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="icons/FACTURAS-POR-CONFIRMAR.png" alt="" width="100" height="100">
                                </div>
                                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <h5 style="color:#005574;"><strong>POR CONFIRMAR</strong></h5>
                                    <h1 style="color:#005574;"><?php echo $countOpenWH; ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xlg-3 col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="icons/FACTURAS-PENDIENTES.png" alt="" width="100" height="100">
                                </div>
                                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <h5 style="color:#005574;"><strong>POR PAGAR</strong></h5>
                                    <h1 style="color:#005574;"><?php echo $countCloseWH; ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xlg-3 col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="icons/GANANCIA-TOTAL.png" alt="" width="100" height="100">
                                </div>
                                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <h5 style="color:#005574;"><strong>GANANCIA TOTAL</strong></h5>
                                    <h1 style="color:#005574;"><?php echo $countOceanWH; ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xlg-3 col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="icons/POR-COBRAR.png" alt="" width="100" height="100">
                                </div>
                                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <h5 style="color:#005574;"><strong>POR COBRAR</strong></h5>
                                    <h1 style="color:#005574;"><?php echo $countAirWH; ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h5 style="color:#005574;"> <strong> BUSCAR ENVIOS DE UN CLIENTE</strong></h5>
                    <hr>
                    <div class="alert alert-danger register-success" role="alert">
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="user_from">BUSCAR CLIENTE</label>
                                <input id="id_user_from" hidden>
                                <input type="text" class="form-control" placeholder="Nombre"
                                    aria-label="Recipient's username" aria-describedby="basic-addon2"
                                    onkeyup="searchUserPackage();" id="user_from">

                            </div>
                            <ul id="myUS">
                            </ul>
                        </div>

                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="user_from" style="color:black;">DESTINATARIO</label>
                                <div class="input-group mb-3">
                                    <input id="id_user_to" hidden>
                                    <input type="text" class="form-control" placeholder="Nombre"
                                        aria-label="Recipient's username" aria-describedby="basic-addon2"
                                        onkeyup="searchFilter();" id="myInput">


                                </div>
                                <ul id="myDestiner">
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-4 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="trip" style="color:black;">TIPO DE ENVIO</label>

                                <select id="trip_invoice" class="input-register-warehouse">
                                    <?php  

                                                foreach ($trips as $key => $value) {
                                            ?>
                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php
                                                }

                                            ?>
                                </select>

                            </div>
                        </div>

                        <div class="col-sm-4 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="trip" style="color:black;">FECHA DE SALIDA</label>
                                <input id="date_send_invoice" type="date" placeholder="Fecha"
                                    class="form-control input-register-warehouse"
                                    value="<?php echo date('Y-m-d 00:00:00'); ?>">
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-2 col-lg-2 col-xl-2">
                            <button id="whinvoice" onclick="whinvoice();" class="btn-cargar-cajas"
                                style="margin-top:25px; margin-left:30px;">LISTAR WAREHOUSES A FACTURAR</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="WH_INVOICES" class="card" style="display: none;">
                        <div class="card-body">
                            <h3 class="text-center">
                                <div id="client_data_invoice">

                                </div>
                            </h3>
                            <table class="table table-warehouse"
                                style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                <th></th>
                                <th>NRO WH</th>
                                <th>CANTIDAD DE CAJAS</th>
                                <th>COURIER</th>

                                <th>DEPARTAMENTO</th>
                                <th>DESTINO</th>
                                <tbody id="WH-INVOICES-TABLE">
                                    <tr></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <hr>


            <div class="container-fluid">

                <div class="card">
                    <div class="card-body" style="text-align:center; width:100%, padding:100px;" align="center">
                        <div class="btn-group text-center" style="text-align:center; width:100%, padding:100px;"
                            align="center">

                            <button class="btn btn-primary" type="button" id="btnfacturasPendientes"
                                onclick="showFacturasPendientes();"
                                style="background-color:#005574; color:white; border:2px solid white; width:300px;">FACTURAS
                                POR CONFIRMAR</button>


                            <button class="btn btn-primary" type="button" id="btnfacturasPorPagar"
                                onclick="showFacturasPorPagar();"
                                style="background-color:white; color:#005574; border:2px solid #005574; width:300px;">FACTURAS
                                POR PAGAR</button>
                            <button class="btn btn-primary" type="button" id="btnfacturasPagadas"
                                onclick="showFacturasPagadas();"
                                style="background-color:white; color:#005574; border:2px solid #005574; width:300px;">FACTURAS
                                PAGADAS</button>




                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">

                        <div class="row">

                            <div class_="col-lg-10 col-xlg-10 col-md-10">
                                <div class="row">

                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <div class="input-group mb-3">
                                            <input id="myInputS" onkeyup="searchFilterW();" type="text"
                                                placeholder="Nombre del cliente"
                                                class="form-control mt-0 group-delivery">
                                            <ul id="myULS">
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-4"></div>
                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                        <input id="date_in_s" type="date" class="form-control"
                                            value="<?php echo $date_in; ?>">
                                    </div>
                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                        <input id="date_out" type="date" class="form-control"
                                            value="<?php echo $date_out; ?>">
                                    </div>
                                    <div class="col-sm-1 col-md-1 col-lg-1" align="right">
                                        <button onclick="searchWarehouse();" class="btn btn-primary"
                                            style="background-color:white; color:#ff554dff; border:2px solid #ff554dff; width:100%;"><i
                                                style="color:#ff554dff;" class="fas fa-check"></i> Filtrar</button>
                                    </div>
                                </div>

                                <hr style="background-color:#005574;">
                            </div>

                            <div id="facturasPorPagar">
                                <div class="row">

                                    <div class="col-sm-9 col-md-9 col-lg-9">
                                        <div align="left"
                                            style="background:white; color:#005574; width:100%; padding:8px;">
                                            <h4>FACTURAS POR PAGAR</h4>
                                        </div>
                                    </div><br>
                                    
                                </div>



                                <br>

                                <div class="row">
                                    <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                                    <div class_="col-lg-10 col-xlg-10 col-md-10">
                                        <table class="table table-warehouse"
                                            style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                            <thead>
                                                <th style="border: 2px solid #e3e3e3ff;">NRO FACTURA</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    CASILLERO</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    CLIENTE</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                                    F. EMISIÓN</th>
                                                <!-- <th>Casillero</th> -->

                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    F. VENCIMIENTO</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    MONTO TOTAL</th>

                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    TIPO DE ENVIO</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    STATUS</th>
                                                <th style="border: 2px solid #e3e3e3ff; background-color:#white;"></th>
                                            </thead>
                                            <tbody>
                                                <?php  

                                                    foreach ($allInvoicesForPay as $key => $value) {
                                                ?>
                                                <tr>


                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        FACT-<?php echo $value['id']; ?></td>
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $users->findById($value['user_id'])['code']; ?></td>
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $users->findById($value['user_id'])['name']; ?></td>


                                                    <!-- <td><?php echo $value['casillero']; ?></td> -->
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $value['fecha_emision']; ?></td>
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $value['fecha_vencimiento']; ?></td>
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $value['total_invoce']; ?></td>

                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php 
                                                            if( $value['type_trip_id'] == 1){
                                                        ?>
                                                        <i class="fas fa-plane fa-2x fa-lg" style="margin-top:10px;">
                                                        </i>
                                                        <?php 
                                                            }else{
                                                        ?>
                                                        <i class="fas fa-ship fa-2x fa-lg" style="margin-top:10px;">
                                                        </i>
                                                        <?php  
            
                                                        };
                                                    ?>

                                                    </td>


                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php 
                                                                if( $value['status_id'] == 1){
                                                            ?>
                                                        <h6 style="color:#005574;">POR CONFIRMAR</h6>
                                                        <?php 
                                                            }else if( $value['status_id'] == 2) {
                                                        ?>
                                                            <h6 style="color:#fc3c3d;">POR PAGAR</h6>
                                                        <?php  
                                                        }else{
                                                            ?>
                                                        <h6 style="color:green;">PAGADA</h6>
                                                        <?php
                                                            };
                                                        ?>

                                                    </td>

                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"
                                                        class="text-center">
                                                        <div class="btn-group" style="content-align:center; width:100%">

                                                            <button target="_blank"
                                                                    href="closeTicketFormat.php?id=<?php echo $value['id']; ?>"
                                                                    class="btn btn-primary"
                                                                    style="background-color:white; color:#005574; border:2px solid #005574;" disabled><i
                                                                        class="fas fa-edit"></i></button>

                                                            <a target="_blank"
                                                                    href="invoceFormat.php?id=<?php echo $value['id'];?>"
                                                                    class="btn btn-primary"
                                                                    style="background-color:orange; color:white; border:2px solid white;"><i
                                                                        class="fas fa-file"></i></a>

                                                            

                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                    }

                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                                </div>
                            </div>


                            <div id="facturasPendientes" style="display:none;">
                                <div class="row">

                                    <div class="col-sm-9 col-md-9 col-lg-9">
                                        <div align="left"
                                            style="background:white; color:#005574; width:100%; padding:8px;">
                                            <h4>FACTURAS PENDIENTES</h4>
                                        </div>
                                    </div><br>
                                   
                                </div>



                                <br>

                                <div class="row">
                                    <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                                    <div class_="col-lg-10 col-xlg-10 col-md-10">
                                        <table class="table table-warehouse"
                                            style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                            <thead>
                                                <th style="border: 2px solid #e3e3e3ff;">NRO FACTURA</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    CASILLERO</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    CLIENTE</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                                    F. EMISIÓN</th>
                                                <!-- <th>Casillero</th> -->

                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    F. VENCIMIENTO</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    MONTO TOTAL</th>

                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    TIPO DE ENVIO</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    STATUS</th>
                                                <th style="border: 2px solid #e3e3e3ff; background-color:#white;"></th>
                                            </thead>
                                            <tbody>
                                                <?php  

                                                    foreach ($allInvoicesForConfirm as $key => $value) {
                                                ?>
                                                <tr>


                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        FACT-<?php echo $value['id']; ?></td>
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $users->findById($value['user_id'])['code']; ?></td>
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $users->findById($value['user_id'])['name']; ?></td>


                                                    <!-- <td><?php echo $value['casillero']; ?></td> -->
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $value['fecha_emision']; ?></td>
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $value['fecha_vencimiento']; ?></td>
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $value['total_invoce']; ?></td>

                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php 
                                                            if( $value['type_trip_id'] == 1){
                                                        ?>
                                                        <i class="fas fa-plane fa-2x fa-lg" style="margin-top:10px;">
                                                        </i>
                                                        <?php 
                                                            }else{
                                                        ?>
                                                        <i class="fas fa-ship fa-2x fa-lg" style="margin-top:10px;">
                                                        </i>
                                                        <?php  
            
                                                        };
                                                    ?>

                                                    </td>


                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php 
                                                                if( $value['status_id'] == 1){
                                                            ?>
                                                        <h6 style="color:#005574;">POR CONFIRMAR</h6>
                                                        <?php 
                                                            }else if( $value['status_id'] == 2) {
                                                        ?>
                                                            <h6 style="color:#fc3c3d;">POR PAGAR</h6>
                                                        <?php  
                                                        }else{
                                                            ?>
                                                        <h6 style="color:green;">PAGADA</h6>
                                                        <?php
                                                            };
                                                        ?>

                                                    </td>

                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"
                                                        class="text-center">
                                                        <div class="btn-group" style="content-align:center; width:100%">

                                                            <button target="_blank"
                                                                    href="closeTicketFormat.php?id=<?php echo $value['id']; ?>"
                                                                    class="btn btn-primary"
                                                                    style="background-color:white; color:#005574; border:2px solid #005574;" disabled><i
                                                                        class="fas fa-edit"></i></button>

                                                            <a target="_blank"
                                                                    href="invoceFormat.php?id=<?php echo $value['id'];?>"
                                                                    class="btn btn-primary"
                                                                    style="background-color:orange; color:white; border:2px solid white;"><i
                                                                        class="fas fa-file"></i></a>

                                                            

                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                    }

                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                                </div>
                            </div>

                            <div id="facturasPagadas" style="display:none;">
                                <div class="row">

                                    <div class="col-sm-9 col-md-9 col-lg-9">
                                        <div align="left"
                                            style="background:white; color:#005574; width:100%; padding:8px;">
                                            <h4>FACTURAS PAGADAS</h4>
                                        </div>
                                    </div><br>
                                    
                                </div>



                                <br>

                                <div class="row">
                                    <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                                    <div class_="col-lg-10 col-xlg-10 col-md-10">
                                        <table class="table table-warehouse"
                                            style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                            <thead>
                                                <th style="border: 2px solid #e3e3e3ff;">NRO FACTURA</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    CASILLERO</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    CLIENTE</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                                    F. EMISIÓN</th>
                                                <!-- <th>Casillero</th> -->

                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    F. VENCIMIENTO</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    MONTO TOTAL</th>

                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    TIPO DE ENVIO</th>
                                                <th
                                                    style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                                    STATUS</th>
                                                <th style="border: 2px solid #e3e3e3ff; background-color:#white;"></th>
                                            </thead>
                                            <tbody>
                                                <?php  

                                                    foreach ($allInvoicesPay as $key => $value) {
                                                ?>
                                                <tr>


                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        FACT-<?php echo $value['id']; ?></td>
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $users->findById($value['user_id'])['code']; ?></td>
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $users->findById($value['user_id'])['name']; ?></td>


                                                    <!-- <td><?php echo $value['casillero']; ?></td> -->
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $value['fecha_emision']; ?></td>
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $value['fecha_vencimiento']; ?></td>
                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $value['total_invoce']; ?></td>

                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php 
                                                            if( $value['type_trip_id'] == 1){
                                                        ?>
                                                        <i class="fas fa-plane fa-2x fa-lg" style="margin-top:10px;">
                                                        </i>
                                                        <?php 
                                                            }else{
                                                        ?>
                                                        <i class="fas fa-ship fa-2x fa-lg" style="margin-top:10px;">
                                                        </i>
                                                        <?php  
            
                                                        };
                                                    ?>

                                                    </td>


                                                    <td
                                                        style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php 
                                                                if( $value['status_id'] == 1){
                                                            ?>
                                                        <h6 style="color:#005574;">POR CONFIRMAR</h6>
                                                        <?php 
                                                            }else if( $value['status_id'] == 2) {
                                                        ?>
                                                            <h6 style="color:#fc3c3d;">POR PAGAR</h6>
                                                        <?php  
                                                        }else{
                                                            ?>
                                                        <h6 style="color:green;">PAGADA</h6>
                                                        <?php
                                                            };
                                                        ?>

                                                    </td>

                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"
                                                        class="text-center">
                                                      

                                                            <a target="_blank"
                                                                    href="invoceFormat.php?id=<?php echo $value['id'];?>"
                                                                    class="btn btn-primary"
                                                                    style="background-color:orange; color:white; border:2px solid white;"><i
                                                                        class="fas fa-file"></i></a>

                                                            

                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                    }

                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                                </div>
                            </div>







                        </div>
                    </div>

                    <footer class="footer text-center"> 2021 © Total Envios
                    </footer>
                </div>

            </div>
            <?php 

        include 'foot.php';

    ?>
</body>

</html>