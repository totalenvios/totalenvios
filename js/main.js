$(document).ready(function () {
  /** comportamiento del wizard */
  var current_fs, next_fs, previous_fs; //fieldsets
  var opacity;

  $(".next").click(function () {
    current_fs = $(this).parent();
    next_fs = $(this).parent().next();

    //Add Class Active
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate(
      { opacity: 0 },
      {
        step: function (now) {
          // for making fielset appear animation
          opacity = 1 - now;

          current_fs.css({
            display: "none",
            position: "relative",
          });
          next_fs.css({ opacity: opacity });
        },
        duration: 600,
      }
    );
  });

  $(".previous").click(function () {
    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //Remove class active
    $("#progressbar li")
      .eq($("fieldset").index(current_fs))
      .removeClass("active");

    //show the previous fieldset
    previous_fs.show();

    //hide the current fieldset with style
    current_fs.animate(
      { opacity: 0 },
      {
        step: function (now) {
          // for making fielset appear animation
          opacity = 1 - now;

          current_fs.css({
            display: "none",
            position: "relative",
          });
          previous_fs.css({ opacity: opacity });
        },
        duration: 600,
      }
    );
  });

  $(".radio-group .radio").click(function () {
    $(this).parent().find(".radio").removeClass("selected");
    $(this).addClass("selected");
  });

  $(".submit").click(function () {
    return false;
  });
  /*******/

  const venezuela_var = $("#countryVenezuela_edit").val();
  if (venezuela_var == 1) {
    $("#countryVenezuela_edit").val(1);
    $(".select_state").fadeIn("fast");

    $(".address_state").fadeOut("fast");
    $(".address_city").fadeOut("fast");
    $(".address_address").fadeOut("fast");
  } else {
    $("#countryVenezuela_edit").val(0);
    $(".select_state").fadeOut("fast");
    $(".select_city").fadeOut("fast");
    $(".select_address").fadeOut("fast");

    $(".address_state").fadeIn("fast");
    $(".address_city").fadeIn("fast");
    $(".address_address").fadeIn("fast");
  }

  const confirm_email = $("#confirm-email").val();
  const email = $("#email").val();

  if (email == confirm_email) {
    $("#confirmacion-correcta").css("display", "block");

    $("#error-confirmacion").css("display", "none");
  } else {
    $("#error-confirmacion").css("display", "block");
    $("#confirmacion-correcta").css("display", "none");
  }

  // CAMBIO EN MEDIDAS CUANDO YA ESTAN CALCULADOS LOS COSTOS

  /*$('#tarifa_base').on('change', function () {

		if(($('#box-width').val()>0)&&($('#box-lenght').val()>0)&&($('#box-height').val()>0)){
			
		}
		
	});	*/

  $("#box-height").on("change", function () {
    var ancho = parseInt($("#box-width").val());
    var largo = parseInt($("#box-lenght").val());
    var alto = parseInt($("#box-height").val());
    var peso = parseInt($("#box-weight").val());

    // calcular pie cubico y lb volumetricas

    var piecub = (largo * ancho * alto) / 1728;
    var lbVol = (alto * largo * ancho) / 166;

    var piecubRend = piecub.toFixed(3);
    var lbVolRend = lbVol.toFixed(3);
    var pesokg = peso * 0.4535;
    var metrovol = lbVol / 355;
    var metrocub = piecub * 0.2832;

    // redondeo de las medidas
    var pesor = pesokg.toFixed(3);
    var metrocubr = metrocub.toFixed(3);

    $("#pieCubico").val(piecubRend);
    $("#LBVOL").val(lbVolRend);
    $("#pesolb").val(peso);
    $("#pesokg").val(pesor);
    $("#metrocubico").val(metrocubr);

    // CALCULO DE COSTOS DE PAQUETES

    var tarifa_currier = $("#tarifa_base").val();
    var tarifa_te = $("#tasa_base_te").val();
    var tarifa_interna = $("#tarifaInterna_base").val();
    var tarifa_material = $("#material_tasa").val();
    const trip_id = $("#trip").val();

    /************************************************* */
    var costo_currier = 0;
    var monto_te = 0;
    var costo_material = 0;
    var costo_interno = 0;

    //calculo del costo interno
    if (tarifa_interna > 0) {
      if (trip_id == 1) {
        //El envio es aereo
        if (peso > lbVol) {
          costo_interno = peso * tarifa_interna;
        } else {
          costo_interno = lbVolRend * tarifa_interna;
        }
      } else {
        costo_interno = piecubRend * tarifa_interna;
      }
    } else {
      costo_interno = 0;
    }

    if (trip_id == 1) {
      //El envio es aereo
      if (peso > lbVol) {
        costo_currier = peso * tarifa_currier;
        monto_te = peso * tarifa_te;
      } else {
        costo_currier = lbVolRend * tarifa_currier;
        monto_te = lbVolRend * tarifa_te;
      }
    } else {
      costo_currier = piecubRend * tarifa_currier;
      monto_te = piecubRend * tarifa_te;
    }

    /** CALCULO DE TARIFA INTERNA*/

    console.log();

    /**CALCULO COSTO MATERIAL */
    if (tarifa_material > 0) {
      costo_material = tarifa_material;
    } else {
      costo_material = 0;
    }

    /**CALCULOS TOTALES */
    var costo_total = 0;
    var monto_total = 0;
    var ganancia = 0;
    var porcGanancia = 0;

    costo_total = costo_currier + costo_interno + costo_material;
    total = monto_te;
    ganancia = total - costo_total;
    porcGanancia = (ganancia / total) * 100;

    // redondeo de datos finales

    var costo_currierr = costo_currier.toFixed(3);
    var costo_internor = costo_interno.toFixed(3);

    var costo_totalr = costo_total.toFixed(3);
    var totalr = total.toFixed(3);
    var gananciar = ganancia.toFixed(3);
    var porcGananciar = porcGanancia.toFixed(3);

    //asignar valores a los campos correspondientes

    $("#currier_tasa").val(costo_currierr);
    $("#costo_interno").val(costo_internor);
    $("#material_tasa").val(costo_material);
    $("#costo_total").val(costo_totalr);
    $("#total").val(totalr);
    $("#ganancia").val(gananciar);
    $("#porcGanancia").val(porcGananciar);
  });

  $("#box-width").on("change", function () {
    var ancho = parseInt($("#box-width").val());
    var largo = parseInt($("#box-lenght").val());
    var alto = parseInt($("#box-height").val());
    var peso = parseInt($("#box-weight").val());

    // calcular pie cubico y lb volumetricas

    var piecub = (largo * ancho * alto) / 1728;
    var lbVol = (alto * largo * ancho) / 166;

    var piecubRend = piecub.toFixed(3);
    var lbVolRend = lbVol.toFixed(3);
    var pesokg = peso * 0.4535;
    var metrovol = lbVol / 355;
    var metrocub = piecub * 0.2832;

    // redondeo de las medidas
    var pesor = pesokg.toFixed(3);
    var metrocubr = metrocub.toFixed(3);

    $("#pieCubico").val(piecubRend);
    $("#LBVOL").val(lbVolRend);
    $("#pesolb").val(peso);
    $("#pesokg").val(pesor);
    $("#metrocubico").val(metrocubr);

    // CALCULO DE COSTOS DE PAQUETES

    var tarifa_currier = $("#tarifa_base").val();
    var tarifa_te = $("#tasa_base_te").val();
    var tarifa_interna = $("#tarifaInterna_base").val();
    var tarifa_material = $("#material_tasa").val();
    const trip_id = $("#trip").val();

    /************************************************* */
    var costo_currier = 0;
    var monto_te = 0;
    var costo_material = 0;
    var costo_interno = 0;

    //calculo del costo interno
    if (tarifa_interna > 0) {
      if (trip_id == 1) {
        //El envio es aereo
        if (peso > lbVol) {
          costo_interno = peso * tarifa_interna;
        } else {
          costo_interno = lbVolRend * tarifa_interna;
        }
      } else {
        costo_interno = piecubRend * tarifa_interna;
      }
    } else {
      costo_interno = 0;
    }

    if (trip_id == 1) {
      //El envio es aereo
      if (peso > lbVol) {
        costo_currier = peso * tarifa_currier;
        monto_te = peso * tarifa_te;
      } else {
        costo_currier = lbVolRend * tarifa_currier;
        monto_te = lbVolRend * tarifa_te;
      }
    } else {
      costo_currier = piecubRend * tarifa_currier;
      monto_te = piecubRend * tarifa_te;
    }

    /** CALCULO DE TARIFA INTERNA*/

    /**CALCULO COSTO MATERIAL */
    if (tarifa_material > 0) {
      costo_material = tarifa_material;
    } else {
      costo_material = 0;
    }

    /**CALCULOS TOTALES */
    var costo_total = 0;
    var monto_total = 0;
    var ganancia = 0;
    var porcGanancia = 0;

    costo_total = costo_currier + costo_interno + costo_material;
    total = monto_te;
    ganancia = total - costo_total;
    porcGanancia = (ganancia / total) * 100;

    // redondeo de datos finales

    var costo_currierr = costo_currier.toFixed(3);
    var costo_internor = costo_interno.toFixed(3);

    var costo_totalr = costo_total.toFixed(3);
    var totalr = total.toFixed(3);
    var gananciar = ganancia.toFixed(3);
    var porcGananciar = porcGanancia.toFixed(3);

    //asignar valores a los campos correspondientes

    $("#currier_tasa").val(costo_currierr);
    $("#costo_interno").val(costo_internor);
    $("#material_tasa").val(costo_material);
    $("#costo_total").val(costo_totalr);
    $("#total").val(totalr);
    $("#ganancia").val(gananciar);
    $("#porcGanancia").val(porcGananciar);
  });

  $("#box-weight").on("change", function () {
    var ancho = parseInt($("#box-width").val());
    var largo = parseInt($("#box-lenght").val());
    var alto = parseInt($("#box-height").val());
    var peso = parseInt($("#box-weight").val());

    // calcular pie cubico y lb volumetricas

    var piecub = (largo * ancho * alto) / 1728;
    var lbVol = (alto * largo * ancho) / 166;

    var piecubRend = piecub.toFixed(3);
    var lbVolRend = lbVol.toFixed(3);
    var pesokg = peso * 0.4535;
    var metrovol = lbVol / 355;
    var metrocub = piecub * 0.2832;

    // redondeo de las medidas
    var pesor = pesokg.toFixed(3);
    var metrocubr = metrocub.toFixed(3);

    $("#pieCubico").val(piecubRend);
    $("#LBVOL").val(lbVolRend);
    $("#pesolb").val(peso);
    $("#pesokg").val(pesor);
    $("#metrocubico").val(metrocubr);

    // CALCULO DE COSTOS DE PAQUETES

    var tarifa_currier = $("#tarifa_base").val();
    var tarifa_te = $("#tasa_base_te").val();
    var tarifa_interna = $("#tarifaInterna_base").val();
    var tarifa_material = $("#material_tasa").val();
    const trip_id = $("#trip").val();

    /************************************************* */
    var costo_currier = 0;
    var monto_te = 0;
    var costo_material = 0;
    var costo_interno = 0;

    //calculo del costo interno
    if (tarifa_interna > 0) {
      if (trip_id == 1) {
        //El envio es aereo
        if (peso > lbVol) {
          costo_interno = peso * tarifa_interna;
        } else {
          costo_interno = lbVolRend * tarifa_interna;
        }
      } else {
        costo_interno = piecubRend * tarifa_interna;
      }
    } else {
      costo_interno = 0;
    }

    if (trip_id == 1) {
      //El envio es aereo
      if (peso > lbVol) {
        costo_currier = peso * tarifa_currier;
        monto_te = peso * tarifa_te;
      } else {
        costo_currier = lbVolRend * tarifa_currier;
        monto_te = lbVolRend * tarifa_te;
      }
    } else {
      costo_currier = piecubRend * tarifa_currier;
      monto_te = piecubRend * tarifa_te;
    }

    /** CALCULO DE TARIFA INTERNA*/

    /**CALCULO COSTO MATERIAL */
    if (tarifa_material > 0) {
      costo_material = tarifa_material;
    } else {
      costo_material = 0;
    }

    /**CALCULOS TOTALES */
    var costo_total = 0;
    var monto_total = 0;
    var ganancia = 0;
    var porcGanancia = 0;

    costo_total = costo_currier + costo_interno + costo_material;
    total = monto_te;
    ganancia = total - costo_total;
    porcGanancia = (ganancia / total) * 100;

    // redondeo de datos finales

    var costo_currierr = costo_currier.toFixed(3);
    var costo_internor = costo_interno.toFixed(3);

    var costo_totalr = costo_total.toFixed(3);
    var totalr = total.toFixed(3);
    var gananciar = ganancia.toFixed(3);
    var porcGananciar = porcGanancia.toFixed(3);

    //asignar valores a los campos correspondientes

    $("#currier_tasa").val(costo_currierr);
    $("#costo_interno").val(costo_internor);
    $("#material_tasa").val(costo_material);
    $("#costo_total").val(costo_totalr);
    $("#total").val(totalr);
    $("#ganancia").val(gananciar);
    $("#porcGanancia").val(porcGananciar);
  });

  $("#box-lenght").on("change", function () {
    var ancho = parseInt($("#box-width").val());
    var largo = parseInt($("#box-lenght").val());
    var alto = parseInt($("#box-height").val());
    var peso = parseInt($("#box-weight").val());

    // calcular pie cubico y lb volumetricas

    var piecub = (largo * ancho * alto) / 1728;
    var lbVol = (alto * largo * ancho) / 166;

    var piecubRend = piecub.toFixed(3);
    var lbVolRend = lbVol.toFixed(3);
    var pesokg = peso * 0.4535;
    var metrovol = lbVol / 355;
    var metrocub = piecub * 0.2832;

    // redondeo de las medidas
    var pesor = pesokg.toFixed(3);
    var metrocubr = metrocub.toFixed(3);

    $("#pieCubico").val(piecubRend);
    $("#LBVOL").val(lbVolRend);
    $("#pesolb").val(peso);
    $("#pesokg").val(pesor);
    $("#metrocubico").val(metrocubr);

    // CALCULO DE COSTOS DE PAQUETES

    var tarifa_currier = $("#tarifa_base").val();
    var tarifa_te = $("#tasa_base_te").val();
    var tarifa_interna = $("#tarifaInterna_base").val();
    var tarifa_material = $("#material_tasa").val();
    const trip_id = $("#trip").val();

    /************************************************* */
    var costo_currier = 0;
    var monto_te = 0;
    var costo_material = 0;
    var costo_interno = 0;

    //calculo del costo interno
    if (tarifa_interna > 0) {
      if (trip_id == 1) {
        //El envio es aereo
        if (peso > lbVol) {
          costo_interno = peso * tarifa_interna;
        } else {
          costo_interno = lbVolRend * tarifa_interna;
        }
      } else {
        costo_interno = piecubRend * tarifa_interna;
      }
    } else {
      costo_interno = 0;
    }

    if (trip_id == 1) {
      //El envio es aereo
      if (peso > lbVol) {
        costo_currier = peso * tarifa_currier;
        monto_te = peso * tarifa_te;
      } else {
        costo_currier = lbVolRend * tarifa_currier;
        monto_te = lbVolRend * tarifa_te;
      }
    } else {
      costo_currier = piecubRend * tarifa_currier;
      monto_te = piecubRend * tarifa_te;
    }

    /** CALCULO DE TARIFA INTERNA*/

    /**CALCULO COSTO MATERIAL */
    if (tarifa_material > 0) {
      costo_material = tarifa_material;
    } else {
      costo_material = 0;
    }

    /**CALCULOS TOTALES */
    var costo_total = 0;
    var monto_total = 0;
    var ganancia = 0;
    var porcGanancia = 0;

    costo_total = costo_currier + costo_interno + costo_material;
    total = monto_te;
    ganancia = total - costo_total;
    porcGanancia = (ganancia / total) * 100;

    // redondeo de datos finales

    var costo_currierr = costo_currier.toFixed(3);
    var costo_internor = costo_interno.toFixed(3);
    var costo_totalr = costo_total.toFixed(3);
    var totalr = total.toFixed(3);
    var gananciar = ganancia.toFixed(3);
    var porcGananciar = porcGanancia.toFixed(3);

    //asignar valores a los campos correspondientes

    $("#currier_tasa").val(costo_currierr);
    $("#costo_interno").val(costo_internor);
    $("#material_tasa").val(costo_material);
    $("#costo_total").val(costo_totalr);
    $("#total").val(totalr);
    $("#ganancia").val(gananciar);
    $("#porcGanancia").val(porcGananciar);
  });
  /********************************************* */

  /**CAMBIOS DE TIPO DE VIAJE */

  $("#trip").on("change", function () {
    const trip_id = $("#trip").val();
    if (trip_id == 1) {
      $("#aereo").css("display", "block");
      $("#maritimo").css("display", "none");
    } else {
      $("#maritimo").css("display", "block");
      $("#aereo").css("display", "none");
    }
    const wh_id = $("#warehouse_id").val();
    //** CONFIRMAR SI HAY CAJAS REGISTRADAS PARA ESE WAREHOUSE */
    $.ajax({
      method: "POST",
      url: "ajax/findBoxesForWarehouse.php",
      data: { wh_id: wh_id },
      success: function (res) {
        if (res == 0) {
          //NO HAY CAJAS REGISTRADAS PARA ESE WH

          // ciudad destino
          const ciudad = $("#address_send").val();
          const currier_id = $("#currier").val();

          $.ajax({
            method: "POST",
            url: "ajax/findCostForCurrier.php",
            data: { trip_id: trip_id, region: ciudad, currier_id: currier_id },
            success: function (res) {
              //ACTUALIZAR VALORES DE TARIFAS
              console.log(res);
              console.log("entre a cambio de viaje");
              const dataUser = res.split("-");
              const tarifa = dataUser[0];
              const tarifaTE = dataUser[1];

              $("#tarifa_base").val(tarifa);
              $("#tasa_base_te").val(tarifaTE);

              // RECALCULAR LA INFORMACION

              var ancho = parseInt($("#box-width").val());
              var largo = parseInt($("#box-lenght").val());
              var alto = parseInt($("#box-height").val());
              var peso = parseInt($("#box-weight").val());

              // calcular pie cubico y lb volumetricas

              var piecub = (largo * ancho * alto) / 1728;
              var lbVol = (alto * largo * ancho) / 166;

              var piecubRend = piecub.toFixed(3);
              var lbVolRend = lbVol.toFixed(3);
              var pesokg = peso * 0.4535;
              var metrovol = lbVol / 355;
              var metrocub = piecub * 0.2832;

              // redondeo de las medidas
              var pesor = pesokg.toFixed(3);
              var metrocubr = metrocub.toFixed(3);

              $("#pieCubico").val(piecubRend);
              $("#LBVOL").val(lbVolRend);
              $("#pesolb").val(peso);
              $("#pesokg").val(pesor);
              $("#metrocubico").val(metrocubr);

              // CALCULO DE COSTOS DE PAQUETES

              var tarifa_currier = $("#tarifa_base").val();
              var tarifa_te = $("#tasa_base_te").val();
              var tarifa_interna = $("#tarifaInterna_base").val();
              var tarifa_material = $("#material_tasa").val();
              const trip_id = $("#trip").val();

              /************************************************* */
              var costo_currier = 0;
              var monto_te = 0;
              var costo_material = 0;
              var costo_interno = 0;

              //calculo del costo interno
              if (tarifa_interna > 0) {
                if (trip_id == 1) {
                  //El envio es aereo
                  if (peso > lbVol) {
                    costo_interno = peso * tarifa_interna;
                  } else {
                    costo_interno = lbVolRend * tarifa_interna;
                  }
                } else {
                  costo_interno = piecubRend * tarifa_interna;
                }
              } else {
                costo_interno = 0;
              }

              if (trip_id == 1) {
                //El envio es aereo
                if (peso > lbVol) {
                  costo_currier = peso * tarifa_currier;
                  monto_te = peso * tarifa_te;
                } else {
                  costo_currier = lbVolRend * tarifa_currier;
                  monto_te = lbVolRend * tarifa_te;
                }
              } else {
                costo_currier = piecubRend * tarifa_currier;
                monto_te = piecubRend * tarifa_te;
              }

              /** CALCULO DE TARIFA INTERNA*/

              /**CALCULO COSTO MATERIAL */
              if (tarifa_material > 0) {
                costo_material = tarifa_material;
              } else {
                costo_material = 0;
              }

              /**CALCULOS TOTALES */
              var costo_total = 0;
              var monto_total = 0;
              var ganancia = 0;
              var porcGanancia = 0;

              costo_total = costo_currier + costo_interno + costo_material;
              total = monto_te;
              ganancia = total - costo_total;
              porcGanancia = (ganancia / total) * 100;

              // redondeo de datos finales

              var costo_currierr = costo_currier.toFixed(3);
              var costo_internor = costo_interno.toFixed(3);
              var costo_totalr = costo_total.toFixed(3);
              var totalr = total.toFixed(3);
              var gananciar = ganancia.toFixed(3);
              var porcGananciar = porcGanancia.toFixed(3);

              //asignar valores a los campos correspondientes

              $("#currier_tasa").val(costo_currierr);
              $("#costo_interno").val(costo_internor);
              $("#material_tasa").val(costo_material);
              $("#costo_total").val(costo_totalr);
              $("#total").val(totalr);
              $("#ganancia").val(gananciar);
              $("#porcGanancia").val(porcGananciar);
            },
          });
        } else {
          //HAY CAJAS REGISTRADAS
          var boxes = res;
          console.log(boxes);
        }
      },
    });

    $.ajax({
      method: "POST",
      url: "ajax/findCostForCurrier.php",
      data: { trip_id: trip_id, region: ciudad, currier_id: currier_id },
      success: function (res) {
        console.log(res);
        console.log("entre a cambio de viaje parte 2");
        const dataUser = res.split("-");
        const tarifa = dataUser[0];
        const tarifaTE = dataUser[1];

        $("#tarifa_base").val(tarifa);
        $("#tasa_base_te").val(tarifaTE);

        /* Recalculas costos de las cajas */

        var ancho = parseInt($("#box-width").val());
        var largo = parseInt($("#box-lenght").val());
        var alto = parseInt($("#box-height").val());
        var peso = parseInt($("#box-weight").val());

        // calcular pie cubico y lb volumetricas

        var piecub = (largo * ancho * alto) / 1728;
        var lbVol = (alto * largo * ancho) / 166;

        var piecubRend = piecub.toFixed(3);
        var lbVolRend = lbVol.toFixed(3);
        var pesokg = peso * 0.4535;
        var metrovol = lbVol / 355;
        var metrocub = piecub * 0.2832;

        // redondeo de las medidas
        var pesor = pesokg.toFixed(3);
        var metrocubr = metrocub.toFixed(3);

        $("#pieCubico").val(piecubRend);
        $("#LBVOL").val(lbVolRend);
        $("#pesolb").val(peso);
        $("#pesokg").val(pesor);
        $("#metrocubico").val(metrocubr);

        // CALCULO DE COSTOS DE PAQUETES

        var tarifa_currier = $("#tarifa_base").val();
        var tarifa_te = $("#tasa_base_te").val();
        var tarifa_interna = $("#tarifaInterna_base").val();
        var tarifa_material = $("#material_tasa").val();
        const trip_id = $("#trip").val();

        /************************************************* */
        var costo_currier = 0;
        var monto_te = 0;
        var costo_material = 0;
        var costo_interno = 0;

        //calculo del costo interno
        if (tarifa_interna > 0) {
          if (trip_id == 1) {
            //El envio es aereo
            if (peso > lbVol) {
              costo_interno = peso * tarifa_interna;
            } else {
              costo_interno = lbVolRend * tarifa_interna;
            }
          } else {
            costo_interno = piecubRend * tarifa_interna;
          }
        } else {
          costo_interno = 0;
        }

        if (trip_id == 1) {
          //El envio es aereo
          if (peso > lbVol) {
            costo_currier = peso * tarifa_currier;
            monto_te = peso * tarifa_te;
          } else {
            costo_currier = lbVolRend * tarifa_currier;
            monto_te = lbVolRend * tarifa_te;
          }
        } else {
          costo_currier = piecubRend * tarifa_currier;
          monto_te = piecubRend * tarifa_te;
        }

        /** CALCULO DE TARIFA INTERNA*/

        /**CALCULO COSTO MATERIAL */
        if (tarifa_material > 0) {
          costo_material = tarifa_material;
        } else {
          costo_material = 0;
        }

        /**CALCULOS TOTALES */
        var costo_total = 0;
        var monto_total = 0;
        var ganancia = 0;
        var porcGanancia = 0;

        costo_total = costo_currier + costo_interno + costo_material;
        total = monto_te;
        ganancia = total - costo_total;
        porcGanancia = (ganancia / total) * 100;

        // redondeo de datos finales

        var costo_currierr = costo_currier.toFixed(3);
        var costo_internor = costo_interno.toFixed(3);

        var costo_totalr = costo_total.toFixed(3);
        var totalr = total.toFixed(3);
        var gananciar = ganancia.toFixed(3);
        var porcGananciar = porcGanancia.toFixed(3);

        //asignar valores a los campos correspondientes

        $("#currier_tasa").val(costo_currierr);
        $("#costo_interno").val(costo_internor);
        $("#material_tasa").val(costo_material);
        $("#costo_total").val(costo_totalr);
        $("#total").val(totalr);
        $("#ganancia").val(gananciar);
        $("#porcGanancia").val(porcGananciar);
        /******************************** */
      },
    });
  });

  /** */

  $("#costo_extra_select").on("change", function () {
    const cost_id = $("#costo_extra_select").val();
    $.ajax({
      method: "POST",
      url: "ajax/getCostForInvoice.php",
      data: { cost_id: cost_id },
      success: function (res) {
        console.log(res);
        $("#costo_extra_value").val(res);
      },
    });
  });

  $("#user_from").on("change", function () {
    const cost_id = $("#user_from").val();

    $("#alias").val(cost_id);
  });

  $("#tracking").on("change", function () {
    const cost_id = $("#user_from").val();

    $("#alias").val(cost_id);
  });

  $("#region").on("change", function () {
    const trip_id = $("#trip").val();
    // ciudad destino
    const ciudad = $("#address_send").val();
    const currier_id = $("#currier").val();

    // $('#tarifa').val(trip_id);
    // $('#tarifaInterna').val(region);
    // $('#tasa').val(currier_id);

    $.ajax({
      method: "POST",
      url: "ajax/findCostForCurrier.php",
      data: { trip_id: trip_id, region: ciudad, currier_id: currier_id },
      success: function (res) {
        console.log("entre al cambio de region");
        console.log(res);
        const dataUser = res.split("-");
        const tarifa = dataUser[0];
        const tarifaTE = dataUser[1];

        $("#tarifa_base").val(tarifa);
        $("#tasa_base_te").val(tarifaTE);

        var ancho = parseInt($("#box-width").val());
        var largo = parseInt($("#box-lenght").val());
        var alto = parseInt($("#box-height").val());
        var peso = parseInt($("#box-weight").val());

        // calcular pie cubico y lb volumetricas

        var piecub = (largo * ancho * alto) / 1728;
        var lbVol = (alto * largo * ancho) / 166;

        var piecubRend = piecub.toFixed(3);
        var lbVolRend = lbVol.toFixed(3);
        var pesokg = peso * 0.4535;
        var metrovol = lbVol / 355;
        var metrocub = piecub * 0.2832;

        // redondeo de las medidas
        var pesor = pesokg.toFixed(3);
        var metrocubr = metrocub.toFixed(3);

        $("#pieCubico").val(piecubRend);
        $("#LBVOL").val(lbVolRend);
        $("#pesolb").val(peso);
        $("#pesokg").val(pesor);
        $("#metrocubico").val(metrocubr);

        // CALCULO DE COSTOS DE PAQUETES

        var tarifa_currier = $("#tarifa_base").val();
        var tarifa_te = $("#tasa_base_te").val();
        var tarifa_interna = $("#tarifaInterna_base").val();
        var tarifa_material = $("#material_tasa").val();
        const trip_id = $("#trip").val();

        /************************************************* */
        var costo_currier = 0;
        var monto_te = 0;
        var costo_material = 0;
        var costo_interno = 0;

        //calculo del costo interno
        if (tarifa_interna > 0) {
          if (trip_id == 1) {
            //El envio es aereo
            if (peso > lbVol) {
              costo_interno = peso * tarifa_interna;
            } else {
              costo_interno = lbVolRend * tarifa_interna;
            }
          } else {
            costo_interno = piecubRend * tarifa_interna;
          }
        } else {
          costo_interno = 0;
        }

        if (trip_id == 1) {
          //El envio es aereo
          if (peso > lbVol) {
            costo_currier = peso * tarifa_currier;
            monto_te = peso * tarifa_te;
          } else {
            costo_currier = lbVolRend * tarifa_currier;
            monto_te = lbVolRend * tarifa_te;
          }
        } else {
          costo_currier = piecubRend * tarifa_currier;
          monto_te = piecubRend * tarifa_te;
        }

        /** CALCULO DE TARIFA INTERNA*/

        /**CALCULO COSTO MATERIAL */
        if (tarifa_material > 0) {
          costo_material = tarifa_material;
        } else {
          costo_material = 0;
        }

        /**CALCULOS TOTALES */
        var costo_total = 0;
        var monto_total = 0;
        var ganancia = 0;
        var porcGanancia = 0;

        costo_total = costo_currier + costo_interno + costo_material;
        total = monto_te;
        ganancia = total - costo_total;
        porcGanancia = (ganancia / total) * 100;

        // redondeo de datos finales

        var costo_currierr = costo_currier.toFixed(3);
        var costo_internor = costo_interno.toFixed(3);

        var costo_totalr = costo_total.toFixed(3);
        var totalr = total.toFixed(3);
        var gananciar = ganancia.toFixed(3);
        var porcGananciar = porcGanancia.toFixed(3);

        //asignar valores a los campos correspondientes

        $("#currier_tasa").val(costo_currierr);
        $("#costo_interno").val(costo_internor);
        $("#material_tasa").val(costo_material);
        $("#costo_total").val(costo_totalr);
        $("#total").val(totalr);
        $("#ganancia").val(gananciar);
        $("#porcGanancia").val(porcGananciar);
      },
    });
  });

  $("#select_new_intern_currier").on("change", function () {
    const trip_id = $("#trip").val();
    // ciudad destino
    const ciudad = $("#address_send").val();
    const currier_id = $("#select_new_intern_currier").val();

    // $('#tarifa').val(trip_id);
    // $('#tarifaInterna').val(region);
    // $('#tasa').val(currier_id);

    $.ajax({
      method: "POST",
      url: "ajax/findCostForCurrierIntern.php",
      data: { trip_id: trip_id, region: ciudad, currier_id: currier_id },
      success: function (res) {
        console.log(res);
        const dataUser = res.split("-");
        const tarifa = dataUser[0];
        const tarifaTE = dataUser[1];

        $("#tarifaInterna_base").val(tarifaTE);
      },
    });
  });

  $("#address_send").on("change", function () {
    const trip_id = $("#trip").val();
    if (trip_id == 1) {
      $("#aereo").css("display", "block");
      $("#maritimo").css("display", "none");
    } else {
      $("#maritimo").css("display", "block");
      $("#aereo").css("display", "none");
    }

    // ciudad destino
    const ciudad = $("#address_send").val();
    const currier_id = $("#currier").val();

    // $('#tarifa').val(trip_id);
    // $('#tarifaInterna').val(region);
    // $('#tasa').val(currier_id);

    $.ajax({
      method: "POST",
      url: "ajax/findCostForCurrier.php",
      data: { trip_id: trip_id, region: ciudad, currier_id: currier_id },
      success: function (res) {
        console.log("entre a cambio de ciudad");
        console.log(res);
        const dataUser = res.split("-");
        const tarifa = dataUser[0];
        const tarifaTE = dataUser[1];

        $("#tarifa_base").val(tarifa);
        $("#tasa_base_te").val(tarifaTE);

        var ancho = parseInt($("#box-width").val());
        var largo = parseInt($("#box-lenght").val());
        var alto = parseInt($("#box-height").val());
        var peso = parseInt($("#box-weight").val());

        // calcular pie cubico y lb volumetricas

        var piecub = (largo * ancho * alto) / 1728;
        var lbVol = (alto * largo * ancho) / 166;

        var piecubRend = piecub.toFixed(3);
        var lbVolRend = lbVol.toFixed(3);
        var pesokg = peso * 0.4535;
        var metrovol = lbVol / 355;
        var metrocub = piecub * 0.2832;

        // redondeo de las medidas
        var pesor = pesokg.toFixed(3);
        var metrocubr = metrocub.toFixed(3);

        $("#pieCubico").val(piecubRend);
        $("#LBVOL").val(lbVolRend);
        $("#pesolb").val(peso);
        $("#pesokg").val(pesor);
        $("#metrocubico").val(metrocubr);

        // CALCULO DE COSTOS DE PAQUETES

        var tarifa_currier = $("#tarifa_base").val();
        var tarifa_te = $("#tasa_base_te").val();
        var tarifa_interna = $("#tarifaInterna_base").val();
        var tarifa_material = $("#material_tasa").val();
        const trip_id = $("#trip").val();

        /************************************************* */
        var costo_currier = 0;
        var monto_te = 0;
        var costo_material = 0;
        var costo_interno = 0;

        //calculo del costo interno
        if (tarifa_interna > 0) {
          if (trip_id == 1) {
            //El envio es aereo
            if (peso > lbVol) {
              costo_interno = peso * tarifa_interna;
            } else {
              costo_interno = lbVolRend * tarifa_interna;
            }
          } else {
            costo_interno = piecubRend * tarifa_interna;
          }
        } else {
          costo_interno = 0;
        }

        if (trip_id == 1) {
          //El envio es aereo
          if (peso > lbVol) {
            costo_currier = peso * tarifa_currier;
            monto_te = peso * tarifa_te;
          } else {
            costo_currier = lbVolRend * tarifa_currier;
            monto_te = lbVolRend * tarifa_te;
          }
        } else {
          costo_currier = piecubRend * tarifa_currier;
          monto_te = piecubRend * tarifa_te;
        }

        /** CALCULO DE TARIFA INTERNA*/

        /**CALCULO COSTO MATERIAL */
        if (tarifa_material > 0) {
          costo_material = tarifa_material;
        } else {
          costo_material = 0;
        }

        /**CALCULOS TOTALES */
        var costo_total = 0;
        var monto_total = 0;
        var ganancia = 0;
        var porcGanancia = 0;

        costo_total = costo_currier + costo_interno + costo_material;
        total = monto_te;
        ganancia = total - costo_total;
        porcGanancia = (ganancia / total) * 100;

        // redondeo de datos finales

        var costo_currierr = costo_currier.toFixed(3);
        var costo_internor = costo_interno.toFixed(3);

        var costo_totalr = costo_total.toFixed(3);
        var totalr = total.toFixed(3);
        var gananciar = ganancia.toFixed(3);
        var porcGananciar = porcGanancia.toFixed(3);

        //asignar valores a los campos correspondientes

        $("#currier_tasa").val(costo_currierr);
        $("#costo_interno").val(costo_internor);
        $("#material_tasa").val(costo_material);
        $("#costo_total").val(costo_totalr);
        $("#total").val(totalr);
        $("#ganancia").val(gananciar);
        $("#porcGanancia").val(porcGananciar);
      },
    });
  });

  // Para el caso de ajustes en el peso y y medidas
  // ***************************************************************************
  //************************************************************************** */

  // Para agregar gastos

  $("#material_tasa").on("change", function () {
    if ($("#material_tasa").val() == "" || $("#material_tasa").val() == 0) {
      var costo_total_cap = parseFloat($("#costo_total").val());
      var total_cap = parseFloat($("#total").val());

      /************************************************* */

      //calculo del costo interno
      var costo_currier = parseFloat($("#currier_tasa").val());
      var costo_interno = parseFloat($("#costo_interno").val());
      /**CALCULOS TOTALES */
      var costo_total = 0;
      var ganancia = 0;
      var porcGanancia = 0;

      costo_total = costo_currier + costo_interno;
      ganancia = total_cap - costo_total;
      porcGanancia = (ganancia / total_cap) * 100;

      // redondeo de datos finales

      var costo_total_ren = costo_total.toFixed(3);
      var ganancia_ren = ganancia.toFixed(3);
      var porc_gan_ren = porcGanancia.toFixed(3);

      //asignar valores a los campos correspondientes
      $("#costo_total").val(costo_total_ren);
      $("#total").val(total_cap);
      $("#ganancia").val(ganancia_ren);
      $("#porcGanancia").val(porc_gan_ren);
    } else {
      var costo_total_cap = parseFloat($("#costo_total").val());
      var tarifa_material = parseInt($("#material_tasa").val());
      var total_cap = parseFloat($("#total").val());

      /************************************************* */
      if (tarifa_material > 0) {
        //calculo del costo interno
        var costo_currier = parseFloat($("#currier_tasa").val());
        var costo_interno = parseFloat($("#costo_interno").val());
        /**CALCULOS TOTALES */
        var costo_total = 0;
        var ganancia = 0;
        var porcGanancia = 0;

        costo_total = costo_currier + costo_interno + tarifa_material;
        ganancia = total_cap - costo_total;
        porcGanancia = (ganancia / total_cap) * 100;

        // redondeo de datos finales

        var costo_total_ren = costo_total.toFixed(3);
        var ganancia_ren = ganancia.toFixed(3);
        var porc_gan_ren = porcGanancia.toFixed(3);

        //asignar valores a los campos correspondientes
        $("#costo_total").val(costo_total_ren);
        $("#total").val(total_cap);
        $("#ganancia").val(ganancia_ren);
        $("#porcGanancia").val(porc_gan_ren);
      } else {
        //calculo del costo interno

        /**CALCULOS TOTALES */
        var costo_total = 0;
        var ganancia = 0;
        var porcGanancia = 0;

        costo_total = costo_total_cap + tarifa_material;
        ganancia = total_cap - costo_total;
        porcGanancia = (ganancia / total_cap) * 100;

        // redondeo de datos finales

        var costo_total_ren = costo_total.toFixed(3);
        var ganancia_ren = ganancia.toFixed(3);
        var porc_gan_ren = porcGanancia.toFixed(3);

        //asignar valores a los campos correspondientes
        $("#costo_total").val(costo_total_ren);
        $("#total").val(total_cap);
        $("#ganancia").val(ganancia_ren);
        $("#porcGanancia").val(porc_gan_ren);
      }
    }
  });

  $("#costo_total").on("change", function () {
    var ancho = parseInt($("#box-width").val());
    var largo = parseInt($("#box-lenght").val());
    var alto = parseInt($("#box-height").val());
    var peso = parseInt($("#box-weight").val());

    // calcular pie cubico y lb volumetricas

    var piecub = (largo * ancho * alto) / 1728;
    var lbVol = (alto * largo * ancho) / 166;

    var piecubRend = piecub.toFixed(3);
    var lbVolRend = lbVol.toFixed(3);
    var pesokg = peso * 0.4535;
    var metrovol = lbVol / 355;
    var metrocub = piecub * 0.2832;

    // redondeo de las medidas
    var pesor = pesokg.toFixed(3);
    var metrocubr = metrocub.toFixed(3);

    $("#pieCubico").val(piecubRend);
    $("#LBVOL").val(lbVolRend);
    $("#pesolb").val(peso);
    $("#pesokg").val(pesor);
    $("#metrocubico").val(metrocubr);

    // CALCULO DE COSTOS DE PAQUETES

    var tarifa_currier = $("#tarifa_base").val();
    var tarifa_te = $("#tasa_base_te").val();
    var tarifa_interna = $("#tarifaInterna_base").val();
    var tarifa_material = parseInt($("#material_tasa").val());
    const trip_id = $("#trip").val();

    /************************************************* */
    var costo_currier = 0;
    var monto_te = 0;
    var costo_material = 0;
    var costo_interno = 0;

    //calculo del costo interno
    if (tarifa_interna > 0) {
      if (trip_id == 1) {
        //El envio es aereo
        if (peso > lbVol) {
          costo_interno = peso * tarifa_interna;
        } else {
          costo_interno = lbVolRend * tarifa_interna;
        }
      } else {
        costo_interno = piecubRend * tarifa_interna;
      }
    } else {
      costo_interno = 0;
    }

    if (trip_id == 1) {
      //El envio es aereo
      if (peso > lbVol) {
        costo_currier = peso * tarifa_currier;
        monto_te = peso * tarifa_te;
      } else {
        costo_currier = lbVolRend * tarifa_currier;
        monto_te = lbVolRend * tarifa_te;
      }
    } else {
      costo_currier = piecubRend * tarifa_currier;
      monto_te = piecubRend * tarifa_te;
    }

    /** CALCULO DE TARIFA INTERNA*/

    /**CALCULO COSTO MATERIAL */
    if (tarifa_material > 0) {
      costo_material = tarifa_material;
    } else {
      costo_material = 0;
    }

    /**CALCULOS TOTALES */
    var costo_total = 0;
    var total = 0;
    var ganancia = 0;
    var porcGanancia = 0;

    costo_total = costo_currier + costo_interno + costo_material;
    total = monto_te;
    ganancia = total - costo_total;
    porcGanancia = (ganancia / total) * 100;

    // redondeo de datos finales

    var costo_currierr = costo_currier.toFixed(3);
    var costo_internor = costo_interno.toFixed(3);

    //asignar valores a los campos correspondientes

    $("#currier_tasa").val(costo_currierr);
    $("#costo_interno").val(costo_internor);
    $("#material_tasa").val(costo_material);
    $("#costo_total").val(costo_total);
    $("#total").val(total);
    $("#ganancia").val(ganancia);
    $("#porcGanancia").val(porcGanancia);
  });

  $("#total").on("change", function () {
    var total_cap = parseFloat($("#total").val());
    var costo_total = parseFloat($("#costo_total").val());
    /**CALCULOS TOTALES */
    var ganancia = 0;
    var porcGanancia = 0;

    ganancia = total_cap - costo_total;
    porcGanancia = (ganancia / total_cap) * 100;

    // redondeo de datos finales
    var ganancia_ren = ganancia.toFixed(3);
    var porc_gan_ren = porcGanancia.toFixed(3);

    //asignar valores a los campos correspondientes
    $("#ganancia").val(ganancia_ren);
    $("#porcGanancia").val(porc_gan_ren);
  });

  // Para agregar seguro

  $("#box-name").on("change", function () {
    const box_name = $("#box-name").val();

    if ($("#box-name").val() == "") {
      const trip_id = $("#trip").val();
      const region = $("#region").val();
      const currier_id = $("#currier").val();

      $.ajax({
        method: "POST",
        url: "ajax/findCostForCurrier.php",
        data: { trip_id: trip_id, region: region, currier_id: currier_id },
        success: function (res) {
          console.log(res);
          const dataUser = res.split("-");
          const costo = dataUser[0];
          const tasa_inter = dataUser[2];
          const tasa = dataUser[1];

          $("#tarifa_base").val(costo);
          $("#tarifaInterna_base").val(tasa_inter);
          $("#tasa_base").val(tasa);

          $("#box-lenght").val(0);
          $("#box-width").val(0);
          $("#box-height").val(0);
          $("#box-weight").val(0);
          $("#pieCubico").val(0, 00);
          $("#LBVOL").val(0, 00);
          $("#seguro").val(0);
          $("#logistica").val(0);
          $("#gastosExtra").val(0);
          $("#tarifa").val(0, 00);
          $("#tarifaInterna").val(0, 00);
          $("#tasa").val(0, 00);
          $("#total").val(0, 00);
          $("#total_base").val(0, 00);
          $("#ganancia").val(0, 00);
          $("#porcGanancia").val(0, 00);
        },
      });
    }
  });

  // BUSQUEDA PRUEBA
  // *********************************************
  $(".check_user").on("change", function () {
    if ($(".check_user").attr("checked")) {
      $("#client_form").css("display", "block");
      $("#name_form").css("display", "none");
    } else {
      $("#client_form").css("display", "none");
      $("#name_form").css("display", "block");
    }
  });

  // ********************************************
  $("#user_type").on("change", function () {
    const tipo_usuario = $(this).val();
    if (tipo_usuario == 2) {
      $("#user-register").css("display", "block");
      $("#company-register").css("display", "none");
    } else {
      $("#user-register").css("display", "none");
      $("#company-register").css("display", "block");
    }
  });

  $("#address_destiny_destiner").on("change", function () {
    const destiny = $(this).val();
    if (destiny == 1) {
      $("#countryVenezuela").val(1);
      $(".select_state_destiner").fadeIn("fast");

      $(".address_state_destiner").fadeOut("fast");
      $(".address_city_destiner").fadeOut("fast");
      $(".address_address_destiner").fadeOut("fast");
    } else {
      $("#countryVenezuela_destiner").val(0);
      $(".select_state_destiner").fadeOut("fast");
      $(".select_city_destiner").fadeOut("fast");
      $(".select_address_destiner").fadeOut("fast");

      $(".address_state_destiner").fadeIn("fast");
      $(".address_city_destiner").fadeIn("fast");
      $(".address_address_destiner").fadeIn("fast");
    }
  });

  $("#address_destiny_destiner_dos").on("change", function () {
    const destiny = $(this).val();
    if (destiny == 1) {
      $("#countryVenezuela").val(1);
      $(".select_state_destiner_dos").fadeIn("fast");

      $(".address_state_destiner_dos").fadeOut("fast");
      $(".address_city_destiner_dos").fadeOut("fast");
      $(".address_address_destiner_dos").fadeOut("fast");
    } else {
      $("#countryVenezuela_destiner_dos").val(0);
      $(".select_state_destiner_dos").fadeOut("fast");
      $(".select_city_destiner_dos").fadeOut("fast");
      $(".select_address_destiner_dos").fadeOut("fast");

      $(".address_state_destiner_dos").fadeIn("fast");
      $(".address_city_destiner_dos").fadeIn("fast");
      $(".address_address_destiner_dos").fadeIn("fast");
    }
  });

  $("#address_destiny_destiner_for_client").on("change", function () {
    const destiny = $(this).val();
    if (destiny == 1) {
      $("#countryVenezuela").val(1);
      $(".select_state_destiner_for_client").fadeIn("fast");

      $(".address_state_destiner_for_client").fadeOut("fast");
      $(".address_city_destiner_for_client").fadeOut("fast");
      $(".address_address_destiner_for_client").fadeOut("fast");
    } else {
      $("#countryVenezuela_destiner_for_client").val(0);
      $(".select_state_destiner_for_client").fadeOut("fast");
      $(".select_city_destiner_for_client").fadeOut("fast");
      $(".select_address_destiner_for_client").fadeOut("fast");

      $(".address_state_destiner_for_client").fadeIn("fast");
      $(".address_city_destiner_for_client").fadeIn("fast");
      $(".address_address_destiner_for_client").fadeIn("fast");
    }
  });

  $("#select_state_destiner_for_client").on("change", function () {
    const state = $(this).val();
    $.ajax({
      method: "POST",
      url: "ajax/getCitiesDestiner.php",
      data: { state: state },
      success: function (res) {
        console.log(res);
        $("#select_city_destiner_for_client").html(res);
        $(".select_city_destiner_for_client").fadeIn("fast");
        $(".select_address_destiner_for_client").fadeIn("fast");
      },
    });
  });

  $("#address_destiny_company").on("change", function () {
    const destiny = $(this).val();
    if (destiny == 1) {
      $("#countryVenezuela").val(1);
      $(".select_state_company").fadeIn("fast");

      $(".address_state_company").fadeOut("fast");
      $(".address_city_company").fadeOut("fast");
      $(".address_address_company").fadeOut("fast");
    } else {
      $("#countryVenezuela").val(0);
      $(".select_state_company").fadeOut("fast");
      $(".select_city_company").fadeOut("fast");
      $(".select_address_company").fadeOut("fast");

      $(".address_state_company").fadeIn("fast");
      $(".address_city_company").fadeIn("fast");
      $(".address_address_company").fadeIn("fast");
    }
  });

  $("#address_destiny").on("change", function () {
    const destiny = $(this).val();
    if (destiny == 1) {
      $("#countryVenezuela").val(1);
      $(".select_state").fadeIn("fast");

      $(".address_state").fadeOut("fast");
      $(".address_city").fadeOut("fast");
      $(".address_address").fadeOut("fast");
    } else {
      console.log($("#countryVenezuela").val());

      $(".select_state").fadeOut("fast");
      $(".select_city").fadeOut("fast");
      $(".select_address").fadeOut("fast");

      $(".address_state").css("display", "block");
      $(".address_city").css("display", "block");
      $(".address_address").css("display", "block");
    }
  });

  /**EDICIÓN DE USUARIOS **/

  $("#address_destiny_for_edit_user").on("change", function () {
    const destiny = $(this).val();
    if (destiny == 1) {
      $("#countryVenezuela").val(1);
      $(".select_state_for_edit_usuario").fadeIn("fast");

      $(".address_state_for_edit_usuario").fadeOut("fast");
      $(".address_city_for_edit_usuario").fadeOut("fast");
      $(".address_address_for_edit_usuario").fadeOut("fast");
    } else {
      console.log($("#countryVenezuela").val());

      $(".select_state_for_edit_usuario").fadeOut("fast");
      $(".select_city_for_edit_usuario").fadeOut("fast");
      $(".select_address_for_edit_usuario").fadeOut("fast");

      $(".address_state_for_edit_usuario").css("display", "block");
      $(".address_city_for_edit_usuario").css("display", "block");
      $(".address_address_for_edit_usuario").css("display", "block");
    }
  });

  $("#select_state_for_edit_usuario").on("change", function () {
    const state = $(this).val();
    $.ajax({
      method: "POST",
      url: "ajax/getCitiesDestiner.php",
      data: { state: state },
      success: function (res) {
        console.log(res);
        $("#select_city_for_edit_usuario").html(res);
        $(".select_city_for_edit_usuario").fadeIn("fast");
        $(".select_address_for_edit_usuario").fadeIn("fast");
      },
    });
  });

  $("#select_state").on("change", function () {
    const state = $(this).val();
    $.ajax({
      method: "POST",
      url: "ajax/getCitiesDestiner.php",
      data: { state: state },
      success: function (res) {
        console.log(res);
        $("#select_city").html(res);
        $(".select_city").fadeIn("fast");
        $(".select_address").fadeIn("fast");
      },
    });
  });

  $("#select_state_company").on("change", function () {
    const state = $(this).val();
    $.ajax({
      method: "POST",
      url: "ajax/getCitiesDestiner.php",
      data: { state: state },
      success: function (res) {
        console.log(res);
        $("#select_city_company").html(res);
        $(".select_city_company").fadeIn("fast");
        $(".select_address_company").fadeIn("fast");
      },
    });
  });

  $("#select_state_destiner").on("change", function () {
    const state = $(this).val();
    $.ajax({
      method: "POST",
      url: "ajax/getCitiesDestiner.php",
      data: { state: state },
      success: function (res) {
        console.log(res);
        $("#select_city_destiner").html(res);
        $(".select_city_destiner").fadeIn("fast");
        $(".select_address_destiner").fadeIn("fast");
      },
    });
  });

  $("#select_state_destiner_dos").on("change", function () {
    const state = $(this).val();
    $.ajax({
      method: "POST",
      url: "ajax/getCitiesDestiner.php",
      data: { state: state },
      success: function (res) {
        console.log(res);
        $("#select_city_destiner_dos").html(res);
        $(".select_city_destiner_dos").fadeIn("fast");
        $(".select_address_destiner_dos").fadeIn("fast");
      },
    });
  });
});

function changeSateWh() {
  const id_state_wh = $("#wState").val();

  if (id_state_wh == 1) {
    // warehouse  abierto
    $("#wState").val(2);
    $("#wState").html('<i class="fas fa-lock"></i>');
  } else {
    //warehouse cerrado
    $("#wState").val(1);
    $("#wState").html('<i class="fas fa-unlock"></i>');
  }
}

function deleteExtraCost(id) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteExtraCostTable.php",
    data: { id: id },
    success: function (res) {
      $("#extra-cost-table").html(res);
    },
  });
}

function editExtraCost(id) {
  $.ajax({
    method: "POST",
    url: "ajax/findExtraCostForConfirm.php",
    data: { id: id },
    success: function (res) {
      const dataUser = res.split("-");

      const id_extra_cost = dataUser[0];
      const id_service = dataUser[1];
      const costo = dataUser[2];

      $("#id_extra_cost").val(id_extra_cost);
      $("#costo_extra_select").val(id_service);
      $("#costo_extra_value").val(costo);

      $("#agregar_costo_extra").css("display", "none");
      $("#editar_gastos_extras").css("display", "block");
    },
  });
}

function editGastosExtras() {
  const id = $("#id_extra_cost").val();
  const extracost_id = $("#costo_extra_select").val();
  const cost = $("#costo_extra_value").val();

  $.ajax({
    method: "POST",
    url: "ajax/editExtraCostForTable.php",
    data: { id: id, extracost_id: extracost_id, cost: cost },
    success: function (res) {
      $("#agregar_costo_extra").css("display", "block");
      $("#editar_gastos_extras").css("display", "none");
      $("#extra-cost-table").html(res);
    },
  });
}

function cargarGastosExtras() {
  var subtotal_boxes = parseFloat($("#subtotal_boxes").val());
  var subtotal_gastos_extras = parseFloat(
    $("#subtotal_costo_extra_value").val()
  );
  var gastos_extras = parseFloat($("#costo_extra_value").val());
  var gastos_extras_id = $("#costo_extra_select").val();

  /* guardar costos extra*/

  $.ajax({
    method: "POST",
    url: "ajax/addExtraCost.php",
    data: { gastos_extras_id: gastos_extras_id, gastos_extras: gastos_extras },
    success: function (res) {
      var suma = 0;
      suma = subtotal_gastos_extras + gastos_extras;
      total = 0;
      total = subtotal_boxes + suma;
      $("#subtotal_costo_extra_value").val(suma);
      $("#total_factura").val(total);
      $("#costo_extra_value").val(0);
      $("#extra-cost-table").html(res);
    },
  });
}

function updateWarehouseData() {
  const wh_id = $("#wh_id_update").val();
  const id_destiner = $("#id_destiny_wh").val();
  const user_from = $("#id_user_from").val();
  const user_to = $("#id_user_to").val();
  const name_user_from = $("#user_from").val();
  const name_user_to = $("#myInput").val();
  const country_id = $("#country_id_wh").val();
  const state_id = $("#state_id_wh").val();
  const city_id = $("#city_id_wh").val();

  const country_name = "ddfdfd";
  const state_name = "sddsfdsf";
  const city_name = "dfdsfdf";

  const depart_id = $("#depart_id_wh").val();
  const address_destiner = $("#address_edit_wh").val();
  const currier_id = $("#currier_id_wh").val();
  const trip_id = $("#trip_id_wh").val();
  const status_wh = $("#status_wh").val();
  const ship_status = $("#ship_status").val();
  const date_send = $("#date_send_wh").val();

  var nota_wh = $("#nota_wh").val();
  var num_guia = $("#num_guia").val();

  if (nota_wh == "sin comentarios" || nota_wh == "") {
    nota_wh = "sin comentarios";
  } else {
    nota_wh = $("#nota_wh").val();
  }

  if (num_guia == "" || num_guia == 0) {
    num_guia = 0;
  } else {
    num_guia = $("#num_guia").val();
  }

  $.ajax({
    method: "POST",
    url: "ajax/updateWarehouse.php",
    data: {
      wh_id: wh_id,
      id: id_destiner,
      user_from: user_from,
      user_to: user_to,
      name_user_from: name_user_from,
      name_user_to: name_user_to,
      country_id: country_id,
      state_id: state_id,
      city_id: city_id,
      country_name: country_name,
      state_name: state_name,
      city_name: city_name,
      address_destiner: address_destiner,
      depart_id: depart_id,
      currier_id: currier_id,
      trip_id: trip_id,
      date_send: date_send,
      status_wh: status_wh,
      ship_status: ship_status,
      nota_wh: nota_wh,
      num_guia: num_guia,
    },
    success: function (res) {
      console.log(res);
      $(".register-success").html("¡Registro exitoso!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}
//**FUNCIONAMIENTO DEL CAMBIO DE TIPO DE ENVIO POR MEDIO DE BOTONES */
function changeTripAir() {
  const trip_id = 1;
  if (trip_id == 1) {
    $("#aereo").css("display", "block");
    $("#maritimo").css("display", "none");

    $("#trip_air").css("background-color", "#005574");
    $("#trip_ocean").css("background-color", "white");
    $("#trip").val(1);
  } else {
    $("#maritimo").css("display", "block");
    $("#aereo").css("display", "none");
  }
  const wh_id = $("#warehouse_id").val();

  const ciudad = $("#address_send").val();
  const currier_id = $("#currier").val();

  if (ciudad == 0) {
    $(".register-error").html("Ingresa una ciudad de destino");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/findCostForCurrier.php",
      data: { trip_id: trip_id, region: ciudad, currier_id: currier_id },
      success: function (res) {
        console.log("ENTRE EN EL COSTO POR TIPO DE ENVIO");
        //ACTUALIZAR VALORES DE TARIFAS
        console.log(res);
        const dataUser = res.split("-");
        const tarifa = dataUser[0];
        const tarifaTE = dataUser[1];

        $("#tarifa_base").val(tarifa);
        $("#tasa_base_te").val(tarifaTE);

        // RECALCULAR LA INFORMACION

        var ancho = parseInt($("#box-width").val());
        var largo = parseInt($("#box-lenght").val());
        var alto = parseInt($("#box-height").val());
        var peso = parseInt($("#box-weight").val());

        // calcular pie cubico y lb volumetricas

        var piecub = (largo * ancho * alto) / 1728;
        var lbVol = (alto * largo * ancho) / 166;

        var piecubRend = piecub.toFixed(3);
        var lbVolRend = lbVol.toFixed(3);
        var pesokg = peso * 0.4535;
        var metrovol = lbVol / 355;
        var metrocub = piecub * 0.2832;

        // redondeo de las medidas
        var pesor = pesokg.toFixed(3);
        var metrocubr = metrocub.toFixed(3);

        $("#pieCubico").val(piecubRend);
        $("#LBVOL").val(lbVolRend);
        $("#pesolb").val(peso);
        $("#pesokg").val(pesor);
        $("#metrocubico").val(metrocubr);

        // CALCULO DE COSTOS DE PAQUETES

        var tarifa_currier = $("#tarifa_base").val();
        var tarifa_te = $("#tasa_base_te").val();
        var tarifa_interna = $("#tarifaInterna_base").val();
        var tarifa_material = $("#material_tasa").val();
        const trip_id = $("#trip").val();

        /************************************************* */
        var costo_currier = 0;
        var monto_te = 0;
        var costo_material = 0;
        var costo_interno = 0;

        //calculo del costo interno
        if (tarifa_interna > 0) {
          if (trip_id == 1) {
            //El envio es aereo
            if (peso > lbVol) {
              costo_interno = peso * tarifa_interna;
            } else {
              costo_interno = lbVolRend * tarifa_interna;
            }
          } else {
            costo_interno = piecubRend * tarifa_interna;
          }
        } else {
          costo_interno = 0;
        }

        if (trip_id == 1) {
          //El envio es aereo
          if (peso > lbVol) {
            costo_currier = peso * tarifa_currier;
            monto_te = peso * tarifa_te;
          } else {
            costo_currier = lbVolRend * tarifa_currier;
            monto_te = lbVolRend * tarifa_te;
          }
        } else {
          costo_currier = piecubRend * tarifa_currier;
          monto_te = piecubRend * tarifa_te;
        }

        /** CALCULO DE TARIFA INTERNA*/

        /**CALCULO COSTO MATERIAL */
        if (tarifa_material > 0) {
          costo_material = tarifa_material;
        } else {
          costo_material = 0;
        }

        /**CALCULOS TOTALES */
        var costo_total = 0;
        var monto_total = 0;
        var ganancia = 0;
        var porcGanancia = 0;

        costo_total = costo_currier + costo_interno + costo_material;
        total = monto_te + costo_total;
        ganancia = total - costo_total;
        porcGanancia = (ganancia / total) * 100;

        // redondeo de datos finales

        var costo_currierr = costo_currier.toFixed(3);
        var costo_internor = costo_interno.toFixed(3);

        var costo_totalr = costo_total.toFixed(3);
        var totalr = total.toFixed(3);
        var gananciar = ganancia.toFixed(3);
        var porcGananciar = porcGanancia.toFixed(3);

        //asignar valores a los campos correspondientes

        $("#currier_tasa").val(costo_currierr);
        $("#costo_interno").val(costo_internor);
        $("#material_tasa").val(costo_material);
        $("#costo_total").val(costo_totalr);
        $("#total").val(totalr);
        $("#ganancia").val(gananciar);
        $("#porcGanancia").val(porcGananciar);

        $.ajax({
          method: "POST",
          url: "ajax/findBoxesForWarehouse.php",
          data: { wh_id: wh_id },
          success: function (res) {
            if (res > 0) {
              console.log(res);
              console.log("ENTRE AL CONTEO DE CAJAS");
              var tarifa_currier = $("#tarifa_base").val();
              var tarifa_te = $("#tasa_base_te").val();
              //HAY CAJAS REGISTRADAS
              $.ajax({
                method: "POST",
                url: "ajax/updateBoxesForTrip.php",
                data: {
                  trip_id: trip_id,
                  tarifa_currier: tarifa_currier,
                  tarifa_te: tarifa_te,
                  wh_id: wh_id,
                },
                success: function (res) {
                  console.log(res);
                  console.log("ACTUALIZA LOS DATOS");
                  $("#boxes-table").html(res);
                },
              });
            }
          },
        });
      },
    });
  }
}
function changeTripOcean() {
  const trip_id = 2;
  if (trip_id == 2) {
    $("#aereo").css("display", "none");
    $("#maritimo").css("display", "block");

    $("#trip_air").css("background-color", "white");
    $("#trip_ocean").css("background-color", "#005574");
    $("#trip").val(2);
  } else {
    $("#maritimo").css("display", "block");
    $("#aereo").css("display", "none");
  }
  const wh_id = $("#warehouse_id").val();
  //** CONFIRMAR SI HAY CAJAS REGISTRADAS PARA ESE WAREHOUSE */
  const ciudad = $("#address_send").val();
  const currier_id = $("#currier").val();

  if (ciudad == 0) {
    $(".register-error").html("Ingresa una ciudad de destino");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/findCostForCurrier.php",
      data: { trip_id: trip_id, region: ciudad, currier_id: currier_id },
      success: function (res) {
        console.log("ENTRE EN EL COSTO POR TIPO DE ENVIO");
        //ACTUALIZAR VALORES DE TARIFAS
        console.log(res);
        const dataUser = res.split("-");
        const tarifa = dataUser[0];
        const tarifaTE = dataUser[1];

        $("#tarifa_base").val(tarifa);
        $("#tasa_base_te").val(tarifaTE);

        // RECALCULAR LA INFORMACION

        var ancho = parseInt($("#box-width").val());
        var largo = parseInt($("#box-lenght").val());
        var alto = parseInt($("#box-height").val());
        var peso = parseInt($("#box-weight").val());

        // calcular pie cubico y lb volumetricas

        var piecub = (largo * ancho * alto) / 1728;
        var lbVol = (alto * largo * ancho) / 166;

        var piecubRend = piecub.toFixed(3);
        var lbVolRend = lbVol.toFixed(3);
        var pesokg = peso * 0.4535;
        var metrovol = lbVol / 355;
        var metrocub = piecub * 0.2832;

        // redondeo de las medidas
        var pesor = pesokg.toFixed(3);
        var metrocubr = metrocub.toFixed(3);

        $("#pieCubico").val(piecubRend);
        $("#LBVOL").val(lbVolRend);
        $("#pesolb").val(peso);
        $("#pesokg").val(pesor);
        $("#metrocubico").val(metrocubr);

        // CALCULO DE COSTOS DE PAQUETES

        var tarifa_currier = $("#tarifa_base").val();
        var tarifa_te = $("#tasa_base_te").val();
        var tarifa_interna = $("#tarifaInterna_base").val();
        var tarifa_material = $("#material_tasa").val();
        const trip_id = $("#trip").val();

        /************************************************* */
        var costo_currier = 0;
        var monto_te = 0;
        var costo_material = 0;
        var costo_interno = 0;

        //calculo del costo interno
        if (tarifa_interna > 0) {
          if (trip_id == 1) {
            //El envio es aereo
            if (peso > lbVol) {
              costo_interno = peso * tarifa_interna;
            } else {
              costo_interno = lbVolRend * tarifa_interna;
            }
          } else {
            costo_interno = piecubRend * tarifa_interna;
          }
        } else {
          costo_interno = 0;
        }

        if (trip_id == 1) {
          //El envio es aereo
          if (peso > lbVol) {
            costo_currier = peso * tarifa_currier;
            monto_te = peso * tarifa_te;
          } else {
            costo_currier = lbVolRend * tarifa_currier;
            monto_te = lbVolRend * tarifa_te;
          }
        } else {
          costo_currier = piecubRend * tarifa_currier;
          monto_te = piecubRend * tarifa_te;
        }

        /** CALCULO DE TARIFA INTERNA*/

        /**CALCULO COSTO MATERIAL */
        if (tarifa_material > 0) {
          costo_material = tarifa_material;
        } else {
          costo_material = 0;
        }

        /**CALCULOS TOTALES */
        var costo_total = 0;
        var monto_total = 0;
        var ganancia = 0;
        var porcGanancia = 0;

        costo_total = costo_currier + costo_interno + costo_material;
        total = monto_te + costo_total;
        ganancia = total - costo_total;
        porcGanancia = (ganancia / total) * 100;

        // redondeo de datos finales

        var costo_currierr = costo_currier.toFixed(3);
        var costo_internor = costo_interno.toFixed(3);

        var costo_totalr = costo_total.toFixed(3);
        var totalr = total.toFixed(3);
        var gananciar = ganancia.toFixed(3);
        var porcGananciar = porcGanancia.toFixed(3);

        //asignar valores a los campos correspondientes

        $("#currier_tasa").val(costo_currierr);
        $("#costo_interno").val(costo_internor);
        $("#material_tasa").val(costo_material);
        $("#costo_total").val(costo_totalr);
        $("#total").val(totalr);
        $("#ganancia").val(gananciar);
        $("#porcGanancia").val(porcGananciar);
        // ACTUALIZAR DATOS EN CASO DE EXISTIR CAJAS
        $.ajax({
          method: "POST",
          url: "ajax/findBoxesForWarehouse.php",
          data: { wh_id: wh_id },
          success: function (res) {
            if (res > 0) {
              console.log(res);
              console.log("ENTRE AL CONTEO DE CAJAS");
              var tarifa_currier = $("#tarifa_base").val();
              var tarifa_te = $("#tasa_base_te").val();
              //HAY CAJAS REGISTRADAS
              $.ajax({
                method: "POST",
                url: "ajax/updateBoxesForTrip.php",
                data: {
                  trip_id: trip_id,
                  tarifa_currier: tarifa_currier,
                  tarifa_te: tarifa_te,
                  wh_id: wh_id,
                },
                success: function (res) {
                  console.log(res);
                  console.log("ACTUALIZA LOS DATOS");
                  $("#boxes-table").html(res);
                },
              });
            }
          },
        });
      },
    });
  }
}
/***************************************************************** */
function registerDestinerForClient() {
  var id = $("#destiner_id").val();
  const name = $("#name_destiner").val();
  const lastname = $("#lastname_destiner").val();
  const contact_phone = $("#contact_phone_destiner").val();
  const country_id = $("#address_destiny_destiner_for_client").val();
  const user_id = $("#user_id").val();

  let address = "";

  // la seleccion del pais es venezuela
  if (country_id == 1) {
    address = $("#select_address_destiner_for_client").val();
  } else {
    address = $("#address_destiner_for_client").val();
  }

  let address_state = "";
  if (country_id == 1) {
    address_state = $("#select_state_destiner_for_client").val();
  } else {
    address_state = $("#address_state_destiner_for_client").val();
  }

  let address_city = "";
  if (country_id == 1) {
    address_city = $("#select_city_destiner_for_client").val();
  } else {
    address_city = $("#address_city_destiner_for_client").val();
  }

  $.ajax({
    method: "POST",
    url: "ajax/addDestinerForClient.php",
    data: {
      name: name,
      lastname: lastname,
      contact_phone: contact_phone,
      address: address,
      country_id: country_id,
      state_id: address_state,
      city_id: address_city,
      user_id: user_id,
    },
    success: function (res) {
      $(".register-success").html("¡Registro exitoso!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function editarDestiner(id) {
  console.log(id);
  $.ajax({
    method: "POST",
    url: "ajax/searchDestinerDataById.php",
    data: { id: id },
    success: function (res) {
      console.log(res);
      const dataUser = res.split("-");

      const id = dataUser[0];
      const name = dataUser[1];
      const lastname = dataUser[2];
      const country_id = dataUser[3];

      const state_id = dataUser[4];
      const city_id = dataUser[5];
      const address = dataUser[6];
      const contact_phone = dataUser[7];

      const country_name = dataUser[8];
      const state_name = dataUser[9];
      const city_name = dataUser[10];

      if (state_id == 0) {
        $(".address_state_destiner_for_client").css("display", "block");
        $(".select_state_destiner_for_client").css("display", "none");

        $("#address_state_destiner_for_client").val(state_name);
        console.log("entre en el nombre del estado");
      } else {
        $(".address_state_destiner_for_client").css("display", "none");
        $(".select_state_destiner_for_client").css("display", "block");

        $(
          "#select_state_destiner_for_client option[value=" + state_id + "]"
        ).attr("selected", true);

        console.log("entre en el identificador del estado");
      }
      if (city_id == 0) {
        $(".address_city_destiner_for_client").css("display", "block");
        $(".select_city_destiner_for_client").css("display", "none");

        $("#address_city_destiner_for_client").val(city_name);

        $(".address_address_destiner_for_client").css("display", "block");
        $(".select_address_destiner_for_client").css("display", "none");
        $("#address_address_destiner_for_client").val(address);
        console.log("entre en el nombre de la ciudad");
      } else {
        $(".address_city_destiner_for_client").css("display", "none");
        $(".select_city_destiner_for_client").css("display", "block");

        $(
          "#select_city_destiner_for_client option[value=" + city_id + "]"
        ).attr("selected", true);

        $(".address_address_destiner_for_client").css("display", "none");
        $(".select_address_destiner_for_client").css("display", "block");
        $("#select_address_destiner_for_client").val(address);
        console.log("entre en el identificador de la ciudad");
      }

      $(
        "#address_destiny_destiner_for_client option[value=" + country_id + "]"
      ).attr("selected", true);
      $("#destiner_id").val(id);
      $("#name_destiner").val(name);
      $("#lastname_destiner").val(lastname);
      $("#contact_phone_destiner").val(contact_phone);

      $("#button-edit-destiner-for-client").css("display", "block");
      $("#button-register-destiner-for-client").css("display", "none");
    },
  });
}

function seleccionarInternCurrier() {
  const trip_id = $("#trip").val();
  // ciudad destino
  const ciudad = $("#address_send").val();
  const currier_id = $("#select_new_intern_currier").val();

  // $('#tarifa').val(trip_id);
  // $('#tarifaInterna').val(region);
  // $('#tasa').val(currier_id);

  $.ajax({
    method: "POST",
    url: "ajax/findCostForCurrierIntern.php",
    data: { trip_id: trip_id, region: ciudad, currier_id: currier_id },
    success: function (res) {
      console.log(res);
      const dataUser = res.split("-");
      const tarifa = dataUser[0];
      const tarifaTE = dataUser[1];

      $("#tarifaInterna_base").val(tarifaTE);

      var ancho = parseInt($("#box-width").val());
      var largo = parseInt($("#box-lenght").val());
      var alto = parseInt($("#box-height").val());
      var peso = parseInt($("#box-weight").val());

      // calcular pie cubico y lb volumetricas

      var piecub = (largo * ancho * alto) / 1728;
      var lbVol = (alto * largo * ancho) / 166;

      var piecubRend = piecub.toFixed(3);
      var lbVolRend = lbVol.toFixed(3);
      var pesokg = peso * 0.4535;
      var metrovol = lbVol / 355;
      var metrocub = piecub * 0.2832;

      // redondeo de las medidas
      var pesor = pesokg.toFixed(3);
      var metrocubr = metrocub.toFixed(3);

      $("#pieCubico").val(piecubRend);
      $("#LBVOL").val(lbVolRend);
      $("#pesolb").val(peso);
      $("#pesokg").val(pesor);
      $("#metrocubico").val(metrocubr);

      // CALCULO DE COSTOS DE PAQUETES

      var tarifa_currier = $("#tarifa_base").val();
      var tarifa_te = $("#tasa_base_te").val();
      var tarifa_interna = $("#tarifaInterna_base").val();
      var tarifa_material = $("#material_tasa").val();
      const trip_id = $("#trip").val();

      /************************************************* */
      var costo_currier = 0;
      var monto_te = 0;
      var costo_material = 0;
      var costo_interno = 0;

      //calculo del costo interno
      if (tarifa_interna > 0) {
        if (trip_id == 1) {
          //El envio es aereo
          if (peso > lbVol) {
            costo_interno = peso * tarifa_interna;
          } else {
            costo_interno = lbVolRend * tarifa_interna;
          }
        } else {
          costo_interno = piecubRend * tarifa_interna;
        }
      } else {
        costo_interno = 0;
      }

      if (trip_id == 1) {
        //El envio es aereo
        if (peso > lbVol) {
          costo_currier = peso * tarifa_currier;
          monto_te = peso * tarifa_te;
        } else {
          costo_currier = lbVolRend * tarifa_currier;
          monto_te = lbVolRend * tarifa_te;
        }
      } else {
        costo_currier = piecubRend * tarifa_currier;
        monto_te = piecubRend * tarifa_te;
      }

      /** CALCULO DE TARIFA INTERNA*/

      /**CALCULO COSTO MATERIAL */
      if (tarifa_material > 0) {
        costo_material = tarifa_material;
      } else {
        costo_material = 0;
      }

      /**CALCULOS TOTALES */
      var costo_total = 0;
      var monto_total = 0;
      var ganancia = 0;
      var porcGanancia = 0;

      costo_total = costo_currier + costo_interno + costo_material;
      total = monto_te + costo_total;
      ganancia = total - costo_total;
      porcGanancia = (ganancia / total) * 100;

      // redondeo de datos finales

      var costo_currierr = costo_currier.toFixed(3);
      var costo_internor = costo_interno.toFixed(3);

      var costo_totalr = costo_total.toFixed(3);
      var totalr = total.toFixed(3);
      var gananciar = ganancia.toFixed(3);
      var porcGananciar = porcGanancia.toFixed(3);

      //asignar valores a los campos correspondientes

      $("#currier_tasa").val(costo_currierr);
      $("#costo_interno").val(costo_internor);
      $("#material_tasa").val(costo_material);
      $("#costo_total").val(costo_totalr);
      $("#total").val(totalr);
      $("#ganancia").val(gananciar);
      $("#porcGanancia").val(porcGananciar);
    },
  });
  /*$('.currier_Intern').css('display','none');
	$('.button_register_new_currier').css('display','block');*/
}

function registerParam() {
  const name = $("#name").val();
  const description = $("#description").val();
  const cost = $("#cost").val();
  $.ajax({
    method: "POST",
    url: "ajax/addParametres.php",
    data: { name: name, description: description, cost_value: cost },
    success: function (res) {
      $(".register-success").html("¡Registro exitoso!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".register-success").fadeOut("fast");
      }, 2000);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function newCurrierIntern() {
  $(".currier_Intern").css("display", "block");
  $(".button_register_new_currier").css("display", "none");
  $.ajax({
    method: "POST",
    url: "ajax/newCurrierList.php",
    success: function (res) {
      $(".currier_Intern").html(res);
    },
  });
}

function addnewCost(id, wh_id) {
  const cost_id = $("#extra_cost").val();
  const box_id = $("#box_id").val();
  const value_cost = $("#costo").val();

  $.ajax({
    method: "POST",
    url: "ajax/createExtraCost.php",
    data: { cost_id: cost_id, box_id: box_id, value_cost: value_cost },
    success: function (res) {
      $("#extra_cos").val("");
      // $("#box_id").val('');
      $("#costo").val("");
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
  });
}

function addInterCurrier() {
  const currier_id = $("#currier_id").val();
  const city_id = $("#city_id").val();
  const trip_id = $("#trip_id").val();
  const tarifa = $("#tarifa").val();
  const tarifaTE = $("#tarifaTE").val();

  $.ajax({
    method: "POST",
    url: "ajax/createInternCurrier.php",
    data: {
      currier_id: currier_id,
      city_id: city_id,
      trip_id: trip_id,
      tarifa: tarifa,
      tarifaTE: tarifaTE,
    },
    success: function (res) {
      $("#currier_id").val("");
      $("#city_id").val("");
      $("#trip_id").val("");
      $("#tarifa").val("0.00");
      $("#tarifaTE").val("0.00");

      $(".register-success").html("¡Registro exitoso!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".register-success").fadeOut("fast");
      }, 2000);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function addNewInterCurrier() {
  const currier_name = $("#intern_currier_name").val();

  $.ajax({
    method: "POST",
    url: "ajax/createNewInternCurrier.php",
    data: { currier_name: currier_name },
    success: function (res) {
      $("#intern_currier_name").val("");

      $(".register-success").html("¡Registro exitoso!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function addCityCurrierIntern() {
  const country = $("#country").val();
  const state_id = $("#state_id").val();
  const region_id = $("#region_id").val();
  const city_name = $("#city_name").val();

  $.ajax({
    method: "POST",
    url: "ajax/createNewCity.php",
    data: {
      country: country,
      state_id: state_id,
      region_id: region_id,
      city_name: city_name,
    },
    success: function (res) {
      $("#country").val("");
      $("#state_id").val("");
      $("#region_id").val("");
      $("#city_name").val("");

      $(".register-success").html("¡Registro exitoso!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

//******* */

function addTarifaCurrier() {
  const currier_id = $("#currier_id_tarifa").val();
  const city_id = $("#city_id_tarifa").val();
  const trip_id = $("#trip_id_tarifa").val();
  const tarifa = $("#tarifa_currier").val();
  const tarifaTE = $("#tarifaTE_currier").val();

  $.ajax({
    method: "POST",
    url: "ajax/createCurrier.php",
    data: {
      currier_id: currier_id,
      city_id: city_id,
      trip_id: trip_id,
      tarifa: tarifa,
      tarifaTE: tarifaTE,
    },
    success: function (res) {
      $("#currier_id_tarifa").val("");
      $("#city_id_tarifa").val("");
      $("#trip_id_tarifa").val("");
      $("#tarifa_currier").val("0.00");
      $("#tarifaTE_currier").val("0.00");

      $(".register-success").html("¡Registro exitoso!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".register-success").fadeOut("fast");
      }, 2000);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function addNewCurrier() {
  const currier_name = $("#currier_name").val();

  $.ajax({
    method: "POST",
    url: "ajax/createNewCurrier.php",
    data: { currier_name: currier_name },
    success: function (res) {
      $("#currier_name").val("");

      $(".register-success").html("¡Registro exitoso!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function addCityCurrier() {
  const country = $("#country_currier").val();
  const state_id = $("#state_id_currier").val();
  const region_id = $("#region_id_currier").val();
  const city_name = $("#city_name_currier").val();

  $.ajax({
    method: "POST",
    url: "ajax/createNewCity.php",
    data: {
      country: country,
      state_id: state_id,
      region_id: region_id,
      city_name: city_name,
    },
    success: function (res) {
      $("#country_currier").val("");
      $("#state_id_currier").val("");
      $("#region_id_currier").val("");
      $("#city_name_currier").val("");

      $(".register-success").html("¡Registro exitoso!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

//********** */

function openRegisterCurrier() {
  $("#register-currier-intern-open").css("display", "block");
}

function openRegisterCity() {
  $("#register-city-intern-open").css("display", "block");
}

function register(w) {
  const type_user_id = $("#user_type").val();
  //el registro es de un cliente
  if (type_user_id == 2) {
    const countryVenezuela = $("#countryVenezuela").val();
    const name = $("#name").val();
    const lastname = $("#lastname").val();
    const email = $("#email").val();
    const password = $("#password").val();
    const phone = $("#phone").val();
    const phone_house = $("#phone_house").val();
    const documentU = $("#document").val();
    //selecciona el pais desde donde se registra el cliente
    const country = $("#address_destiny").val();
    const code = "TE-0000";

    let address = "";
    if (w == 1) {
      // la seleccion del pais es venezuela
      if (countryVenezuela == 1) {
        address = $("#select_address").val();
      } else {
        address = $("#address").val();
      }
    } else {
      address = "NONE";
    }

    let address_state = "";
    if (w == 1) {
      if (countryVenezuela == 1) {
        address_state = $("#select_state").val();
      } else {
        address_state = $("#address_state").val();
      }
    } else {
      address_state = "NONE";
    }

    let address_city = "";
    if (w == 1) {
      if (countryVenezuela == 1) {
        address_city = $("#select_city").val();
      } else {
        address_city = $("#address_city").val();
      }
    } else {
      address_city = "NONE";
    }

    const headquarter = $("#headquarter").val();

    const born_date = $("#born_date").val();
    const user_type = $("#user_type").val();

    if (name == "") {
      $(".user-error").html("Debe ingresar un nombre");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un nombre");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (lastname == "") {
      $(".user-error").html("Debe ingresar un apellido");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un apellido");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (born_date == "" || born_date == "0") {
      $(".user-error").html("Debe ingresar una fecha de nacimiento");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una fecha de nacimiento");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (email == "" || isEmail(email) == false) {
      $(".user-error").html("Debe ingresar un email correcto");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un email");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (password == "") {
      $(".user-error").html("Debe ingresar un password");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un password");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (phone == "") {
      $(".user-error").html("Debe ingresar un teléfono");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un telefono");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (documentU == "") {
      $(".user-error").html("Debe ingresar un documento");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un documento");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (country == "" || country == "0") {
      $(".user-error").html("Debe ingresar un país");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un país");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (address == "") {
      $(".user-error").html("Debe ingresar una dirección");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una dirección");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (address_state == "") {
      $(".user-error").html("Debe ingresar un estado");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un estado");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (address_city == "") {
      $(".user-error").html("Debe ingresar una ciudad");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una ciudad");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (headquarter == "" || headquarter == "0") {
      $(".user-error").html("Debe ingresar una agencia");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una agencia");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (code == "" || code == "0") {
      $(".user-error").html("Debe ingresar un codigo de casillero");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un codigo de casillero");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else {
      $(".register-button").html(
        '<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>'
      );
      $.ajax({
        method: "POST",
        url: "ajax/createUser.php",
        data: {
          w: w,
          countryVenezuela: countryVenezuela,
          name: name,
          lastname: lastname,
          email: email,
          password: password,
          phone: phone,
          documentU: documentU,
          country: country,
          address: address,
          headquarter: headquarter,
          code: code,
          born_date: born_date,
          user_type: user_type,
          address_city: address_city,
          address_state: address_state,
        },
        success: function (res) {
          $(".register-button").html("Registrar");
          if (res != "Existe") {
            if (w == 1) {
              // selectUserForP(dataUser[0]);

              const texto = res;
              let procesado;

              // trim() elimina los espacios a ambos lados de nuestra cadena
              procesado = texto.trim(); // > "Texto de ejemplo"

              $url = "confirm_register_view.php?id=" + procesado;
              $(".register-success").html("Usuario registrado.");
              $(".register-success").fadeIn("fast");

              setTimeout(function () {
                $(location).attr("href", $url);
              }, 2000);

              $(document).scrollTop(0);
            } else {
              console.log(res);
              const dataUser = res.split("-");
              selectUserForP(dataUser[0], dataUser[1]);
              $(".user-success").html("Usuario registrado.");
              $(".user-success").fadeIn("fast");
              setTimeout(function () {
                $("#newUserModal").modal("hide");
                $("#warehouseModal").modal("show");
                $(".user-success").fadeOut("fast");
              }, 3000);
            }
          } else if (res == "Existe") {
            $(".user-error").html("El correo que intenta registrar ya existe");
            $(".user-error").fadeIn("fast");
            $(document).scrollTop(0);
            setTimeout(function () {
              $(".user-error").fadeOut("fast");
            }, 3000);

            $(".register-error").html(
              "El correo que intenta registrar ya existe"
            );
            $(".register-error").fadeIn("fast");
            $(document).scrollTop(0);
            setTimeout(function () {
              $(".register-error").fadeOut("fast");
            }, 3000);
          }
        },
      });
    }
  } else {
    const countryVenezuela = $("#countryVenezuela").val();
    const name = $("#company_name").val();
    const lastname = "empresa";
    const email = $("#company_email").val();
    const password = $("#company_password").val();
    const phone = $("#company_phone").val();
    const phone_house = "";
    const documentU = $("#company_document").val();
    const country = $("#address_destiny_company").val();
    const code = "TE-0000";

    let address = "";
    if (w == 1) {
      if (countryVenezuela == 1) {
        address = $("#select_address_company").val();
      } else {
        address = $("#address_company").val();
      }
    } else {
      address = "NONE";
    }

    let address_state = "";
    if (w == 1) {
      if (countryVenezuela == 1) {
        address_state = $("#select_state_company").val();
      } else {
        address_state = $("#address_state_company").val();
      }
    } else {
      address_state = "NONE";
    }

    let address_city = "";
    if (w == 1) {
      if (countryVenezuela == 1) {
        address_city = $("#select_city_company").val();
      } else {
        address_city = $("#address_city_company").val();
      }
    } else {
      address_city = "NONE";
    }

    const headquarter = $("#headquarter").val();

    const born_date = "1990-01-01 00:00:00";
    const user_type = $("#user_type").val();

    if (name == "") {
      $(".user-error").html("Debe ingresar un nombre");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un nombre");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (lastname == "") {
      $(".user-error").html("Debe ingresar un apellido");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un apellido");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (born_date == "" || born_date == "0") {
      $(".user-error").html("Debe ingresar una fecha de nacimiento");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una fecha de nacimiento");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (email == "" || isEmail(email) == false) {
      $(".user-error").html("Debe ingresar un email correcto");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un email");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (password == "") {
      $(".user-error").html("Debe ingresar un password");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un password");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (phone == "") {
      $(".user-error").html("Debe ingresar un teléfono");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un telefono");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (documentU == "") {
      $(".user-error").html("Debe ingresar un documento");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un documento");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (country == "" || country == "0") {
      $(".user-error").html("Debe ingresar un país");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un país");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (address == "") {
      $(".user-error").html("Debe ingresar una dirección");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una dirección");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (address_state == "") {
      $(".user-error").html("Debe ingresar un estado");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un estado");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (address_city == "") {
      $(".user-error").html("Debe ingresar una ciudad");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una ciudad");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (headquarter == "" || headquarter == "0") {
      $(".user-error").html("Debe ingresar una agencia");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una agencia");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (code == "" || code == "0") {
      $(".user-error").html("Debe ingresar un codigo de casillero");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un codigo de casillero");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else {
      $(".register-button").html(
        '<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>'
      );
      $.ajax({
        method: "POST",
        url: "ajax/createUser.php",
        data: {
          w: w,
          countryVenezuela: countryVenezuela,
          name: name,
          lastname: lastname,
          email: email,
          password: password,
          phone: phone,
          documentU: documentU,
          country: country,
          address: address,
          headquarter: headquarter,
          code: code,
          born_date: born_date,
          user_type: user_type,
          address_city: address_city,
          address_state: address_state,
        },
        success: function (res) {
          $(".register-button").html("Registrar");
          const texto = res;
          let procesado;

          // trim() elimina los espacios a ambos lados de nuestra cadena
          procesado = texto.trim(); // > "Texto de ejemplo"

          $url = "confirm_register_view.php?id=" + procesado;
          $(".register-success").html("Usuario registrado.");
          $(".register-success").fadeIn("fast");

          setTimeout(function () {
            $(location).attr("href", $url);
          }, 2000);

          $(document).scrollTop(0);
        },
      });
    }
  }
}

function DeleteClientAndDestiner() {
  const id = $("#user_id_modal").val();
  $.ajax({
    method: "POST",
    url: "ajax/deleteClientAndDestiner.php",
    data: { id: id },
    success: function (res) {
      $(".register-success").html("Data eliminada");
      $(".register-success").fadeIn("fast");
      setTimeout(function () {
        location.reload();
      }, 3000);

      $(document).scrollTop(0);
    },
  });
}

function cambiarUserType() {
  const id = $("#user_id_modal_type").val();
  const user_type = $("#user_type_client").val();
  $.ajax({
    method: "POST",
    url: "ajax/updateUserType.php",
    data: { id: id, user_type: user_type },
    success: function (res) {
      $(".register-success").html("Edición exitosa");
      $(".register-success").fadeIn("fast");
      setTimeout(function () {
        location.reload();
      }, 1000);

      $(document).scrollTop(0);
    },
  });
}

function showEditAlert(id) {
  $.ajax({
    method: "POST",
    url: "ajax/searchAlertData.php",
    data: { id: id },
    success: function (res) {
      console.log(res);

      const dataUser = res.split("-");
      const alert_id = dataUser[0];
      const tracking = dataUser[1];
      const id_client = dataUser[2];
      const client_name = dataUser[3];
      const package_name = dataUser[4];
      const transport_id = dataUser[6];
      const cost = dataUser[7];
      const content = dataUser[8];

      $("#alert_id").val(alert_id);
      $("#nroTracking").val(tracking);
      $("#id_client_user_type").val(id_client);
      $("#client_name").val(client_name);
      $("#package_name_user_type").val(package_name);
      $("#transport_id").val(transport_id);
      $("#cost_user_type").val(cost);
      $("#content_user_type").val(content);

      $("#editar-alerts").css("display", "block");
    },
  });
}

function editAlert() {
  const id = $("#alert_id").val();
  const tracking = $("#nroTracking").val();
  const id_client = $("#id_client_user_type").val();
  const client_name = $("#client_name").val();
  const package_name = $("#package_name_user_type").val();
  const transport_id = $("#transport_id").val();
  const cost = $("#cost_user_type").val();
  const content = $("#content_user_type").val();

  $.ajax({
    method: "POST",
    url: "ajax/updateAlert.php",
    data: {
      id: id,
      tracking: tracking,
      id_client: id_client,
      client_name: client_name,
      package_name: package_name,
      transport_id: transport_id,
      cost: cost,
      content: content,
    },
    success: function (res) {
      console.log(res);

      $(".register-success").html("Edición exitosa");
      $(".register-success").fadeIn("fast");
      setTimeout(function () {
        location.reload();
      }, 1000);

      $(document).scrollTop(0);
    },
  });
}

function deleteModalForClient(id) {
  $("#deleteClientForModal").modal("show");
  $("#user_id_modal").val(id);
}

function openUserTypeModal(id) {
  $("#userTypeModal").modal("show");
  $("#user_id_modal_type").val(id);
}

// REGISTRO DE CLIENTE DESDE EL MODULO DE CLIENTES
function registerModClient(w) {
  const type_user_id = $("#user_type").val();
  console.log(type_user_id);
  //el registro es de un cliente.
  console.log("entro aqui 1");
  if (type_user_id == 2) {
    console.log("entro aqui 2");
    const countryVenezuela = $("#countryVenezuela").val();
    const name = $("#name").val();
    const lastname = $("#lastname").val();
    const alias = $("#alias_destiner").val();
    const cedula = $("#cedula_destiner").val();
    const email = $("#email").val();
    console.log(email);
    const password = $("#password").val();
    const phone = $("#phone").val();
    const phone_house = $("#phone_house").val();
    const documentU = $("#document").val();
    //selecciona el pais desde donde se registra el cliente
    const country = $("#address_destiny").val();
    const code = "TE-0000";

    console.log(w);
    console.log(countryVenezuela);

    let address = "";
    if (w == 1) {
      // la seleccion del pais es venezuela
      if (countryVenezuela == 1) {
        address = $("#select_address").val();
      } else {
        address = $("#address").val();
      }
    } else {
      address = "NONE";
    }

    let address_state = "";
    if (w == 1) {
      if (countryVenezuela == 1) {
        address_state = $("#select_state").val();
      } else {
        address_state = $("#address_state").val();
      }
    } else {
      address_state = "NONE";
    }

    let address_city = "";
    if (w == 1) {
      if (countryVenezuela == 1) {
        address_city = $("#select_city").val();
      } else {
        address_city = $("#address_city").val();
      }
    } else {
      address_city = "NONE";
    }

    const headquarter = $("#headquarter").val();

    const born_date = $("#born_date").val();
    const user_type = $("#user_type").val();

    if (name == "") {
      $(".user-error").html("Debe ingresar un nombre");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un nombre");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (lastname == "") {
      $(".user-error").html("Debe ingresar un apellido");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un apellido");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (born_date == "" || born_date == "0") {
      $(".user-error").html("Debe ingresar una fecha de nacimiento");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una fecha de nacimiento");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (email == "" || isEmail(email) == false) {
      $(".user-error").html("Debe ingresar un email correcto");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un email");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (password == "") {
      $(".user-error").html("Debe ingresar un password");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un password");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (phone == "") {
      $(".user-error").html("Debe ingresar un teléfono");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un telefono");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (documentU == "") {
      $(".user-error").html("Debe ingresar un documento");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un documento");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (country == "" || country == "0") {
      $(".user-error").html("Debe ingresar un país");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un país");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (address == "") {
      $(".user-error").html("Debe ingresar una dirección");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una dirección");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (address_state == "") {
      $(".user-error").html("Debe ingresar un estado");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un estado");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (address_city == "") {
      $(".user-error").html("Debe ingresar una ciudad");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una ciudad");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (headquarter == "" || headquarter == "0") {
      $(".user-error").html("Debe ingresar una agencia");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una agencia");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (code == "" || code == "0") {
      $(".user-error").html("Debe ingresar un codigo de casillero");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un codigo de casillero");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else {
      $(".register-button").html(
        '<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>'
      );

      console.log("1 " + type_user_id);
      $.ajax({
        method: "POST",

        url: "ajax/createUser.php",
        data: {
          w: w,
          countryVenezuela: countryVenezuela,
          phone_house: phone_house,
          name: name,
          lastname: lastname,
          email: email,
          password: password,
          phone: phone,
          documentU: documentU,
          country: country,
          address: address,
          headquarter: headquarter,
          code: code,
          born_date: born_date,
          user_type: user_type,
          address_city: address_city,
          address_state: address_state,
        },
        success: function (res) {
          $(".register-button").html("Registrar");
          if (res != "Existe") {
            if (w == 1) {
              // selectUserForP(dataUser[0]);

              $(".register-success").html("Usuario registrado.");
              $(".register-success").fadeIn("fast");

              // setTimeout(function(){
              // 	$(location).attr('href','destinatario.php');
              // }, 2000);

              //   $(document).scrollTop(0);
            } else {
              console.log(res);
              const dataUser = res.split("-");
              selectUserForP(dataUser[0], dataUser[1]);
              $(".user-success").html("Usuario registrado.");
              $(".user-success").fadeIn("fast");
              setTimeout(function () {
                $("#newUserModal").modal("hide");
                $("#warehouseModal").modal("show");
                $(".user-success").fadeOut("fast");
              }, 3000);
            }
          } else if (res == "Existe") {
            $(".user-error").html("El correo que intenta registrar ya existe");
            $(".user-error").fadeIn("fast");
            $(document).scrollTop(0);
            setTimeout(function () {
              $(".user-error").fadeOut("fast");
            }, 3000);

            $(".register-error").html(
              "El correo que intenta registrar ya existe"
            );
            $(".register-error").fadeIn("fast");
            $(document).scrollTop(0);
            setTimeout(function () {
              $(".register-error").fadeOut("fast");
            }, 3000);
          }
        },
      });
    }
  } else {
    const countryVenezuela = $("#countryVenezuela").val();
    const name = $("#company_name").val();
    const lastname = "empresa";
    const email = $("#company_email").val();
    const password = $("#company_password").val();
    const phone = $("#company_phone").val();
    const phone_house = "";
    const documentU = $("#company_document").val();
    const country = $("#address_destiny_company").val();
    const code = "TE-0000";

    let address = "";
    if (w == 1) {
      if (countryVenezuela == 1) {
        address = $("#select_address_company").val();
      } else {
        address = $("#address_company").val();
      }
    } else {
      address = "NONE";
    }

    let address_state = "";
    if (w == 1) {
      if (countryVenezuela == 1) {
        address_state = $("#select_state_company").val();
      } else {
        address_state = $("#address_state_company").val();
      }
    } else {
      address_state = "NONE";
    }

    let address_city = "";
    if (w == 1) {
      if (countryVenezuela == 1) {
        address_city = $("#select_city_company").val();
      } else {
        address_city = $("#address_city_company").val();
      }
    } else {
      address_city = "NONE";
    }

    const headquarter = $("#headquarter").val();

    const born_date = "1990-01-01 00:00:00";
    const user_type = $("#user_type").val();

    if (name == "") {
      $(".user-error").html("Debe ingresar un nombre");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un nombre");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (lastname == "") {
      $(".user-error").html("Debe ingresar un apellido");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un apellido");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (born_date == "" || born_date == "0") {
      $(".user-error").html("Debe ingresar una fecha de nacimiento");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una fecha de nacimiento");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (email == "" || isEmail(email) == false) {
      $(".user-error").html("Debe ingresar un email correcto");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un email");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (password == "") {
      $(".user-error").html("Debe ingresar un password");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un password");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (phone == "") {
      $(".user-error").html("Debe ingresar un teléfono");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un telefono");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (documentU == "") {
      $(".user-error").html("Debe ingresar un documento");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un documento");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (country == "" || country == "0") {
      $(".user-error").html("Debe ingresar un país");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un país");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (address == "") {
      $(".user-error").html("Debe ingresar una dirección");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una dirección");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (address_state == "") {
      $(".user-error").html("Debe ingresar un estado");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un estado");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (address_city == "") {
      $(".user-error").html("Debe ingresar una ciudad");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una ciudad");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (headquarter == "" || headquarter == "0") {
      $(".user-error").html("Debe ingresar una agencia");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar una agencia");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else if (code == "" || code == "0") {
      $(".user-error").html("Debe ingresar un codigo de casillero");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 3000);
      $(".register-error").html("Debe ingresar un codigo de casillero");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 3000);
    } else {
      $(".register-button").html(
        '<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>'
      );
      console.log("2 " + type_user_id);
      $.ajax({
        method: "POST",
        url: "ajax/createUser.php",
        data: {
          w: w,
          countryVenezuela: countryVenezuela,
          name: name,
          lastname: lastname,
          email: email,
          password: password,
          phone: phone,
          documentU: documentU,
          country: country,
          address: address,
          headquarter: headquarter,
          code: code,
          born_date: born_date,
          user_type: user_type,
          address_city: address_city,
          address_state: address_state,
        },
        success: function (res) {
          $(".register-button").html("Registrar");
          $(".register-success").html("Usuario registrado.");
          $(".register-success").fadeIn("fast");

          setTimeout(function () {
            $(location).attr("href", "destinatario.php");
          }, 2000);

          $(document).scrollTop(0);
        },
      });
    }
  }
}

function editClientForMod() {
  const type_user_id = 2;
  const id = $("#user_id_client").val();
  //el registro es de un cliente
  if (type_user_id == 2) {
    const countryVenezuela = $("#countryVenezuela_edit").val();
    const name = $("#name").val();
    const lastname = $("#lastname").val();
    const email = $("#email").val();
    const confirm_email = $("#confirm-email").val();
    const password = $("#password").val();
    const phone = $("#phone").val();
    const phone_house = $("#phone_house").val();
    const documentU = $("#document").val();
    //selecciona el pais desde donde se registra el cliente
    const country = $("#address_destiny").val();

    let address = "";

    // la seleccion del pais es venezuela
    if (countryVenezuela == 1) {
      address = $("#select_address").val();
    } else {
      address = $("#address").val();
    }

    let address_state = "";

    if (countryVenezuela == 1) {
      address_state = $("#select_state").val();
    } else {
      address_state = $("#address_state").val();
    }

    let address_city = "";

    if (countryVenezuela == 1) {
      address_city = $("#select_city").val();
    } else {
      address_city = $("#address_city").val();
    }

    //validación de campos

    if (confirm_email != email) {
      $(".user-error").html(
        "La confirmación de su correo electronico es erronea"
      );
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 2000);
      $(".register-error").html("Confirme su correo electronico");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
    } else if (name == "") {
      $(".user-error").html("Debe ingresar un nombre");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 2000);
      $(".register-error").html("Debe ingresar un nombre");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
    } else if (lastname == "") {
      $(".user-error").html("Debe ingresar un apellido");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 2000);
      $(".register-error").html("Debe ingresar un apellido");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
    } else if (born_date == "" || born_date == "0") {
      $(".user-error").html("Debe ingresar una fecha de nacimiento");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 2000);
      $(".register-error").html("Debe ingresar una fecha de nacimiento");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
    } else if (email == "" || isEmail(email) == false) {
      $(".user-error").html("Debe ingresar un email correcto");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 2000);
      $(".register-error").html("Debe ingresar un email");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
    } else if (phone == "") {
      $(".user-error").html("Debe ingresar un teléfono");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 2000);
      $(".register-error").html("Debe ingresar un telefono");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
    } else if (documentU == "") {
      $(".user-error").html("Debe ingresar un documento");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 2000);
      $(".register-error").html("Debe ingresar un documento");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
    } else if (country == "" || country == "0") {
      $(".user-error").html("Debe ingresar un país");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 2000);
      $(".register-error").html("Debe ingresar un país");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
    } else if (address == "") {
      $(".user-error").html("Debe ingresar una dirección");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 2000);
      $(".register-error").html("Debe ingresar una dirección");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
    } else if (address_state == "") {
      $(".user-error").html("Debe ingresar un estado");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 2000);
      $(".register-error").html("Debe ingresar un estado");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
    } else if (address_city == "") {
      $(".user-error").html("Debe ingresar una ciudad");
      $(".user-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".user-error").fadeOut("fast");
      }, 2000);
      $(".register-error").html("Debe ingresar una ciudad");
      $(".register-error").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
    } else {
      $.ajax({
        method: "POST",
        url: "ajax/updateUserForClient.php",
        data: {
          id: id,
          countryVenezuela: countryVenezuela,
          name: name,
          lastname: lastname,
          email: email,
          phone: phone,
          phone_house: phone_house,
          documentU: documentU,
          country: country,
          address: address,
          address_city: address_city,
          address_state: address_state,
        },
        success: function (res) {
          $(".register-button").html("Registrar");
          if (res != "Existe") {
            $(".register-success").html("Edición de usuario exitosa");
            $(".register-success").fadeIn("fast");

            console.log(res);
            setTimeout(function () {
              $(location).attr("href", "users.php");
            }, 2000);

            $(document).scrollTop(0);
          } else if (res == "Existe") {
            $(".user-error").html("El correo que intenta registrar ya existe");
            $(".user-error").fadeIn("fast");
            $(document).scrollTop(0);
            setTimeout(function () {
              $(".user-error").fadeOut("fast");
            }, 3000);

            $(".register-error").html(
              "El correo que intenta registrar ya existe"
            );
            $(".register-error").fadeIn("fast");
            $(document).scrollTop(0);
            setTimeout(function () {
              $(".register-error").fadeOut("fast");
            }, 3000);
          }
        },
      });
    }
  }
}

function deleteDestiner(id) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteDestiner.php",
    data: { id: id },
    success: function (res) {
      console.log(res);
      $(".register-success").html("Destinatario eliminado");
      $(".register-success").fadeIn("fast");
      setTimeout(function () {
        location.reload();
        // $(location).attr('href','destinatario.php');
      }, 3000);

      $(document).scrollTop(0);
    },
  });
}

//REGISTRO DE DESTINATARIOS

function showCliente() {
  $("#cliente-table").css("display", "block");
  $("#destinatario-table").css("display", "none");
  $("#cliente-s").css("background-color", "white");
  $("#cliente-s").css("color", "#005574");
  $("#cliente-s").css("border", "2px solid #005574");
  $("#destinatario-s").css("background-color", "#005574");
  $("#destinatario-s").css("color", "white");
  $("#destinatario-s").css("border", "2px solid #005574");
}

function showDestinatario() {
  $("#cliente-table").css("display", "none");
  $("#destinatario-table").css("display", "block");
  $("#destinatario-s").css("background-color", "white");
  $("#destinatario-s").css("color", "#005574");
  $("#destinatario-s").css("border", "2px solid #005574");
  $("#cliente-s").css("background-color", "#005574");
  $("#cliente-s").css("color", "white");
  $("#cliente-s").css("border", "2px solid #005574");
}

function sendEmail(id) {
  const email = "";
  $.ajax({
    method: "POST",
    url: "ajax/rsendEmail.php",
    data: { id: id, new_email: email },
    success: function (res) {
      $(".register-success").html("Correo reenviado");
      setTimeout(function () {
        location.reload();
      }, 30000);
    },
  });
}

function editCourier() {
  setTimeout(function () {
    $(location).attr("href", "settings-status.php");
  }, 1000);
}

function rsendEmail() {
  const id = $("#id_register_user").val();
  const email = $("#new_email").val();

  $.ajax({
    method: "POST",
    url: "ajax/rsendEmail.php",
    data: { id: id, email: email },
    success: function (res) {
      $(".user-error").html("El correo que intenta registrar ya existe");
    },
  });
}

function openExtraCost(id) {
  // const valor_id = $('#id_whr').val();
  const div_name = "#nombre" + id;
  $(div_name).val(id);
}
// **************************REGISTRO DE NUEVO DESTINATARIO*******************************************************************************************
// *******************************************************************************************************************************************************
function RegDestinatario() {
  const w = 1;
  const countryVenezuela = 1;
  const user_id = $("#user_id_for_destiner").val();
  const name = $("#name_destiner").val();
  const lastname = $("#lastname_destiner").val();
  const alias = $("#alias_destiner").val();
  const cedula = $("#cedula_destiner").val();
  const correo = $("#correo_destiner").val();
  const phone = $("#contact_phone_destiner").val();
  const country = $("#address_destiny_destiner_dos").val();

  let address = "";
  if (w == 1) {
    if (countryVenezuela == 1) {
      address = $("#select_address_destiner_dos").val();
    } else {
      address = $("#address_destiner_dos").val();
    }
  } else {
    address = "NONE";
  }

  let address_state = "";
  if (w == 1) {
    if (countryVenezuela == 1) {
      address_state = $("#select_state_destiner_dos").val();
    } else {
      address_state = $("#address_state_destiner_dos").val();
    }
  } else {
    address_state = "NONE";
  }

  let address_city = "";
  if (w == 1) {
    if (countryVenezuela == 1) {
      address_city = $("#select_city_destiner_dos").val();
    } else {
      address_city = $("#address_city_destiner_dos").val();
    }
  } else {
    address_city = "NONE";
  }

  if (name == "") {
    $(".user-error").html("Debe ingresar un nombre");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un nombre");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (lastname == "") {
    $(".user-error").html("Debe ingresar un apellido");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un apellido");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (alias == "") {
    $(".user-error").html("Debe ingresar un alias");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un alias");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (cedula == "") {
    $(".user-error").html("Debe ingresar una cédula");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar una cédula");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (correo == "") {
    $(".user-error").html("Debe ingresar un correo");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un correo");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (phone == "") {
    $(".user-error").html("Debe ingresar un teléfono");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un teléfono");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (address == "") {
    $(".user-error").html("Debe ingresar una dirección");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar una dirección");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (address_state == "") {
    $(".user-error").html("Debe ingresar un estado");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un estado");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (address_city == "") {
    $(".user-error").html("Debe ingresar una ciudad");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar una ciudad");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    $(".register-button").html(
      '<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>'
    );

    console.log(alias, cedula, correo);

    $.ajax({
      method: "POST",
      url: "ajax/createAddressee.php",
      data: {
        name: name,
        lastname: lastname,
        alias: alias,
        cedula: cedula,
        correo: correo,
        phone: phone,
        address: address,
        address_city: address_city,
        address_state: address_state,
        address_destiny: country,
        user_id: user_id,
      },
      success: function (res) {
        console.log(res);
        $(".register-success").html("Destinatario Creado!");
        $(".register-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          // location.reload();
          $(location).attr("href", "destinatario.php");
        }, 10000);
      },
    });
  }
}

function whinvoice() {
  const id_client_invoice = $("#id_user_from").val();
  const name_invoice = $("#user_from").val();
  const id_destiner_invoice = $("#id_user_to").val();
  const destiner_invoice = $("#myInput").val();
  const trip = $("#trip_invoice").val();
  const date_send = $("#date_send_invoice").val();

  $.ajax({
    method: "POST",
    url: "ajax/findWarehouseForInvoice.php",
    data: {
      client_name: name_invoice,
      trip_type: trip,
      send_date: date_send,
      client_id: id_client_invoice,
      id_destiner_invoice: id_destiner_invoice,
      destiner_invoice: destiner_invoice,
    },
    success: function (res) {
      if (res == 0) {
        $(".register-success").html(
          "¡EL USUARIO NO TIENE WAREHOUSE POR FACTURAR!"
        );
        $(".register-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          location.reload();
        }, 1000);
      } else {
        setTimeout(function () {
          location.href =
            "invoiceForClient.php?client_name=" +
            name_invoice +
            "&trip_type=" +
            trip +
            "&send_date=" +
            date_send +
            "&client_id=" +
            id_client_invoice +
            "&id_destiner_invoice=" +
            id_destiner_invoice +
            "&destiner_invoice=" +
            destiner_invoice;
        }, 1000);
      }
    },
  });
}
// *******************************************************************************************************************************************************

function registerNew() {
  const name = $("#name").val();
  const lastname = $("#lastname").val();
  const born_date = $("#born_date").val();
  const email = $("#email").val();
  const user_type = $("#user_type").val();
  const password = $("#password").val();
  const phone = $("#phone").val();
  const documentU = $("#document").val();
  const country = $("#country").val();
  const address = $("#address").val();
  const headquarter = $("#headquarter").val();

  if (name == "") {
    $(".register-error").html("Debe ingresar un nombre");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (lastname == "") {
    $(".register-error").html("Debe ingresar un apellido");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (born_date == "" || born_date == "0") {
    $(".register-error").html("Debe ingresar una fecha de nacimiento");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (email == "" || isEmail(email) == false) {
    $(".register-error").html("Debe ingresar un email correcto");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (user_type == "" || user_type == "0") {
    $(".register-error").html("Debe ingresar un tipo de usuario");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (password == "") {
    $(".register-error").html("Debe ingresar un password");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (phone == "") {
    $(".register-error").html("Debe ingresar un teléfono");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (documentU == "") {
    $(".register-error").html("Debe ingresar un documento");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (country == "" || country == "0") {
    $(".register-error").html("Debe ingresar un país");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (address == "") {
    $(".register-error").html("Debe ingresar una dirección");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (headquarter == "" || headquarter == "0") {
    $(".register-error").html("Debe ingresar una agencia");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/createUser.php",
      data: {
        w: 2,
        name: name,
        lastname: lastname,
        email: email,
        password: password,
        phone: phone,
        documentU: documentU,
        country: country,
        address: address,
        headquarter: headquarter,
        born_date: born_date,
        user_type: user_type,
      },
      success: function (res) {
        if (res == 1) {
          $(".register-success").html("¡Usuario Creado!");
          $(".register-success").fadeIn("fast");
          $(".modal").scrollTop(0);
          setTimeout(function () {
            location.reload();
          }, 2000);
        } else if (res == "Existe") {
          $(".register-error").html(
            "El correo que intenta registrar ya existe"
          );
          $(".register-error").fadeIn("fast");
          $(".modal").scrollTop(0);
          setTimeout(function () {
            $(".register-error").fadeOut("fast");
          }, 3000);
        }
      },
    });
  }
}

function login() {
  const email = $("#email").val();
  const password = $("#password").val();
  if (email == "") {
    $(".register-error").html("Debe ingresar un email");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (password == "") {
    $(".register-error").html("Debe ingresar un password");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/loginAccount.php",
      data: { email: email, password: password },
      success: function (res) {
        console.log(res);
        if (res == 2) {
          $(".register-error").html("El password es incorrecto");
          $(".register-error").fadeIn("fast");
          $(document).scrollTop(0);
          setTimeout(function () {
            $(".register-error").fadeOut("fast");
          }, 3000);
        } else if (res == 4) {
          $(".register-error").html("El email no existe, debe registrarse");
          $(".register-error").fadeIn("fast");
          $(document).scrollTop(0);
          setTimeout(function () {
            $(".register-error").fadeOut("fast");
          }, 3000);
        } else if (res == 1) {
          $(".register-success").html("¡Bienvenido!");
          $(".register-success").fadeIn("fast");
          $(document).scrollTop(0);
          setTimeout(function () {
            location.href = "dashboard.php";
          }, 3000);
        } else if (res == 5) {
          $(".register-success").html("¡Bienvenido!");
          $(".register-success").fadeIn("fast");
          $(document).scrollTop(0);
          setTimeout(function () {
            location.href = "casillero.php";
          }, 3000);
        } else {
          $(".register-error").html(
            "Aún no has confirmado tu correo electrónico"
          );
          $(".register-error").fadeIn("fast");
          $(document).scrollTop(0);
          setTimeout(function () {
            $(".register-error").fadeOut("fast");
          }, 3000);
        }
      },
    });
  }
}

function addAlert() {
  var formData = new FormData(document.getElementById("alert-invoice-form"));

  if ($("#package_name").val() == "") {
    const tracking = $("#tracking").val();
    const date_r = $("#date_r").val();
    const id_client = $("#idClient").val();
    const transp = $("#transp").val();
    const content = $("#content").val();
    const date_in = $("#date_in").val();
    const cost = $("#cost").val();
    const package_name = "N/A";

    if (date_in == "") {
      $(".error-warning").html("Debe ingresar una fecha de llegada");
      $(".error-warning").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".error-warning").fadeOut("fast");
      }, 3000);
    } else if (transp == "") {
      $(".error-warning").html("Debe ingresar un empresa de transporte");
      $(".error-warning").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".error-warning").fadeOut("fast");
      }, 3000);
    } else if (content == "") {
      $(".error-warning").html("Debe ingresar el contenido del paquete");
      $(".error-warning").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".error-warning").fadeOut("fast");
      }, 3000);
    } else if (cost == "") {
      $(".error-warning").html("Debe ingresar el valor del paquete");
      $(".error-warning").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".error-warning").fadeOut("fast");
      }, 3000);
    } else {
      formData.append("id_client", id_client);
      formData.append("date_r", date_r);
      formData.append("tracking", tracking);
      formData.append("date_in", date_in);
      formData.append("transp", transp);
      formData.append("content", content);
      formData.append("cost", cost);
      formData.append("package_name", package_name);

      $.ajax({
        url: "ajax/addAlert.php",
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
      }).done(function (res) {
        console.log(res);
        //   $('.success-warning').html('¡Pre - Alerta registrada exitosamente!');
        //   $('.success-warning').fadeIn('fast');
        //   $('.modal').scrollTop(0);
        setTimeout(function () {
          location.reload();
        }, 1000);
      });
    }
  } else {
    const tracking = $("#tracking").val();
    const date_r = $("#date_r").val();
    const package_name = $("#package_name").val();
    const id_client = 0;
    const transp = $("#transp").val();
    const content = $("#content").val();
    const date_in = $("#date_in").val();
    const cost = $("#cost").val();

    if (date_in == "") {
      $(".error-warning").html("Debe ingresar una fecha de llegada");
      $(".error-warning").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".error-warning").fadeOut("fast");
      }, 3000);
    } else if (transp == "") {
      $(".error-warning").html("Debe ingresar un empresa de transporte");
      $(".error-warning").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".error-warning").fadeOut("fast");
      }, 3000);
    } else if (content == "") {
      $(".error-warning").html("Debe ingresar el contenido del paquete");
      $(".error-warning").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".error-warning").fadeOut("fast");
      }, 3000);
    } else if (cost == "") {
      $(".error-warning").html("Debe ingresar el valor del paquete");
      $(".error-warning").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".error-warning").fadeOut("fast");
      }, 3000);
    } else {
      formData.append("package_name", package_name);
      formData.append("date_r", date_r);
      formData.append("tracking", tracking);
      formData.append("date_in", date_in);
      formData.append("transp", transp);
      formData.append("content", content);
      formData.append("id_client", id_client);

      formData.append("cost", cost);

      $.ajax({
        url: "ajax/addAlert.php",
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
      }).done(function (res) {
        console.log(res);
        //   $('.success-warning').html('¡Pre - Alerta registrada exitosamente!');
        //   $('.success-warning').fadeIn('fast');
        //   $('.modal').scrollTop(0);
        setTimeout(function () {
          location.reload();
        }, 1000);
      });
    }
  }
}

function addAlertforUser() {
  var formData = new FormData(document.getElementById("alert-invoice-form"));

  const tracking = $("#tracking").val();
  const date_r = $("#date_r").val();
  const id_client = $("#client_id").val();
  const transp = $("#transp").val();
  const content = $("#content").val();
  const date_in = $("#date_in").val();
  const cost = $("#cost").val();
  const package_name = "N/A";
  console.log($("#client_id").val());
  if (date_in == "") {
    $(".error-warning").html("Debe ingresar una fecha de llegada");
    $(".error-warning").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".error-warning").fadeOut("fast");
    }, 3000);
  } else if (transp == "") {
    $(".error-warning").html("Debe ingresar un empresa de transporte");
    $(".error-warning").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".error-warning").fadeOut("fast");
    }, 3000);
  } else if (content == "") {
    $(".error-warning").html("Debe ingresar el contenido del paquete");
    $(".error-warning").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".error-warning").fadeOut("fast");
    }, 3000);
  } else if (cost == "") {
    $(".error-warning").html("Debe ingresar el valor del paquete");
    $(".error-warning").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".error-warning").fadeOut("fast");
    }, 3000);
  } else {
    formData.append("id_client", id_client);
    formData.append("date_r", date_r);
    formData.append("tracking", tracking);
    formData.append("date_in", date_in);
    formData.append("transp", transp);
    formData.append("content", content);
    formData.append("cost", cost);
    formData.append("package_name", package_name);

    $.ajax({
      url: "ajax/addAlert.php",
      type: "post",
      dataType: "html",
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
    }).done(function (res) {
      console.log(res);
      //   $('.success-warning').html('¡Pre - Alerta registrada exitosamente!');
      //   $('.success-warning').fadeIn('fast');
      //   $('.modal').scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 1000);
    });
  }
}

function addDestinforUser() {
  const user_client = document.getElementById("user_id").value;
  const name = $("#name").val();
  const apellido = $("#apellido").val();
  const alias = $("#alias").val();
  const cedula = $("#cedula").val();
  const correo = $("#correo").val();
  const direccion = $("#direccion").val();
  const telefono = $("#telefono").val();
  const pais = $("#pais").val();
  const estado = $("#estado").val();
  const ciudad = $("#ciudad").val();

  console.log(
    user_client,
    name,
    apellido,
    alias,
    cedula,
    correo,
    direccion,
    telefono,
    pais,
    estado,
    ciudad
  );

  if (
    name == "" ||
    apellido == "" ||
    alias == "" ||
    cedula == "" ||
    correo == "" ||
    direccion == "" ||
    telefono == "" ||
    pais == "" ||
    estado == "" ||
    ciudad == ""
  ) {
    $(".error-warning").html("Debe completar todos los campos");
    $(".error-warning").fadeIn("fast");
    $(".modal").scrollTop(0);
  } else {
    console.log("todo bien");
    $.ajax({
      url: "ajax/addDestinatario.php",
      type: "post",
      dataType: "html",
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
    }).done(function (res) {
      console.log(res);
      setTimeout(function () {
        location.reload();
      }, 1000);
    });
  }

  // }
}

function showEditAlias(id, alias) {
  console.log(id, alias);
  $("#exampleModal3").modal("show");

  document.getElementById("aliasId").value = id;
  document.getElementById("aliasD").value = alias;
}

function editAlias() {
  const id = $("#aliasId").val();
  const alias = $("#aliasD").val();

  console.log(id, alias);

  if (alias == "") {
    console.log("viene vacio");
    alert("Vacio debe completar el campo");
  } else {
    console.log("viene lleno");

    $.ajax({
      method: "POST",
      url: "ajax/editAlias.php",
      data: {
        id: id,
        alias: alias,
      },
      success: function (res) {
        console.log(res);
        // location.reload();
      },
    });
  }
}

function showeditDestin(
  id,
  id2,
  id3,
  id4,
  id5,
  id6,
  id7,
  id8,
  id9,
  id10,
  id11
) {
  console.log(id, id2, id3, id4, id5, id6, id7, id8, id9, id10, id11);
  $("#exampleModal2").modal("show");

  document.getElementById("id").value = id;
  document.getElementById("nameDestinatario").value = id2;
  document.getElementById("apeDestinatario").value = id3;
  document.getElementById("aliDestinatario").value = id4;
  document.getElementById("cedDestinatario").value = id5;
  document.getElementById("corDestinatario").value = id6;
  document.getElementById("telDestinatario").value = id7;
  document.getElementById("address_destiny_destiner_dos").value = id8;
  document.getElementById("select_state_destiner_dos").value = id9;
  document.getElementById("select_city_destiner_dos").value = id10;
  document.getElementById("dirDestinatario").value = id11;
}

function ediDestin() {
  const countryVenezuela = 1;

  const id = $("#id").val();
  const user_id = $("#user_id").val();
  const name = $("#nameDestinatario").val();
  const lastname = $("#apeDestinatario").val();
  const alias = $("#aliDestinatario").val();
  const cedula = $("#cedDestinatario").val();
  const correo = $("#corDestinatario").val();
  const phone = $("#telDestinatario").val();
  const country = $("#address_destiny_destiner_dos").val();
  const state_id = $("#select_state_destiner_dos").val();
  const city_id = $("#select_city_destiner_dos").val();
  const address = $("#dirDestinatario").val();

  if (countryVenezuela == 1) {
    address_state = $("#select_state_destiner_dos").val();
    address_city = $("#select_city_destiner_dos").val();
  } else {
    address_state = $("#address_state_destiner_dos").val();
    address_city = $("#address_city_destiner_dos").val();
  }

  console.log(
    id,
    user_id,
    name,
    lastname,
    alias,
    cedula,
    correo,
    phone,
    country,
    state_id,
    city_id,
    address,
    address_state,
    address_city
  );

  if (
    name === "" ||
    lastname === " " ||
    alias === "" ||
    cedula === "" ||
    correo === " " ||
    phone === "" ||
    country === null ||
    state_id === null ||
    address === "" ||
    city_id === null ||
    address_state === null ||
    address_city === null
  ) {
    console.log("esta vacio ");
    alert("Por favor completar los campos");
  } else {
    console.log("esta lleno ");

    $.ajax({
      method: "POST",
      url: "ajax/editDestinerForClient.php",
      data: {
        id: id,
        name: name,
        lastname: lastname,
        alias: alias,
        cedula: cedula,
        correo: correo,
        country_id: countryVenezuela,
        contact_phone: phone,
        address: address,
        address_city: address_city,
        address_state: address_state,
        state_id: state_id,
        city_id: city_id,
        address_destiny: country,
        user_id: user_id,
      },
      success: function (res) {
        console.log(res);
        location.reload();
        // const dataUser = res.split("-");

        // const id = dataUser[0];
        // const name = dataUser[1];

        // $("#country_id_register").val(id);
        // $("#country_name").val(name);

        // $("#edit-general-status-button").css("display", "block");
        // $("#register-general-status-button").css("display", "none");
      },
    });
  }
}

function editCountry(id) {
  $.ajax({
    method: "POST",
    url: "ajax/searchCountryById.php",
    data: { id: id },
    success: function (res) {
      // console.log(res);
      const dataUser = res.split("-");

      const id = dataUser[0];
      const name = dataUser[1];

      $("#country_id_register").val(id);
      $("#country_name").val(name);

      $("#edit-general-status-button").css("display", "block");
      $("#register-general-status-button").css("display", "none");
    },
  });
}

function editCountryForConfig(id) {
  var id = parseInt($("#country_id_register").val());
  const name = $("#country_name").val();

  $.ajax({
    method: "POST",
    url: "ajax/editCountry.php",
    data: { id: id, name: name },
    success: function (res) {
      $(".register-success").html("¡Edición exitosa!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function addCountryForConfig() {
  const name = $("#country_name").val();

  if (name == "") {
    $(".register-error").html("Debe ingresar un estado");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/addCountryForConfig.php",
      data: { name: name },
      success: function (res) {
        console.log(res);
        $(".register-success").html("¡Pais registrado exitosamente!");
        $(".register-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          location.reload();
        }, 2000);
      },
    });
  }
}

function deleteCountry(id) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteCountry.php",
    data: { id: id },
    success: function (res) {
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
  });
}

function editParametres(id) {
  $.ajax({
    method: "POST",
    url: "ajax/searchParamById.php",
    data: { id: id },
    success: function (res) {
      // console.log(res);
      const dataUser = res.split("-");

      const id = dataUser[0];
      const name = dataUser[1];
      const description = dataUser[2];
      const cost_value = dataUser[3];

      $("#param_id").val(id);
      $("#name").val(name);
      $("#description").val(description);
      $("#cost").val(cost_value);

      $("#edit-param-button").css("display", "block");
      $("#register-param-button").css("display", "none");
    },
  });
}

function editCurrier(id) {
  $.ajax({
    method: "POST",
    url: "ajax/searchCurrierById.php",
    data: { id: id },
    success: function (res) {
      // console.log(res);
      const dataUser = res.split("-");

      const id = dataUser[0];
      const currier_intern_id = dataUser[1];
      const city_id = dataUser[2];
      const trip_id = dataUser[3];
      const tarifa_intern = dataUser[4];
      const tarifaTe_intern = dataUser[5];

      $("#general_status_id").val(id);
      $("#currier_id_tarifa").val(currier_intern_id);
      $("#city_id_tarifa").val(city_id);
      $("#trip_id_tarifa").val(trip_id);
      $("#tarifa_currier").val(tarifa_intern);
      $("#tarifaTE_currier").val(tarifaTe_intern);

      $(".name-currier-hide").css("display", "none");

      $("#edit-general-status-button").css("display", "block");
      $("#register-general-status-button").css("display", "none");
    },
  });
}

function editForCurrier(id) {
  var id = parseInt($("#general_status_id").val());
  const currier = $("#currier_id_tarifa").val();
  const ciudad = $("#city_id_tarifa").val();
  const trip = $("#trip_id_tarifa").val();
  const tarifa = $("#tarifa_currier").val();
  const tarifa_te = $("#tarifaTE_currier").val();

  $.ajax({
    method: "POST",
    url: "ajax/editCurrier.php",
    data: {
      id: id,
      currier: currier,
      ciudad: ciudad,
      trip: trip,
      tarifa: tarifa,
      tarifa_te: tarifa_te,
    },
    success: function (res) {
      $(".register-success").html("¡Edición exitosa!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function editDestinerForClient() {
  var id = $("#destiner_id").val();
  const name = $("#name_destiner").val();
  const lastname = $("#lastname_destiner").val();
  const contact_phone = $("#contact_phone_destiner").val();
  const country_id = $("#address_destiny_destiner_for_client").val();
  const user_id = $("#user_id").val();

  let address = "";

  // la seleccion del pais es venezuela
  if (country_id == 1) {
    address = $("#select_address_destiner_for_client").val();
  } else {
    address = $("#address_destiner_for_client").val();
  }

  let address_state = "";
  if (country_id == 1) {
    address_state = $("#select_state_destiner_for_client").val();
  } else {
    address_state = $("#address_state_destiner_dos").val();
  }

  let address_city = "";
  if (country_id == 1) {
    address_city = $("#select_city_destiner_for_client").val();
  } else {
    address_city = $("#address_city_destiner_for_client").val();
  }

  $.ajax({
    method: "POST",
    url: "ajax/editDestinerForClient.php",
    data: {
      id: id,
      name: name,
      lastname: lastname,
      contact_phone: contact_phone,
      address: address,
      country_id: country_id,
      state_id: address_state,
      city_id: address_city,
      user_id: user_id,
    },
    success: function (res) {
      $(".register-success").html("¡Edición exitosa!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function deleteCurrier(id) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteCurrier.php",
    data: { id: id },
    success: function (res) {
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
  });
}

function editCities(id) {
  $.ajax({
    method: "POST",
    url: "ajax/searchCity.php",
    data: { id: id },
    success: function (res) {
      // console.log(res);
      const dataUser = res.split("-");

      const id = dataUser[0];
      const name = dataUser[1];
      const region_id = dataUser[2];
      const state_id = dataUser[3];
      const country = dataUser[4];

      $("#general_status_id").val(id);
      $("#city_name").val(name);
      $("#region_id").val(region_id);
      $("#state_id").val(state_id);
      $("#country").val(country);

      $("#edit-general-status-button").css("display", "block");
      $("#register-general-status-button").css("display", "none");
    },
  });
}

function editCity(id) {
  var id = parseInt($("#general_status_id").val());
  const city_name = $("#city_name").val();
  const region_id = $("#region_id").val();
  const state_id = $("#state_id").val();
  const country = $("#country").val();
  $.ajax({
    method: "POST",
    url: "ajax/editCities.php",
    data: {
      id: id,
      city_name: city_name,
      region_id: region_id,
      state_id: state_id,
      country: country,
    },
    success: function (res) {
      console.log(res);
      $(".register-success").html("¡Edición exitosa!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function deleteCities(id) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteCities.php",
    data: { id: id },
    success: function (res) {
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
  });
}

function editInternCurrier(id) {
  $.ajax({
    method: "POST",
    url: "ajax/searchInternCurrierById.php",
    data: { id: id },
    success: function (res) {
      // console.log(res);
      const dataUser = res.split("-");

      const id = dataUser[0];
      const currier_intern_id = dataUser[1];
      const city_id = dataUser[2];
      const trip_id = dataUser[3];
      const tarifa_intern = dataUser[4];
      const tarifaTe_intern = dataUser[5];

      $("#general_status_id").val(id);
      $("#currier_id").val(currier_intern_id);
      $("#city_id").val(city_id);
      $("#trip_id").val(trip_id);
      $("#tarifa").val(tarifa_intern);
      $("#tarifaTE").val(tarifaTe_intern);

      $(".name-currier-hide").css("display", "none");

      $("#edit-general-status-button").css("display", "block");
      $("#register-general-status-button").css("display", "none");
    },
  });
}

function editInternForCurrier(id) {
  var id = parseInt($("#general_status_id").val());
  const currier = $("#currier_id").val();
  const ciudad = $("#city_id").val();
  const trip = $("#trip_id").val();
  const tarifa = $("#tarifa").val();
  const tarifa_te = $("#tarifaTE").val();

  $.ajax({
    method: "POST",
    url: "ajax/editInternCurrier.php",
    data: {
      id: id,
      currier: currier,
      ciudad: ciudad,
      trip: trip,
      tarifa: tarifa,
      tarifa_te: tarifa_te,
    },
    success: function (res) {
      $(".register-success").html("¡Edición exitosa!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function deleteInternCurrier(id) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteInternCurrier.php",
    data: { id: id },
    success: function (res) {
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
  });
}

//*********************************** */

function addGeneralStatus() {
  const name = $("#name_general_status").val();
  const description = $("#description_general_status").val();
  const siglas = $("#siglas_general_status").val();

  if (name == "") {
    $(".register-error").html("Debe ingresar un estado");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/addGeneralStatus.php",
      data: { name: name, description: description, siglas: siglas },
      success: function (res) {
        console.log(res);
        $(".register-success").html("¡Categoria registrada exitosamente!");
        $(".register-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          location.reload();
        }, 2000);
      },
    });
  }
}

function editGeneralStatus(id) {
  $.ajax({
    method: "POST",
    url: "ajax/searchGeneralStatus.php",
    data: { id: id },
    success: function (res) {
      // console.log(res);
      const dataUser = res.split("-");

      const id = dataUser[0];
      const name = dataUser[1];
      const description = dataUser[2];
      const siglas = dataUser[3];

      $("#general_status_id").val(id);
      $("#name_general_status").val(name);
      $("#description_general_status").val(description);
      $("#siglas_general_status").val(siglas);

      $("#edit-general-status-button").css("display", "block");
      $("#register-general-status-button").css("display", "none");
    },
  });
}

function editStatus() {
  var id = parseInt($("#general_status_id").val());
  const name = $("#name_general_status").val();
  const description = $("#description_general_status").val();
  const siglas = $("#siglas_general_status").val();

  $.ajax({
    method: "POST",
    url: "ajax/editGeneralStatus.php",
    data: { id: id, name: name, description: description, siglas: siglas },
    success: function (res) {
      console.log(res);

      $("#general_status_is").val(0);
      $("#name_general_status").val("");
      $("#description_general_status").val("");
      $("#siglas_general_status").val("");

      $("#register-general-status-button").css("display", "block");
      $("#edit-general-status-button").css("display", "none");

      $(".register-success").html("!EDICIÓN EXITOSA!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function deleteGeneralStatus(id) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteGeneralStatus.php",
    data: { id: id },
    success: function (res) {
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
  });
}

//************************************* */

function editDepartGet(id) {
  $.ajax({
    method: "POST",
    url: "ajax/searchDepartById.php",
    data: { id: id },
    success: function (res) {
      // console.log(res);
      const dataUser = res.split("-");

      const id = dataUser[0];
      const name = dataUser[1];
      const description = dataUser[2];

      $("#depart-id").val(id);
      $("#name-depart").val(name);
      $("#description-depart").val(description);

      $("#edit-general-status-button").css("display", "block");
      $("#register-general-status-button").css("display", "none");
    },
  });
}

function editDepart() {
  var id = parseInt($("#depart-id").val());
  const name = $("#name-depart").val();
  const description = $("#description-depart").val();

  $.ajax({
    method: "POST",
    url: "ajax/editDepart.php",
    data: { id: id, name: name, description: description },
    success: function (res) {
      console.log(res);

      $("#depart-id").val(0);
      $("#name-depart").val("");
      $("#description-depart").val("");

      $("#depart-register-button").css("display", "block");
      $("#depart-edit-button").css("display", "none");

      $(".register-success").html("!EDICIÓN EXITOSA!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function deleteDepartToConfig(id) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteDepart.php",
    data: { id: id },
    success: function (res) {
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
  });
}

function deleteParametres(id) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteParametres.php",
    data: { id: id },
    success: function (res) {
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
  });
}

function editRegionGet(id) {
  $.ajax({
    method: "POST",
    url: "ajax/searchRegionById.php",
    data: { id: id },
    success: function (res) {
      // console.log(res);
      const dataUser = res.split("-");

      const id = dataUser[0];
      const name = dataUser[1];

      $("#region-id").val(id);
      $("#name-region").val(name);

      $("#region-edit-button").css("display", "block");
      $("#region-register-button").css("display", "none");
    },
  });
}

function editRegion() {
  var id = parseInt($("#region-id").val());
  const name = $("#name-region").val();

  $.ajax({
    method: "POST",
    url: "ajax/editRegion.php",
    data: { id: id, name: name },
    success: function (res) {
      console.log(res);

      $("#region-id").val(0);
      $("#name-region").val("");

      $("#region-register-button").css("display", "block");
      $("#region-edit-button").css("display", "none");

      $(".register-success").html("!EDICIÓN EXITOSA!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function deleteRegionToConfig(id) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteRegion.php",
    data: { id: id },
    success: function (res) {
      // console.log(res);
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
  });
}

function deleteCourier(id) {
  // console.log(id);
  $.ajax({
    method: "POST",
    url: "ajax/deleteCourier.php",
    data: { id: id },
    success: function (res) {
      // console.log(res);
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
  });
}

function editCategory(id) {
  $.ajax({
    method: "POST",
    url: "ajax/searchCategoryById.php",
    data: { id: id },
    success: function (res) {
      // console.log(res);
      const dataUser = res.split("-");

      const id = dataUser[0];
      const name = dataUser[1];

      $("#category_id").val(id);
      $("#name").val(name);

      $("#register-category-button").css("display", "none");
      $("#edit-category-button").css("display", "block");
    },
  });
}

function editCategoryForConfig() {
  var id = parseInt($("#category_id").val());
  const name = $("#name").val();

  $.ajax({
    method: "POST",
    url: "ajax/editCategory.php",
    data: { id: id, name: name },
    success: function (res) {
      // console.log(res);

      $("#category_id").val(0);
      $("#name").val("");

      $("#register-category-button").css("display", "block");
      $("#edit-category-button").css("display", "none");

      $(".register-success").html("¡EDICIÓN EXITOSA!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".register-success").fadeOut("fast");
      }, 2000);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function deleteCategory(id) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteCategoryToConfig.php",
    data: { id: id },
    success: function (res) {
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
  });
}

function editBoxesCost(id) {
  $.ajax({
    method: "POST",
    url: "ajax/searchBoxForId.php",
    data: { id: id },
    success: function (res) {
      // console.log(res);
      const dataUser = res.split("-");

      const id = dataUser[0];
      const name = dataUser[1];
      const width = dataUser[2];
      const height = dataUser[3];
      const lenght = dataUser[4];
      const weigth = dataUser[5];
      const box_cost = dataUser[6];
      const cubic_feet = dataUser[7];
      const lb_vol = dataUser[8];

      $("#box_id").val(id);
      $("#name").val(name);
      $("#width").val(width);
      $("#height").val(height);
      $("#lenght").val(lenght);
      $("#weigth").val(weigth);

      $("#cubic_f").val(cubic_feet);
      $("#lb_vol").val(lb_vol);
      $("#box_cost").val(box_cost);

      $("#cubic_feet_edit").css("display", "block");
      $("#lb_vol_edit").css("display", "block");
      $("#edit_box_button").css("display", "block");
      $("#register_box_button").css("display", "none");
    },
  });
}

function editBOX() {
  var id = parseInt($("#box_id").val());
  const name = $("#name").val();
  const width = $("#width").val();
  const height = $("#height").val();
  const lenght = $("#lenght").val();
  const weigth = $("#weigth").val();
  const cubic_feet = $("#cubic_f").val();
  const lb_vol = $("#lb_vol").val();
  const box_cost = $("#box_cost").val();

  $.ajax({
    method: "POST",
    url: "ajax/updateBox.php",
    data: {
      id: id,
      name: name,
      width: width,
      height: height,
      lenght: lenght,
      weigth: weigth,
      cubic_feet: cubic_feet,
      lb_vol: lb_vol,
      box_cost: box_cost,
    },
    success: function (res) {
      console.log(res);

      $("#box_id").val(0);
      $("#name").val("");
      $("#width").val(0);
      $("#height").val(0);
      $("#lenght").val(0);
      $("#weigth").val(0);
      $("#cubic_f").val(0);
      $("#lb_vol").val(0);
      $("#box_cost").val(0);

      $("#register_box_button").css("display", "block");
      $("#edit_box_button").css("display", "none");
      $("#cubic_feet_edit").css("display", "none");
      $("#lb_vol_edit").css("display", "none");

      $(".register-success").html("EDICIÓN EXITOSA");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function deleteBoxToConfig(id) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteBoxToConfig.php",
    data: { id: id },
    success: function (res) {
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
  });
}

function editParam() {
  var id = parseInt($("#param_id").val());
  const name = $("#name").val();
  const description = $("#description").val();
  const cost_value = $("#cost").val();

  $.ajax({
    method: "POST",
    url: "ajax/updateParam.php",
    data: {
      id: id,
      name: name,
      description: description,
      cost_value: cost_value,
    },
    success: function (res) {
      // console.log(res);

      $("#param_id").val(0);
      $("#name").val("");
      $("#description").val("");
      $("#cost").val(0.0);

      $("#register-param-button").css("display", "block");
      $("#edit-param-button").css("display", "none");
      $(".register-success").html("EDICIÓN EXITOSA");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function registerCurrierCost() {
  const currier_id = $("#currier_id_cost").val();
  const cost = $("#currier-cost_register").val();
  const tasa = $("#currier-tasa_register").val();
  const type = $("#type_cost").val();
  const region = $("#region_cost").val();
  const cost_inter = $("#cost_inter_register").val();

  if (currier_id == "") {
    $(".register-error").html("Debe ingresar un nombre");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (region == "" || region == "0") {
    $(".register-error").html("Debe ingresar una region");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (type == "" || type == "0") {
    $(".register-error").html("Debe ingresar un tipo de viaje");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (cost == "" || cost <= "0") {
    $(".register-error").html("Debe ingresar un costo");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (tasa == "" || tasa <= "0") {
    $(".register-error").html("Debe ingresar una tasa");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/addCurrierCost.php",
      data: {
        currier_id: currier_id,
        type: type,
        cost: cost,
        tasa: tasa,
        region: region,
        cost_inter: cost_inter,
      },
      success: function (res) {
        console.log(res);
        $(".register-success").html("¡Currier registrado exitosamente!");
        $(".register-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          location.reload();
        }, 1000);
      },
    });
  }
}

function addInvoice() {
  const id_warehouse = $("#warehouse").val();
  const client = $("#client").val();
  const date_i = $("#date_i").val();
  const value = 20;

  if (id_warehouse == "0" || id_warehouse == 0) {
    $(".addInvoice-error").html("Debe ingresar un warehouse");
    $(".addInvoice-error").fadeIn("fast");
  } else if (client == "0" || client == 0) {
    $(".addInvoice-error").html("Debe ingresar un cliente");
    $(".addInvoice-error").fadeIn("fast");
  } else if (id_warehouse == "0" || id_warehouse == 0) {
    $(".addInvoice-error").html("Debe ingresar un warehouse");
    $(".addInvoice-error").fadeIn("fast");
  } else if (date_i == "") {
    $(".addInvoice-error").html("Debe ingresar una fecha");
    $(".addInvoice-error").fadeIn("fast");
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/addInvoice.php",
      data: {
        id_warehouse: id_warehouse,
        client: client,
        date_i: date_i,
        value: value,
      },
      success: function (res) {
        console.log(res);
        $(".addInvoice-success").html("Factura creada con exito!");
        $(".addInvoice-success").fadeIn("fast");
        setTimeout(function () {
          location.reload();
        }, 3000);
      },
    });
  }
}

function addPacking() {
  const code = "0";
  const date_in = $("#date_in").val();
  const date_out = $("#date_out").val();
  const name = $("#name").val();
  const note = $("#note").val();
  const id_warehouse = $("#id_warehouse").val();
  const id_currier = $("#id_currier").val();
  const id_trip = $("#id_trip").val();
  const country = $("#country").val();
  const state = $("#state").val();
  const bulk = $("#idBulk").val();

  if (code == "") {
    $(".register-error").html("Debe ingresar un codigo");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (date_in == "") {
    $(".register-error").html("Debe ingresar una fecha de ingreso");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (date_out == "") {
    $(".register-error").html("Debe ingresar una fecha de salida");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (name == "") {
    $(".register-error").html("Debe ingresar un nombre");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (note == "") {
    $(".register-error").html("Debe ingresar una nota de referencia");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (id_warehouse == "0" || id_warehouse == 0) {
    $(".register-error").html(
      "Debe ingresar un warehouse para abrir el packing"
    );
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (id_currier == "0" || id_currier == 0) {
    $(".register-error").html("Debe ingresar un currier");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (id_trip == "0" || id_trip == 0) {
    $(".register-error").html("Debe ingresar un tipo de viaje");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (country == "0" || country == 0) {
    $(".register-error").html("Debe ingresar un pais de destino");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/addPacking.php",
      data: {
        code: code,
        date_in: date_in,
        date_out: date_out,
        name: name,
        note: note,
        id_warehouse: id_warehouse,
        id_currier: id_currier,
        id_trip: id_trip,
        country: country,
        state: state,
        bulk: bulk,
      },
      success: function (res) {
        console.log(res);
        if (res == 1) {
          $(".register-success").html("¡Packing registrado exitosamente!");
          $(".register-success").fadeIn("fast");
          $(".modal").scrollTop(0);
          setTimeout(function () {
            location.reload();
          }, 2000);
        }
      },
    });
  }
}

function addPackage() {
  var formData = new FormData(document.getElementById("formuploadajax"));
  $(".formuploadajax").fadeOut("fast");

  const code = $("#code").val();
  const user_from = $("#id_user_from").val();
  const user_to = 0;
  const date_in = $("#fecha_registro").val();
  const date_out = $("#fecha_registro").val();
  const alias = $("#alias").val();
  const operator = $("#operator_id").val();
  const headquarter = 0;
  const stock = 0;
  const tracking = $("#tracking").val();
  const description = $("#description").val();
  const state = 1;
  const height = $("#height").val();
  const lenght = $("#lenght").val();
  const width = $("#width").val();
  const weight = $("#weight").val();
  const transp = $("#transp").val();
  const address_send = "direccion";
  const cost = $("#cost").val();

  // console.log(transp);

  if (code == "") {
    $(".register-error").html("Debe ingresar un numero de casillero");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (date_in == "") {
    $(".register-error").html("Debe ingresar una fecha de ingreso");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (address_send == "") {
    $(".register-error").html("Debe ingresar una direccion de envio");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (state == "" || state == 0 || state == "0") {
    $(".register-error").html("Debe ingresar un estado");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (transp == "" || transp == 0 || transp == "0") {
    $(".register-error").html("Debe ingresar un transportista");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    formData.append("code", code);
    formData.append("user_from", user_from);
    formData.append("user_to", user_to);
    formData.append("date_in", date_in);
    formData.append("address_send", address_send);
    formData.append("name", name);
    formData.append("date_out", date_out);
    formData.append("alias", alias);
    formData.append("operator", operator);
    formData.append("headquarter", headquarter);
    formData.append("stock", stock);
    formData.append("tracking", tracking);
    formData.append("description", description);
    formData.append("state", state);
    formData.append("height", height);
    formData.append("lenght", lenght);
    formData.append("width", width);
    formData.append("weight", weight);
    formData.append("transp", transp);
    formData.append("cost", cost);

    $.ajax({
      url: "ajax/addPackage.php",
      type: "post",
      dataType: "html",
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
    }).done(function (res) {
      console.log(res);
      $(".register-success").html("¡Paquete registrado exitosamente!");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    });
  }
}

function addCategory() {
  const name = $("#name").val();

  if (name == "") {
    $(".register-error").html("Debe ingresar un nombre");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/addCategory.php",
      data: { name: name },
      success: function (res) {
        console.log(res);
        $(".register-success").html("¡Categoria registrada exitosamente!");
        $(".register-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          location.reload();
        }, 2000);
      },
    });
  }
}

function addWarehouse() {
  /** VERIFICAR QUE EL WH TIENE AL MENOS UNA CAJA ANTES DE GUARDAR */
  const id_wareh = $("#warehouse_id").val();
  $.ajax({
    method: "POST",
    url: "ajax/findBoxesForWarehouse.php",
    data: { wh_id: id_wareh },
    success: function (res) {
      console.log(res);
      if (res > 0) {
        /**EL WH YA TIENE CAJAS ASIGNADAS ES PERMITIDO CREAR EL WAREHOUSE */

        const code = $("#code-title").html();
        const user_from = $("#id_user_from").val();
        const user_to = $("#id_user_to").val();
        const date_out = $("#date_send").val();
        const address_send = $("#address_send").val();

        const client_name = $("#user_from").val();
        const destiner_name = $("#myInput").val();
        const country = 1;
        // const package = $('#package').val();
        const description = 0;
        const cost = 0;
        const box = 1;
        const state = $("#wState").val();
        const id_currier = $("#currier").val();
        const trip = $("#trip").val();
        const casillero = 1;
        const insurance = 0;
        const state_user = 0;
        const ship_state = 2;

        //detalles del destino del wh

        const country_id = $("#country_destiner_get").val();
        const state_id = $("#state_destiner_get").val();
        const city_id = $("#address_send").val();
        const address = $("#address_address_destiner_get").val();
        var nota = $("#nota").val();
        var num_guia = $("#num_guia").val();

        // const category = $('#category').val();
        const depart = $("#depart").val();

        if (nota == "") {
          nota = "sin comentarios";
        } else {
          nota = $("#nota").val();
        }

        if (num_guia == "") {
          num_guia = 0;
        } else {
          num_guia = $("#num_guia").val();
        }

        if (code == "") {
          $(".registerw-error").html("Debe ingresar un codigo");
          $(".registerw-error").fadeIn("fast");
          $(".page-wrapper").scrollTop(0);
          setTimeout(function () {
            $(".registerw-error").fadeOut("fast");
          }, 3000);
        } else if (user_from == "") {
          $(".register-error").html("Debe ingresar un emisor");
          $(".register-error").fadeIn("fast");
          $(".page-wrapper").scrollTop(0);
          setTimeout(function () {
            $(".registerw-error").fadeOut("fast");
          }, 3000);
        } else if (user_to == "") {
          $(".register-error").html("Debe ingresar un receptor");
          $(".register-error").fadeIn("fast");
          $(".page-wrapper").scrollTop(0);
          setTimeout(function () {
            $(".registerw-error").fadeOut("fast");
          }, 3000);
        } else if (trip == "" || trip == 0 || trip == "0") {
          $(".register-error").html("Debe ingresar un tipo de envio");
          $(".register-error").fadeIn("fast");
          $(".page-wrapper").scrollTop(0);
          setTimeout(function () {
            $(".registerw-error").fadeOut("fast");
          }, 3000);
        } else if (
          address_send == "" ||
          address_send == 0 ||
          address_send == "0"
        ) {
          $(".register-error").html("Debe ingresar un destino");
          $(".register-error").fadeIn("fast");
          $(".page-wrapper").scrollTop(0);
          setTimeout(function () {
            $(".registerw-error").fadeOut("fast");
          }, 3000);
        } else if (date_out == "") {
          $(".registerw-error").html("Debe ingresar una fecha de envio");
          $(".registerw-error").fadeIn("fast");
          $(".page-wrapper").scrollTop(0);
          setTimeout(function () {
            $(".registerw-error").fadeOut("fast");
          }, 3000);
        } else if (country == "" || country == 0 || country == "0") {
          $(".registerw-error").html("Debe ingresar un destino");
          $(".registerw-error").fadeIn("fast");
          $(".page-wrapper").scrollTop(0);
          setTimeout(function () {
            $(".registerw-error").fadeOut("fast");
          }, 3000);
        } else if (state == "" || state == 0 || state == "0") {
          $(".registerw-error").html("Debe ingresar un estado");
          $(".registerw-error").fadeIn("fast");
          $(".page-wrapper").scrollTop(0);
          setTimeout(function () {
            $(".registerw-error").fadeOut("fast");
          }, 3000);
        } else if (state == "" || state == 0 || state == "0") {
          $(".registerw-error").html("Debe ingresar un estado");
          $(".registerw-error").fadeIn("fast");
          $(".page-wrapper").scrollTop(0);
          setTimeout(function () {
            $(".registerw-error").fadeOut("fast");
          }, 3000);
        } else if (casillero == "") {
          $(".registerw-error").html("Debe ingresar un casillero");
          $(".registerw-error").fadeIn("fast");
          $(".page-wrapper").scrollTop(0);
          setTimeout(function () {
            $(".registerw-error").fadeOut("fast");
          }, 3000);
        } else {
          // var boxes = JSON.parse(localStorage.getItem('boxes'));

          $.ajax({
            method: "POST",
            url: "ajax/addWarehouse.php",
            data: {
              id: id_wareh,
              client_name: client_name,
              destiner_name: destiner_name,
              code: code,
              user_from: user_from,
              user_to: user_to,
              date_out: date_out,
              country: country,
              description: description,
              state: state,
              cost: cost,
              box: box,
              id_currier: id_currier,
              trip: trip,
              address_send: address_send,
              casillero: casillero,
              depart: depart,
              insurance: insurance,
              state_user: state_user,
              ship_state: ship_state,
              country_id: country_id,
              state_id: state_id,
              city_id: city_id,
              address: address,
              nota: nota,
              num_guia: num_guia,
            },
            success: function (res) {
              console.log(res);

              $(".register-success").html(
                "¡Warehouse registrado exitosamente!"
              );
              $(".register-success").fadeIn("fast");
              $(document).scrollTop(0);
              setTimeout(function () {
                location.href = "warehouses.php";
              }, 2000);
            },
          });
        }
      } else {
        /**EL WH NO TIENE CAJAS ASIGNADAS NO ES PERMITIDO GUARADAR LA DATA */
        $(".register-error").html("¡NO TIENES CAJAS PARA ESTE WAREHOUSE!");
        $(".register-error").fadeIn("fast");
        $(document).scrollTop(0);
        setTimeout(function () {
          $(".register-error").fadeOut("fast");
        }, 2000);
      }
    },
  });
}

function registerCurrier() {
  const name = $("#name").val();
  const cost = $("#currier-cost").val();
  const tasa = $("#currier-tasa").val();
  const type = $("#type").val();
  const region = $("#region").val();
  const cost_inter = $("#cost_inter").val();

  if (name == "") {
    $(".register-error").html("Debe ingresar un nombre");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (region == "" || region == "0") {
    $(".register-error").html("Debe ingresar una region");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (type == "" || type == "0") {
    $(".register-error").html("Debe ingresar un tipo de viaje");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (cost == "" || cost <= "0") {
    $(".register-error").html("Debe ingresar un costo");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (tasa == "" || tasa <= "0") {
    $(".register-error").html("Debe ingresar una tasa");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/addCurrier.php",
      data: {
        name: name,
        type: type,
        cost: cost,
        tasa: tasa,
        region: region,
        cost_inter: cost_inter,
      },
      success: function (res) {
        console.log(res);
        $(".register-success").html("¡Currier registrado exitosamente!");
        $(".register-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          location.reload();
        }, 1000);
      },
    });
  }
}

function addRegion() {
  const name = $("#name-region").val();

  if (name == "") {
    $(".region-error").html("Debe ingresar un nombre");
    $(".region-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".region-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/addRegion.php",
      data: { name: name },
      success: function (res) {
        console.log(res);
        $(".register-success").html("!Registro de región exitoso!");
        $(".register-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          location.reload();
        }, 2000);
      },
    });
  }
}

function addBOX() {
  const name = $("#name").val();
  const width = $("#width").val();
  const height = $("#height").val();
  const weigth = $("#weigth").val();
  const lenght = $("#lenght").val();
  const box_cost = $("#box_cost").val();

  console.log(weigth);

  $.ajax({
    method: "POST",
    url: "ajax/addBox.php",
    data: {
      name: name,
      width: width,
      height: height,
      weigth: weigth,
      lenght: lenght,
      box_cost: box_cost,
    },
    success: function (res) {
      console.log(res);
      $(".register-success").html("EDICIÓN EXITOSA");
      $(".register-success").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".register-error").fadeOut("fast");
      }, 2000);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function addDepart() {
  const name = $("#name-depart").val();
  const description = $("#description-depart").val();

  if (name == "") {
    $(".region-error").html("Debe ingresar un nombre");
    $(".region-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".region-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/addDepart.php",
      data: { name: name, description: description },
      success: function (res) {
        console.log(res);
        $(".register-success").html("¡Depart registrado exitosamente!");
        $(".register-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          location.reload();
        }, 2000);
      },
    });
  }
}

function registerCountry() {
  const name = $("#name").val();
  const region = $("#region").val();

  if (name == "") {
    $(".register-error").html("Debe ingresar un nombre");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (region == "" || region == "0") {
    $(".register-error").html("Debe ingresar una region");
    $(".register-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/addCountry.php",
      data: { name: name, region: region },
      success: function (res) {
        console.log(res);
        $(".register-success").html("¡Destino registrado exitosamente!");
        $(".register-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          location.reload();
        }, 2000);
      },
    });
  }
}

function editWarehouse() {
  const code = $("#code-title").html();
  const user_from = $("#idUserFor").val();
  const user_to = $("#idUser").val();
  const date_in = $("#date_in").val();
  const date_out = $("#date_send").val();
  const country = $("#country").val();
  const package = $("#package").val();
  const state = 1;
  const height = 0;
  const lenght = 0;
  const width = 0;
  const weight = 0;
  const description = "";
  const cost = "";
  const box = $("#box").val();
  const id_currier = $("#currier").val();
  const trip = $("#trip").val();
  const id = $("#idWarehouse").val();
  console.log(user_to);
  $.ajax({
    method: "POST",
    url: "ajax/editWarehouse.php",
    data: {
      code: code,
      user_from: user_from,
      user_to: user_to,
      date_in: date_in,
      date_out: date_out,
      country: country,
      package: package,
      description: description,
      state: state,
      height: height,
      lenght: lenght,
      width: width,
      weight: weight,
      cost: cost,
      box: box,
      id_currier: id_currier,
      trip: trip,
      id: id,
    },
    success: function (res) {
      console.log(res);
      $(".register-success").html("¡Warehouse modificado exitosamente!");
      $(".register-success").fadeIn("fast");
      $("#main-wrapper").scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });

  // if (code == '') {
  // 	$('.register-error').html('Debe ingresar un codigo');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else if(user_from == ''){
  // 	$('.register-error').html('Debe ingresar un emisor');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else if(user_to == ''){
  // 	$('.register-error').html('Debe ingresar un receptor');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else if(trip == '' || trip == 0 || trip == '0'){
  // 	$('.register-error').html('Debe ingresar un tipo de viaje');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else if(date_in == ''){
  // 	$('.register-error').html('Debe ingresar una fecha de ingreso');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else if(date_out == ''){
  // 	$('.register-error').html('Debe ingresar una fecha de envio');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else if(country == '' || country == 0 || country == '0'){
  // 	$('.register-error').html('Debe ingresar un destino');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else if(package == ''){
  // 	$('.register-error').html('Debe ingresar un paquete');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else if(state == '' || state == 0 || state == '0'){
  // 	$('.register-error').html('Debe ingresar un estado');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else if(height == 0){
  // 	$('.register-error').html('Debe ingresar el alto');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else if(lenght == 0){
  // 	$('.register-error').html('Debe ingresar el largo');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else if(width == 0){
  // 	$('.register-error').html('Debe ingresar el ancho');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else if(weight == 0){
  // 	$('.register-error').html('Debe ingresar el peso');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else if(cost == 0){
  // 	$('.register-error').html('Debe ingresar el costo');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else if(box == '' || box == 0 || box == '0'){
  // 	$('.register-error').html('Debe ingresar un box');
  //     $('.register-error').fadeIn('fast');
  //     $('.modal').scrollTop(0);
  //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);
  // }else{
  // 	$.ajax({
  // 		method:'POST',
  // 		url:'ajax/addWarehouse.php',
  // 		data:{code:code, user_from:user_from, user_to:user_to, date_in:date_in, date_out:date_out, country:country, package:package,
  // 			description:description, state:state, height:height,
  // 			lenght:lenght, width:width, weight:weight, cost:cost, box:box, id_currier:id_currier, trip:trip},
  // 		success:function (res) {
  // 			console.log(res);
  // 			$('.register-success').html('¡Warehouse registrado exitosamente!');
  // 		    $('.register-success').fadeIn('fast');
  // 		    $('.modal').scrollTop(0);
  // 		    setTimeout(function(){ location.reload(); }, 2000);
  // 		}
  // 	})
  // }
}

function bulkPacking() {
  var counter = 0;
  var list = [];

  $(".wcheck").each(function () {
    if (this.checked == true) {
      counter++;
      var id = $(this).attr("warehouse");
      list.push(id);
    }
  });

  if (counter > 0) {
    var json = JSON.stringify(list);

    $.ajax({
      method: "POST",
      url: "ajax/packingTemp.php",
      data: { json: json },
      success: function (res) {
        console.log(res);
        location.href = "packing.php?bulk=" + res;
      },
    });
  }
}

function showFacturasPendientes() {
  $("#facturasPendientes").css("display", "block");
  $("#facturasPagadas").css("display", "none");
  $("#facturasPorPagar").css("display", "none");

  $("#btnfacturasPorPagar").css("background-color", "white");
  $("#btnfacturasPendientes").css("background-color", "#005574");
  $("#btnfacturasPagadas").css("background-color", "white");

  $("#btnfacturasPorPagar").css("color", "#005574");
  $("#btnfacturasPendientes").css("color", "white");
  $("#btnfacturasPagadas").css("color", "#005574");

  $("#btnfacturasPorPagar").css("border", "2px solid #005574");
  $("#btnfacturasPendientes").css("border", "2px solid white");
  $("#btnfacturasPagadas").css("border", "2px solid #005574");

  $("#btnfacturasPorPagar").css("width", "300px");
  $("#btnfacturasPendientes").css("width", "300px");
  $("#btnfacturasPagadas").css("width", "300px");
}

function showFacturasPorPagar() {
  $("#facturasPorPagar").css("display", "block");
  $("#facturasPendientes").css("display", "none");
  $("#facturasPagadas").css("display", "none");

  $("#btnfacturasPendientes").css("background-color", "white");
  $("#btnfacturasPorPagar").css("background-color", "#005574");
  $("#btnfacturasPagadas").css("background-color", "white");

  $("#btnfacturasPendientes").css("color", "#005574");
  $("#btnfacturasPorPagar").css("color", "white");
  $("#btnfacturasPagadas").css("color", "#005574");

  $("#btnfacturasPendientes").css("border", "2px solid #005574");
  $("#btnfacturasPorPagar").css("border", "2px solid white");
  $("#btnfacturasPagadas").css("border", "2px solid #005574");

  $("#btnfacturasPendientes").css("width", "300px");
  $("#btnfacturasPorPagar").css("width", "300px");
  $("#btnfacturasPagadas").css("width", "300px");
}

function showFacturasPagadas() {
  $("#facturasPagadas").css("display", "block");
  $("#facturasPendientes").css("display", "none");
  $("#facturasPorPagar").css("display", "none");

  $("#btnfacturasPorPagar").css("background-color", "white");
  $("#btnfacturasPagadas").css("background-color", "#005574");
  $("#btnfacturasPendientes").css("background-color", "white");

  $("#btnfacturasPorPagar").css("color", "#005574");
  $("#btnfacturasPagadas").css("color", "white");
  $("#btnfacturasPendientes").css("color", "#005574");

  $("#btnfacturasPorPagar").css("border", "2px solid #005574");
  $("#btnfacturasPagadas").css("border", "2px solid white");
  $("#btnfacturasPendientes").css("border", "2px solid #005574");

  $("#btnfacturasPorPagar").css("width", "300px");
  $("#btnfacturasPagadas").css("width", "300px");
  $("#btnfacturasPendientes").css("width", "300px");
}

function bulkinvoices() {
  var counter = 0;
  var list = [];

  $(".wcheck").each(function () {
    if (this.checked == true) {
      counter++;
      var id = $(this).attr("warehouse");
      list.push(id);
    }
  });

  if (counter > 0) {
    var json = JSON.stringify(list);

    var fecha_salida = $("#date_send").val();
    var user_id = $("#client_id").val();
    var total_invoice = $("#total_factura").val();
    var type_trip_id = $("#trip_type_id").val();
    var destiner_id = $("#destiner_id").val();

    $.ajax({
      method: "POST",
      url: "ajax/invoicesTemp.php",
      data: {
        json: json,
        fecha_salida: fecha_salida,
        user_id: user_id,
        total_invoice: total_invoice,
        type_trip_id: type_trip_id,
        destiner_id: destiner_id,
      },
      success: function (res) {
        console.log(res);

        $(".register-success").html("¡FACTURA CREADA CON EXITO!");
        $(".register-success").fadeIn("fast");
        $(".container-fluid").scrollTop(0);
        setTimeout(function () {
          location.href = "invoices.php";
        }, 2000);
        //window.open("invoceFormat.php?id="+res, '_blank');
      },
    });
  }
}

function findwhForPacking() {
  const depart = $("#depart_packing").val();
  const trip = $("#trip_packing").val();
  const currier = $("#courier_packing").val();
  const date_send = $("#date_send_packing").val();
  const destiner = $("#city_destiner_packing").val();

  $.ajax({
    method: "POST",
    url: "ajax/findWarehouseForPacking.php",
    data: {
      departamento: depart,
      tipo_envio: trip,
      courier: currier,
      region: destiner,
      fecha_salida: date_send,
    },
    success: function (res) {
      if (res == 0) {
        $(".register-success").html(
          "¡EL USUARIO NO TIENE WAREHOUSE POR FACTURAR!"
        );
        $(".register-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          location.reload();
        }, 1000);
      } else {
        setTimeout(function () {
          location.href =
            "listPackingList.php?depart=" +
            depart +
            "&currier=" +
            currier +
            "&send_date=" +
            date_send +
            "&destiny=" +
            destiner +
            "&trip_id=" +
            trip;
        }, 1000);
      }
    },
  });
}

function getBulkPackages() {
  var data = [];
  var packages = JSON.parse(localStorage.getItem("packages"));
  var html = "";
  Object.keys(packages).forEach((key) => {
    html += '<div class="row">';

    if (packages[key].id != null) {
      var id = packages[key].id;
      html +=
        '<div class="col-md-6"># Paquete Seleccionado:</div><div class="col-md-6"><input disabled="disabled" type="text" class="form-control" value="' +
        id +
        '"></div><br>';
    }

    html += "</div>";
  });
  $(".bulkPackages").html(html);
}

function bulkPackages() {
  var counter = 0;
  var list = [];

  $(".pcheck").each(function () {
    if (this.checked == true) {
      counter++;
      var id = $(this).attr("package");
      var dataTemp = { id: id };
      list.push(dataTemp);
    }
  });

  localStorage.setItem("packages", JSON.stringify(list));

  location.href = "warehouses.php?bulk=true";
}

function countChecks() {
  var counter = 0;
  $(".wcheck").each(function () {
    if (this.checked == true) {
      counter++;
    }
  });
  if (counter > 0) {
    $("#createPacking").fadeIn("fast");
  } else {
    $("#createPacking").css("display", "none");
  }
}

function countChecksInvoices() {
  console.log("holalalalalaal entre al conteo");
  var counter = 0;
  $(".wcheck").each(function () {
    if (this.checked == true) {
      counter++;
    }
  });

  console.log(counter);
  if (counter > 0) {
    var i = 0;
    var suma = 0;
    for (i = 0; i < counter; i = i + 1) {
      suma = suma + parseFloat($(".subtotal-box")[i].innerHTML);
    }
    console.log(suma);
    $("#subtotal_boxes").val(suma);

    var extra_cost = parseFloat($("#subtotal_costo_extra_value").val());
    var total = 0;
    total = suma + extra_cost;
    $("#total_factura").val(total);
  } else {
    $("#subtotal_boxes").val(0);
  }
}

function countPChecks() {
  var counter = 0;
  $(".pcheck").each(function () {
    if (this.checked == true) {
      counter++;
    }
  });
  if (counter > 0) {
    $("#createBulkP").fadeIn("fast");
  } else {
    $("#createBulkP").css("display", "none");
  }
}

function addHeadQ(argument) {
  const name = $("#name").val();
  if (name == "") {
    $(".addH-error").html("Debe ingresar un nombre");
    $(".addH-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".addH-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/addHQ.php",
      data: { name: name },
      success: function (res) {
        console.log(res);
        $(".addH-success").html("¡Agencia creada exitosamente!");
        $(".addH-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          location.reload();
        }, 2000);
      },
    });
  }
}

function addStock(argument) {
  const name = $("#name").val();
  const headquarter = $("#headquarter").val();
  if (name == "") {
    $(".addStock-error").html("Debe ingresar un nombre");
    $(".addStock-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".addStock-error").fadeOut("fast");
    }, 3000);
  } else if (headquarter == "" || headquarter == "0" || headquarter == 0) {
    $(".addStock-error").html("Debe ingresar una agencia");
    $(".addStock-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".addStock-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/addStock.php",
      data: { name: name, headquarter: headquarter },
      success: function (res) {
        console.log(res);
        $(".addStock-success").html("¡Almacen creado exitosamente!");
        $(".addStock-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          location.reload();
        }, 2000);
      },
    });
  }
}

function fillStock() {
  const headquarter = $("#headquarter").val();
  if (headquarter == "" || headquarter == 0 || headquarter == "0") {
    $(".stock-div").html("");
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/stockFill.php",
      data: { headquarter: headquarter },
      success: function (res) {
        $(".stock-div").html(res);
      },
    });
  }
}

function searchFilter() {
  console.log("search");
  var q = $("#myInput").val();
  var t = 1;
  if (q != "") {
    $.ajax({
      method: "POST",
      url: "ajax/searchUserP.php",
      data: { q: q, t: t },
      success: function (res) {
        console.log(res);
        $("#myUL").html(res);
      },
    });

    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");

    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("a")[0];
      txtValue = a.textContent || a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  } else {
    $("#idUser").val("0");
    $("#myInput").val("");
    $("#myUL").html("");
  }
}

function searchForAlert() {
  console.log("search");
  $("#addPackageBtn").prop("disabled", false);
  $("#myAlert").html("");

  var q = $("#tracking").val();
  if (q != "") {
    $.ajax({
      method: "POST",
      url: "ajax/searchForAlert.php",
      data: { q: q },
      success: function (res) {
        console.log(res);
        if (res == 1) {
          $("#myAlert").html("El tracking ya existe");
          $("#addPackageBtn").prop("disabled", true);
        } else {
          $("#addPackageBtn").prop("disabled", false);
        }
      },
    });

    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("tracking");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myAlert");
    li = ul.getElementsByTagName("li");

    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("a")[0];
      txtValue = a.textContent || a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  } else {
    // $('#idAlert').val('0');
    $("#tracking").val("");
    $("#myAlert").html("");
  }
}

function searchForAlertF() {
  console.log("search");
  var q = $("#trackingF").val();
  if (q != "") {
    $.ajax({
      method: "POST",
      url: "ajax/searchForAlert.php",
      data: { q: q },
      success: function (res) {
        console.log(res);
        $("#myAlertF").html(res);
      },
    });

    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("trackingF");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myAlertF");
    li = ul.getElementsByTagName("li");

    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("a")[0];
      txtValue = a.textContent || a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  } else {
    // $('#idAlert').val('0');
    $("#trackingF").val("");
    $("#myAlertF").html("");
  }
}

function searchFilterW() {
  console.log("search");
  var q = $("#myInputS").val();
  console.log(q);
  var t = 1;
  if (q != "") {
    console.log("qq");
    $.ajax({
      method: "POST",
      url: "ajax/searchUserW.php",
      data: { q: q, t: t },
      success: function (res) {
        console.log(res);
        $("#myULS").html(res);
      },
    });

    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("myInputS");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myULS");
    li = ul.getElementsByTagName("li");

    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("a")[0];
      txtValue = a.textContent || a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  } else {
    $("#idUserW").val("0");
    $("#myInputS").val("");
    $("#myULS").html("");
  }
}

function searchUserPackage() {
  localStorage.clear();
  var q = $("#user_from").val();
  var t = 2;
  if (q != "") {
    $.ajax({
      method: "POST",
      url: "ajax/searchUserP.php",
      data: { q: q, t: t },
      success: function (res) {
        console.log(res);
        $("#myUS").html(res);
      },
    });

    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("user_from");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUS");
    li = ul.getElementsByTagName("li");

    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("a")[0];
      txtValue = a.textContent || a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  } else {
    $("#idUserFor").val("0");
    $("#user_from").val("");
    $("#myUS").html("");
  }
}

function details(id) {
  window.open("warehouse.php?id=" + id, "_blank");
}

function PDFgenerate(id) {
  window.open("closeTicketFormat.php?id=" + id, "_blank");
}

function deleteWP(id, go) {
  var id = id;
  var go = go;

  if (go == 1) {
    $.ajax({
      method: "POST",
      url: "ajax/deleteWP.php",
      data: { id: id },
      success: function (res) {
        console.log(res);
        if (res == 1) {
          $("#" + id + "wtr").remove();
        }
      },
    });
  } else {
    $("#" + id + "delete").fadeIn("fast");
  }
}

function searchWPackage() {
  var q = $("#package").val();
  var w = 1;

  if (q != "") {
    $.ajax({
      method: "POST",
      url: "ajax/searchWPackages.php",
      data: { q: q, w: w },
      success: function (res) {
        console.log(res);
        $("#myUW").html(res);
      },
    });

    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("package");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUW");
    li = ul.getElementsByTagName("li");

    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("a")[0];
      txtValue = a.textContent || a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  }
}

function searchWPackageM() {
  var q = $("#packageM").val();
  var w = 2;

  if (q != "") {
    $.ajax({
      method: "POST",
      url: "ajax/searchWPackages.php",
      data: { q: q, w: w },
      success: function (res) {
        console.log(res);
        $("#myUWM").html(res);
      },
    });

    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("package");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUW");
    li = ul.getElementsByTagName("li");

    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("a")[0];
      txtValue = a.textContent || a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  }
}

function findBox() {
  const box = $("#box").val();
  $.ajax({
    method: "POST",
    url: "ajax/findBox.php",
    data: { box: box },
    success: function (res) {
      console.log(res);
      var data = res.split("-");
      var width = data[0];
      var height = data[1];
      var weight = data[2];
      var length = data[3];

      $("#width").val(width);
      $("#height").val(height);
      $("#weight").val(weight);
      $("#lenght").val(length);
    },
  });
}

function selectAlert(date, tracking, width, height, weight, lenght) {
  $("#date_in").val(date);
  $("#tracking").val(tracking);
  $("#trackingF").val(tracking);
  $("#width").val(width);
  $("#height").val(height);
  $("#weight").val(weight);
  $("#lenght").val(lenght);
  $("#myAlert").html("");
  $("#myAlertF").html("");
}

function selectBox(
  id,
  name,
  width,
  height,
  weight,
  lenght,
  cubic_feet,
  lb_vol
) {
  // $('#box-id').val(id);
  $("#box-name").val(name);
  // $('#box-code').val(code);
  $("#box-width").val(width);
  $("#box-height").val(height);
  $("#box-weight").val(weight);
  $("#box-lenght").val(lenght);
  $("#pieCubico").val(cubic_feet);
  $("#LBVOL").val(lb_vol);

  $("#pesolb").val(weight);

  // $('#box-description').val(description);
  // $('#box-cost').val(cost);
  $("#myBOX").html("");

  // calculos de costo de cajas
  var ancho = parseInt($("#box-width").val());
  var largo = parseInt($("#box-lenght").val());
  var alto = parseInt($("#box-height").val());
  var peso = parseInt($("#box-weight").val());

  // calcular pie cubico y lb volumetricas

  var piecub = (largo * ancho * alto) / 1728;
  var lbVol = (alto * largo * ancho) / 166;

  var piecubRend = piecub.toFixed(3);
  var lbVolRend = lbVol.toFixed(3);
  var pesokg = peso * 0.4535;
  var metrovol = lbVol / 355;
  var metrocub = piecub * 0.2832;

  // redondeo de las medidas
  var pesor = pesokg.toFixed(3);
  var metrocubr = metrocub.toFixed(3);

  $("#pieCubico").val(piecubRend);
  $("#LBVOL").val(lbVolRend);
  $("#pesolb").val(peso);
  $("#pesokg").val(pesor);
  $("#metrocubico").val(metrocubr);

  // CALCULO DE COSTOS DE PAQUETES

  var tarifa_currier = $("#tarifa_base").val();
  var tarifa_te = $("#tasa_base_te").val();
  var tarifa_interna = $("#tarifaInterna_base").val();
  var tarifa_material = $("#material_tasa").val();
  const trip_id = $("#trip").val();

  /************************************************* */
  var costo_currier = 0;
  var monto_te = 0;
  var costo_material = 0;
  var costo_interno = 0;

  //calculo del costo interno
  if (tarifa_interna > 0) {
    if (trip_id == 1) {
      //El envio es aereo
      if (peso > lbVol) {
        costo_interno = peso * tarifa_interna;
      } else {
        costo_interno = lbVolRend * tarifa_interna;
      }
    } else {
      costo_interno = piecubRend * tarifa_interna;
    }
  } else {
    costo_interno = 0;
  }

  if (trip_id == 1) {
    //El envio es aereo
    if (peso > lbVol) {
      costo_currier = peso * tarifa_currier;
      monto_te = peso * tarifa_te;
    } else {
      costo_currier = lbVolRend * tarifa_currier;
      monto_te = lbVolRend * tarifa_te;
    }
  } else {
    costo_currier = piecubRend * tarifa_currier;
    monto_te = piecubRend * tarifa_te;
  }

  /** CALCULO DE TARIFA INTERNA*/

  /**CALCULO COSTO MATERIAL */
  if (tarifa_material > 0) {
    costo_material = tarifa_material;
  } else {
    costo_material = 0;
  }

  /**CALCULOS TOTALES */
  var costo_total = 0;
  var monto_total = 0;
  var ganancia = 0;
  var porcGanancia = 0;

  costo_total = costo_currier + costo_interno + costo_material;
  total = monto_te + costo_total;
  ganancia = total - costo_total;
  porcGanancia = (ganancia / total) * 100;

  // redondeo de datos finales

  var costo_currierr = costo_currier.toFixed(3);
  var costo_internor = costo_interno.toFixed(3);

  var costo_totalr = costo_total.toFixed(3);
  var totalr = total.toFixed(3);
  var gananciar = ganancia.toFixed(3);
  var porcGananciar = porcGanancia.toFixed(3);

  //asignar valores a los campos correspondientes

  $("#currier_tasa").val(costo_currierr);
  $("#costo_interno").val(costo_internor);
  $("#material_tasa").val(costo_material);
  $("#costo_total").val(costo_totalr);
  $("#total").val(totalr);
  $("#ganancia").val(gananciar);
  $("#porcGanancia").val(porcGananciar);
}

function modalDestinerOpen() {
  $("#warehouseModal").modal("hide");
  $("#newDestinerModal").modal("show");
  const id_user = $("#id_user_from").val();
  $("#user_id_for_destiner").val(id_user);
  console.log(id_user);
}

function showOnHand() {
  $("#whOnHand").css("display", "block");
  $("#whComplete_wh").css("display", "none");
  $("#whClientList_wh").css("display", "none");

  $("#whOpen").css("background-color", "white");
  $("#whOpen").css("color", "#005574");
  $("#whOpen").css("border", "2px solid #005574");
  $("#whComplete").css("background-color", "#005574");
  $("#whComplete").css("color", "white");
  $("#whComplete").css("border", "2px solid #005574");
  $("#whListClient").css("background-color", "#005574");
  $("#whListClient").css("color", "white");
  $("#whListClient").css("border", "2px solid #005574");
}
function showCompleteWh() {
  $("#whOnHand").css("display", "none");
  $("#whComplete_wh").css("display", "block");
  $("#whClientList_wh").css("display", "none");

  $("#whComplete").css("background-color", "white");
  $("#whComplete").css("color", "#005574");
  $("#whComplete").css("border", "2px solid #005574");
  $("#whOpen").css("background-color", "#005574");
  $("#whOpen").css("color", "white");
  $("#whOpen").css("border", "2px solid #005574");
  $("#whListClient").css("background-color", "#005574");
  $("#whListClient").css("color", "white");
  $("#whListClient").css("border", "2px solid #005574");
}
function showListClient() {
  $("#whOnHand").css("display", "none");
  $("#whComplete_wh").css("display", "none");
  $("#whClientList_wh").css("display", "block");

  $("#whListClient").css("background-color", "white");
  $("#whListClient").css("color", "#005574");
  $("#whListClient").css("border", "2px solid #005574");

  $("#whComplete").css("background-color", "#005574");
  $("#whComplete").css("color", "white");
  $("#whComplete").css("border", "2px solid #005574");

  $("#whOpen").css("background-color", "#005574");
  $("#whOpen").css("color", "white");
  $("#whOpen").css("border", "2px solid #005574");
}

function modalBoxAddtOpen() {
  $("#addBoxModal").modal("show");
  const id_user = $("#box-id-read").val();
  const wh_id = $("#wh_id_update").val();
  $("#box-id").val(id_user);
  $("#warehouse_id").val(wh_id);
  console.log(id_user);

  $.ajax({
    method: "POST",
    url: "ajax/findBoxesForEdit.php",
    data: { id: id_user },
    success: function (res) {
      console.log(res);
      const dataUser = res.split("-");

      const cost_taf_type_trip = dataUser[17];

      const taf_inter_type_trip = dataUser[15];
      const tasa_type_trip = dataUser[13];

      $("#tarifa_base").val(tasa_type_trip);
      $("#tasa_base_te").val(cost_taf_type_trip);
      $("#tarifaInterna_base").val(taf_inter_type_trip);
    },
  });
}

function modalBoxEditOpen() {
  $("#editBoxModal").modal("show");
  const id_user = $("#box-id-read").val();
  const wh_id = $("#wh_id_update").val();
  $("#box-id").val(id_user);
  $("#warehouse_id").val(wh_id);
  console.log(id_user);

  $.ajax({
    method: "POST",
    url: "ajax/findBoxesForEdit.php",
    data: { id: id_user },
    success: function (res) {
      console.log(res);
      const dataUser = res.split("-");
      const id = dataUser[0];
      const name = dataUser[1];
      const height = dataUser[2];
      const width = dataUser[3];
      const weigth = dataUser[4];
      const lenght = dataUser[5];
      const description = dataUser[6];
      const cost = dataUser[7];
      const category_id = dataUser[8];
      const kg_weigth = dataUser[9];
      const cubic_feet = dataUser[10];
      const cubic_m = dataUser[11];
      const lb_vol = dataUser[12];
      const cost_taf_type_trip = dataUser[17];
      const cost_type_trip = dataUser[14];
      const taf_inter_type_trip = dataUser[15];
      const cost_inter_type_trip = dataUser[16];
      const tasa_type_trip = dataUser[13];
      const monto_calculo_type_trip = dataUser[18];
      const cost_total = dataUser[19];
      const ganancia_cost_type_trip = dataUser[20];
      const porcentaje_ganancia_cost_type_trip = dataUser[21];
      const gastosExtra = dataUser[22];

      $("#box-name").val(name);
      $("#box-height").val(height);
      $("#box-width").val(width);
      $("#box-weight").val(weigth);
      $("#box-lenght").val(lenght);
      $("#box-description").val(description);
      $("#box-cost").val(cost);
      $("#category").val(category_id);
      $("#pesokg").val(kg_weigth);
      $("#pieCubico").val(cubic_feet);
      $("#metrocubico").val(cubic_m);
      $("#LBVOL").val(lb_vol);
      $("#tarifa_base").val(cost_taf_type_trip);
      $("#currier_tasa").val(cost_type_trip);
      $("#tarifaInterna_base").val(taf_inter_type_trip);
      $("#costo_interno").val(cost_inter_type_trip);
      $("#tasa_base_te").val(tasa_type_trip);
      $("#costo_total").val(monto_calculo_type_trip);
      $("#total").val(cost_total);
      $("#ganancia").val(ganancia_cost_type_trip);
      $("#porcGanancia").val(porcentaje_ganancia_cost_type_trip);
      $("#material_tasa").val(gastosExtra);
    },
  });
}

function editBoxForWarehouse() {
  const box_id = $("#id_box_edit").val();
  const height = $("#box-height-edit").val();
  const width = $("#box-width-edit").val();
  const lenght = $("#box-lenght-edit").val();
  const weight = $("#box-weight-edit").val();
  const cost = $("#box-cost-edit").val();
  const category = $("#category-edit").val();
  const description = $("#box-description-edit").val();
  const wh_id = $("#id_wh_edit").val();

  $.ajax({
    method: "POST",
    url: "ajax/editBoxForWarehouse.php",
    data: {
      id: box_id,
      height: height,
      width: width,
      lenght: lenght,
      weight: weight,
      cost: cost,
      category: category,
      description: description,
    },
    success: function (res) {
      console.log(res);

      setTimeout(function () {
        const texto = wh_id;
        let procesado;

        // trim() elimina los espacios a ambos lados de nuestra cadena
        procesado = texto.trim();
        $url = "warehouseDetails.php?id=" + procesado;
        $(location).attr("href", $url);
      }, 1000);
    },
  });
}

function selectUser(id, name) {
  $("#nuevo-destinatario").css("display", "block");
  $("#id_user_from").val(id);
  $("#myInput").val(name);
  $("#myUL").html("");
}

function selectUserForP(id, name) {
  $("#idUserFor").val(id);
  $("#user_from").val(name);
}

function selectUserForW(id, name) {
  $("#idUser").val(id);
  $("#user_from").val(name);
}

function selectUserW(
  id_destiner,
  name_destiner,
  contact_phone,
  estado,
  city_id,
  state_id,
  country_id,
  address,
  contact_phone
) {
  $(".region").css("display", "block");
  $(".ciudad_destino").css("display", "block");

  $("#id_user_to").val(id_destiner);
  // $('#idUser').val(id);
  // $('#code').val(code);

  $("#myInput").val(name_destiner);
  $("#address_send").val(city_id);
  $("#region").val(estado);
  $("#country_destiner_get").val(country_id);
  $("#state_destiner_get").val(state_id);
  $("#address_address_destiner_get").val(address);
  $("#contact_phone_get").val(contact_phone);

  const trip_id = $("#trip").val();
  // ciudad destino
  const ciudad = $("#address_send").val();
  const currier_id = $("#currier").val();

  // $('#tarifa').val(trip_id);
  // $('#tarifaInterna').val(region);
  // $('#tasa').val(currier_id);

  $.ajax({
    method: "POST",
    url: "ajax/findCostForCurrier.php",
    data: { trip_id: trip_id, region: ciudad, currier_id: currier_id },
    success: function (res) {
      console.log(res);
      console.log("estoy en la consulta de tarifas");
      const dataUser = res.split("-");
      const tarifa = dataUser[0];
      const tarifaTE = dataUser[1];

      $("#tarifa_base").val(tarifa);
      $("#tasa_base_te").val(tarifaTE);

      /* CALCULAR LOS COSTOS EN CASO DE QUE YA ESTEN CARGADO EN EL FORM*/
      if (
        $("#box-height").val() > 0 &&
        $("#box-width").val() > 0 &&
        $("#box-lenght").val() > 0
      ) {
        var ancho = parseInt($("#box-width").val());
        var largo = parseInt($("#box-lenght").val());
        var alto = parseInt($("#box-height").val());
        var peso = parseInt($("#box-weight").val());

        // calcular pie cubico y lb volumetricas

        var piecub = (largo * ancho * alto) / 1728;
        var lbVol = (alto * largo * ancho) / 166;

        var piecubRend = piecub.toFixed(3);
        var lbVolRend = lbVol.toFixed(3);
        var pesokg = peso * 0.4535;
        var metrovol = lbVol / 355;
        var metrocub = piecub * 0.2832;

        // redondeo de las medidas
        var pesor = pesokg.toFixed(3);
        var metrocubr = metrocub.toFixed(3);

        $("#pieCubico").val(piecubRend);
        $("#LBVOL").val(lbVolRend);
        $("#pesolb").val(peso);
        $("#pesokg").val(pesor);
        $("#metrocubico").val(metrocubr);

        // CALCULO DE COSTOS DE PAQUETES

        var tarifa_currier = $("#tarifa_base").val();
        var tarifa_te = $("#tasa_base_te").val();
        var tarifa_interna = $("#tarifaInterna_base").val();
        var tarifa_material = $("#material_tasa").val();
        const trip_id = $("#trip").val();

        /************************************************* */
        var costo_currier = 0;
        var monto_te = 0;
        var costo_material = 0;
        var costo_interno = 0;

        //calculo del costo interno
        if (tarifa_interna > 0) {
          if (trip_id == 1) {
            //El envio es aereo
            if (peso > lbVol) {
              costo_interno = peso * tarifa_interna;
            } else {
              costo_interno = lbVolRend * tarifa_interna;
            }
          } else {
            costo_interno = piecubRend * tarifa_interna;
          }
        } else {
          costo_interno = 0;
        }

        if (trip_id == 1) {
          //El envio es aereo
          if (peso > lbVol) {
            costo_currier = peso * tarifa_currier;
            monto_te = peso * tarifa_te;
          } else {
            costo_currier = lbVolRend * tarifa_currier;
            monto_te = lbVolRend * tarifa_te;
          }
        } else {
          costo_currier = piecubRend * tarifa_currier;
          monto_te = piecubRend * tarifa_te;
        }

        /** CALCULO DE TARIFA INTERNA*/

        console.log();

        /**CALCULO COSTO MATERIAL */
        if (tarifa_material > 0) {
          costo_material = tarifa_material;
        } else {
          costo_material = 0;
        }

        /**CALCULOS TOTALES */
        var costo_total = 0;
        var monto_total = 0;
        var ganancia = 0;
        var porcGanancia = 0;

        costo_total = costo_currier + costo_interno + costo_material;
        total = monto_te;
        ganancia = total - costo_total;
        porcGanancia = (ganancia / total) * 100;

        // redondeo de datos finales

        var costo_currierr = costo_currier.toFixed(3);
        var costo_internor = costo_interno.toFixed(3);

        var costo_totalr = costo_total.toFixed(3);
        var totalr = total.toFixed(3);
        var gananciar = ganancia.toFixed(3);
        var porcGananciar = porcGanancia.toFixed(3);

        //asignar valores a los campos correspondientes

        $("#currier_tasa").val(costo_currierr);
        $("#costo_interno").val(costo_internor);
        $("#material_tasa").val(costo_material);
        $("#costo_total").val(costo_totalr);
        $("#total").val(totalr);
        $("#ganancia").val(gananciar);
        $("#porcGanancia").val(porcGananciar);
      }
    },
  });

  // $('#address_send').val(country);

  // $('#contactAddress').val(address);

  // $("#contactPhone").fadeIn('fast');
  // $("#contactAddress").fadeIn('fast');
  $("#myDestiner").html("");
}
function selectDestinerForCode(
  id_user,
  id_destiner,
  name_user,
  name_destiner,
  contact_phone,
  estado,
  city,
  address
) {
  // mostrar input con datos de destino
  $(".region").css("display", "block");
  $(".ciudad_destino").css("display", "block");
  $("#id_user_from").val(id_user);
  $("#id_user_to").val(id_destiner);
  $("#user_id_for_destiner").val(id_user);
  // $('#idUser').val(id);
  // $('#code').val(code);
  $("#user_from").val(name_user);
  $("#myInput").val(name_destiner);
  $("#address_send").val(city);

  // $('#address_send').val(country);
  $("#region").val(estado);
  // $('#contactAddress').val(address);

  // $("#contactPhone").fadeIn('fast');
  // $("#contactAddress").fadeIn('fast');
  $("#myUS").html("");

  // $('#destiner').css('display','block');
  $;
}

function RegUsuarioWarehouse(w) {
  const countryVenezuela = $("#countryVenezuela").val();
  const name = $("#name_wh").val();
  const lastname = $("#lastname_wh").val();
  const email = $("#email_wh").val();
  const password = "totalenviosuser";
  const phone = $("#phone_wh").val();
  const phone_house = $("#phone_house_wh").val();
  const documentU = $("#document_wh").val();
  const country = $("#address_destiny_company").val();

  const code = "TE-0000";

  let address = "";

  if (w == 1) {
    if (countryVenezuela == 1) {
      address = $("#select_address_company").val();
    } else {
      address = $("#address_company").val();
    }
  } else {
    address = "NONE";
  }

  let address_state = "";
  if (w == 1) {
    if (countryVenezuela == 1) {
      address_state = $("#select_state_company").val();
    } else {
      address_state = $("#address_state_company").val();
    }
  } else {
    address_state = "NONE";
  }

  let address_city = "";
  if (w == 1) {
    if (countryVenezuela == 1) {
      address_city = $("#select_city_company").val();
    } else {
      address_city = $("#address_city_company").val();
    }
  } else {
    address_city = "NONE";
  }

  const headquarter = 1;
  const born_date = $("#born_date_wh").val();
  const user_type = $("#user_type_wh").val();

  if (name == "") {
    $(".user-error").html("Debe ingresar un nombre");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un nombre");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (lastname == "") {
    $(".user-error").html("Debe ingresar un apellido");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un apellido");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (born_date == "" || born_date == "0") {
    $(".user-error").html("Debe ingresar una fecha de nacimiento");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar una fecha de nacimiento");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (email == "" || isEmail(email) == false) {
    $(".user-error").html("Debe ingresar un email correcto");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un email");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (password == "") {
    $(".user-error").html("Debe ingresar un password");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un password");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (phone == "") {
    $(".user-error").html("Debe ingresar un teléfono");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un telefono");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (documentU == "") {
    $(".user-error").html("Debe ingresar un documento");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un documento");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (country == "" || country == "0") {
    $(".user-error").html("Debe ingresar un país");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un país");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (address == "") {
    $(".user-error").html("Debe ingresar una dirección");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar una dirección");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (address_state == "") {
    $(".user-error").html("Debe ingresar un estado");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un estado");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (address_city == "") {
    $(".user-error").html("Debe ingresar una ciudad");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar una ciudad");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (headquarter == "" || headquarter == "0") {
    $(".user-error").html("Debe ingresar una agencia");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar una agencia");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (code == "" || code == "0") {
    $(".user-error").html("Debe ingresar un codigo de casillero");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un codigo de casillero");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    $(".register-button").html(
      '<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>'
    );
    $.ajax({
      method: "POST",
      url: "ajax/createUserForWh.php",
      data: {
        w: w,
        countryVenezuela: countryVenezuela,
        name: name,
        lastname: lastname,
        email: email,
        password: password,
        phone: phone,
        documentU: documentU,
        country: country,
        address: address,
        headquarter: headquarter,
        code: code,
        born_date: born_date,
        user_type: user_type,
        address_city: address_city,
        address_state: address_state,
        phone_house: phone_house,
      },
      success: function (res) {
        console.log(res);
        $("#user_id").val(res);

        $(".user-success").html("Usuario registrado.");
        $(".user-success").fadeIn("fast");
        $("#card-registro-usuarios-wh").css("display", "none");

        // $('.register-button').html('Registrar');
        // if (res != 'Existe') {

        //     if (w == 1) {
        // 		const dataUser = res.split("-");
        //     	// selectUserForP(dataUser[0]);
        // 		$('#id_register_user').html(dataUser[0]);
        // 		setTimeout(function(){
        // 			$('#confirmRegisterModal').modal('show');
        // 		}, 1000);

        // 		setTimeout(function(){
        // 			$(location).attr('href','login.php');
        // 		}, 3000);

        // 	    $(document).scrollTop(0);
        //     }else{
        //     	const dataUser = res.split("-");
        //     	selectUserForP(dataUser[0], dataUser[1]);
        //     	$(".user-success").html('Usuario registrado.');
        //     	$('.user-success').fadeIn('fast');
        //     	setTimeout(function(){ $('#newUserModal').modal('hide'); $('#warehouseModal').modal('show'); $('.user-success').fadeOut('fast'); }, 3000);
        //     }

        // }else if(res == 'Existe'){

        // 	$('.user-error').html('El correo que intenta registrar ya existe');
        //     $('.user-error').fadeIn('fast');
        //     $(document).scrollTop(0);
        //     setTimeout(function(){ $('.user-error').fadeOut('fast'); }, 3000);

        //     $('.register-error').html('El correo que intenta registrar ya existe');
        //     $('.register-error').fadeIn('fast');
        //     $(document).scrollTop(0);
        //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);

        // }
      },
    });
  }
}

function salirRegistro() {
  setTimeout(function () {
    $(location).attr("href", "registerWarehouse.php");
  }, 2000);
}

function RegDestinerForUserWarehouse() {
  const name = $("#name_destiner_user").val();
  const lastname = $("#lastname_destiner_user").val();
  // const email = $("#email_destiner").val();
  // const password = "totalenviosuser";
  const phone = $("#contact_phone_destiner_user").val();
  // const phone_house = $("#phone_house_wh").val();
  // const documentU = $("#document_wh").val();
  const country = $("#address_destiny_destiner").val();
  const user_id = $("#user_id").val();

  // const code = 'TE-0000';

  let address = "";
  const countryVenezuela = 1;

  if (countryVenezuela == 1) {
    address = $("#select_address_destiner").val();
  } else {
    address = $("#address_destiner").val();
  }

  let address_state = "";

  if (countryVenezuela == 1) {
    address_state = $("#select_state_destiner").val();
  } else {
    address_state = $("#address_state_destiner").val();
  }

  let address_city = "";

  if (countryVenezuela == 1) {
    address_city = $("#select_city_destiner").val();
    $("#ciudad").val(address_city);
  } else {
    address_city = $("#address_city_destiner").val();
  }

  // const headquarter = 1;
  // const born_date = $('#born_date_wh').val();
  // const user_type = $('#user_type_wh').val();

  if (name == "") {
    $(".user-error").html("Debe ingresar un nombre");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un nombre");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (lastname == "") {
    $(".user-error").html("Debe ingresar un apellido");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un apellido");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (phone == "") {
    $(".user-error").html("Debe ingresar un teléfono");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un telefono");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (country == "" || country == "0") {
    $(".user-error").html("Debe ingresar un país");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un país");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (address == "") {
    $(".user-error").html("Debe ingresar una dirección");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar una dirección");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (address_state == "") {
    $(".user-error").html("Debe ingresar un estado");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar un estado");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else if (address_city == "") {
    $(".user-error").html("Debe ingresar una ciudad");
    $(".user-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".user-error").fadeOut("fast");
    }, 3000);
    $(".register-error").html("Debe ingresar una ciudad");
    $(".register-error").fadeIn("fast");
    $(document).scrollTop(0);
    setTimeout(function () {
      $(".register-error").fadeOut("fast");
    }, 3000);
  } else {
    $(".register-button").html(
      '<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>'
    );
    $.ajax({
      method: "POST",
      url: "ajax/createAddressee.php",
      data: {
        name: name,
        lastname: lastname,
        phone: phone,
        address: address,
        address_city: address_city,
        address_state: address_state,
        address_destiny: country,
        user_id: user_id,
      },
      success: function (res) {
        if (res > 0) {
          console.log(res);
          setTimeout(function () {
            $(location).attr("href", "registerWarehouse.php");
          }, 3000);
        } else {
          console.log(res);
        }

        // $('.register-button').html('Registrar');
        // if (res != 'Existe') {

        //     if (w == 1) {
        // 		const dataUser = res.split("-");
        //     	// selectUserForP(dataUser[0]);
        // 		$('#id_register_user').html(dataUser[0]);
        // 		setTimeout(function(){
        // 			$('#confirmRegisterModal').modal('show');
        // 		}, 1000);

        // 		setTimeout(function(){
        // 			$(location).attr('href','login.php');
        // 		}, 3000);

        // 	    $(document).scrollTop(0);
        //     }else{
        //     	const dataUser = res.split("-");
        //     	selectUserForP(dataUser[0], dataUser[1]);
        //     	$(".user-success").html('Usuario registrado.');
        //     	$('.user-success').fadeIn('fast');
        //     	setTimeout(function(){ $('#newUserModal').modal('hide'); $('#warehouseModal').modal('show'); $('.user-success').fadeOut('fast'); }, 3000);
        //     }

        // }else if(res == 'Existe'){

        // 	$('.user-error').html('El correo que intenta registrar ya existe');
        //     $('.user-error').fadeIn('fast');
        //     $(document).scrollTop(0);
        //     setTimeout(function(){ $('.user-error').fadeOut('fast'); }, 3000);

        //     $('.register-error').html('El correo que intenta registrar ya existe');
        //     $('.register-error').fadeIn('fast');
        //     $(document).scrollTop(0);
        //     setTimeout(function(){ $('.register-error').fadeOut('fast'); }, 3000);

        // }
      },
    });
  }
}

function selectUserP(id, name) {
  const q = id;
  const t = 1;
  $("#nuevo-destinatario").css("display", "block");
  $("#id_user_from").val(id);
  $("#user_from").val(name);

  $.ajax({
    method: "POST",
    url: "ajax/searchUserW.php",
    data: { q: q, t: t },
    success: function (res) {
      console.log(res);
      $("#myUS").html("");
      $("#myDestiner").html(res);
    },
  });
}

function selectWPackage(id, height, weight, width, lenght) {
  $("#idPackage").val(id);
  $("#package").val(id);

  $("#height").val(height);
  $("#weight").val(weight);
  $("#width").val(width);
  $("#lenght").val(lenght);
  $("#myUW").html("");
}

function selectWPackageM(id, height, weight, width, lenght) {
  $("#idPackageM").val(id);
  $("#packageM").val(id);

  $("#heightM").val(height);
  $("#weightM").val(weight);
  $("#widthM").val(width);
  $("#lenghtM").val(lenght);
  $("#myUWM").html("");
}

function searchUser() {
  const q = $("#search-user").val();

  $.ajax({
    method: "POST",
    url: "ajax/searchUsers.php",
    data: { q: q },
    success: function (res) {
      console.log(res);
      $(".users-table").html(res);
    },
  });
}

function searchUserA() {
  const q = $("#search-user-a").val();

  $.ajax({
    method: "POST",
    url: "ajax/searchUsersA.php",
    data: { q: q },
    success: function (res) {
      console.log(res);
      $(".users-table-a").html(res);
    },
  });
}

function searchPackage() {
  const idUser = $("#id_user_from").val();
  const date_in = $("#date_in_s").val();
  const date_out = $("#date_out").val();

  $.ajax({
    method: "POST",
    url: "ajax/searchPackages.php",
    data: { idUser: idUser, date_in: date_in, date_out: date_out },
    success: function (res) {
      console.log(res);
      $(".packages-table").html(res);
    },
  });
}

function searchAlert() {
  const idUser = $("#client_id").val();
  const date_in = $("#date_in_s").val();
  const date_out = $("#date_out").val();
  $.ajax({
    method: "POST",
    url: "ajax/searchAlerts.php",
    data: { idUser: idUser, date_in: date_in, date_out: date_out },
    success: function (res) {
      console.log(res);
      $(".packages-table").html(res);
    },
  });
}

function searchFactPag() {
  const idUser = $("#idUser").val();
  const date_in = $("#date_in_s").val();
  const date_out = $("#date_out").val();

  console.log(idUser, date_in, date_out);
  $.ajax({
    method: "POST",
    url: "ajax/searchFactPag.php",
    data: { idUser: idUser, date_in: date_in, date_out: date_out },
    success: function (res) {
      console.log(res);
    },
  });
}

function searchWarehouse() {
  const idUser = $("#idUserW").val();
  const date_in = $("#date_in_s").val();
  const date_out = $("#date_out").val();
  $.ajax({
    method: "POST",
    url: "ajax/searchWarehouses.php",
    data: { idUser: idUser, date_in: date_in, date_out: date_out },
    success: function (res) {
      console.log(res);
      $(".warehouse-table").html(res);
    },
  });
}

function registerDest(id_user, name_user) {
  $("#id_user_from").val(id_user);
  $("#nuevo-destinatario").css("display", "block");
  // $('#idUser').val(id);
  // $('#code').val(code);
  $("#user_from").val(name_user);

  // $('#address_send').val(country);

  // $('#contactAddress').val(address);

  // $("#contactPhone").fadeIn('fast');
  // $("#contactAddress").fadeIn('fast');
  $("#myUS").html("");
}
function searchBoxes() {
  // $('#box-id').val('0');
  // $('#box-code').val($('#hiddenBoxCode').val());
  var q = $("#box-name").val();
  if (q != "") {
    $.ajax({
      method: "POST",
      url: "ajax/searchBoxes.php",
      data: { q: q },
      success: function (res) {
        console.log(res);
        $("#myBOX").html(res);
      },
    });

    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("box-name");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myBOX");
    li = ul.getElementsByTagName("li");

    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("a")[0];
      txtValue = a.textContent || a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  } else {
    // $('#idAlert').val('0');
    // $('#box-id').val('0');
    // $('#box-code').val($('#hiddenBoxCode').val());
    $("#box-name").val("");
    $("#myBOX").html("");
  }
}

function addIBox() {
  const name = $("#box-name").val();
  const width = $("#box-width").val();
  const height = $("#box-height").val();
  const lenght = $("#box-lenght").val();
  const weight = $("#box-weight").val();

  if (name == "" || name == 0) {
    $(".box-error").html("Debe ingresar un nombre para la caja");
    $(".box-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".box-error").fadeOut("fast");
    }, 2000);
  } else if (width == "" || width == 0) {
    $(".box-error").html("Debe ingresar un ancho para la caja");
    $(".box-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".box-error").fadeOut("fast");
    }, 2000);
  } else if (height == "" || height == 0) {
    $(".box-error").html("Debe ingresar una altura para la caja");
    $(".box-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".box-error").fadeOut("fast");
    }, 2000);
  } else if (lenght == "" || lenght == 0) {
    $(".box-error").html("Debe ingresar un largo para la caja");
    $(".box-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".box-error").fadeOut("fast");
    }, 2000);
  } else if (weight == "" || weight == 0) {
    $(".box-error").html("Debe ingresar un peso para la caja");
    $(".box-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".box-error").fadeOut("fast");
    }, 2000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/addIBox.php",
      data: {
        name: name,
        width: width,
        height: height,
        lenght: lenght,
        weight: weight,
      },
      success: function (res) {
        console.log(res);
        $(".box-success").html("Box ingresado correctamente.");
        $(".box-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          $(".box-success").fadeOut("fast");
          location.reload();
        }, 2000);
      },
    });
  }
}

function addBox() {
  if (
    $("#user_form").val() == "" ||
    $("#myInput").val() == "" ||
    $("#region").val() == "" ||
    $("#depart").val() == "" ||
    $("#currier").val() == "" ||
    $("#tarifa_base").val() == "" ||
    $("#tasa_base_te").val() == "" ||
    $("#tarifa_base").val() == 0 ||
    $("#tasa_base_te").val() == 0 ||
    $("#tarifa_base").val() == 0.0 ||
    $("#tasa_base_te").val() == 0.0
  ) {
    $(".register-box-error").html("FALTA DATOS PARA CÁLCULAR COSTOS.");
    $(".register-box-error").fadeIn("fast");
    setTimeout(function () {
      $(".register-box-error").fadeOut("fast");
    }, 3000);
  } else {
    var dataBoxes = [];
    const id_boxes = $("#box-code").val();
    let name = $("#box-name").val();
    const wh_id = $("#warehouse_id").val();
    const destiner = $("#id_user_to").val();
    const currier_id = $("#currier").val();
    let new_id = 0;
    const trip_id = $("#trip").val();
    const width = $("#box-width").val();
    const height = $("#box-height").val();
    const lenght = $("#box-lenght").val();
    var weight = $("#box-weight").val();
    const description = $("#box-description").val();
    const cost = $("#box-cost").val();
    const category = $("#category").val();

    var piecubtotal = parseFloat($("#pieCubTotales").val());
    var lbvolTotal = parseFloat($("#lbvolTotales").val());
    var pesototal = parseFloat($("#pesoTotal").val());
    var montoTotal = parseFloat($("#monto_total_box").val());
    var costoTotal = parseFloat($("#costo_total_box").val());
    var gananciaTotal = parseFloat($("#ganancia_total").val());
    var porcGananciaTotal = parseFloat($("#total_porcentaje_ganancia").val());

    var tarifa_currier = parseFloat($("#tarifa_base").val());
    var tarifa_te = parseFloat($("#tasa_base_te").val());
    var tarifaInternBase_val = $("#tarifaInterna_base").val();

    if (
      tarifaInternBase_val == "" ||
      tarifaInternBase_val == 0 ||
      tarifaInternBase_val == 0.0 ||
      tarifaInternBase_val == "0.00"
    ) {
      var tarifaInternBase = 0;
      var costo_interno_total = 0;
    } else {
      var tarifaInternBase = parseFloat($("#tarifaInterna_base").val());
      var costo_interno_total = $("#costo_interno").val();
    }

    // medidas del paquete

    const cubic_feet = $("#pieCubico").val();
    const lb_vol = $("#LBVOL").val();
    const insurance = $("#seguro").val();
    const logistic = $("#logistica").val();
    const material = $("#material_tasa").val();
    const costo_currier = $("#currier_tasa").val();
    var monto_te = parseFloat($("#costo_total").val());
    const total = $("#total").val();
    const ganancia = $("#ganancia").val();
    const porc_ganancia = $("#porcGanancia").val();

    var pie_cubi = parseFloat($("#pieCubico").val());
    var lb_vole = parseFloat($("#LBVOL").val());
    var peso_v = parseFloat($("#box-weight").val());
    var total_costo_v = parseFloat($("#total").val());
    var gana_v = parseFloat($("#ganancia").val());
    var porc_gan_v = parseFloat($("#porcGanancia").val());

    var totalPie3 = parseFloat(piecubtotal + pie_cubi);
    var totalbvol = parseFloat(lbvolTotal + lb_vole);
    var totalpeso = parseFloat(pesototal + peso_v);
    var montoTotalT = parseFloat(montoTotal + monto_te);
    var costoTotal_t = parseFloat(costoTotal + total_costo_v);
    var tganancia = parseFloat(gananciaTotal + gana_v);
    var porcenGanancia = parseFloat(porcGananciaTotal + porc_gan_v);

    if (name == "") {
      name = "N/E";
    }

    if (weight == "" || weight == 0 || weight == 0.0 || weight == "0.00") {
      weight = 0;
    }

    if (
      (trip_id == 1 && weight == "") ||
      (trip_id == 1 && weight == 0) ||
      (trip_id == 1 && weight == 0.0)
    ) {
      $(".register-box-error").html("El campo peso es obligatorio");
      $(".register-box-error").fadeIn("fast");
      $(".page-wrapper").scrollTop(0);
      setTimeout(function () {
        $(".register-box-error").fadeOut("fast");
      }, 2000);
    } else if (width == "" || width == 0 || width == 0) {
      $(".register-box-error").html("Debe ingresar un ancho para la caja");
      $(".register-box-error").fadeIn("fast");
      $(".page-wrapper").scrollTop(0);
      setTimeout(function () {
        $(".register-box-error").fadeOut("fast");
      }, 2000);
    } else if (height == "" || height == 0 || height == 0) {
      $(".register-box-error").html("Debe ingresar una altura para la caja");
      $(".register-box-error").fadeIn("fast");
      $(".page-wrapper").scrollTop(0);
      setTimeout(function () {
        $(".register-box-error").fadeOut("fast");
      }, 2000);
    } else if (lenght == "" || lenght == 0 || lenght == 0) {
      $(".register-box-error").html("Debe ingresar un largo para la caja");
      $(".register-box-error").fadeIn("fast");
      $(".page-wrapper").scrollTop(0);
      setTimeout(function () {
        $(".register-box-error").fadeOut("fast");
      }, 2000);
    } else if (description == "" || description == 0) {
      $(".register-box-error").html(
        "Debe ingresar una descripcion para la caja"
      );
      $(".register-box-error").fadeIn("fast");
      $(".page-wrapper").scrollTop(0);
      setTimeout(function () {
        $(".register-box-error").fadeOut("fast");
      }, 2000);
    } else if (cost == "" || cost == 0 || cost == 0.0) {
      $(".register-box-error").html("Debe ingresar un costo para la caja");
      $(".register-box-error").fadeIn("fast");
      $(".page-wrapper").scrollTop(0);
      setTimeout(function () {
        $(".register-box-error").fadeOut("fast");
      }, 2000);
    } else {
      $("#addBoxButton").html("Agregado");
      const id = $("#box-id").val();
      $("#wboxes").fadeIn("fast");
      if (id == "" || id == "0" || id == 0) {
        console.log("entre al primer if");
        $.ajax({
          method: "POST",
          url: "ajax/addBoxes.php",
          data: {
            w: 1,
            id: id_boxes,
            name: name,
            width: width,
            height: height,
            lenght: lenght,
            weight: weight,
            description: description,
            cost: cost,
            category: category,
            wh_id: wh_id,
            trip_id: trip_id,
            destiner: destiner,
            currier_id: currier_id,
            cubic_feet: cubic_feet,
            lb_vol: lb_vol,
            insurance: insurance,
            logistic: logistic,
            extra_cost: material,
            tarifa_currier: tarifa_currier,
            tarifa_te: tarifa_te,
            costo_currier: costo_currier,
            monto_te: monto_te,
            total: total,
            ganancia: ganancia,
            porc_ganancia: porc_ganancia,
            tarifa_interna_base: tarifaInternBase,
            costo_interno_total: costo_interno_total,
          },
          success: function (res) {
            console.log(res);
            $("#boxes-table").html(res);
            new_id = parseInt(id_boxes) + 1;
            $("#box-code").val(new_id);
            $("#box-name").val("");
            $("#box-width").val("");
            $("#box-height").val("");
            $("#box-lenght").val("");
            $("#box-weight").val("");
            $("#box-description").val("");
            $("#box-cost").val("");

            $("#pieCubico").val("");
            $("#LBVOL").val("");
            $("#pesolb").val("");
            $("#pesokg").val("");
            $("#metrocubico").val("");
            $("#kgvol").val("");
            $("#total").val(0.0);
            $("#ganancia").val(0.0);
            $("#porcGanancia").val(0.0);
            $("#currier_tasa").val(0.0);
            $("#material_tasa").val(0.0);
            $("#costo_interno").val(0.0);
            $("#costo_total").val(0.0);
            $("#costo_extra").val(0.0);

            $("#pieCubTotales").val(totalPie3);
            $("#lbvolTotales").val(totalbvol);
            $("#pesoTotal").val(totalpeso);
            $("#monto_total_box").val(montoTotalT);
            $("#costo_total_box").val(costoTotal_t);
            $("#ganancia_total").val(tganancia);
            $("#total_porcentaje_ganancia").val(porcenGanancia);
            // $("#category").val('');
          },
        });
      } else {
        console.log("entre al segundo if");
        $.ajax({
          method: "POST",
          url: "ajax/addBoxes.php",
          data: {
            w: 1,
            id: id_boxes,
            name: name,
            width: width,
            height: height,
            lenght: lenght,
            weight: weight,
            description: description,
            cost: cost,
            category: category,
            wh_id: wh_id,
            trip_id: trip_id,
            destiner: destiner,
            currier_id: currier_id,
            cubic_feet: cubic_feet,
            lb_vol: lb_vol,
            insurance: insurance,
            logistic: logistic,
            extra_cost: material,
            tarifa_currier: tarifa_currier,
            tarifa_te: tarifa_te,
            costo_currier: costo_currier,
            monto_te: monto_te,
            total: total,
            ganancia: ganancia,
            porc_ganancia: porc_ganancia,
            tarifa_interna_base: tarifaInternBase,
            costo_interno_total: costo_interno_total,
          },
          success: function (res) {
            console.log(res);
            $("#boxes-table").html(res);
            new_id = parseInt(id_boxes) + 1;
            $("#box-code").val(new_id);
            $("#box-name").val("");
            $("#box-width").val("");
            $("#box-height").val("");
            $("#box-lenght").val("");
            $("#box-weight").val("");
            $("#box-description").val("");
            $("#box-cost").val("");

            $("#pieCubico").val("");
            $("#LBVOL").val("");
            $("#pesolb").val("");
            $("#pesokg").val("");
            $("#metrocubico").val("");
            $("#kgvol").val("");
            $("#total").val(0.0);
            $("#ganancia").val(0.0);
            $("#porcGanancia").val(0.0);
            $("#currier_tasa").val(0.0);
            $("#material_tasa").val(0.0);
            $("#costo_interno").val(0.0);
            $("#costo_total").val(0.0);
            $("#costo_extra").val(0.0);

            $("#pieCubTotales").val(totalPie3);
            $("#lbvolTotales").val(totalbvol);
            $("#pesoTotal").val(totalpeso);
            $("#monto_total_box").val(montoTotalT);
            $("#costo_total_box").val(costoTotal_t);
            $("#ganancia_total").val(tganancia);
            $("#total_porcentaje_ganancia").val(porcenGanancia);
          },
        });
      }
      setTimeout(function () {
        $("#addBoxButton").html('<i class="fas fa-check"></i>');
      }, 2000);
    }
  }

  // variable de calculos totales
}

function addBoxForModal() {
  var dataBoxes = [];
  const id_boxes = $("#box-code").val();
  let name = $("#box-name").val();
  const wh_id = $("#wh_id_update").val();

  let new_id = 0;

  const width = $("#box-width").val();
  const height = $("#box-height").val();
  const lenght = $("#box-lenght").val();
  var weight = $("#box-weight").val();
  const description = $("#box-description").val();
  const cost = $("#box-cost").val();
  const category = $("#category").val();

  var tarifa_currier = parseFloat($("#tarifa_base").val());
  var tarifa_te = parseFloat($("#tasa_base_te").val());
  var tarifaInternBase_val = $("#tarifaInterna_base").val();

  if (
    tarifaInternBase_val == "" ||
    tarifaInternBase_val == 0 ||
    tarifaInternBase_val == 0.0 ||
    tarifaInternBase_val == "0.00"
  ) {
    var tarifaInternBase = 0;
    var costo_interno_total = 0;
  } else {
    var tarifaInternBase = parseFloat($("#tarifaInterna_base").val());
    var costo_interno_total = $("#costo_interno").val();
  }

  // medidas del paquete

  const cubic_feet = $("#pieCubico").val();
  const lb_vol = $("#LBVOL").val();

  const material = $("#material_tasa").val();
  const costo_currier = $("#currier_tasa").val();
  var monto_te = parseFloat($("#costo_total").val());
  const total = $("#total").val();
  const ganancia = $("#ganancia").val();
  const porc_ganancia = $("#porcGanancia").val();

  if (name == "") {
    name = "N/E";
  }

  if (weight == "" || weight == 0 || weight == 0.0 || weight == "0.00") {
    weight = 0;
  }

  if (width == "" || width == 0 || width == 0) {
    $(".register-box-error").html("Debe ingresar un ancho para la caja");
    $(".register-box-error").fadeIn("fast");
    $(".page-wrapper").scrollTop(0);
    setTimeout(function () {
      $(".register-box-error").fadeOut("fast");
    }, 2000);
  } else if (height == "" || height == 0 || height == 0) {
    $(".register-box-error").html("Debe ingresar una altura para la caja");
    $(".register-box-error").fadeIn("fast");
    $(".page-wrapper").scrollTop(0);
    setTimeout(function () {
      $(".register-box-error").fadeOut("fast");
    }, 2000);
  } else if (lenght == "" || lenght == 0 || lenght == 0) {
    $(".register-box-error").html("Debe ingresar un largo para la caja");
    $(".register-box-error").fadeIn("fast");
    $(".page-wrapper").scrollTop(0);
    setTimeout(function () {
      $(".register-box-error").fadeOut("fast");
    }, 2000);
  } else if (description == "" || description == 0) {
    $(".register-box-error").html("Debe ingresar una descripcion para la caja");
    $(".register-box-error").fadeIn("fast");
    $(".page-wrapper").scrollTop(0);
    setTimeout(function () {
      $(".register-box-error").fadeOut("fast");
    }, 2000);
  } else if (cost == "" || cost == 0 || cost == 0.0) {
    $(".register-box-error").html("Debe ingresar un costo para la caja");
    $(".register-box-error").fadeIn("fast");
    $(".page-wrapper").scrollTop(0);
    setTimeout(function () {
      $(".register-box-error").fadeOut("fast");
    }, 2000);
  } else {
    $("#addBoxButton").html("Agregado");
    const id = $("#box-id").val();
    $("#wboxes").fadeIn("fast");
    if (id == "" || id == "0" || id == 0) {
      console.log("entre al primer if");
      $.ajax({
        method: "POST",
        url: "ajax/addBoxesForModal.php",
        data: {
          w: 1,
          id: id_boxes,
          name: name,
          width: width,
          height: height,
          lenght: lenght,
          weight: weight,
          description: description,
          cost: cost,
          category: category,
          wh_id: wh_id,
          cubic_feet: cubic_feet,
          lb_vol: lb_vol,

          extra_cost: material,
          tarifa_currier: tarifa_currier,
          tarifa_te: tarifa_te,
          costo_currier: costo_currier,
          monto_te: monto_te,
          total: total,
          ganancia: ganancia,
          porc_ganancia: porc_ganancia,
          tarifa_interna_base: tarifaInternBase,
          costo_interno_total: costo_interno_total,
        },
        success: function (res) {
          console.log(res);

          $(".register-success").html("Box registrado correctamente.");
          $(".register-success").fadeIn("fast");
          $(".modal").scrollTop(0);
          setTimeout(function () {
            location.reload();
          }, 2000);
        },
      });
    } else {
      console.log("entre al segundo if");

      $.ajax({
        method: "POST",
        url: "ajax/addBoxesForModal.php",
        data: {
          w: 1,
          id: id_boxes,
          name: name,
          width: width,
          height: height,
          lenght: lenght,
          weight: weight,
          description: description,
          cost: cost,
          category: category,
          wh_id: wh_id,
          cubic_feet: cubic_feet,
          lb_vol: lb_vol,

          extra_cost: material,
          tarifa_currier: tarifa_currier,
          tarifa_te: tarifa_te,
          costo_currier: costo_currier,
          monto_te: monto_te,
          total: total,
          ganancia: ganancia,
          porc_ganancia: porc_ganancia,
          tarifa_interna_base: tarifaInternBase,
          costo_interno_total: costo_interno_total,
        },
        success: function (res) {
          console.log(res);

          $(".register-success").html("Box registrado correctamente.");
          $(".register-success").fadeIn("fast");
          $(".modal").scrollTop(0);
          setTimeout(function () {
            location.reload();
          }, 2000);
        },
      });
    }
  }

  // variable de calculos totales
}

function editBoxForTable(box_id, wh_id) {
  console.log(box_id);
  $.ajax({
    method: "POST",
    url: "ajax/findBoxForTable.php",
    data: { id: box_id },
    success: function (res) {
      console.log(res);
      const dataUser = res.split("-");

      const id = dataUser[0];
      const width = dataUser[1];
      const height = dataUser[2];
      const lenght = dataUser[3];
      const weigth = dataUser[4];
      const cost = dataUser[5];
      const description = dataUser[6];
      const category_id = dataUser[7];
      const kg_weight = dataUser[8];
      const cubic_feet = dataUser[9];
      const cubic_m = dataUser[10];
      const lb_vol = dataUser[11];
      const m_vol = dataUser[12];
      const cost_taf_type_trip = dataUser[13];
      const tasa_type_trip = dataUser[14];
      const cost_type_trip = dataUser[15];
      const monto_calculo_type_trip = dataUser[16];
      const cost_total = dataUser[17];
      const ganancia_cost_type_trip = dataUser[18];
      const porcentaje_ganancia_cost_type_trip = dataUser[19];

      $("#box-width").val(width);
      $("#box-id").val(id);
      $("#wh-id").val(wh_id);
      $("#box-height").val(height);
      $("#box-lenght").val(lenght);
      $("#box-weight").val(weigth);
      $("#box-description").val(description);
      $("#box-cost").val(cost);
      $("#category").val(category_id);

      $("#pieCubico").val(cubic_feet);
      $("#LBVOL").val(lb_vol);
      $("#pesolb").val(weigth);
      $("#pesokg").val(kg_weight);
      $("#metrocubico").val(cubic_m);
      $("#total").val(cost_total);
      $("#ganancia").val(ganancia_cost_type_trip);
      $("#porcGanancia").val(porcentaje_ganancia_cost_type_trip);
      $("#tarifa_base").val(cost_taf_type_trip);
      $("#tasa_base_te").val(tasa_type_trip);
      $("#currier_tasa").val(cost_type_trip);
      $("#costo_total").val(monto_calculo_type_trip);

      $("#register-box-for-table").css("display", "none");
      $("#edit-box-for-table").css("display", "block");
    },
  });
}

function editBox() {
  const width = $("#box-width").val();
  const box_id = $("#box-id").val();
  const height = $("#box-height").val();
  const lenght = $("#box-lenght").val();
  const weight = $("#box-weight").val();
  const description = $("#box-description").val();
  const cost = $("#box-cost").val();
  const category = $("#category").val();

  const pieCubico = $("#pieCubico").val();
  const lbVol = $("#LBVOL").val();
  const pesokg = $("#pesokg").val();
  const metrocubico = $("#metrocubico").val();
  const total = $("#total").val();
  const ganancia = $("#ganancia").val();
  const porcganancia = $("#porcGanancia").val();
  const tarifacurrier = $("#tarifa_base").val();
  const tarifa_te = $("#tasa_base_te").val();
  const costo_currier = $("#currier_tasa").val();
  const monto_te = $("#costo_total").val();
  const wh_id = $("#warehouse_id").val();

  const tarifa_interna = $("#tarifaInterna_base").val();
  const costo_interno = $("#costo_interno").val();
  const gastoExtra = $("#material_tasa").val();
  const name = $("#box-name").val();

  $.ajax({
    method: "POST",
    url: "ajax/editBoxForTable.php",
    data: {
      id: box_id,
      width: width,
      height: height,
      lenght: lenght,
      weight: weight,
      description: description,
      cost: cost,
      category: category,
      pieCubico: pieCubico,
      lbVol: lbVol,
      pesokg: pesokg,
      metrocubico: metrocubico,
      total: total,
      ganancia: ganancia,
      porcganancia: porcganancia,
      tarifa_te: tarifa_te,
      tarifacurrier: tarifacurrier,
      costo_currier: costo_currier,
      monto_te: monto_te,
      wh_id: wh_id,
      tarifa_interna: tarifa_interna,
      costo_interno: costo_interno,
      gastoExtra: gastoExtra,
      name: name,
    },

    success: function (res) {
      console.log(res);
      $("#register-box-for-table").css("display", "block");
      $("#edit-box-for-table").css("display", "none");
      $("#boxes-table").html(res);

      new_id = parseInt(box_id) + 1;
      $("#box-code").val(new_id);
      $("#box-name").val("");
      $("#box-width").val("");
      $("#box-height").val("");
      $("#box-lenght").val("");
      $("#box-weight").val("");
      $("#box-description").val("");
      $("#box-cost").val("");

      $("#pieCubico").val("");
      $("#LBVOL").val("");
      $("#pesolb").val("");
      $("#pesokg").val("");
      $("#metrocubico").val("");
      $("#kgvol").val("");
      $("#total").val(0.0);
      $("#ganancia").val(0.0);
      $("#porcGanancia").val(0.0);
      $("#currier_tasa").val(0.0);
      $("#material_tasa").val(0.0);
      $("#costo_extra").val(0.0);

      var piecubtotal = parseFloat($("#pieCubTotales").val());
      var lbvolTotal = parseFloat($("#lbvolTotales").val());
      var pesototal = parseFloat($("#pesoTotal").val());
      var costoTotal = parseFloat($("#costo_total_box").val());
      var gananciaTotal = parseFloat($("#ganancia_total").val());
      var porcGananciaTotal = parseFloat($("#total_porcentaje_ganancia").val());

      var pie_cubi = parseFloat($("#pieCubico").val());
      var lb_vole = parseFloat($("#LBVOL").val());
      var peso_v = parseFloat($("#box-weight").val());
      var total_costo_v = parseFloat($("#total").val());
      var gana_v = parseFloat($("#ganancia").val());
      var porc_gan_v = parseFloat($("#porcGanancia").val());

      var totalPie3 = parseFloat(piecubtotal + pie_cubi);
      var totalbvol = parseFloat(lbvolTotal + lb_vole);
      var totalpeso = parseFloat(pesototal + peso_v);
      var costoTotal_t = parseFloat(costoTotal + total_costo_v);
      var tganancia = parseFloat(gananciaTotal + gana_v);
      var porcenGanancia = parseFloat(porcGananciaTotal + porc_gan_v);

      $("#pieCubTotales").val(totalPie3);
      $("#lbvolTotales").val(totalbvol);
      $("#pesoTotal").val(totalpeso);
      $("#costo_total_box").val(costoTotal_t);
      $("#ganancia_total").val(tganancia);
      $("#total_porcentaje_ganancia").val(porcenGanancia);
    },
  });
}

function editBoxModal() {
  const width = $("#box-width").val();
  const box_id = $("#box-id").val();
  const height = $("#box-height").val();
  const lenght = $("#box-lenght").val();
  const weight = $("#box-weight").val();
  const description = $("#box-description").val();
  const cost = $("#box-cost").val();
  const category = $("#category").val();

  const pieCubico = $("#pieCubico").val();
  const lbVol = $("#LBVOL").val();
  const pesokg = $("#pesokg").val();
  const metrocubico = $("#metrocubico").val();
  const total = $("#total").val();
  const ganancia = $("#ganancia").val();
  const porcganancia = $("#porcGanancia").val();
  const tarifacurrier = $("#tarifa_base").val();
  const tarifa_te = $("#tasa_base_te").val();
  const costo_currier = $("#currier_tasa").val();
  const monto_te = $("#costo_total").val();
  const wh_id = $("#warehouse_id").val();

  const tarifa_interna = $("#tarifaInterna_base").val();
  const costo_interno = $("#costo_interno").val();
  const gastoExtra = $("#material_tasa").val();
  const name = $("#box-name").val();

  $.ajax({
    method: "POST",
    url: "ajax/editBoxForTable.php",
    data: {
      id: box_id,
      width: width,
      height: height,
      lenght: lenght,
      weight: weight,
      description: description,
      cost: cost,
      category: category,
      pieCubico: pieCubico,
      lbVol: lbVol,
      pesokg: pesokg,
      metrocubico: metrocubico,
      total: total,
      ganancia: ganancia,
      porcganancia: porcganancia,
      tarifa_te: tarifa_te,
      tarifacurrier: tarifacurrier,
      costo_currier: costo_currier,
      monto_te: monto_te,
      wh_id: wh_id,
      tarifa_interna: tarifa_interna,
      costo_interno: costo_interno,
      gastoExtra: gastoExtra,
      name: name,
    },

    success: function (res) {
      console.log(res);
      $("#boxes-table").html(res);

      new_id = parseInt(box_id) + 1;
      $("#box-code").val(new_id);
      $("#box-name").val("");
      $("#box-width").val("");
      $("#box-height").val("");
      $("#box-lenght").val("");
      $("#box-weight").val("");
      $("#box-description").val("");
      $("#box-cost").val("");

      $("#pieCubico").val("");
      $("#LBVOL").val("");
      $("#pesolb").val("");
      $("#pesokg").val("");
      $("#metrocubico").val("");
      $("#kgvol").val("");
      $("#total").val(0.0);
      $("#ganancia").val(0.0);
      $("#porcGanancia").val(0.0);
      $("#currier_tasa").val(0.0);
      $("#material_tasa").val(0.0);
      $("#costo_extra").val(0.0);

      var piecubtotal = parseFloat($("#pieCubTotales").val());
      var lbvolTotal = parseFloat($("#lbvolTotales").val());
      var pesototal = parseFloat($("#pesoTotal").val());
      var costoTotal = parseFloat($("#costo_total_box").val());
      var gananciaTotal = parseFloat($("#ganancia_total").val());
      var porcGananciaTotal = parseFloat($("#total_porcentaje_ganancia").val());

      var pie_cubi = parseFloat($("#pieCubico").val());
      var lb_vole = parseFloat($("#LBVOL").val());
      var peso_v = parseFloat($("#box-weight").val());
      var total_costo_v = parseFloat($("#total").val());
      var gana_v = parseFloat($("#ganancia").val());
      var porc_gan_v = parseFloat($("#porcGanancia").val());

      var totalPie3 = parseFloat(piecubtotal + pie_cubi);
      var totalbvol = parseFloat(lbvolTotal + lb_vole);
      var totalpeso = parseFloat(pesototal + peso_v);
      var costoTotal_t = parseFloat(costoTotal + total_costo_v);
      var tganancia = parseFloat(gananciaTotal + gana_v);
      var porcenGanancia = parseFloat(porcGananciaTotal + porc_gan_v);

      $("#pieCubTotales").val(totalPie3);
      $("#lbvolTotales").val(totalbvol);
      $("#pesoTotal").val(totalpeso);
      $("#costo_total_box").val(costoTotal_t);
      $("#ganancia_total").val(tganancia);
      $("#total_porcentaje_ganancia").val(porcenGanancia);

      $("#register-box-for-table").css("display", "block");
      $("#edit-box-for-table").css("display", "none");
      $("#editBoxModal").modal("hide");
    },
  });
}

function editBoxForMod() {
  const width = $("#box-width").val();
  const box_id = $("#box-id").val();
  const height = $("#box-height").val();
  const lenght = $("#box-lenght").val();
  const weight = $("#box-weight").val();
  const description = $("#box-description").val();
  const cost = $("#box-cost").val();
  const category = $("#category").val();

  const pieCubico = $("#pieCubico").val();
  const lbVol = $("#LBVOL").val();
  const pesokg = $("#pesokg").val();
  const metrocubico = $("#metrocubico").val();
  const total = $("#total").val();
  const ganancia = $("#ganancia").val();
  const porcganancia = $("#porcGanancia").val();
  const tarifacurrier = $("#tarifa_base").val();
  const tarifa_te = $("#tasa_base_te").val();
  const costo_currier = $("#currier_tasa").val();
  const monto_te = $("#costo_total").val();
  const wh_id = $("#warehouse_id").val();

  const tarifa_interna = $("#tarifaInterna_base").val();
  const costo_interno = $("#costo_interno").val();
  const gastoExtra = $("#material_tasa").val();
  const name = $("#box-name").val();

  $.ajax({
    method: "POST",
    url: "ajax/editBoxForMod.php",
    data: {
      id: box_id,
      width: width,
      height: height,
      lenght: lenght,
      weight: weight,
      description: description,
      cost: cost,
      category: category,
      pieCubico: pieCubico,
      lbVol: lbVol,
      pesokg: pesokg,
      metrocubico: metrocubico,
      total: total,
      ganancia: ganancia,
      porcganancia: porcganancia,
      tarifa_te: tarifa_te,
      tarifacurrier: tarifacurrier,
      costo_currier: costo_currier,
      monto_te: monto_te,
      wh_id: wh_id,
      tarifa_interna: tarifa_interna,
      costo_interno: costo_interno,
      gastoExtra: gastoExtra,
      name: name,
    },

    success: function (res) {
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
  });
}

function changeStatusShip(wh_id) {
  $("#shipStatusModal").modal("show");
  $("#wh_id_to_change").val(wh_id);
}

function cambiarShipStatus() {
  const wh_id = $("#wh_id_to_change").val();
  const ship_status = $("#status_ship_id").val();
  $.ajax({
    method: "POST",
    url: "ajax/updateShipStatuWh.php",
    data: { id: wh_id, shipStatus: ship_status },
    success: function (res) {
      setTimeout(function () {
        location.reload();
      }, 1000);
    },
  });
}
function CRTrip(id) {
  let trip = $("#cr-trip-" + id).val();
  $.ajax({
    method: "POST",
    url: "ajax/updateCRTrip.php",
    data: { id: id, trip: trip },
    success: function (res) {
      console.log(res);
    },
  });
}

function deleteCR(id) {
  var id = id;
  $.ajax({
    method: "POST",
    url: "ajax/deleteCR.php",
    data: { id: id },
    success: function (res) {
      console.log(res);
      location.reload();
    },
  });
}

function updateCRCost(id) {
  let cost = $("#cr-cost-" + id).val();
  $.ajax({
    method: "POST",
    url: "ajax/updateCRCost.php",
    data: { id: id, cost: cost },
    success: function (res) {
      console.log(res);
    },
  });
}

function updateCRCostA(id) {
  let cost = $("#cr-costa-" + id).val();
  $.ajax({
    method: "POST",
    url: "ajax/updateCRCosta.php",
    data: { id: id, cost: cost },
    success: function (res) {
      console.log(res);
    },
  });
}

function updateCRTasa(id) {
  let tasa = $("#cr-tasa-" + id).val();
  $.ajax({
    method: "POST",
    url: "ajax/updateCRTasa.php",
    data: { id: id, tasa: tasa },
    success: function (res) {
      console.log(res);
    },
  });
}

function editWarehouseState(id) {
  let name = $("#name-s-" + id).val();
  var id = id;
  $.ajax({
    method: "POST",
    url: "ajax/updateState.php",
    data: { name: name, id: id },
    success: function (res) {
      console.log(res);
    },
  });
}
function deleteWState(id) {
  var id = id;
  $.ajax({
    method: "POST",
    url: "ajax/deleteState.php",
    data: { id: id },
    success: function (res) {
      location.reload();
    },
  });
}
function addWStates() {
  const name = $("#name-state").val();
  if (name == "") {
    $(".register-state-error").html("Debe ingresar un nombre");
    $(".register-state-error").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-state-error").fadeOut("fast");
    }, 3000);
  } else {
    $.ajax({
      method: "POST",
      url: "ajax/addState.php",
      data: { name: name },
      success: function (res) {
        console.log(res);
        $(".register-success").html("Estado creado");
        $(".register-success").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          $(".register-state-success").fadeOut("fast");
          location.reload();
        }, 2000);
      },
    });
  }
}

function deleteBox(id, id_w) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteBox.php",
    data: { id: id, id_w: id_w },
    success: function (res) {
      console.log(res);
      $("#boxes-table").html(res);
    },
  });
}

function deleteNewInternCurrier(id) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteNewInternCurrier.php",
    data: { id: id },
    success: function (res) {
      console.log(res);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}
function deleteNewCurrier(id) {
  $.ajax({
    method: "POST",
    url: "ajax/deleteNewCurrier.php",
    data: { id: id },
    success: function (res) {
      console.log(res);
      setTimeout(function () {
        location.reload();
      }, 2000);
    },
  });
}

function deleteBoxDetails(id) {
  id_w = $("#wh_id_update").val();
  $.ajax({
    method: "POST",
    url: "ajax/deleteBox.php",
    data: { id: id, id_w: id_w },
    success: function (res) {
      console.log(res);
      $("#boxes-table").html(res);
    },
  });
}

function del(
  id,
  name,
  width,
  height,
  lenght,
  weight,
  description,
  cost,
  label,
  category
) {
  var arr = [];

  var boxes = JSON.parse(localStorage.getItem("boxes")).sort();
  var cual = 0;

  var contador = 0;

  Object.keys(boxes).forEach((key) => {
    arr.push({
      id: boxes[key].id,
      width: boxes[key].width,
      height: boxes[key].height,
      lenght: boxes[key].lenght,
      weight: boxes[key].weight,
      description: boxes[key].description,
      cost: boxes[key].cost,
      label: boxes[key].label,
      category: boxes[key].category,
    });
  });

  Object.keys(boxes).forEach((key) => {
    contador++;
    if (boxes[key].id == id) {
      cual = parseFloat(key) + 1;
    }
  });

  var removeItem = (boxes, i) =>
    boxes.slice(0, i - 1).concat(boxes.slice(i, boxes.length));

  var filteredItems = removeItem(boxes, cual);
  localStorage.setItem("boxes", JSON.stringify(filteredItems));
  $("#" + id).remove();
  // count();
}

function addWarehouseId(id) {
  $("#warehouseId").val(id);
}

function addPackageToWarehouse() {
  const w = $("#warehouseId").val();
  const p = $("#packageM").val();
  if (p != "0" && p != "" && p != 0) {
    $.ajax({
      method: "POST",
      url: "ajax/addWarehousePackage.php",
      data: { w: w, p: p },
      success: function (res) {
        console.log(res);
        if (res == 1) {
          $(".register-successM").html("Paquete agregado al Warehouse!");
          $(".register-successM").fadeIn("fast");
          $(".modal").scrollTop(0);
          setTimeout(function () {
            $(".register-successM").fadeOut("fast");
            $("#addPackageModal").modal("hide");
          }, 3000);
        }
      },
    });
  } else {
    $(".register-errorM").html("Debe ingresar un paquete");
    $(".register-errorM").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-errorM").fadeOut("fast");
    }, 3000);
  }
}
function addDeleteText(id) {
  $("#wDelete").val(id);
  $("#deleteH3").html("Esta seguro que desea eliminar el warehouse #" + id);
}

function addDeleteTextP(id) {
  $("#pDelete").val(id);
  $("#deleteH3").html("Esta seguro que desea eliminar el packing #" + id);
}
function searchPacking() {
  var id = $("#idPacking").val();
  var date_in = $("#date_in_p").val();
  var date_out = $("#date_out_p").val();

  $.ajax({
    method: "POST",
    url: "ajax/searchPacking.php",
    data: { id: id, date_in: date_in, date_out: date_out },
    success: function (res) {
      console.log(res);
      $(".packing-table").html(res);
    },
  });
}

function deletePState(id) {
  var id = id;
  $.ajax({
    method: "POST",
    url: "ajax/deletePState.php",
    data: { id: id },
    success: function (res) {
      console.log(res);
      $(".state-success").html("Estado eliminado!");
      $(".state-success").fadeIn("fast");
      $(document).scrollTop(0);
      setTimeout(function () {
        location.reload();
      }, 3000);
    },
  });
}
function deletePackage(id) {
  // var id = $('#pDelete').val();

  $.ajax({
    method: "POST",
    url: "ajax/deletePackage.php",
    data: { id: id },
    success: function (res) {
      console.log(res);
      $(".register-successD").html("Packing eliminado!");
      $(".register-successD").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".register-successD").fadeOut("fast");
        location.reload();
      }, 1000);
    },
  });
}

function deletePacking() {
  var id = $("#pDelete").val();

  $.ajax({
    method: "POST",
    url: "ajax/deletePacking.php",
    data: { id: id },
    success: function (res) {
      console.log(res);
      $(".register-successD").html("Packing eliminado!");
      $(".register-successD").fadeIn("fast");
      $(".modal").scrollTop(0);
      setTimeout(function () {
        $(".register-successD").fadeOut("fast");
        location.reload();
      }, 3000);
    },
  });
}
function deleteWarehouse(id) {
  const w = $("#wDelete").val();
  if (w != "0" && w != "" && w != 0) {
    $.ajax({
      method: "POST",
      url: "ajax/deleteWarehouse.php",
      data: { w: w },
      success: function (res) {
        console.log(res);
        $(".register-successD").html("Warehouse eliminado!");
        $(".register-successD").fadeIn("fast");
        $(".modal").scrollTop(0);
        setTimeout(function () {
          $(".register-successD").fadeOut("fast");
          location.reload();
        }, 1000);
      },
    });
  } else {
    $(".register-errorD").html("Debe ingresar un warehouse");
    $(".register-errorD").fadeIn("fast");
    $(".modal").scrollTop(0);
    setTimeout(function () {
      $(".register-errorD").fadeOut("fast");
    }, 3000);
  }
}
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function AgregarDest() {
  $("#registrarDestinatario").css("display", "block");
  $("#boton-destinatario").css("display", "none");
  $("#registrarUsuario").css("display", "none");
  $("#warehouseModal").modal("hide");
}

function RegUsuario() {
  $("#muchosDestinatarios").css("display", "none");
  $("#cargarDestinatarios").css("display", "block");
  $("#cerrarModal").css("display", "block");
  $("#card-registro-usuarios-wh").css("display", "none");
}

function cerrarModal() {
  $("#newUserModal").modal("hide");
  $("#warehouseModal").modal("show");
}

function AgregarDestinatario() {
  $("#newDestinerModal").modal("hide");
  $("#warehouseModal").modal("show");
}
