<?php

	error_reporting(0);
	ini_set('display_errors', 0);

	require __DIR__.'/vendor/autoload.php';

	include 'includes/connection.class.php';
	include 'includes/warehouses.class.php';
	include 'includes/packages.class.php';
	include 'includes/users.class.php';

	use Spipu\Html2Pdf\Html2Pdf;

	$id = $_REQUEST['id'];
	$warehouses = new Warehouses;
	$users = new Users;
	$warehouseData = $warehouses->findById($id);
	$userData = $users->findById($warehouseData['id_from']);
	$userDataTo = $users->findById($warehouseData['id_to']);
	
	try {
		// header("Content-Disposition: attachment; filename=sample.pdf");
		$html2pdf = new Html2Pdf('P','A3', 'fr', false, 'ISO-8859-15');
	    $html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML('<div style="border:1px solid black; text-align:center;"><img style="width:50%; margin-left:25px;" src="assets/img/logo.png"></div>
			<div style="height:200px;border:1px solid black;">
				<h2>Fecha: '.$warehouseData['date_in'].'</h2>
			</div>
			<div style="height:200px;border:1px solid black;;">
				<h2>NRO # '.$warehouseData['id'].'</h2>
			</div>
			<div style="height:200px;border:1px solid black;;">
				<h2>Origen: '.$userData['name'].' '.$userData['lastname'].'</h2>
			</div>
			<div style="height:200px;border:1px solid black;;">
				<h2>Destino: '.$userDataTo['name'].' '.$userDataTo['lastname'].'</h2>
			</div>
			<div style="height:200px;border:1px solid black;;">
				<h2></h2>
			</div>
			<div style="height:200px;border:1px solid black;;">
				
			</div>
			<div style="height:200px;border:1px solid black;;">
				
			</div>
			');
		$html2pdf->output();
	} catch (PDOException $e) {
		echo $e;
	}


?>
