<?php  

include 'includes/connection.class.php';
include 'includes/invoices.class.php';
include 'includes/packages.class.php';
include 'includes/users.class.php';
include 'includes/warehouses.class.php';



$departamento = $_REQUEST['depart'];
$courier = $_REQUEST['currier'];
$fecha_de_salida = $_REQUEST['send_date'];
$region = $_REQUEST['destiny'];
$tipo_envio = $_REQUEST['trip_id'];

$warehouses = new Warehouses;

    $invoices = new Invoices;
    $allInvoices = $invoices->findWarehouseForPacking($departamento,$tipo_envio,$courier,$region,$fecha_de_salida);


session_start();
if ($_SESSION['state'] != 1) {
    header('Location: login.php');
}
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

    include 'head.php';

?>
</head>

<body>
    <!-- Modal -->

    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
        <?php 
        include 'navbar.php'; 
    ?>

        <div class="page-wrapper" style="background:#e3e3e3ff;">





            <div class="container-fluid">
                <h5 style="color:#005574; font-weight:600;">WAREHOUSES LISTOS</h5>



                <div class="card">
                    <div class="card-body">









                        <br>

                        <div class="row">
                            <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                            <div class_="col-lg-10 col-xlg-10 col-md-10">
                                <table class="table table-warehouse"
                                    style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                    <thead>
                                        <th style="border: 2px solid #e3e3e3ff;">SELECCIONAR</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                            WAREHOUSE</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                            CASILLERO</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                            DESTINATARIO</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                            DESTINO</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                            CANTIDAD DE CAJAS</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                            COSTO CAJAS</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                            PIE CÚBICO</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                            LB VOL</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                            ANCHO</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                            ALTO</th>
                                        





                                    </thead>
                                    <tbody>
                                        <?php  

                                            foreach ($allInvoices as $key => $value) {
                                        ?>
                                        <tr>

                                            <td
                                                style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <input onclick="countChecksInvoices();" type="checkbox"
                                                    class="form-check-input wcheck wcheck<?php echo $value['id'];?>"
                                                    id="wcheck<?php echo $value['id'];?>"
                                                    warehouse="<?php echo $value['id'];?>"></td>
                                            <td
                                                style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php echo $value['code']; ?></td>
                                            <td
                                                style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                TE-<?php echo $value['id_from']; ?></td>
                                            <td
                                                style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php echo $value['destiner_name']; ?></td>
                                            <td
                                                style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php echo $warehouses->findcityNameForwh($region)[0]['name']; ?>
                                            </td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $warehouses->findTripById($tipo_envio)['name']; ?></td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"
                                                class="subtotal-box">
                                                <?php echo $warehouses->calculateCostBox($value['id'])[0]['SUMTOTAL']; ?>
                                            </td>
                                            <td
                                                style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php echo $warehouses->calculatepie3($value['id'])[0]['TOTALPIE']; ?>
                                            </td>
                                            <td
                                                style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php echo $warehouses->calculatelbvol($value['id'])[0]['TOTALLBVOL']; ?>
                                            </td>
                                            <td
                                                style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php echo $warehouses->calculateancho($value['id'])[0]['TOTALANCHO']; ?>
                                            </td>
                                            <td
                                                style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php echo $warehouses->calculatealto($value['id'])[0]['TOTALALTO']; ?>
                                            </td>
                                            










                                        </tr>
                                        <?php
                                            }

                                        ?>
                                    </tbody>
                                </table>
                                <div class="row">


                                    <div class="col-sm-12 col-md-12 col-lg-12"
                                        style="text-align:right; content-align:right;">
                                        <div class="btn-group" align="right">

                                            <a onclick="bulkinvoices();" class="btn btn-primary"
                                                style="background-color:white; color:#ff554dff; border:2px solid #ff554dff;"><i
                                                    style="color:#ff554dff;" class="fas fa-check"></i> CREAR PACKING
                                                LIST</a>
                                        </div>

                                        <!-- <a align="right" href="registerWarehouse.php" class="btn" style="background-color:#ff554dff; color:white;"><i style="color:white;" class="fas fa-plus"></i> Crear</a> -->

                                    </div>
                                </div>






                            </div>
                            <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                        </div>
                    </div>
                </div>

                <footer class="footer text-center"> 2021 © Total Envios
                </footer>
            </div>

        </div>
        <?php 

    include 'foot.php';

?>
</body>

</html>