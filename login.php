<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

        include 'head.php';

    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    
</head>

<body class="m-0 vh-100 row justify-content-center align-items-center" style="background:#005574 !important;">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
        <!-- <div class="row" style="width:100%; height:auto; background:black;">
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4"></div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4" style="width:100%; height:100%; background:red;"> -->
                
                            <!-- <div class="row">
                                <div class="col-lg-12 col-xlg-12 col-md-12"> -->
                                <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5 text-center">
                                  
                                    <div  class="card" style="border-radius: 20px;">
                                        <div class="card-body">
                                        <div class="encabezado"><img id="imag" src="./logo-home-te.png"/></div>
                            
                                        <div class="alert alert-danger register-error" style="background:#ff554dff !important; color:white !important; font-weight:600 !important;" role="alert">
                                        </div>
                                        <div class="alert alert-success register-success" style="background:#005574 !important; color:white !important; font-weight:600 !important;" role="alert">
                                        </div>
                                            
                                            <div class="form-group login">
                                                <input name="email" id="email" type="email" 
                                                style="background: url('icons/email-blue.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;" 
                                                class="campos-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="correo electrónico">
                                                <!-- <i class="fas fa-lock fa-2x fa-lg" style="margin-top:10px;"> </i> -->
                                            </div>
                                            <div class="form-group login">
                                                <input id="password" type="text" 
                                                style="background: url('icons/password.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;"
                                                class="campos-input" id="exampleInputPassword1" placeholder="Contraseña">
                                            </div>
                                            <button onclick="login();" class="boton-total-envios"><strong>ENTRAR</strong></button><br>
                                            <div align="center"><a href="index.php" id="footer" align="center" style="color:#005574; font-size:12px;">¿Aun no tienes cuenta? <br> <label style="font-weight:900;">REGÍSTRATE AQUÍ</label></a></div><br><br>

                                            <!-- <div align="center">
                                            2021 © Total Envios <a href="https://www.wrappixel.com/"></a>
                                            </div> -->
                                                 
                                        </div>
                                    </div>
                                </div>
                       
                        
                    <!-- </div>
                </div>     -->
            <!-- </div> -->
            <!-- </div>
            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4"></div>
        </div> -->
         
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="js/app-style-switcher.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.js"></script>
    <script src="js/main.js"></script>
</body>


</html>