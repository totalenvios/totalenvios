<?php  

    error_reporting(0);
    ini_set('display_errors', 0);
    session_start();

    include_once 'includes/connection.class.php';
    include_once 'includes/users.class.php';

    $users = new Users;
    $email = '';

    if (isset($_SESSION['email'])) {
        $email = $_SESSION['email'];
    }else{
        $email = '';
    }

    $confirmed = '';
    if ($email != '') {
        $confirmed = $users->findConfirmed($_SESSION['email'])['confirmed'];
    }else{
        $confirmed = 'empty';
    }

?>
<header class="topbar" data-navbarbg="skin5">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<nav class="navbar navbar-light navbar-expand" style="background-color: white; box-shadow: 0px 1px 5px 3px rgba(204,204,204,0.92);
-webkit-box-shadow: 0px 1px 5px 3px rgba(204,204,204,0.92);
-moz-box-shadow: 0px 1px 5px 3px rgba(204,204,204,0.92); color:#005574">
  <div class="imagen-logo"><a class="navbar-brand" href="#">
                    <!-- Dark Logo icon -->
                    <img class="logo" src="logo_final.png" alt="homepage" />
                </a></div>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      
      <?php  

        if ($_SESSION['user_type'] == 3) {
    ?>
          <li class="nav-item">
                <a class="nav-link" href="alerts.php"><strong>PREALERTAS</strong></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="packages.php" ><strong>PAQUETES</strong></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="warehouses.php"><strong>WAREHOUSE</strong></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="invoices.php" ><strong>FACTURACIÓN</strong></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="packing.php" ><strong>CONSOLIDADO</strong></a>
          </li>
          
          <li class="nav-item">
            <a class="nav-link" href="users.php" ><strong>CLIENTES</strong></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="settings.php" ><strong>CONTROL PANEL</strong></a>
          </li>
        
    <?php
        }else if($_SESSION['user_type'] == 2){
    ?>
          <!-- <li class="nav-item">
            <a class="nav-link" href="dashboard.php">DashBoard</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="users.php">Usuarios</a>
          </li> -->
         
          <!-- <div class="dropdown"> -->
    <?php
        }

      ?>
      
      
    </ul>
    
    <form class="form-inline my-2 my-lg-0">
    <?php  
                if ($confirmed == 0 && $confirmed != 'empty') {
            ?>
                    <div class="alert alert-danger confirm-alert" role="alert">
                        No has confirmado tu cuenta, revisa tu bandeja de entrada y spam.
                    </div>
            <?php
                }

            ?>
    
    <!-- <a class="profile-pic" style="text-decoration:none;">
                        <?php 
                            if (isset($_SESSION['name'])) {
                                echo $_SESSION['name']; 
                            }
                        ?> -->
    <div class="d-md-flex">
                                <ol class="breadcrumb ms-auto">
                                    <li><a href="logout.php" class="fw-normal">Logout</a></li>
                                </ol>
                            </div>
    </form>
  </div>
</nav>
</header>