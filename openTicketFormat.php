<?php
    require './vendor/autoload.php';

	include 'includes/connection.class.php';
	include 'includes/warehouses.class.php';
	include 'includes/packages.class.php';
	include 'includes/users.class.php';

    use Spipu\Html2Pdf\Html2Pdf;

    $id = $_REQUEST['id'];
    $warehouses = new Warehouses;
    $users = new Users;
    $warehouseData = $warehouses->findById($id);
    $userData = $users->findById($warehouseData['id_from']);
    $userDataTo = $users->findAddresseeById($warehouseData['id_to'])[0];
    // print_r($userDataTo);
    $tipo = $warehouses->findTripById($warehouseData['trip_id'])['name'];
    $totalBox = $warehouses->countBoxwarehouses($id);
    $total = $totalBox['total'];
    $boxes = $warehouses->findAllBoxesW($id);

// Variable para el conteo total de cajas para ese warehouse
$n=1;

try {
    
    $html2pdf = new Html2Pdf();
    for ($i = 1; $i <= $n; $i++) {
        $html2pdf->writeHTML('
            <div style="border-width: 1px; border-style: solid; border-color: black; width:576px; height:300px;">
            <table class="default">
            <tr>
                <td><div align="center" style="width:302px; height:94px; border-width: 1px; border-style: solid; border-color: black;"><img src="./logo_final.png" style="margin-top:12px;"/></div></td>
                <td><div style="width:151px; height:94px; border-width: 1px; border-style: solid; border-color: black; font-size:23px;">FECHA:<br>'.$warehouseData["date_send"].'</div></td>
                <td><div align="center" style="width:151px; height:94px; border-width: 1px; border-style: solid; border-color: black; background-color:black; color:white; font-size:60px; font-weight:bold;">12</div></td>
            </tr>
            </table>
            <table class="default">
            <tr>
                <td><div style="width:228px; height:75px; border-width: 1px; border-style: solid; border-color: black; font-size:23px;" >&nbsp;ORIGEN:<br>&nbsp;'.$userData["name"].' '.$userData["lastname"].'</div></td>
                <td><div style="width:381px; height:75px; border-width: 1px; border-style: solid; border-color: black; font-size:32px; font-weight: bold;">&nbsp;DESTINO:<br>&nbsp;'.$userDataTo["name"].' '.$userDataTo["lastname"].'</div></td>
            </tr>
            </table>
            <table class="default">
            <tr>
                <td><div style="width:192px; height:170px; border-width: 1px; border-style: solid; border-color: black; font-size:25px;" class="float">&nbsp;TELÉFONOS:<br><br>&nbsp;'.$userData["phone"].'</div></td>
            </tr>
            </table>
            <table class="default">
            <tr>
                <td><div align="center" style="width:112px; height:56px; border-width: 1px; border-style: solid; border-color: black; background-color:black; color:white; font-size:27px; font-weight: bold;"  class="float">PESO/L<br>B </div></td>
                <td><div align="center" style="width:225px; height:56px; border-width: 1px; border-style: solid; border-color: black; background-color:black; color:white; font-size:27px; font-weight: bold;" class="float">MEDIDAS/IN </div></td>
                <td><div align="center" style="width:131px; height:56px; border-width: 1px; border-style: solid; border-color: black; background-color:black; color:white; font-size:27px; font-weight: bold;"  class="float">VL/LB</div></td>
                <td><div align="center" style="width:131px; height:56px; border-width: 1px; border-style: solid; border-color: black; background-color:black; color:white; font-size:27px; font-weight: bold;" class="float">FT3</div></td>
            </tr>
            </table>
            <table class="default">
            <tr>
                <td><div align="center" style="width:112px; height:94px; border-width: 1px; border-style: solid; border-color: black; font-size:25px;"  class="float"><br>&nbsp;10.00</div></td>
                <td><div align="center" style="width:225px; height:94px; border-width: 1px; border-style: solid; border-color: black; font-size:25px;" class="float"><br>&nbsp;11.00x12.00x10.00</div></td>
                <td><div align="center" style="width:131px; height:94px; border-width: 1px; border-style: solid; border-color: black; font-size:25px;"  class="float"><br>&nbsp;10.00</div></td>
                <td><div align="center" style="width:131px; height:94px; border-width: 1px; border-style: solid; border-color: black; font-size:25px;" class="float" ><br>&nbsp;10.40</div></td>
            </tr>
            </table>
            </div>');
    
            $html2pdf->writeHTML('<br><div style="border-width: 1px; border-style: solid; border-color: black; width:576px; height:377px;">
            <table class="default">
            <tr>
                <td><div align="center" style="width:226px; height:75px; border-width: 1px; border-style: solid; border-color: black; font-size:60px; font-weight: bold;">'.$tipo.'</div></td>
                <td><div align="center" style="width:377px; height:75px; border-width: 1px; border-style: solid; border-color: black; font-size:35px; font-weight: bold;">'.$warehouseData["address_send"].'</div></td>
            </tr>
            </table>
            <table class="default">
            <tr>
                <td><div align="center"style="width:415px; height:132px; border-width: 1px; border-style: solid; border-color: black; background-color:black;"><a style="font-size:60px; color:white; font-weight: bold; text-decoration: none; margin-top:25px;">'.$warehouseData["code"].'</a></div></td>
                <td><div align="center" style="width:188px; height:132px; border-width: 1px; border-style: solid; border-color: black; font-size:24px; font-weight: bold;"><a style="font-size:25px; color:black; text-decoration: none">BULTO</a><br><br><a style="font-size:50px; color:black; text-decoration: none; font-weight: bold;">'.$i.'-'.$n.'</a></div></td>
            </tr>
            </table>
            <table class="default">
            <tr>
                <td><div align="center" style="width:151px; height:170px;" class="float"> <img src="./qrte.png" style="margin-top:12px;"/></div></td>
                <td><div style="width:453px; height:170px; " class="float"><br><a style="font-size:35px; color:blue; text-decoration: underline; margin-top:21px;">www.totalenvios.com</a><br><a style="font-size:25px; color:black; text-decoration: none;">¡Gracias por preferirnos!</a></div></td>
            </tr>
            </table>
            </div>
            ');
    }

    foreach ($boxes as $key => $value) {
        $box = $warehouses->findBoxesById($value['id_box']);
        $pc = number_format((($box['width']*$box['height']*$box['lenght'])/1728),4);
        $lb = number_format((($box['width']*$box['height']*$box['lenght'])/166),4);
        $html2pdf->writeHTML('<div style="border-width: 1px; border-style: solid; border-color: black; width:576px; height:300px;">
            <table class="default">
            <tr>
                <td><div align="center" style="width:302px; height:94px; border-width: 1px; border-style: solid; border-color: black;"><img src="./logo_final.png" style="margin-top:12px;"/></div></td>
                <td><div style="width:151px; height:94px; border-width: 1px; border-style: solid; border-color: black; font-size:23px;">FECHA:<br>'.$warehouseData["date_send"].'</div></td>
                <td><div align="center" style="width:151px; height:94px; border-width: 1px; border-style: solid; border-color: black; background-color:black; color:white; font-size:60px; font-weight:bold;">12</div></td>
            </tr>
            </table>
            <table class="default">
            <tr>
                <td><div style="width:228px; height:75px; border-width: 1px; border-style: solid; border-color: black; font-size:23px;" >&nbsp;ORIGEN:<br>&nbsp;'.$userData["name"].' '.$userData["lastname"].'</div></td>
                <td><div style="width:381px; height:75px; border-width: 1px; border-style: solid; border-color: black; font-size:32px; font-weight: bold;">&nbsp;DESTINO:<br>&nbsp;'.$userDataTo["name"].' '.$userDataTo["lastname"].'</div></td>
            </tr>
            </table>
            <table class="default">
            <tr>
                <td><div style="width:192px; height:170px; border-width: 1px; border-style: solid; border-color: black; font-size:25px;" class="float">&nbsp;TELÉFONOS:<br><br>&nbsp;'.$userData["phone"].'</div></td>
            </tr>
            </table>
            <table class="default">
            <tr>
                <td><div align="center" style="width:112px; height:56px; border-width: 1px; border-style: solid; border-color: black; background-color:black; color:white; font-size:27px; font-weight: bold;"  class="float">PESO/L<br>B </div></td>
                <td><div align="center" style="width:225px; height:56px; border-width: 1px; border-style: solid; border-color: black; background-color:black; color:white; font-size:27px; font-weight: bold;" class="float">PIES CUBICOS </div></td>
                <td><div align="center" style="width:131px; height:56px; border-width: 1px; border-style: solid; border-color: black; background-color:black; color:white; font-size:27px; font-weight: bold;"  class="float">VL/LB</div></td>
            </tr>
            </table>
            <table class="default">
            <tr>
                <td><div align="center" style="width:112px; height:94px; border-width: 1px; border-style: solid; border-color: black; font-size:25px;"  class="float"><br>&nbsp;10.00</div>'.$box['weight'].'</td>
                <td><div align="center" style="width:225px; height:94px; border-width: 1px; border-style: solid; border-color: black; font-size:25px;" class="float"><br>&nbsp;'.$pc.'</div></td>
                <td><div align="center" style="width:131px; height:94px; border-width: 1px; border-style: solid; border-color: black; font-size:25px;"  class="float"><br>&nbsp;'.$lb.'</div></td>
            </tr>
            </table>
            </div>');
    
            $html2pdf->writeHTML('<br><div style="border-width: 1px; border-style: solid; border-color: black; width:576px; height:377px;">
            <table class="default">
            <tr>
                <td><div align="center" style="width:226px; height:75px; border-width: 1px; border-style: solid; border-color: black; font-size:60px; font-weight: bold;">'.$tipo.'</div></td>
                <td><div align="center" style="width:377px; height:75px; border-width: 1px; border-style: solid; border-color: black; font-size:35px; font-weight: bold;">'.$warehouseData["address_send"].'</div></td>
            </tr>
            </table>
            <table class="default">
            <tr>
                <td><div align="center"style="width:415px; height:132px; border-width: 1px; border-style: solid; border-color: black; background-color:black;"><a style="font-size:60px; color:white; font-weight: bold; text-decoration: none; margin-top:25px;">'.$warehouseData["code"].'</a></div></td>
                <td><div align="center" style="width:188px; height:132px; border-width: 1px; border-style: solid; border-color: black; font-size:24px; font-weight: bold;"><a style="font-size:25px; color:black; text-decoration: none">BULTO</a><br><br><a style="font-size:50px; color:black; text-decoration: none; font-weight: bold;">'.$i.'-'.$n.'</a></div></td>
            </tr>
            </table>
            <table class="default">
            <tr>
                <td><div align="center" style="width:151px; height:170px;" class="float"> <img src="./qrte.png" style="margin-top:12px;"/></div></td>
                <td><div style="width:453px; height:170px; " class="float"><br><a style="font-size:35px; color:blue; text-decoration: underline; margin-top:21px;">www.totalenvios.com</a><br><a style="font-size:25px; color:black; text-decoration: none;">¡Gracias por preferirnos!</a></div></td>
            </tr>
            </table>
            </div>
            ');
    }
    $html2pdf->output();
} catch (PDOException $e) {
    echo $e;
}
?>