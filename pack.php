
        <?php

            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
            
            include 'includes/connection.class.php';
            include 'includes/users.class.php';
            include 'includes/packages.class.php';
            include 'includes/headquarters.class.php';
            include 'includes/stock.class.php';
            include 'includes/packing.class.php';
            include 'includes/warehouses.class.php';


            $id = $_REQUEST['id'];
            $users = new Users;
            $allUsers = $users->findAll();
            $packages = new Packages;
            $headquarters = new Headquarters;
            $allHeadquarters = $headquarters->findAll();
            $allOperators = $users->findAllOperators();
            $allClients = $users->findAllClients();
            $states = $packages->findStates();
            $stock = new Stock;
            $packing = new Packing;
            $allPacking = $packing->findAll();
            $curriers = $packing->findCurriers();
            $warehouses = new Warehouses;
            $allWarehouses = $warehouses->findAll();
            $trips = $warehouses->findTrips();
            $countries = $warehouses->findCountries();
            $packing_states = $packing->findStates();

            session_start();

            if ($_SESSION['state'] != 1) {
                header('Location: login.php');
            }

            $date_in = date('2020-m-d');
            $date_out = date('Y-m-d');
            $bulk = isset($_REQUEST['bulk']) ? $_REQUEST['bulk'] : 0;
            $dataPacking = $packing->findById($id);
            $dataWarehouses = $packing->findDataWarehouses($id);

        ?>

        <!DOCTYPE html>
        <html dir="ltr" lang="en">

        <head>
            <?php 

                include 'head.php';

            ?>
        </head>
        
        <body>
            <input id="idUser" type="hidden" value="0">
            <input id="idUserFor" type="hidden" value="0">
            <input id="idBulk" type="hidden" value="<?php echo $bulk; ?>">
        
            <div class="preloader">
                <div class="lds-ripple">
                    <div class="lds-pos"></div>
                    <div class="lds-pos"></div>
                </div>
            </div>
            <div id="main-wrapper" 
                data-header-position="absolute" data-boxed-layout="full">
                <?php 

                    include 'navbar.php'; 
                    if ($_SESSION['state'] != 1) {
                        header('Location: login.php');
                    }

                ?>
               
                <div class="page-wrapper">
                    <div class="page-breadcrumb bg-white">
                        <div class="row align-items-center">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                <h4 class="page-title">Packing: #<?php echo $id; ?></h4>
                            </div>
                            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                                <div class="d-md-flex">
                                    <ol class="breadcrumb ms-auto">
                                        <li><a href="logout.php" class="fw-normal">Logout</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="alert alert-danger register-error" role="alert">
                                </div>
                                <div class="alert alert-success register-success" role="alert">
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group mb-4">
                                            <label for="example-email" class="col-md-12 p-0">Fecha Creacion *</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <input type="text" placeholder="Fecha Entrada" readonly="readonly"
                                                    class="form-control p-0 border-0" id="date_in" value="<?php echo $dataPacking['date_in']; ?> 00:00:00">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0">Nombre *</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <input readonly="readonly" id="name" type="text" placeholder="Nombre"
                                                    class="form-control p-0 border-0" value="<?php echo $dataPacking['name']; ?>"> </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0">Nota *</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <input readonly="readonly" id="note" type="text" placeholder="Nota *" class="form-control p-0 border-0" value="<?php echo $dataPacking['note']; ?>"> </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label class="col-sm-12">Warehouse *</label>
                                            <div class="col-sm-12 border-bottom">
                                                <?php  

                                                    foreach ($dataWarehouses as $key => $value) {
                                                ?>
                                                        <input readonly="readonly" type="text" class="form-control wpacking" value="<?php echo $value['id_warehouse']; ?>">
                                                <?php
                                                    }

                                                ?>
                                                
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label class="col-sm-12">Currier *</label>
                                            <div class="col-sm-12 border-bottom">
                                                <input readonly="readonly" type="text" class="form-control" value="<?php echo $packing->findCurrierById($dataPacking['id_currier'])['name']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="example-email" class="col-md-12 p-0">Fecha Salida *</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <input readonly="readonly" type="text" placeholder="Fecha Salida" class="form-control p-0 border-0" id="date_out" value="<?php echo $dataPacking['date_out']; ?> 00:00:00">
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0">Tipo de viaje</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <input readonly="readonly" type="text" class="form-control" value="<?php echo $packing->findTripById($dataPacking['id_trip'])['name']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0">Destino *</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <input readonly="readonly" type="text" class="form-control" value="<?php echo $warehouses->findCountryById($dataPacking['id_destiny'])['name']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0">Estado *</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <input readonly="readonly" type="text" class="form-control" value="<?php echo $packing->findStateById($dataPacking['state'])['name']; ?>">
                                            </div>
                                        </div>
                                        <!-- <div class="form-group mb-4">
                                            <div class="col-sm-12">
                                                <button onclick="addPacking();" class="btn btn-success">Registrar</button>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer text-center"> 2021 © Total Envios
                    </footer>
                </div>
                
            </div>

            <?php 

                include 'foot.php';

            ?>
            <?php  
                if ($bulk != 0) {
                    echo '<script type="text/javascript">'
                    . '$( document ).ready(function() {'
                    . '$("#packageModal").modal("show");'
                    . '});'
                    . '</script>';
                }
            ?>
        </body>

        </html>