
        <?php

            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
            
            include 'includes/connection.class.php';
            include 'includes/users.class.php';
            include 'includes/packages.class.php';
            include 'includes/headquarters.class.php';
            include 'includes/stock.class.php';
            include 'includes/packing.class.php';
            include 'includes/warehouses.class.php';
            include 'includes/transport.class.php';


            $id = $_REQUEST['id'];
            $users = new Users;
            $allUsers = $users->findAll();
            $packages = new Packages;
            $warehouse = new Warehouses;
            $headquarters = new Headquarters;
            $allHeadquarters = $headquarters->findAll();
            $allOperators = $users->findAllOperators();
            $allClients = $users->findAllClients();
            $states = $packages->findStates();
            $stock = new Stock;
            $packing = new Packing;
            $transp = new Transport;
            // $pack = $packing->findById();
            $allPacking = $packing->findAll();
            $allTransp = $transp->findAll();
            $curriers = $packing->findCurriers();
            $warehouses = new Warehouses;
            $allWarehouses = $warehouses->findAll();
            $trips = $warehouses->findTrips();
            $countries = $warehouses->findCountries();
            $packing_states = $packing->findStates();
            

            session_start();

            if ($_SESSION['state'] != 1) {
                header('Location: login.php');
            }

            $date_in = date('2020-m-d');
            $date_out = date('Y-m-d');
            $bulk = isset($_REQUEST['bulk']) ? $_REQUEST['bulk'] : 0;
            $dataPacking = $packing->findById($id);
            $dataPackage = $packages->findById($id);
            $packages_states = $packages->findStates();
            $userData = $warehouse->listUserDataToReceive($dataPackage['user_from']);
            $transport_name = $packages->findTransportForId($dataPackage['id_transp'])['name'];
            // print_r($dataPackage); 
            // $dataWarehouses = $packing->findDataWarehouses($id);
            // $oneWarehouseData = $warehouses->findById($dataWarehouses[0]['id_warehouse']);

        ?>

        <!DOCTYPE html>
        <html dir="ltr" lang="en">

        <head>
            <?php 

                include 'head.php';

            ?>
        </head>
        
        <body style="background:#e3e3e3ff;">
            
            <input id="id_user_form" type="hidden" value="0">
            <input id="idUser" type="hidden" value="0">
            <input id="idUserFor" type="hidden" value="0">
            <input id="idBulk" type="hidden" value="<?php echo $bulk; ?>">
            <input type="hidden" id="packId" value="<?php echo $id; ?>">
            <div class="preloader">
                <div class="lds-ripple">
                    <div class="lds-pos"></div>
                    <div class="lds-pos"></div>
                </div>
            </div>
            <div id="main-wrapper" 
                data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
                <?php 

                    include 'navbar.php'; 
                    if ($_SESSION['state'] != 1) {
                        header('Location: login.php');
                    }

                ?>
               
                <div class="page-wrapper" style="background:#e3e3e3ff;">
                <div class="row">
                    <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                        <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                    </div>
                    <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                        <h4 style="color:#005574;"><strong>EDITAR PAQUETE TE-PK<?php echo $id; ?> </strong> </h4>
                    </div>
                </div><br>
                <div class="alert alert-danger register-error" role="alert">
                </div>
                <div class="alert alert-success register-success" role="alert">
                </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <div class="form-group">
                                        <input id="alert_id" type="text"
                                                placeholder="alert_id" value="<?php echo $dataPackage['alert_id']; ?>" class="form-control campos-input-login" hidden>
                                                <input id="package_id" type="text"
                                                placeholder="alert_id" value="<?php echo $id; ?>" class="form-control campos-input-login" hidden>
                                            
                                            <!-- <label class="col-md-12 p-0">Tracking</label> -->
                                            <label class="col-sm-12">TRACKING</label>
                                            <input id="tracking" type="text"
                                                placeholder="Tracking" class="form-control campos-input-login" value="<?php echo $dataPackage['tracking'] ?>">
                                            <ul id="myAlert">
                                            </ul>
                                    </div>
                                </div>
                                <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1 pt-4">
                                    <button class="btn btn-primary" onclick="findTracking();"
                                        style="background-color:#005574; color:white; border:2px solid #005574;"><i
                                        style="color:white;" class="fas fa-search"></i>
                                    </button>   
                                </div>
                                
                                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                        <div class="form-group">
                                            <!-- <label for="example-email" class="col-md-12 p-0">Fecha Entrada *</label> -->
                                            <label class="col-sm-12">FECHA DE REGISTRO</label>
                                            <input readonly="readonly" type="text" placeholder="Fecha Entrada"
                                                class="form-control campos-input-login" id="date_in"
                                                value="<?php echo $date_out; ?>">

                                            <input id="fecha_registro" value=" <?php echo $fechaActual; ?>"
                                                hidden>

                                        </div>
                                </div>
                            </div>
                            <div id="resultado_tracking">
                                   
                                        </div>

                            <div class="row">
                                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                
                                    <div class="form-group">
                                        <!-- <label for="user_from" style="color:black;">Nombre/casillero</label> -->
                                        <div class="input-group mb-3">
                                        <label class="col-sm-12">CLIENTE</label>
                                            <input id="id_user_from" value="<?php echo $dataPackage['user_from'] ?>" hidden>
                                            <input type="text" class="form-control" placeholder="Nombre"
                                                aria-label="Recipient's username"
                                                aria-describedby="basic-addon2"
                                                onkeyup="searchUserPackage();" id="user_from" value="<?php echo $userData[0]['name'].' '.$userData[0]['lastname'];?>">

                                            <div class="input-group-append">
                                                <button
                                                    onclick="$('#packageModal').modal('hide'); $('#newUserModal').modal('show');"
                                                    class="btn btn-outline-secondary"
                                                    style="background-color:#ff554dff; color:white;"><i
                                                        style="color:white"
                                                        class="fas fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <ul id="myUS">
                                        </ul>

                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                <div class="form-group">
                                                    <!-- <label class="col-md-12 p-0">Alias del paquete</label> -->
                                                    <label class="col-sm-12">NOMBRE DEL PAQUETE</label>
                                                    <input id="alias" type="text" placeholder="Nombre del paquete"
                                                        class="form-control campos-input-login" value="<?php echo $dataPackage['alias']?>">
                                                </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                    <div class="form-group">
                                        <!-- <label class="col-md-12 p-0">Alias del paquete</label> -->
                                        <label class="col-sm-12">CASILLERO</label>
                                        <input id="casillero_paquete" type="text" placeholder="Casillero"
                                            class="form-control campos-input-login" value="<?php echo $userData[0]['code'];?>">
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                <div class="col-lg-4 col-xlg-4 col-md-4" hidden>
                                                <div class="form-group">
                                                    <label class="col-md-12 p-0">Operador_id</label>

                                                    <input id="operator_id" type="text" placeholder="Alias"
                                                        class="form-control campos-input-login"
                                                        value="<?php echo $users->findforCode($_SESSION['code'])[0]['id']; ?>"
                                                        readonly>
                                                </div>
                                            </div>
                                <div class="form-group">
                                    <label class="col-md-12 p-0">OPERADOR</label>

                                    <input id="operator" type="text" placeholder="Alias"
                                        class="form-control campos-input-login"
                                        value="<?php echo $_SESSION['name']; ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                <div class="form-group">
                                                    <label class="col-sm-12">TRANSPORTISTA</label>

                                                    <select id="transp" class="form-select campos-input-login">
                                                        <option value="<?php echo $dataPackage['id_transp']; ?>"><?php echo $transport_name; ?></option>
                                                        <?php  
                                                                foreach ($allTransp as $key => $value) {
                                                            ?>
                                                        <option value="<?php echo $value['id']; ?>">
                                                            <?php echo $value['name']; ?></option>
                                                        <?php
                                                                }
                                                            ?>
                                                    </select>

                                                </div>

                                </div>
                               
                                
                                <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                    <div class="form-group">
                                                    <label class="col-sm-12">COSTO</label>

                                                    <input id="cost" type="number" placeholder="Costo"
                                                        class="form-control campos-input-login" value="<?php echo $dataPackage['cost']?>">

                                                </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                    <div class="form-group">
                                                    <label class="col-md-12 p-0">CONTENIDO</label>

                                                    <input id="description" type="text" placeholder="Descripcion"
                                                        class="form-control campos-input-login" value="<?php echo $dataPackage['description']?>"> 
                                    </div>

                                </div>

                               
                            </div>
                            
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                             <center><h5 style="color:#005574;"> <strong>MEDIDAS DEL PAQUETE</strong> </h5></center>
                             <hr> 
                            <div class="row">
                                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                    <div class="form-group">
                                                    <label class="col-sm-12">ALTO</label>

                                                    <input id="height" type="number" placeholder="Alto"
                                                        class="form-control campos-input-login" value="<?php echo $dataPackage['height']?>">

                                                </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                <div class="form-group">
                                                    <label class="col-sm-12">LARGO</label>

                                                    <input id="lenght" type="number" placeholder="Largo"
                                                        class="form-control campos-input-login" value="<?php echo $dataPackage['lenght']?>">

                                                </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                <div class="form-group">
                                                    <label class="col-sm-12">ANCHO</label>

                                                    <input id="width" type="number" placeholder="Ancho"
                                                        class="form-control campos-input-login" value="<?php echo $dataPackage['width']?>">

                                                </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                <div class="form-group">
                                                    <label class="col-sm-12">PESO</label>

                                                    <input id="weight" type="number" placeholder="LB"
                                                        class="form-control campos-input-login" value="<?php echo $dataPackage['weight']?>">

                                                </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10"></div>
                                <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                    <div class="btn-group" align="rigth">
                                        <button class="btn btn-primary" onclick="editPackageForMod();"
                                            style="background-color:#005574; color:white; border:2px solid #005574;"><i
                                            style="color:white;" class="fas fa-check"></i> EDITAR
                                        </button>   
                                        <button class="btn btn-primary" onclick="window.open('packages.php', '_parent');"
                                            style="background-color:red; color:white; border:2px solid #005574;"><i
                                            style="color:white;" class="fas fa-redo"></i> CANCELAR
                                        </button>   
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>

               
                   
                    
                            </div>
                        </div>
                    </div>
                   
                </div>
                
            </div>

            <?php 

                include 'foot.php';

            ?>
            <?php  
                if ($bulk != 0) {
                    echo '<script type="text/javascript">'
                    . '$( document ).ready(function() {'
                    . '$("#packageModal").modal("show");'
                    . '});'
                    . '</script>';
                }
            ?>
        </body>

        </html>