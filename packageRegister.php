<?php
// error_reporting(0);
        // ini_set('display_errors', 0);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        
        include 'includes/connection.class.php';
        include 'includes/users.class.php';
        include 'includes/packages.class.php';
        include 'includes/headquarters.class.php';
        include 'includes/stock.class.php';
        include 'includes/warehouses.class.php';
        include 'includes/depart.class.php';


        $users = new Users;
        $allUsers = $users->findAll();
        $packages = new Packages;
        $allPackages = $packages->findAll();
        $depart = new Depart;
        $allDeparts = $depart->findAll();
        $headquarters = new Headquarters;
        $allHeadquarters = $headquarters->findAll();
        $allOperators = $users->findAllOperators();
        $allClients = $users->findAllClients();
        $states = $packages->findStates();
        $allCountries = $users->findAddressee();
        $allAddressStates = $users->findAddressStates();
        $allAddressCities = $users->findAddressCities();
        $userTypes = $users->findRegisterTypes();
        

        $stock = new Stock;
        $warehouses = new Warehouses;
        $warehouseStates = $warehouses->findWStates();
        $allWarehouses = $warehouses->findAll();
        $findLast = $warehouses->findLast();
        $boxes = $packages->findBoxes();
        $trips = $warehouses->findTrips();
        $categories = $warehouses->findCategories();
        $countries = $warehouses->findCountries();
        session_start();

        $regions = $users->findRegions();

        $userDestiny = $users->findByEmail($_SESSION['email'])['name'].' '.$users->findByEmail($_SESSION['email'])['lastname'];
        $userDestinyId = $users->findByEmail($_SESSION['email'])['id'];
        $dataUser = $users->findByEmail($_SESSION['email']);
        $destiny = $users->findAddressById($dataUser['country']);
        $curriers = $packages->findCurriers();
        $regionCurrier = $users->findRegionByZone($dataUser['country']);

        if ($_SESSION['state'] != 1) {
            header('Location: login.php');
        }

        $date_in = date('2020-m-d');
        $date_out = date('Y-m-d');
        $digits = 5;
        $fecha = new DateTime();
        $dataFecha = $fecha->getTimestamp();
        $lastId = ($warehouses->findLastId()['id']+1);
        $code = 'TE-WH'.$lastId;
        
        $boxCode = substr($dataFecha, -5);
        $bulk = isset($_REQUEST['bulk']) ? $_REQUEST['bulk'] : 0;

        $nextFriday = strtotime('next friday');
        $nextFriday = date('Y-m-d', $nextFriday);

    ?>
<!DOCTYPE html>
    <html dir="ltr" lang="en">

    <head>
        <?php 

            include 'head.php';

        ?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    </head>
    <body>
    <div class="modal fade" id="newUserModal" tabindex="-1" role="dialog" aria-labelledby="newUserModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="newUserModalLabel">Registro de nuevo cliente</h5>
                    <button onclick="$('#newUserModal').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="alert alert-danger user-error" role="alert">
                                </div>
                                <div class="alert alert-success user-success" role="alert">
                                </div>
                                <div class="row">
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                        
                                            </div>
                                            <div class="col-sm-12 col-md-5 col-lg-3 col-xl-5"></div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4" style="text-align:right; content-align:right;">
                                                <div class="btn-group" align="right">
                                                    <a id="muchosDestinatarios" onclick="RegUsuario();" class="btn btn-primary" style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i style="color:white;" class="fas fa-save"></i> Guardar</a>
                                                    <a id="boton-destinatario" onclick="AgregarDest();" class="btn btn-primary" style="background-color:white; color:#ff554dff; border:2px solid #ff554dff;"><i style="color:#ff554dff;" class="fas fa-plus"></i> Registrar destinatario</a>
                                                </div>
                                                    
                                            </div>
                                        </div><br>
                                <div class="card" id="card-registro-usuarios-wh">
                                    <div class="card-body">
                                    <div align="center" style="background-color:#005574; color:white; width:100%; padding:8px;"><h4>Nuevo Usuario</h4></div><br>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label class="user_type">Tipo de usuario *</label>
                                                    
                                                        <select readonly="readonly" id="user_type" class="form-select">
                                                            <option selected="selected" value="2">Cliente</option>
                                                            <option value="5">Empresa</option>
                                                        </select>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="name">Nombre *</label>
                                                    
                                                        <input id="name" type="text" placeholder="Nombre Completo"
                                                            class="form-control"> 
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="lastname">Apellido *</label>
                                                    
                                                        <input id="lastname" type="text" placeholder="Apellido Completo"
                                                            class="form-control"> 
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="document">Documento *</label>
                                                    
                                                        <input id="document" type="text" placeholder="# Documento" class="form-control"> 
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="example-email">Email *</label>
                                                    
                                                        <input type="email" placeholder="johnathan@admin.com"
                                                            class="form-control" name="example-email"
                                                            id="email">
                                                   
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="phone">Teléfono *</label>
                                                
                                                        <input id="phone" type="text" placeholder="123 456 7890" class="form-control">
                                                   
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="phone_house">Teléfono de casa</label>
                                                   
                                                        <input id="phone_house" type="text" placeholder="123 456 7890" class="form-control">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="date" class="col-md-12 p-0">Fecha de Nacimiento *</label>
                                                        <input type="date" placeholder="1990-01-01"
                                                            class="form-control"
                                                            id="born_date">
                                                </div>
                                            </div>
                                        </div>

                                


                                        
                                        
                                            
                                            
                                        </div>
                                        </div>
                                        </div>
                                        
                                        <div id="registrarDestinatario" style="display:none;">
                                        <!-- registro de destinatario oculto hasta completar registro -->
                                        <div class="card">
                                    <div class="card-body">
                                    <div align="center" style="background:#005574; color:white; width:100%; padding:8px;"><h4>Nuevo destinatario</h4></div><br>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="name_destiner">Nombre *</label>
                                                    
                                                        <input id="name_destiner" type="text" placeholder="Nombre Completo"
                                                            class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="lastname_destiner">Apellido *</label>
                                                    
                                                        <input id="lastname_destiner" type="text" placeholder="Apellido Completo"
                                                            class="form-control"> 
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="contact_phone_destiner">Telefono de contacto</label>
                                                    
                                                        <input type="text" placeholder="123 456 7890"
                                                            class="form-control"
                                                            id="contact_phone_destiner">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <div class="form-group">
                                                                            <label for="address_destiny">Pais destino</label>
                                                                                <select id="address_destiny" class="form-select campos-input-login">
                                                                                    <!-- <option value="0">Seleccionar...</option> -->
                                                                                    <?php  
                                                                                        foreach ($allCountries as $key => $value) {
                                                                                    ?>
                                                                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                                                    <?php
                                                                                        }
                                                                                    ?>
                                                                                </select>
                                                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <div class="form-group">
                                                                                <label for="address_state">Estado</label>
                                                                                    <input id="address_state" type="text" placeholder="Estado" class="form-control campos-input-login">
                                                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <div class="form-group">
                                                                                <label for="select_state">Estado</label>
                                                                                    <select id="select_state" class="form-select campos-input-login">
                                                                                    <!-- <option value="0">Seleccionar...</option> -->
                                                                                    <?php  
                                                                                        foreach ($allAddressStates as $key => $value) {
                                                                                    ?>
                                                                                            <option value="<?php echo $value['id_estado']; ?>"><?php echo $value['estado']; ?></option>
                                                                                    <?php
                                                                                        }
                                                                                    ?>
                                                                                    </select>
                                                                                </div>    
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="address_destiner">Dirección</label>
                                                   
                                                        <input id="address_destiner" type="text" placeholder="Dirección de entrega" class="form-control"> 
                                                </div>
                                            </div>
                                            
                                            
                                                
                                                
                                                
                                        </div>
                                                            </div>
                                                            </div>
                                                            
                                        <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                                            
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>

        <!-- MODAL DE REGISTRO DE DESTINATARIO -->
        <!-- ++++++++++++++++++++++++++++++++++++++++++ -->
        <div class="modal fade" id="newDestinerModal" tabindex="-1" role="dialog" aria-labelledby="newDestinerModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="newDestinerModalLabel">Registro de nuevo destinatario</h5>
                    <button onclick="$('#newDestinerModal').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="alert alert-danger user-error" role="alert">
                                </div>
                                <div class="alert alert-success user-success" role="alert">
                                </div>
                                <div class="row">
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                        
                                            </div>
                                            <div class="col-sm-12 col-md-5 col-lg-3 col-xl-5"></div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4" style="text-align:right; content-align:right;">
                                                <div class="btn-group" align="right">
                                                    <a id="muchosDestinatarios" onclick="RegUsuario();" class="btn btn-primary" style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i style="color:white;" class="fas fa-save"></i> Guardar</a>
                                                    <!-- <a id="boton-destinatario" onclick="AgregarDest();" class="btn btn-primary" style="background-color:white; color:#ff554dff; border:2px solid #ff554dff;"><i style="color:#ff554dff;" class="fas fa-plus"></i> Registrar destinatario</a> -->
                                                </div>
                                                    
                                            </div>
                                        </div><br>
                                <div class="card" id="card-registro-usuarios-wh">
                                    <div class="card-body">
                                    
                                    <div align="center" style="background:#005574; color:white; width:100%; padding:8px;"><h5>Destinatario</h5></div><br>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="name_destiner">Nombre *</label>
                                                    
                                                        <input id="name_destiner" type="text" placeholder="Nombre Completo"
                                                            class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="lastname_destiner">Apellido *</label>
                                                    
                                                        <input id="lastname_destiner" type="text" placeholder="Apellido Completo"
                                                            class="form-control"> 
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="contact_phone_destiner">Telefono de contacto</label>
                                                    
                                                        <input type="text" placeholder="123 456 7890"
                                                            class="form-control"
                                                            id="contact_phone_destiner">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <div class="form-group">
                                                                            <label for="address_destiny">Pais destino</label>
                                                                                <select id="address_destiny" class="form-select campos-input-login">
                                                                                    <!-- <option value="0">Seleccionar...</option> -->
                                                                                    <?php  
                                                                                        foreach ($allCountries as $key => $value) {
                                                                                    ?>
                                                                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                                                    <?php
                                                                                        }
                                                                                    ?>
                                                                                </select>
                                                                                </div>
                                            </div>
                        
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 address_state">
                                                                                <div class="form-group">
                                                                                <label for="address_state">Estado</label>
                                                                                    <input id="address_state" type="text" placeholder="Estado" class="form-control campos-input-login">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 select_state" style="display: none;">
                                                                                <div class="form-group">
                                                                                <label for="select_state">Estado</label>
                                                                                    <select id="select_state" class="form-select campos-input-login">
                                                                                    <!-- <option value="0">Seleccionar...</option> -->
                                                                                    <?php  
                                                                                        foreach ($allAddressStates as $key => $value) {
                                                                                    ?>
                                                                                            <option value="<?php echo $value['id_estado']; ?>"><?php echo $value['estado']; ?></option>
                                                                                    <?php
                                                                                        }
                                                                                    ?>
                                                                                    </select>
                                                                                </div>    
                                                                            </div>
                                                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 address_city">
                                                                                <div class="form-group">
                                                                                <label for="address_city">Ciudad</label>
                                                                                    <input id="address_city" type="text" placeholder="Ciudad" class="form-control campos-input-login">
                                                                                </div>    
                                                                            </div>
                                                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 select_city" style="display: none;">
                                                                                <div class="form-group">
                                                                                <label for="select_city">Ciudad</label>
                                                                                    <select id="select_city" class="form-select campos-input-login">
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="address_destiner">Dirección</label>
                                                   
                                                        <input id="address_destiner" type="text" placeholder="Dirección de entrega" class="form-control"> 
                                                </div>
                                            </div>
                                            
                                            
                                                
                                                
                                                
                                        </div>
                                        </div>
                                        </div>
                                        
                                       
                                        <!-- registro de destinatario oculto hasta completar registro -->
                                    
                                    
                                                            
                                                            
                                                            
                                        
                                        
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>
        <!-- ++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div> -->
        <div id="main-wrapper" data-boxed-layout="full">
            <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
                }

            ?>
            
    
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="alert alert-danger register-error" role="alert">
                        </div>
                        <div class="alert alert-success register-success" role="alert">
                        </div>
                     </div>
                </div>             
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <h3 id="code-title"><?php echo $code; ?></h3>
                        <h6 style="margin-bottom:35px; color:black;"><strong>Fecha de registro: </strong><?php echo date('m-d-Y') ?></h6>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3"></div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                        
                        
                           <div align="right"> <button onclick="addWarehouse();" class="btn btn-primary" style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i style="color:white" class="fas fa-save"></i>Guardar</button></div>
                    
                        
                    </div> 
                </div>
                <div class="destino-data">                               
                    <div class="row">
                        <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                            <div class="form-group">
                                <label for="user_from" style="color:black;">Nombre/casillero</label>
                                <div class="input-group mb-3">
                                
                                <input type="text" class="form-control" placeholder="Nombre" aria-label="Recipient's username" aria-describedby="basic-addon2" onkeyup="searchUserPackage();" id="user_from">
                                
                                    <div class="input-group-append">
                                    <button onclick="$('#warehouseModal').modal('hide'); $('#newUserModal').modal('show');" class="btn btn-outline-secondary" style="background-color:#ff554dff; color:white;"><i style="color:white" class="fas fa-plus"></i> <strong>Registrar</strong></button>
                                    </div>
                                </div>
                                <ul id="myUS">
                                            </ul>
                                            <ul id="myDestiner">
                                            </ul>
                            </div>
                        </div>   

                        <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                            <div class="form-group">
                                <label for="user_from" style="color:black;">Destinatario</label>
                                <div class="input-group mb-3">
                                
                                <input type="text" class="form-control" placeholder="Nombre" aria-label="Recipient's username" aria-describedby="basic-addon2" onkeyup="searchFilter();" id="myInput">
                                
                                    <div class="input-group-append">
                                    <button onclick="$('#warehouseModal').modal('hide'); $('#newDestinerModal').modal('show');" class="btn btn-outline-secondary" style="background-color:#ff554dff; color:white;"><i style="color:white" class="fas fa-plus"></i> <strong>Registrar</strong></button>
                                    </div>
                                </div>
                                <ul id="myUS">
                                            </ul>
                                            <ul id="myDestiner">
                                            </ul>
                            </div>
                        </div>   

                        
        
                    <!-- <hr style="border: 1px solid #ff554dff;">   -->
                        
                        <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <div class="form-group">
                                    <label for="contactPhone" style="color:black;">Teléfono de contacto</label>
                                        
                                            <input id="contactPhone" placeholder="Contacto" class="input-register-warehouse"> 
                                       
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <div class="form-group">
                                    <label for="address_send" style="color:black;">Destino</label>
                                        
                                           
                                        <input id="address_send" type="text" class="input-register-warehouse" readonly="readonly">
                                        
                                        
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="trip" style="color:black;">Tipo de envio</label>
                                   
                                        <select id="trip" class="input-register-warehouse">
                                            <?php  

                                                foreach ($trips as $key => $value) {
                                            ?>
                                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                            <?php
                                                }

                                            ?>
                                        </select>
                                   
                            </div>
                        </div>
                            
                        <div class="row">
                
                            <!-- <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <div class="form-group">
                                    <label for="contactAddress">Dirección</label>
                                        
                                        <input id="contactAddress" type="text" class="input-register-warehouse" readonly="readonly">
                                        
                                </div>
                            </div> -->
                                <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                    <div class="form-group">
                                    <label for="depart" style="color:black;">Depart *</label>
                                        <select id="depart" class="input-register-warehouse">
                                                <?php  

                                                    foreach ($allDeparts as $key => $value) {
                                                        $opt = '';
                                                        if ($value['id'] == 1) {
                                                            $opt = 'selected';
                                                        }
                                                ?>
                                                        <option <?php echo $opt; ?> value="<?php echo $value['id']; ?>"><?php echo $value['nombre']; ?></option>
                                                <?php
                                                    }

                                                ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <div class="form-group">
                                    <label for="currier" style="color:black;">Currier *</label>
                                        
                                            <select id="currier" class="input-register-warehouse">
                                                <?php  
                                                    
                                                    foreach ($curriers as $key => $value) {
                                                        $opt = '';
                                                        if ($zoneCurrier[0]['id'] == $value['id']) {
                                                            $opt = 'selected';
                                                        }
                                                ?>
                                                        <option <?php echo $opt; ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                <?php
                                                    }

                                                ?>
                                            </select>
                                </div>
                            </div>
                        
                        
                        <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                            <div class="form-group">
                                <label for="wstate" style="color:black;">Estado *</label>
                                    <select id="wState" class="input-register-warehouse">
                                        <?php  

                                            foreach ($warehouseStates as $key => $value) {
                                        ?>
                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                        <?php
                                            }

                                        ?>
                                    </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                            <div class="form-group">
                                <label for="date_send" style="color:black;">Fecha de Envio *</label>
                                    <input type="date" placeholder="Fecha de Envio" class="input-register-warehouse" id="date_send" value="<?php echo $nextFriday; ?>">
                            </div>
                        </div>
                    </div>                      

                </div><br>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-11 col-lg-11 col-xl-11">
                                <div align="center" style="background:#e3e3e3ff; color:#005574; width:100%; padding:6px;"><h4>Agregar Cajas</h4></div>
                            </div>
                            <div class="col-sm-12 col-md-1 col-lg-1 col-xl-1">
                                <button id="addBoxButton" onclick="addBox();" class="btn-cargar-cajas"><i class="fas fa-plus"></i> Agregar</button>
                            </div>
                        </div>
                        <br>
                            <div class="row">
                            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                            <div class="form-group">
                                                <label for="box-code">Número de caja</label>
                                                    <input id="box-code" type="text" placeholder="Codigo" class="form-control" value="<?php echo $boxCode; ?>" readonly="readonly">
                                            </div>
                                        </div>

                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                    <input id="box-id" type="hidden" value="0">
                                        <div class="form-group">
                                            <label for="box-name">BOX</label>
                                                <input onkeyup="searchBoxes();" id="box-name" type="text" placeholder="Caja" class="form-control">
                                                <ul id="myBOX">
                                                </ul>
                                        </div>
                                    </div>
                                   
                                        <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                            <div class="form-group">
                                                <label for="box-height">Alto *</label>
                                                    <input id="box-height" type="number" placeholder="Alto" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                            <div class="form-group">
                                                <label for="box-width">Ancho *</label>
                                                
                                                    <input id="box-width" type="number" placeholder="Ancho" class="form-control">
                                                
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                            <div class="form-group">
                                                <label for="box-lenght">Largo *</label>
                                                
                                                    <input id="box-lenght" type="number" placeholder="Largo" class="form-control">
                                                
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                            <div class="form-group">
                                                <label for="box-weight">Peso *</label>
                                                
                                                    <input id="box-weight" type="number" placeholder="Peso" class="form-control">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">    
                                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">

                                            <div class="form-group">
                                                <label for="category">Categoria *</label>
                                                
                                                    <select id="category" class="form-control">
                                                        <?php  

                                                            foreach ($categories as $key => $value) {
                                                        ?>
                                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                        <?php
                                                        }

                                                        ?>
                                                    </select>
                                                
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <div class="form-group mb-4">
                                                <label class="col-sm-12">Costo *</label>
                                                
                                                    <input id="box-cost" type="number" placeholder="Costo" class="form-control">
                                                
                                            </div>
                                        </div>
                                    
                                    
                                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <div class="form-group">
                                                <label for="category">Descripción*</label>
                                        
                                                <input id="box-description" type="text" placeholder="Descripcion" class="form-control">
                                            </div>
                                        </div>
                                        
                                    </div>    
                                
                                <div class="col-md-4">
                                    <div id="wboxes" class="card" style="display: none;">
                                        <div class="card-body">
                                            <h3 class="text-center">
                                                Cajas
                                            </h3>
                                            <ul id="myregisterBoxes">
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
            <footer class="footer text-center"> 2021 © Total Envios
                </footer>
                <?php 

            include 'foot.php';

        ?>
        <?php 

            if ($bulk != '0') {
            ?>
                <input id="packagesForBulk" type="hidden" value="1">
            <?php
                echo '<script type="text/javascript">'
                . '$( document ).ready(function() {'
                . '$("#warehouseModal").modal("show");'
                . 'getBulkPackages();'
                . '});'
                . '</script>';
            }else{
            ?>
                <input id="packagesForBulk" type="hidden" value="0">
            <?php
            }
        ?>
        <script>
            $(document).ready(function () {
                localStorage.removeItem('boxes');
            })
        </script>

            </body>

    </html>