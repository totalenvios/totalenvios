<?php  
            // error_reporting(0);
            // ini_set('display_errors', 0);
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
            
            include 'includes/connection.class.php';
            include 'includes/users.class.php';
            include 'includes/packages.class.php';
            include 'includes/headquarters.class.php';
            include 'includes/stock.class.php';

            
            $users = new Users;
            $allUsers = $users->findAll();
            $packages = new Packages;
            $allPackages = $packages->findAll();
            $headquarters = new Headquarters;
            $allHeadquarters = $headquarters->findAll();
            $allOperators = $users->findAllOperators();
            $allClients = $users->findAllClients();
            $states = $packages->findStates();

            $stock = new Stock;

            session_start();

            if ($_SESSION['state'] != 1) {
                header('Location: login.php');
            }

            $dataUser = $users->findByEmail($_SESSION['email']);
            $date_in = date('2020-m-d');
            $date_out = date('Y-m-d h:m:s');
            $allTransp = $packages->findAllTransp();

            $digits = 5;
            $fecha = new DateTime();
            $dataFecha = $fecha->getTimestamp();
            $code = 'TE-P'.substr($dataFecha, -5);


        ?>

        <!DOCTYPE html>
        <html dir="ltr" lang="en">

        <head>
            <?php 

                include 'head.php';

            ?>
        </head>

        <body>
        <input id="idUser" type="hidden" value="0">
        <input id="idUserFor" type="hidden" value="0">
        <input id="code" type="hidden" value="<?php echo $code; ?>">
            <!-- Modal -->
        <div class="modal fade" id="packageModal" tabindex="-1" role="dialog" aria-labelledby="packageModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="packageModalLabel">Paquete:</h5>
                    <button onclick="$('#packageModal').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="alert alert-danger register-error" role="alert">
                                </div>
                                <div class="alert alert-success register-success" role="alert">
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group mb-4">
                                                    <!-- <label class="col-md-12 p-0">Cliente</label> -->
                                                    <div class="col-md-12 border-bottom p-0">
                                                        <input onkeyup="searchUserPackage();" id="user_from" class="form-control" type="text" value="" placeholder="Usuario:">
                                                        <ul id="myUS">
                                                        </ul>
                                                        <input style="display: none;" id="contactPhone" type="text" class="form-control" readonly="readonly" value="0">
                                                        <input style="display: none;" id="contactAddress" type="text" class="form-control" readonly="readonly" value="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <button onclick="$('#packageModal').modal('hide'); $('#newUserModal').modal('show');" class="btn btn-primary">Registrar</button>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group mb-4">
                                                    <label class="col-md-12 p-0">Tracking</label>
                                                    <div class="col-md-12 border-bottom p-0">
                                                        <input onkeyup="searchForAlert();" id="tracking" type="text" placeholder="Tracking"
                                                            class="form-control p-0 border-0"> </div>
                                                        <ul id="myAlert">
                                                        </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                
                                                <div class="form-group mb-4">
                                                    <label class="col-md-12 p-0">Alias del paquete</label>
                                                    <div class="col-md-12 border-bottom p-0">
                                                        <input id="alias" type="text" placeholder="Alias"
                                                            class="form-control p-0 border-0" value="<?php echo $_SESSION['name']; ?>"> </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                
                                                <div class="form-group mb-4">
                                                    <label class="col-md-12 p-0">Operador</label>
                                                    <div class="col-md-12 border-bottom p-0">
                                                        <select id="operator" class="form-select shadow-none p-0 border-0 form-control-line">
                                                            <option value="0">Seleccionar...</option>
                                                            <?php  
                                                                foreach ($allOperators as $key => $value) {
                                                            ?>
                                                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                            <?php
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group mb-4">
                                                    <label for="example-email" class="col-md-12 p-0">Fecha Entrada *</label>
                                                    <div class="col-md-12 border-bottom p-0">
                                                        <input readonly="readonly" type="text" placeholder="Fecha Entrada"
                                                            class="form-control p-0 border-0" id="date_in" value="<?php echo $date_out; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group mb-4">
                                                    <label class="col-md-12 p-0">Direccion - Entrega *</label>
                                                    <div class="col-md-12 border-bottom p-0">
                                                        <!-- <input id="address_send" type="text" placeholder="Entrega" class="form-control p-0 border-0" value="<?php echo $dataUser['address_send']; ?>">  -->
                                                        <input id="address_send" type="text" placeholder="Entrega" class="form-control p-0 border-0" value=""> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0">Descripcion</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <input id="description" type="text" placeholder="Descripcion" class="form-control p-0 border-0"> </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group mb-4">
                                                    <label class="col-sm-12">Alto</label>
                                                    <div class="col-sm-12 border-bottom">
                                                        <input id="height" type="number" placeholder="Alto" class="form-control p-0 border-0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group mb-4">
                                                    <label class="col-sm-12">Largo</label>
                                                    <div class="col-sm-12 border-bottom">
                                                        <input id="lenght" type="number" placeholder="Largo" class="form-control p-0 border-0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group mb-4">
                                                    <label class="col-sm-12">Ancho</label>
                                                    <div class="col-sm-12 border-bottom">
                                                        <input id="width" type="number" placeholder="Ancho" class="form-control p-0 border-0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group mb-4">
                                                    <label class="col-sm-12">Peso</label>
                                                    <div class="col-sm-12 border-bottom">
                                                        <input id="weight" type="number" placeholder="Peso" class="form-control p-0 border-0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group mb-4">
                                                    <label class="col-sm-12">Transportista *</label>
                                                    <div class="col-sm-12 border-bottom">
                                                        <select id="transp" class="form-select shadow-none p-0 border-0 form-control-line">
                                                            <option value="0">Seleccionar...</option>
                                                            <?php  
                                                                foreach ($allTransp as $key => $value) {
                                                            ?>
                                                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                            <?php
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <form enctype="multipart/form-data" id="formuploadajax" method="post" class="upload-form">
                                                    <span class="upload-span-screenshot">ADJUNTA UNA IMAGEN:</span>
                                                    <span class="upload-span-screenshot">5 MB maximo. JPG, PNG, JPEG:</span>
                                                    <input class="btn btn-primary" type="file" id="paquete" name="paquete"/>
                                                </form>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="text-center">
                                            <div class="form-group mb-4">
                                                <div class="col-sm-12">
                                                    <button onclick="addPackage();" class="btn btn-success">Registrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>
        
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <div id="main-wrapper" data-header-position="absolute" data-boxed-layout="full">
            <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
                }

            ?>
            
            <div class="page-wrapper">
              
                <div class="container-fluid">
                    <!-- <div class="row justify-content-center"> -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                
                                <div class_="col-lg-10 col-xlg-10 col-md-10">
                                    <div class="row">
                                    <!-- <div class="col-md-1"> -->
                                <!-- <div class="col-md-2" id="createBulkP">
                                    <button onclick="bulkPackages();" class="btn btn-primary">Warehouse</button>
                                </div> -->
                            <!-- </div> -->
                            <div class="col-md-2">
                                <label for="">Cliente:</label>
                                <div class="input-group mb-3">
                                  <input id="myInput" onkeyup="searchFilter();" type="text" placeholder="Nombre..." class="form-control mt-0">
                                  <ul id="myUL">
                                  </ul>
                                </div>
                            </div>
                            <!-- <div class="col-md-1">
                                <label for="">Estado:</label>
                                 <select id="filterStates" class="form-select shadow-none p-0 border-0 form-control-line">
                                    <option value="0">Seleccionar...</option>
                                    <?php  
                                        foreach ($states as $key => $value) {
                                    ?>
                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-1">
                                <label for="">Tracking:</label>
                                <input onkeyup="searchForAlertF();" id="trackingF" type="text" placeholder="Tracking" class="form-control">
                                <ul id="myAlertF">
                                </ul>
                            </div>
                            <div class="col-md-1">
                                <label for="">Transportista:</label>
                                 <select id="filterTransp" class="form-select shadow-none p-0 border-0 form-control-line">
                                    <option value="0">Seleccionar...</option>
                                    <?php  
                                        foreach ($allTransp as $key => $value) {
                                    ?>
                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-1">
                                <label for="">Operador:</label>
                                <select id="filterOperator" class="form-select shadow-none p-0 form-control-line">
                                    <option value="0">Seleccionar...</option>
                                    <?php  
                                        foreach ($allOperators as $key => $value) {
                                    ?>
                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div> -->
                            <div class="col-md-1"></div>
                            <div class="col-md-2">
                                <label for="">Fecha Inicio:</label>
                                <input id="date_in_s" type="date" class="form-control" value="<?php echo $date_in; ?>">
                            </div>
                            <div class="col-md-2">
                                <label for="">Fecha Final:</label>
                                <input id="date_out" type="date" class="form-control" value="<?php echo $date_out; ?>">
                            </div>
                            <div class="col-md-1">
                               
                                
                                <button id="filtrar" onclick="searchPackage();" class="btn btn-primary" style="margin-top:30%; border:2px solid #005574; color:#005574; background-color:white;"><i class="fas fa-check"></i> Filtrar</button>
                                
                            </div>
                                        
                                        
                                    </div>
                                
                                <hr style="background-color:#005574;">
                            </div>
                            <div class="row">
                           
                           <div class="col-sm-9 col-md-9 col-lg-9">
                               <div align="left" style="background:white; color:#005574; width:100%; padding:8px;"><h4>Almacen</h4></div>
                           </div><br>
                           <div class="col-sm-3 col-md-3 col-lg-3" style="text-align:right; content-align:right;">
                               <div class="btn-group" align="right">
                               <a href="#" class="btn btn-primary" onclick="$('#packageModal').modal('show');" class="btn btn-primary" style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i style="color:white;" class="fas fa-plus"></i> Registrar</a>
                               <a onclick="bulkPackages();" class="btn btn-primary" style="background-color:white; color:#ff554dff; border:2px solid #ff554dff;"><i style="color:#ff554dff;" class="fas fa-check"></i> warehouse</a>
                               </div>
                           
                               <!-- <a align="right" href="registerWarehouse.php" class="btn" style="background-color:#ff554dff; color:white;"><i style="color:white;" class="fas fa-plus"></i> Crear</a> -->
                               
                           </div>
                        </div>
                        
                          
                       </div>
                       <br>
                       <div class="row">
                                <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                                <div class_="col-lg-10 col-xlg-10 col-md-10">

                        <table class="table packages-table" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                            <thead>
                                <th style="border: 2px solid #e3e3e3ff;"></th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Estado</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Nro. paquete</th>       
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Tracking</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Ingreso</th>
                                <!-- <th>Casillero</th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Destino</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Región</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Proveedor</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Descripcion</th>
                                <!-- <th>Almacen</th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Operador</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Pre - Alerta</th>
                                
                                <!-- <th>Imagen</th> -->
                                <!-- <th>Accion</th> -->
                            </thead>
                            <tbody>
                                <?php  

                                    foreach ($allPackages as $key => $value) {
                                ?>
                                        <tr>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><input onclick="countPChecks();" type="checkbox" class="form-check-input pcheck pcheck<?php echo $value['id'];?>" id="pcheck<?php echo $value['id'];?>" package="<?php echo $value['id'];?>"></td>
                                            <!-- <td><?php echo $value['id']; ?></td> -->
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['code']; ?></td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php 
                                                    if ($value['tracking'] == null) {
                                                        echo "N/A";
                                                    }else{
                                                        echo $value['tracking'];; 
                                                    }
                                                    
                                                ?>
                                            </td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['date_in']; ?></td>
                                            <!-- <td><?php echo $value['code']; ?></td> -->
                                             <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $users->findById($value['user_from'])['name'].' '.$users->findById($value['user_from'])['lastname']; ?></td>
                                             <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['alias']; ?></td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['description']; ?></td>
                                            <!-- <td>
                                                <?php 
                                                    if ($value['id_stock'] == 0) {
                                                        echo "No tiene Almacen";
                                                    }else{
                                                        echo $stock->findById($value['id_stock'])['name']; 
                                                    }
                                                    
                                                ?>
                                            </td> -->
                                           
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php 
                                                    if ($value['operator'] == 0) {
                                                        echo "No tiene operador";
                                                    }else{
                                                        echo $users->findById($value['operator'])['name']; 
                                                    }
                                                    
                                                ?>
                                            </td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php  

                                                    $packages->findByAlert($value['tracking'])['id'];

                                                ?>
                                            </td>
                                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                <?php 
                                                    echo $packages->findStateById($value['state'])['name'];
                                                ?>
                                            </td>
                                            <!-- <td><a href="assets/img/paquetes/<?php echo $value['img'];?>"><img class="pack-img" src="assets/img/paquetes/<?php echo $value['img'];?>" alt=""></a> </td> -->
                                            <!-- <td><button class="btn btn-primary"><i class="fas fa-eye"></i></button></td> -->
                                        </tr>
                                <?php
                                    }

                                ?>
                            </tbody>
                        </table>
                        </div>
                        <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                        </div>
                    </div>
                </div>
                <footer class="footer text-center"> 2021 © Total Envios
                </footer>
            </div>
            
        </div>
        <?php 

            include 'foot.php';

        ?>
        </body>

        </html>