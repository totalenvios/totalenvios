<?php  
            // error_reporting(0);
            // ini_set('display_errors', 0);
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
            
            include 'includes/connection.class.php';
            include 'includes/users.class.php';
            include 'includes/packages.class.php';
            include 'includes/headquarters.class.php';
            include 'includes/stock.class.php';
            include 'includes/transport.class.php';
            include 'includes/warehouses.class.php';

            
            $users = new Users;
            $transp = new Transport;
            $allUsers = $users->findAll();
            $packages = new Packages;
            $warehouses = new Warehouses;

            $trips = $warehouses->findTrips();

            $allPackagesForConfirmed = $packages->findAll();
            $allPackagesConfirmed = $packages->findAllConfirmed();
            $countpackageConfirmed = $packages->countConfirmed()['total_paquetes'];
            $countpackageNoConfirmed = $packages->countAll()['total_paquetes'];
            $countpackageIncomplete = $packages->countIncomplete()['total_paquetes'];
            $countpackageAlert = $packages->countAlerts()['alerts'];

            $headquarters = new Headquarters;
            $allHeadquarters = $headquarters->findAll();
            $allOperators = $users->findAllOperators();
            $allClients = $users->findAllClients();
            $states = $packages->findStates();

            $stock = new Stock;

            session_start();

            if ($_SESSION['state'] != 1) {
                header('Location: login.php');
            }

            $dataUser = $users->findByEmail($_SESSION['email']);
            $date_in = date('2020-m-d');
            $date_test = strtotime($date_in);
            $date_out = date('m-d-Y');
            $fechaActual = date('Y-m-d H:i:s');
            $allTransp = $packages->findAllTransp();

            $digits = 5;
            $fecha = new DateTime();
            $dataFecha = $fecha->getTimestamp();
            $code = 'TE-PK'.substr($dataFecha, -5);


        ?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

                include 'head.php';

            ?>
</head>

<body>
    <input id="idUser" type="hidden" value="0">
    <input id="idUserFor" type="hidden" value="0">
    <input id="code" type="hidden" value="<?php echo $code; ?>">

    <div class="modal fade" id="confirmarEnvioModal" tabindex="-1" role="dialog" aria-labelledby="packageModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="packageModalLabel">CONFIRMAR ENVÍO DEL PAQUETE</h5>
                    <button onclick="$('#packageModal').modal('hide');" type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="alert alert-danger register-error" role="alert">
                                </div>
                                <div class="alert alert-success register-success" role="alert">
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <input type="text" value="" id="package_id_modal" hidden>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="reempaque"
                                                style="width:20px;height:20px; padding:7px;">
                                            <label class="form-check-label" for="flexCheckDefault"
                                                style="padding-left:7px; padding-top:8px;">
                                                REEMPAQUE Y CONSOLIDADO
                                            </label>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label for="contact_phone_destiner">TIPO DE ENVIO</label>

                                            <select id="trip_id_package" class="form-select campos-input-login">
                                                <option value="0">Seleccione
                                                </option>
                                                <?php  
                                                foreach ( $trips as $key => $value) {
                                                
                                                ?>
                                                <option value="<?php echo $value['id']; ?>">
                                                    <?php echo $value['name']; ?></option>
                                                <?php
                                                    }                                                             
                                                    ?> }
                                                ?>
                                            </select>

                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="3" id="insurance"
                                                style="width:20px;height:20px; padding:7px;">
                                            <label class="form-check-label" for="flexCheckDefault"
                                                style="padding-left:7px;  padding-top:8px;">
                                                PAQUETE ASEGURADO
                                            </label>
                                        </div>


                                        <div class="text-center">
                                            <div class="form-group mb-4">
                                                <div class="col-sm-12">
                                                    <button onclick="confirmarEnvio();"
                                                        class="boton-total-envios-registro">CONFIRMAR</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="packageModal" tabindex="-1" role="dialog" aria-labelledby="packageModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="packageModalLabel">REGISTRAR PAQUETE</h5>
                    <button onclick="$('#packageModal').modal('hide');" type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="alert alert-danger register-error" role="alert">
                                </div>
                                <div class="alert alert-success register-success" role="alert">
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-11 col-xlg-11 col-md-11">
                                                <div class="form-group">
                                                    <input id="alert_id" type="text" placeholder="alert_id" value="0"
                                                        class="form-control campos-input-login" hidden>

                                                    <!-- <label class="col-md-12 p-0">Tracking</label> -->
                                                    <label class="col-sm-12">TRACKING</label>
                                                    <input id="tracking" type="text" placeholder="Tracking"
                                                        class="form-control campos-input-login">
                                                    <ul id="myAlert">
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-1 col-xlg-1 col-md-1 pt-4">
                                                <button class="btn btn-primary" onclick="findTracking();"
                                                    style="background-color:#005574; color:white; border:2px solid #005574;"><i
                                                        style="color:white;" class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                        <div id="resultado_tracking">

                                        </div>
                                        <div class="row">
                                            <div class="col-lg-5 col-xlg-5 col-md-5">
                                                <div class="form-group">
                                                    <!-- <label for="user_from" style="color:black;">Nombre/casillero</label> -->
                                                    <div class="input-group mb-3">
                                                        <label class="col-sm-12">CLIENTE</label>
                                                        <input id="id_user_from" hidden>
                                                        <input type="text" class="form-control" placeholder="Nombre"
                                                            aria-label="Recipient's username"
                                                            aria-describedby="basic-addon2"
                                                            onkeyup="searchUserPackage();" id="user_from">

                                                        <div class="input-group-append">
                                                            <button
                                                                onclick="$('#packageModal').modal('hide'); $('#newUserModal').modal('show');"
                                                                class="btn btn-outline-secondary"
                                                                style="background-color:#ff554dff; color:white;"><i
                                                                    style="color:white"
                                                                    class="fas fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                    <ul id="myUS">
                                                    </ul>

                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-xlg-5 col-md-5">
                                                <div class="form-group">
                                                    <!-- <label class="col-md-12 p-0">Alias del paquete</label> -->
                                                    <label class="col-sm-12">NOMBRE DEL PAQUETE</label>
                                                    <input id="alias" type="text" placeholder="Nombre del paquete"
                                                        class="form-control campos-input-login" value="">
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-xlg-2 col-md-2">
                                                <div class="form-group">
                                                    <!-- <label class="col-md-12 p-0">Alias del paquete</label> -->
                                                    <label class="col-sm-12">CASILLERO</label>
                                                    <input id="casillero_paquete" type="text" placeholder="Casillero"
                                                        class="form-control campos-input-login" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <label class="col-sm-12">TRANSPORTISTA</label>

                                                    <select id="transp" class="form-select campos-input-login">
                                                        <option value="0">Seleccionar...</option>
                                                        <?php  
                                                                foreach ($allTransp as $key => $value) {
                                                            ?>
                                                        <option value="<?php echo $value['id']; ?>">
                                                            <?php echo $value['name']; ?></option>
                                                        <?php
                                                                }
                                                            ?>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <!-- <label class="col-md-12 p-0">Alias del paquete</label> -->
                                                    <label class="col-sm-12">PROVEEDOR</label>
                                                    <input id="proveedor_paquete" type="text" placeholder="Proveedor"
                                                        class="form-control campos-input-login" value="">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-xlg-4 col-md-4" hidden>
                                                <div class="form-group">
                                                    <label class="col-md-12 p-0">Operador_id</label>

                                                    <input id="operator_id" type="text" placeholder="Alias"
                                                        class="form-control campos-input-login"
                                                        value="<?php echo $users->findforCode($_SESSION['code'])[0]['id']; ?>"
                                                        readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <label class="col-md-12 p-0">OPERADOR</label>

                                                    <input id="operator" type="text" placeholder="Alias"
                                                        class="form-control campos-input-login"
                                                        value="<?php echo $_SESSION['name']; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <!-- <label for="example-email" class="col-md-12 p-0">Fecha Entrada *</label> -->
                                                    <label class="col-sm-12">FECHA DE REGISTRO</label>
                                                    <input readonly="readonly" type="text" placeholder="Fecha Entrada"
                                                        class="form-control campos-input-login" id="date_in"
                                                        value="<?php echo $date_out; ?>">

                                                    <input id="fecha_registro" value=" <?php echo $fechaActual; ?>"
                                                        hidden>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-xlg-2 col-md-2">
                                                <div class="form-group">
                                                    <label class="col-sm-12">COSTO</label>

                                                    <input id="cost" type="number" placeholder="Costo"
                                                        class="form-control campos-input-login">

                                                </div>
                                            </div>
                                            <div class="col-lg-10 col-xlg-10 col-md-10">
                                                <div class="form-group">
                                                    <label class="col-md-12 p-0">CONTENIDO</label>

                                                    <input id="description" type="text" placeholder="Descripcion"
                                                        class="form-control campos-input-login"> </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <label class="col-sm-12">ALTO</label>

                                                    <input id="height" type="number" placeholder="Alto"
                                                        class="form-control campos-input-login">

                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <label class="col-sm-12">LARGO</label>

                                                    <input id="lenght" type="number" placeholder="Largo"
                                                        class="form-control campos-input-login">

                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <label class="col-sm-12">ANCHO</label>

                                                    <input id="width" type="number" placeholder="Ancho"
                                                        class="form-control campos-input-login">

                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <label class="col-sm-12">PESO</label>

                                                    <input id="weight" type="number" placeholder="LB"
                                                        class="form-control campos-input-login">

                                                </div>
                                            </div>
                                        </div>
                                        <div class=row>



                                            <div class="col-lg-4 col-xlg-4 col-md-4">
                                                <form enctype="multipart/form-data" id="formuploadajax" method="post"
                                                    class="upload-form">
                                                    <span class="upload-span-screenshot">ADJUNTA UNA IMAGEN:</span>
                                                    <span class="upload-span-screenshot">5 MB maximo. JPG, PNG,
                                                        JPEG:</span>
                                                    <input class="btn btn-primary" type="file"
                                                        style="width:100%; background:#005574;" id="paquete"
                                                        name="paquete" />
                                                </form>
                                            </div>
                                            <div class="col-lg-5 col-xlg-5 col-md-5" hidden>
                                                <div class="form-group">
                                                    <label class="col-md-12 p-0">Direccion - Entrega *</label>

                                                    <!-- <input id="address_send" type="text" placeholder="Entrega" class="form-control p-0 border-0" value="<?php echo $dataUser['address_send']; ?>">  -->
                                                    <input id="address_send" type="text" placeholder="Entrega"
                                                        class="form-control campos-input-login" value="">

                                                </div>
                                            </div>
                                        </div>









                                        <br>
                                        <div class="text-center">
                                            <div class="form-group mb-4">
                                                <div class="col-sm-12">
                                                    <button onclick="addPackage();"
                                                        class="boton-total-envios-registro">Registrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- **************************************************************************************** -->
    <div class="modal fade" id="newUserModal" tabindex="-1" role="dialog" aria-labelledby="newUserModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="newUserModalLabel">Registro de nuevo cliente</h5>
                    <button onclick="$('#newUserModal').modal('hide');" type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 col-md-12">

                                <div class="alert alert-danger user-error" role="alert">
                                </div>
                                <div class="alert alert-success user-success" role="alert">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">

                                    </div>
                                    <div class="col-sm-12 col-md-5 col-lg-3 col-xl-5"></div>

                                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4"
                                        style="text-align:right; content-align:right;">
                                        <div class="btn-group" align="right">
                                            <a id="muchosDestinatarios" onclick="salirRegistro();"
                                                class="btn btn-primary"
                                                style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i
                                                    style="color:white;" class="fas fa-close"></i> Salir</a>
                                            <a id="boton-destinatario" onclick="AgregarDest();" class="btn btn-primary"
                                                style="background-color:white; color:#ff554dff; border:2px solid #ff554dff;"
                                                disabled><i style="color:#ff554dff;" class="fas fa-plus"></i> Registrar
                                                destinatario</a>
                                        </div>

                                    </div>
                                </div><br>
                                <div class="card" id="card-registro-usuarios-wh">
                                    <div class="card-body">
                                        <div align="center"
                                            style="background-color:#005574; color:white; width:100%; padding:8px;">
                                            <h4>Nuevo Usuario</h4>
                                        </div><br>
                                        <div class="row">
                                            <input id="user_id" type="text" placeholder="Nombre Completo"
                                                class="form-control" hidden>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label class="user_type_wh">Tipo de usuario *</label>

                                                    <select readonly="readonly" id="user_type_wh" class="form-select">
                                                        <option selected="selected" value="2">Cliente</option>
                                                        <option value="5">Empresa</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="name_wh">Nombre *</label>

                                                    <input id="name_wh" type="text" placeholder="Nombre Completo"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="lastname_wh">Apellido *</label>

                                                    <input id="lastname_wh" type="text" placeholder="Apellido Completo"
                                                        class="form-control">
                                                </div>
                                            </div>


                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="document_wh">Documento *</label>

                                                    <input id="document_wh" type="text" placeholder="# Documento"
                                                        class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">

                                                <div class="form-group">
                                                    <label for="company_email_wh">Correo electrónico</label>
                                                    <input type="email" placeholder="example@email.com"
                                                        class="campos-input-login" name="example-email" id="email_wh">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="phone_wh">Teléfono *</label>

                                                    <input id="phone_wh" type="text" placeholder="123 456 7890"
                                                        class="form-control">

                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="phone_house_wh">Teléfono de casa</label>

                                                    <input id="phone_house_wh" type="text" placeholder="123 456 7890"
                                                        class="form-control">

                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="date_wh" class="col-md-12 p-0">Fecha de Nacimiento
                                                        *</label>
                                                    <input type="date" placeholder="1990-01-01" class="form-control"
                                                        id="born_date_wh">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="address_destiny_company">Pais</label>
                                                    <select id="address_destiny_company"
                                                        class="form-select campos-input-login">
                                                        <option value="0">País</option>
                                                        <?php  
                                                                                                    foreach ($allCountries as $key => $value) {
                                                                                                ?>
                                                        <option value="<?php echo $value['id']; ?>">
                                                            <?php echo $value['name']; ?></option>
                                                        <?php
                                                                                                 }
                                                                                                ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 address_state_company">
                                                <div class="form-group">
                                                    <label for="address_state_company">Estado</label>
                                                    <input id="address_state_company" type="text" placeholder="Estado"
                                                        class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 select_state_company"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_state_company">Estado</label>
                                                    <select id="select_state_company"
                                                        class="form-select campos-input-login">
                                                        <option value="0">Estado</option>
                                                        <?php  
                                                                                                    foreach ($allAddressStates as $key => $value) {
                                                                                                ?>
                                                        <option value="<?php echo $value['id_estado']; ?>">
                                                            <?php echo $value['estado']; ?></option>
                                                        <?php
                                                                                                }
                                                                                                ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 address_city_company">
                                                <div class="form-group">
                                                    <label for="address_city_company">Ciudad</label>
                                                    <input id="address_city_company" type="text" placeholder="Ciudad"
                                                        class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 select_city_company"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_city_company">Ciudad</label>
                                                    <select id="select_city_company"
                                                        class="form-select campos-input-login">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div
                                                class="col-sm-10 col-md-10 col-lg-10 col-xl-10 address_address_company">
                                                <div class="form-group">
                                                    <label for="address_company">Dirección</label>
                                                    <input id="address_company" type="text" placeholder="Dirección"
                                                        class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10 select_address_company"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_address_company">Dirección</label>
                                                    <input id="select_address_company" type="text"
                                                        placeholder="Dirección" class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                                <label for="select_address_company">Registrar usuario</label>
                                                <a id="muchosDestinatarios" onclick="RegUsuarioWarehouse(1);"
                                                    class="btn btn-primary"
                                                    style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i
                                                        style="color:white;" class="fas fa-save"></i> Guardar</a>
                                            </div>

                                        </div>








                                    </div>
                                </div>
                            </div>

                            <div id="registrarDestinatario" style="display:none;">
                                <!-- registro de destinatario oculto hasta completar registro -->
                                <div class="card">
                                    <div class="card-body">
                                        <div align="center"
                                            style="background:#005574; color:white; width:100%; padding:8px;">
                                            <h4>Nuevo destinatario</h4>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="name_destiner">Nombre *</label>

                                                    <input id="name_destiner_user" type="text"
                                                        placeholder="Nombre Completo" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="lastname_destiner">Apellido *</label>

                                                    <input id="lastname_destiner_user" type="text"
                                                        placeholder="Apellido Completo" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="contact_phone_destiner">Telefono de contacto</label>

                                                    <input type="text" placeholder="123 456 7890" class="form-control"
                                                        id="contact_phone_destiner_user">

                                                </div>
                                            </div>
                                        </div>
                                        <input id="countryVenezuela" type="hidden" value="0">
                                        <div class="row">

                                            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="address_destiny_destiner">Pais destino</label>
                                                    <select id="address_destiny_destiner"
                                                        class="form-select campos-input-login">
                                                        <option value="0">¿Cuál es destino?</option>
                                                        <?php  
                                                                                        foreach ($allCountries as $key => $value) {
                                                                                    ?>
                                                        <option value="<?php echo $value['id']; ?>">
                                                            <?php echo $value['name']; ?></option>
                                                        <?php
                                                                                        }
                                                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 address_state_destiner">
                                                <div class="form-group">
                                                    <label for="address_state_destiner">Estado</label>
                                                    <input id="address_state_destiner" type="text" placeholder="Estado"
                                                        class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 select_state_destiner"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_state_destiner">Estado</label>
                                                    <select id="select_state_destiner"
                                                        class="form-select campos-input-login">
                                                        <option value="0">¿Cuál estado?</option>
                                                        <?php  
                                                                                        foreach ($allAddressStates as $key => $value) {
                                                                                    ?>
                                                        <option value="<?php echo $value['id_estado']; ?>">
                                                            <?php echo $value['estado']; ?></option>
                                                        <?php
                                                                                        }
                                                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 address_city_destiner">
                                                <div class="form-group">
                                                    <label for="address_city_destiner">Ciudad</label>
                                                    <input id="address_city_destiner" type="text" placeholder="Ciudad"
                                                        class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 select_city_destiner"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_city_destiner">Ciudad</label>
                                                    <select id="select_city_destiner"
                                                        class="form-select campos-input-login">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div
                                                class="col-sm-12 col-md-12 col-lg-12 col-xl-12 address_address_destiner">
                                                <div class="form-group">
                                                    <label for="address_Destiner">Dirección</label>
                                                    <input id="address_destiner" type="text" placeholder="Dirección"
                                                        class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 select_address_destiner"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_address_destiner">Dirección</label>
                                                    <input id="select_address_destiner" type="text"
                                                        placeholder="Dirección" class="form-control campos-input-login">
                                                </div>
                                            </div>

                                            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                                <label for="select_address_company">Registrar nuevo destinatario</label>
                                                <a id="muchosDestinatarios" onclick="RegDestinerForUserWarehouse();"
                                                    class="btn btn-primary"
                                                    style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i
                                                        style="color:white;" class="fas fa-save"></i> Guardar</a>
                                            </div>
                                        </div>





                                    </div>
                                </div>
                            </div>
                            <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <!-- ******************************************************************************************* -->

    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
        <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
                }

            ?>



        <div class="page-wrapper" style="background:#e3e3e3ff;">

            <div class="row">
                <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                    <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                </div>
                <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                    <h3 style="color:#005574;"> <strong>PAQUETES</strong> </h3>
                </div>
            </div><br>
            <div class="row">

                <div class="col-lg-3 col-xlg-3 col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="icons/paquete-alert.png" alt="" width="100" height="100">
                                </div>
                                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <h5 style="color:#005574;"><strong>PREALERTAS PENDIENTES</strong></h5>
                                    <h1 style="color:#005574;"><?php echo $countpackageAlert; ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xlg-3 col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="icons/paquete-confirmed.png" alt="" width="100" height="100">
                                </div>
                                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <h5 style="color:#005574;"><strong>POR ENVIAR</strong></h5>
                                    <h1 style="color:#005574;"><?php echo $countpackageConfirmed; ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xlg-3 col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="icons/paquete-no-confirmed.png" alt="" width="100" height="100">
                                </div>
                                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <h5 style="color:#005574;"><strong>EN ALMACÉN</strong></h5>
                                    <h1 style="color:#005574;"><?php echo $countpackageNoConfirmed; ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xlg-3 col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                    <img src="icons/paquete-for-asign.png" alt="" width="100" height="100">
                                </div>
                                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                    <h5 style="color:#005574;"><strong>POR ASIGNAR</strong></h5>
                                    <h1 style="color:#005574;"><?php echo $countpackageIncomplete; ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <center>
                        <div class="btn-group text-center" style="content-align:center; width:100%, padding:100px;"
                            align="center">

                            <button class="btn btn-primary" type="button" id="cliente-s" onclick="showAlmacen();"
                                style="background-color:#005574; color:white; border:2px solid #005574; width:300px;">EN
                                ALMACÉN</button>


                            <button class="btn btn-primary" type="button" id="destinatario-s" onclick="showEnvios();"
                                style="background-color:white; color:#005574; border:2px solid #005574; width:300px;">POR
                                ENVIAR</button>

                            <button class="btn btn-primary" type="button" id="asignar-s" onclick="showForAsignar();"
                                style="background-color:white; color:#005574; border:2px solid #005574; width:300px;">POR
                                ASIGNAR</button>




                        </div>
                    </center>
                </div>
            </div>



            <!-- <div class="row justify-content-center"> -->
            <div class="card" id="cliente-table">
                <div class="card-body">
                    <div class="row">

                        <div class_="col-lg-10 col-xlg-10 col-md-10">
                            <div class="row">
                                <div class="col-md-5"></div>
                                <div class="col-md-2">
                                    <label for="">Fecha Inicio:</label>
                                    <input id="date_in_s" type="date" class="form-control"
                                        value="<?php echo $date_in; ?>">
                                </div>
                                <div class="col-md-2">
                                    <label for="">Fecha Final:</label>
                                    <input id="date_out" type="date" class="form-control"
                                        value="<?php echo $date_in; ?>">
                                </div>
                                <div class="col-md-1">


                                    <button id="filtrar" onclick="searchPackage();" class="btn btn-primary"
                                        style="margin-top:30%; border:2px solid #005574; color:#005574; background-color:white;"><i
                                            class="fas fa-check"></i> Filtrar</button>

                                </div>


                            </div>

                            <hr style="background-color:#005574;">
                        </div>
                        <div class="row">

                            <div class="col-sm-9 col-md-9 col-lg-9">
                                <div align="left" style="background:white; color:#005574; width:100%; padding:8px;">
                                    <h6>Paquetes en almacén</h5>
                                </div>
                            </div><br>
                            <div class="col-sm-3 col-md-3 col-lg-3" style="text-align:right; content-align:right;">
                                <div class="btn-group" align="right">
                                    <a href="#" class="btn btn-primary" onclick="$('#packageModal').modal('show');"
                                        class="btn btn-primary"
                                        style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i
                                            style="color:white;" class="fas fa-plus"></i> REGISTRAR</a>

                                </div>

                                <!-- <a align="right" href="registerWarehouse.php" class="btn" style="background-color:#ff554dff; color:white;"><i style="color:white;" class="fas fa-plus"></i> Crear</a> -->

                            </div>
                        </div>


                    </div>
                    <br>
                    <div class="row">
                        <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                        <div class_="col-lg-10 col-xlg-10 col-md-10">

                            <table class="table packages-table"
                                style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                <thead>
                                    <!-- <th style="border: 2px solid #e3e3e3ff;"></th> -->


                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        TRACKING</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        PAQUETE</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">F.
                                        INGRESO</th>
                                    <!-- <th>Casillero</th> -->
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        CASILLERO</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        CLIENTE</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        OPERADOR</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        RECIBIDO POR</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        MEDIDAS (WxHxL)</th>
                                    <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Región</th> 

                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        DESCRIPCIÓN</th>-->
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        COSTO</th>
                                    <!--<th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        TRANSPORTISTA</th>
                                     <th>Almacen</th> -->

                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                        STATUS</th>
                                    <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white;">
                                    </th>
                                    <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Pre - Alerta</th> -->

                                    <!-- <th>Imagen</th> -->
                                    <!-- <th>Accion</th> -->
                                </thead>
                                <tbody>
                                    <?php  

                                    foreach ($allPackagesForConfirmed as $key => $value) {
                                ?>
                                    <tr>
                                        <!-- <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><input onclick="countPChecks();" type="checkbox" class="form-check-input pcheck pcheck<?php echo $value['id'];?>" id="pcheck<?php echo $value['id'];?>" package="<?php echo $value['id'];?>"></td> -->

                                        <!-- <td><?php echo $value['id']; ?></td> -->
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php 
                                                    if ($value['tracking'] == null) {
                                                        echo "N/A";
                                                    }else{
                                                        echo $value['tracking'];; 
                                                    }
                                                    
                                                ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['code']; ?></td>


                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php 
                                                $fechaComoEntero = strtotime($value['date_in']);	
                                                $fecha = date("m/d/Y", $fechaComoEntero);
                                                echo $fecha; 
                                            ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $users->findById($value['user_from'])['code']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $users->findById($value['user_from'])['name']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php 
                                                    if ($value['operator'] == 0) {
                                                        echo "No tiene operador";
                                                    }else{
                                                        echo $users->findById($value['operator'])['name']; 
                                                    }
                                                    
                                                ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            RECIBIDO POR
                                        </td>

                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['width'].' x '.$value['height'].' x '.$value['lenght'].'   PESO'.$value['width'].'LB'; ?>
                                        </td>


                                        <!-- <td><?php echo $value['code']; ?></td> 
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['description']; ?></td>-->
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $value['cost']; ?></td>
                                        <!-- <td>
                                                <?php 
                                                    if ($value['id_stock'] == 0) {
                                                        echo "No tiene Almacen";
                                                    }else{
                                                        echo $stock->findById($value['id_stock'])['name']; 
                                                    }
                                                    
                                                ?>
                                            </td> 


                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php echo $transp->findById($value['id_transp'])['name']; ?></td>-->
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php 
                                                    echo $packages->findStateById($value['state'])['name'];
                                                ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <div class="btn-group">
                                                <button
                                                    style="background-color:#005574; color:white; border:1px solid #005574;"
                                                    onclick="window.open('package.php?id=<?php echo $value['id']; ?>', '_parent');"
                                                    class="btn btn-primary"><i class="fas fa-edit"></i></button>
                                                <button onclick="confirmarEnvioModal(<?php echo $value['id']; ?>);"
                                                    class=" btn
                                                    btn-primary"
                                                    style="background-color:#005574; color:white; border:2px solid #005574;"
                                                    title="confirmar envío"><i class="fas fa-check"></i></button>
                                                <button onclick="deletePackage(<?php echo $value['id']?>)"
                                                    class="btn btn-primary"
                                                    style="background-color:red; color:white; border:1px solid #005574;"><i
                                                        class="fas fa-trash"></i>
                                                </button>
                                                <button
                                                    onclick="window.open('invoceReceive.php?id=<?php echo $value['id']; ?>', '_blank');"
                                                    class="btn btn-primary"
                                                    style="background-color:#005574; color:white; border:2px solid #005574;"><i
                                                        class="fas fa-file"></i></button>

                                            </div>

                                        </td>

                                        <!-- <td><a href="assets/img/paquetes/<?php echo $value['img'];?>"><img class="pack-img" src="assets/img/paquetes/<?php echo $value['img'];?>" alt=""></a> </td> -->
                                        <!-- <td><button class="btn btn-primary"><i class="fas fa-eye"></i></button></td> -->
                                    </tr>
                                    <?php
                                    }

                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-1 col-xlg-12 col-md-1"></div>
                    </div>
                </div>
            </div>
            <div class="card" id="for-asign-package" style="display:none;">
                <div class="card-body">
                    <div class="row">

                        <div class_="col-lg-10 col-xlg-10 col-md-10">
                            <div class="row">

                                <div class="col-md-5"></div>
                                <div class="col-md-2">
                                    <label for="">Fecha Inicio:</label>
                                    <input id="date_in_s" type="date" class="form-control"
                                        value="<?php echo $date_in; ?>">
                                </div>
                                <div class="col-md-2">
                                    <label for="">Fecha Final:</label>
                                    <input id="date_out" type="date" class="form-control"
                                        value="<?php echo $date_in; ?>">
                                </div>
                                <div class="col-md-1">


                                    <button id="filtrar" onclick="searchPackage();" class="btn btn-primary"
                                        style="margin-top:30%; border:2px solid #005574; color:#005574; background-color:white;"><i
                                            class="fas fa-check"></i> Filtrar</button>

                                </div>


                            </div>

                            <hr style="background-color:#005574;">
                        </div>
                        <div class="row">

                            <div class="col-sm-9 col-md-9 col-lg-9">
                                <div align="left" style="background:white; color:#005574; width:100%; padding:8px;">
                                    <h6>Paquetes por asignar</h5>
                                </div>
                            </div><br>


                        </div>
                    </div>
                <br>
                <div class="row">
                    <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                    <div class_="col-lg-10 col-xlg-10 col-md-10">

                        <table class="table packages-table"
                            style="text-align:center; table-layout: fixed; text-layout:fixed;">
                            <thead>
                                <!-- <th style="border: 2px solid #e3e3e3ff;"></th> -->


                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                    TRACKING</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                    PAQUETE</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">F.
                                    INGRESO</th>
                                <!-- <th>Casillero</th> -->
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                    CASILLERO</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                    CLIENTE</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                    OPERADOR</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                    RECIBIDO POR</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                    MEDIDAS (WxHxL)</th>
                                <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Región</th> -->

                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                    DESCRIPCIÓN</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                    COSTO</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                    TRANSPORTISTA</th>
                                <!-- <th>Almacen</th> -->

                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                    STATUS</th>
                                <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                                    ACCIÓN</th>
                                <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Pre - Alerta</th> -->

                                <!-- <th>Imagen</th> -->
                                <!-- <th>Accion</th> -->
                            </thead>
                            <tbody>
                                <?php  

                                    foreach ($allPackagesForConfirmed as $key => $value) {
                                ?>
                                <tr>
                                    <!-- <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><input onclick="countPChecks();" type="checkbox" class="form-check-input pcheck pcheck<?php echo $value['id'];?>" id="pcheck<?php echo $value['id'];?>" package="<?php echo $value['id'];?>"></td> -->

                                    <!-- <td><?php echo $value['id']; ?></td> -->
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        <?php 
                                                    if ($value['tracking'] == null) {
                                                        echo "N/A";
                                                    }else{
                                                        echo $value['tracking'];; 
                                                    }
                                                    
                                                ?>
                                    </td>
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        <?php echo $value['code']; ?></td>


                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        <?php 
                                                $fechaComoEntero = strtotime($value['date_in']);	
                                                $fecha = date("m/d/Y", $fechaComoEntero);
                                                echo $fecha; 
                                            ?>
                                    </td>
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        <?php echo $users->findById($value['user_from'])['code']; ?></td>
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        <?php echo $users->findById($value['user_from'])['name']; ?></td>
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        <?php 
                                                    if ($value['operator'] == 0) {
                                                        echo "No tiene operador";
                                                    }else{
                                                        echo $users->findById($value['operator'])['name']; 
                                                    }
                                                    
                                                ?>
                                    </td>
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        RECIBIDO POR
                                    </td>

                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        <?php echo $value['width'].' x '.$value['height'].' x '.$value['lenght'].'   PESO'.$value['width'].'LB'; ?>
                                    </td>


                                    <!-- <td><?php echo $value['code']; ?></td> -->
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        <?php echo $value['description']; ?></td>
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        <?php echo $value['cost']; ?></td>
                                    <!-- <td>
                                                <?php 
                                                    if ($value['id_stock'] == 0) {
                                                        echo "No tiene Almacen";
                                                    }else{
                                                        echo $stock->findById($value['id_stock'])['name']; 
                                                    }
                                                    
                                                ?>
                                            </td> -->


                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        <?php echo $transp->findById($value['id_transp'])['name']; ?></td>
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        <?php 
                                                    echo $packages->findStateById($value['state'])['name'];
                                                ?>
                                    </td>
                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        <button style="background-color:white; color:#005574; border:2px solid #005574;"
                                            onclick="window.open('package.php?id=<?php echo $value['id']; ?>', '_blank');"
                                            class="btn btn-primary"><i class="fas fa-edit"></i></button><button
                                            onclick="deletePackage(<?php echo $value['id']?>)"
                                            class="btn btn-primary delete-button"
                                            style="background-color:#fc3c3d; color:white; border:2px solid white; margin-left:10px;"><i
                                                class="fas fa-trash"></i></button></td>

                                    <!-- <td><a href="assets/img/paquetes/<?php echo $value['img'];?>"><img class="pack-img" src="assets/img/paquetes/<?php echo $value['img'];?>" alt=""></a> </td> -->
                                    <!-- <td><button class="btn btn-primary"><i class="fas fa-eye"></i></button></td> -->
                                </tr>
                                <?php
                                    }

                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-1 col-xlg-12 col-md-1"></div>
                </div>
            </div>
        </div>
        <div class="card" id="destinatario-table" style="display:none;">
            <div class="card-body">
                <div class="row">

                    <div class_="col-lg-10 col-xlg-10 col-md-10">
                        <div class="row">

                            <div class="col-lg-2 col-xlg-2 col-md-2">
                                <label for="">CLIENTE</label>
                                    <input id="id_user_from" hidden>
                                <input id="myInput" onkeyup="searchFilter();" type="text"
                                    placeholder="nombre del cliente" class="campos-input-login">
                                <ul id="myUL">
                                </ul>

                            </div>
                            <div class="col-lg-2 col-xlg-2 col-md-2">
                                <label for="">OPERADOR</label>
                                <input id="id_operador_name" hidden>
                                <input id="operador_name" onkeyup="searchFilterForOperador();" type="text"
                                    placeholder="Indique el operador" class="campos-input-login">
                                <ul id="myOperador">
                                </ul>

                            </div>
                            <div class="col-lg-2 col-xlg-2 col-md-2">
                                <div class="form-group">
                                    <label for="contact_phone_destiner">TIPO DE ENVÍO</label>

                                    <select id="trip_id_filter" class="form-select campos-input-login">
                                        <option value="0">Seleccione
                                        </option>
                                        <?php  
                                                foreach ( $trips as $key => $value) {
                                                
                                                ?>
                                        <option value="<?php echo $value['id']; ?>">
                                            <?php echo $value['name']; ?></option>
                                        <?php
                                                    }                                                             
                                                    ?> }
                                        ?>
                                    </select>

                                </div>
                            </div>
                            <div class="col-lg-2 col-xlg-2 col-md-2">
                                <label for="">FECHA INICIO</label>
                                <input id="date_in_s" type="date" class="campos-input-login"
                                    value="<?php echo $date_in; ?>">
                            </div>
                            <div class="col-lg-2 col-xlg-2 col-md-2">
                                <label for="">FECHA FINAL</label>
                                <input id="date_out" type="date" class="campos-input-login"
                                    value="<?php echo $date_in; ?>">
                            </div>
                            <div class="col-lg-2 col-xlg-2 col-md-2 pt-4">
                                <div class="btn-group">
                                    <button id="filtrar" onclick="filtrarPaquetesPorEnviar();" class="btn btn-primary"
                                        style="border:2px solid #005574; color:white; background-color:#005574; height:50px;"><i
                                            class="fas fa-check"></i> Filtrar</button>
                                    <button id="filtrar" onclick="reloadPackages();" class="btn btn-primary"
                                        style="border:2px solid #005574; color:#005574; background-color:white; height:50px;"><i
                                            class="fas fa-redo"></i> Recargar</button>

                                </div>



                            </div>



                        </div>

                        <hr style="background-color:#005574;">
                    </div>
               



                    <div class="row">

                    <div class="col-sm-9 col-md-9 col-lg-9">
                        <div align="left" style="background:white; color:#005574; width:100%; padding:8px;">
                            <h4>LISTOS PARA ENVIAR</h4>
                        </div>
                    </div><br>
                    <div class="col-sm-3 col-md-3 col-lg-3" style="text-align:right; content-align:right;">
                        <div class="btn-group" align="right">

                            <button onclick="bulkPackages();" class="btn btn-primary" id="crearWarehouseForPackages"
                                style="background-color:white; color:#ff554dff; border:2px solid #ff554dff; display:none;"><i
                                    style="color:#ff554dff;" class="fas fa-check"></i> CREAR WAREHOUSE</button>
                        </div>

                        <!-- <a align="right" href="registerWarehouse.php" class="btn" style="background-color:#ff554dff; color:white;"><i style="color:white;" class="fas fa-plus"></i> Crear</a> -->

                    </div>
                </div>  
        <br>
        <div class="row">
            <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
            <div class_="col-lg-10 col-xlg-10 col-md-10">

                <table class="table packages-table" style="text-align:center; table-layout: fixed; text-layout:fixed;" id="packages-table">
                    <thead>
                        <!-- <th style="border: 2px solid #e3e3e3ff;"></th> -->
                        <th width="50" style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; "></th>
                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                            PAQUETE</th>


                        <!-- <th>Casillero</th> -->
                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                            CASILLERO</th>
                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                            CLIENTE</th>
                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                            OPERADOR</th>
                        <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Región</th> -->
                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                            MEDIDAS (WxHxL)</th>

                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                            TIPO DE ENVIO</th>
                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                            SEGURO</th>
                        <!-- <th>Almacen</th> -->

                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                            STATUS</th>
                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">
                            ACCIÓN</th>
                        <!-- <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Pre - Alerta</th> -->

                        <!-- <th>Imagen</th> -->
                        <!-- <th>Accion</th> -->
                    </thead>
                    <tbody id="package-table-body">
                        <?php  

                                    foreach ($allPackagesConfirmed as $key => $value) {
                                ?>
                        <tr>
                            <!-- <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><input onclick="countPChecks();" type="checkbox" class="form-check-input pcheck pcheck<?php echo $value['id'];?>" id="pcheck<?php echo $value['id'];?>" package="<?php echo $value['id'];?>"></td> -->
                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                <input onclick="countChecksForPackagesOrWarehouse();" type="checkbox"
                                    class="form-check-input wcheckPackages wcheckPackages<?php echo $value['id'];?>"
                                    id="wcheckPackages<?php echo $value['id'];?>" package="<?php echo $value['id'];?>"></td>
                            <!-- <td><?php echo $value['id']; ?></td> -->
                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                <?php echo $value['code']; ?></td>


                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                <?php echo $users->findById($value['user_from'])['code']; ?></td>
                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                <?php echo $users->findById($value['user_from'])['name'].' '.$users->findById($value['user_from'])['lastname']; ?>
                            </td>
                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                <?php 
                                                    if ($value['operator'] == 0) {
                                                        echo "No tiene operador";
                                                    }else{
                                                        echo $users->findById($value['operator'])['name'].' '.$users->findById($value['operator'])['lastname']; 
                                                    }
                                                    
                                                ?>
                            </td>
                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                <?php echo $value['width'].' x '.$value['height'].' x '.$value['lenght'].'   PESO'.$value['width'].'LB'; ?>
                            </td>


                            <td style="border: 2px solid #e3e3e3ff; color: #fc3c3d; background-color:white;">
                                <?php $trip = $packages->findTripForInstruction($value['id'])[0]['trip_id'];
                                            if($trip == 1){ 
                                                echo "AÉREO";
                                            }else{
                                                echo "MARÍTIMO";
                                             } ;
                                             ?>
                            </td>
                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                <?php $insurance = $packages->findTripForInstruction($value['id'])[0]['insurance'];
                                            if($insurance == 1){ 
                                             echo "SI";
                                            }else{
                                                echo "NO";
                                             } ;
                                             ?>
                            </td>
                            </td>



                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                <?php 
                                                    echo $packages->findStateById($value['state'])['name'];
                                                ?>
                            </td>
                            <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                <button style="background-color:#005574; color:white; border:2px solid #005574;"
                                    onclick="window.open('registerWarehouseForPackage.php?id=<?php echo $value['id']; ?>', '_parent');"
                                    class="btn btn-primary"><i class="fas fa-check"></i></button></td>

                            <!-- <td><a href="assets/img/paquetes/<?php echo $value['img'];?>"><img class="pack-img" src="assets/img/paquetes/<?php echo $value['img'];?>" alt=""></a> </td> -->
                            <!-- <td><button class="btn btn-primary"><i class="fas fa-eye"></i></button></td> -->
                        </tr>
                        <?php
                                    }

                                ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-1 col-xlg-12 col-md-1"></div>
        </div>
    </div>
</div>

    </div>
    
    <?php 

            include 'foot.php';

        ?>
</body>

</html>