<?php

include 'includes/connection.class.php';
include 'includes/headquarters.class.php';
include 'includes/users.class.php';

session_start();

$headquarters = new Headquarters;
$usersc = new Users;
$allcountriesRegister = $usersc->findCountryForRegister();
$allCountries = $usersc->findAddressee();
$user_type = $usersc->findByEmail($_SESSION['email'])['user_type'];
$user_id = $usersc->findByEmail($_SESSION['email'])['id'];
// echo $user_type;
$allAddressStates = $usersc->findAddressStates();
$allAddressCities = $usersc->findAddressCities();
$allHeadQ = $headquarters->findAll();
$userTypes = $usersc->findRegisterTypes();
$digits = 5;
$fecha = new DateTime();
$dataFecha = $fecha->getTimestamp();
$code = 'TE -' . substr($dataFecha, -5);

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php

    include 'head.php';


    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        #myMap {
            height: 350px;
            width: 680px;
        }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCnFc9MHexw438xRR2JF6yP044jDeZeF3U&sensor=false">
    </script>
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>


</head>

<body style="background:#e3e3e3ff;">
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
        <input type="hidden" id="user_type" value="<?php echo $user_type ?>">
        <?php

        include 'navbar.php';


        ?>
        <div class="page-wrapper" style="background:#e3e3e3ff;">

            <!-- Modal de confirmación de registro************************************************ -->




            <input id="countryVenezuela" type="hidden" value="0">
            <div class="card">
                <div class="card-body" style="padding:20px;">
                    <div class="row" style="padding:50px;">
                        <div class="col-lg-12 col-xlg-12 col-md-12">


                            <!-- <div class="encabezado"><img id="imag" src="./logo-home-te.png"/></div> -->

                            <div class="alert alert-danger register-error" role="alert">
                            </div>
                            <div class="alert alert-success register-success" role="alert">
                            </div>
                            <div>
                                <div class="row">
                                    <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1">
                                        <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                                    </div>
                                    <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                        <h2 style="color:#005574;"><strong>REGISTRAR NUEVO DESTINATARIO</strong></h2>
                                    </div>
                                </div><br><br>
                                <!-- <hr style="background-color:#ff554dff; height: 20px; opacity: 1.0;"><br> -->
                                <div class="row">

                                    <div class="row">
                                        <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                            <input id="user_id_for_destiner" type="text" class="form-control" value="<?php echo $user_id?>" hidden>
                                        </div>
                                        <div class="col-sm-12 col-md-5 col-lg-3 col-xl-5"></div>


                                    </div><br>
                                    <div class="card" id="card-registro-usuarios-wh">
                                        <div class="card-body">

                                            <div class="row">
                                                <div class="col-sm-12 col-md-3 col-lg-4 col-xl-3">
                                                    <div class="form-group">
                                                        <label for="name_destiner">Nombre *</label>

                                                        <input id="name_destiner" type="text" placeholder="Nombre Completo" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-3 col-lg-4 col-xl-3">
                                                    <div class="form-group">
                                                        <label for="lastname_destiner">Apellido *</label>

                                                        <input id="lastname_destiner" type="text" placeholder="Apellido Completo" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-3 col-lg-4 col-xl-3">
                                                    <div class="form-group">
                                                        <label for="alias_destiner">Alias *</label>

                                                        <input id="alias_destiner" type="text" placeholder="Alias" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-3 col-lg-4 col-xl-3">
                                                    <div class="form-group">
                                                        <label for="cedula_destiner">Cedula *</label>

                                                        <input id="cedula_destiner" type="number" placeholder="Cedula" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-3 col-lg-4 col-xl-3">
                                                    <div class="form-group">
                                                        <label for="contact_phone_destiner">Telefono de contacto</label>

                                                        <input type="number"  placeholder="123 456 7890" class="form-control" id="contact_phone_destiner">

                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-3 col-lg-4 col-xl-3">
                                                    <div class="form-group">
                                                        <label for="email">Email de contacto</label>

                                                        <input type="email" placeholder="abc@gmail.com" class="form-control" id="correo_destiner">

                                                    </div>
                                                </div>
                                            </div>
                                            <input id="countryVenezuela" type="hidden" value="0">
                                            <div class="row">

                                                <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                    <div class="form-group">
                                                        <label for="address_destiny_destiner_dos">Pais destino</label>
                                                        <select id="address_destiny_destiner_dos" class="form-select campos-input-login">
                                                            <option value="0">¿Cuál es destino?</option>
                                                            <?php
                                                            foreach ($allCountries as $key => $value) {
                                                            ?>
                                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 address_state_destiner_dos">
                                                    <div class="form-group">
                                                        <label for="address_state_destiner_dos">Estado</label>
                                                        <input id="address_state_destiner_dos" type="text" placeholder="Estado" class="form-control campos-input-login">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 select_state_destiner_dos" style="display: none;">
                                                    <div class="form-group">
                                                        <label for="select_state_destiner_dos">Estado</label>
                                                        <select id="select_state_destiner_dos" class="form-select campos-input-login">
                                                            <option value="0">¿Cuál estado?</option>
                                                            <?php
                                                            foreach ($allAddressStates as $key => $value) {
                                                            ?>
                                                                <option value="<?php echo $value['id_estado']; ?>"><?php echo $value['estado']; ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 address_city_destiner_dos">
                                                    <div class="form-group">
                                                        <label for="address_city_destiner_dos">Ciudad</label>
                                                        <input id="address_city_destiner_dos" type="text" placeholder="Ciudad" class="form-control campos-input-login">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 select_city_destiner_dos" style="display: none;">
                                                    <div class="form-group">
                                                        <label for="select_city_destiner_dos">Ciudad</label>
                                                        <select id="select_city_destiner_dos" class="form-select campos-input-login">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 address_address_destiner_dos">
                                                    <div class="form-group">
                                                        <label for="address_Destiner_dos">Dirección</label>
                                                        <input id="address_destiner" type="text" placeholder="Dirección" class="form-control campos-input-login">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 select_address_destiner_dos" style="display: none;">
                                                    <div class="form-group">
                                                        <label for="select_address_destiner_dos">Dirección</label>
                                                        <input id="select_address_destiner_dos" type="text" placeholder="Dirección" class="form-control campos-input-login">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group mb-4 text-center">
                                                <div class="col-sm-12">
                                                    <button onclick="RegDestinatario();" class="boton-total-envios-registro"><strong>REGISTRAR</strong></button>
                                                </div>

                                            </div>



                                        </div>
                                    </div>
                                </div>


                                <!-- ******************************************************************************** -->



                            </div>
                        </div>

                    </div>
                </div>

                <script src="bootstrap/dist/js/bootstrap.bundle.min.js"></script>
                <script src="js/app-style-switcher.js"></script>
                <!--Wave Effects -->
                <script src="js/waves.js"></script>
                <!--Menu sidebar -->
                <script src="js/sidebarmenu.js"></script>
                <!--Custom JavaScript -->
                <script src="js/custom.js"></script>
                <script src="js/main.js"></script>
</body>

</html>