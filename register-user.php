<?php  
    
    include 'includes/connection.class.php';
    include 'includes/headquarters.class.php';
    include 'includes/users.class.php';

    $headquarters = new Headquarters;
    $usersc = new Users;
    $allcountriesRegister = $usersc->findCountryForRegister();
    $allCountries = $usersc->findAddressee();
    $allAddressStates = $usersc->findAddressStates();
    $allAddressCities = $usersc->findAddressCities();
    $allHeadQ = $headquarters->findAll();
    $userTypes = $usersc->findRegisterTypes();
    $digits = 5;
    $fecha = new DateTime();
    $dataFecha = $fecha->getTimestamp();
    $code = 'TE -'.substr($dataFecha, -5);

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

        include 'head.php';
        

    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        #myMap {
           height: 350px;
           width: 680px;
        }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCnFc9MHexw438xRR2JF6yP044jDeZeF3U&sensor=false">
    </script>
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>

   
</head>
<body style="background:#e3e3e3ff;">
<div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper"  data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
        <?php 

            include 'navbar.php'; 
            

        ?>
        <div class="page-wrapper" style="background:#e3e3e3ff;">

    <!-- Modal de confirmación de registro************************************************ -->

   
                                    
    
        <input id="countryVenezuela" type="hidden" value="0">
            <div class="card">
                <div class="card-body" style="padding:20px;">
                <div class="row" style="padding:50px;">
                <div class="col-lg-12 col-xlg-12 col-md-12">
                    
                    
                        <!-- <div class="encabezado"><img id="imag" src="./logo-home-te.png"/></div> -->
            
                        <div class="alert alert-danger register-error" role="alert">
                        </div>
                        <div class="alert alert-success register-success" role="alert">
                        </div>
                        <div>
                        <div class="row">
                            <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1">
                                <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                            </div>
                            <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                <h2 style="color:#005574;"><strong>REGISTRAR NUEVO CLIENTE</strong></h2>
                            </div>
                        </div><br><br>
                        <!-- <hr style="background-color:#ff554dff; height: 20px; opacity: 1.0;"><br> -->
                            <div class="row">

                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                    
                                        <div class="form-group">
                                        <!-- <label for="user_type" style="color:#005574;"><strong>Tipo de usuario</strong></label> -->
                                            <select id="user_type" class="campos-input-login" style="background: url('icons/user_type.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                                <option value="0">Tipo de usuario</option>
                                                <option value="2">Cliente</option>
                                                <option value="5">Empresa</option>
                                            </select>
                                        </div>
                                    
                                </div>
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                
                                    
                                        <div class="form-group">
                                        <!-- <label for="headquarter" style="color:#005574;"><strong>Agencia</strong></label> -->
                                            <select id="headquarter" class="campos-input-login" style="background: url('icons/agency.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                                <!-- <option value="0">Seleccionar...</option> -->
                                                <?php  
                                                    foreach ($allHeadQ as $key => $value) {
                                                ?>
                                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                </div> 
                                <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4"></div>  
                                
                                
                            </div>
                            <hr>
                            <div id="user-register" style="display:block;"> 
                           
                                
                            <div class="row">
                                <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                    <div class="form-group">
                                    <!-- <label for="name" style="color:#005574;"><strong>Nombre completo</strong></label> -->
                                        <input id="name" type="text" placeholder="Nombre Completo"
                                        class="campos-input-login" style="background: url('icons/name.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;"> </div>
                                </div>
                                <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
            
                                        <div class="form-group">
                                        <!-- <label for="lastname"style="color:#005574;"><strong>Apellido completo</strong></label> -->
                                            <input id="lastname" type="text" placeholder="Apellido Completo"
                                                class="campos-input-login" style="background: url('icons/name.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;"> </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                        
                                            <div class="form-group">
                                            <!-- <label for="document"style="color:#005574;"><strong>Identificación</strong></label> -->
                                                <input id="document" type="text" placeholder="# Identificación" class="campos-input-login" style="background: url('icons/identification.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;"> 
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                    
                                    <div class="form-group">
                                    <!-- <label for="phone"style="color:#005574;"><strong>Teléfono celular</strong></label> -->
                                        <input id="phone" type="text" placeholder="Telf. celular" class="campos-input-login" style="background: url('icons/phone.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>
                                
                            </div>
                            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                
                                    <div class="form-group">
                                    <!-- <label for="phone_house"style="color:#005574;"><strong>Teléfono de casa</strong></label> -->
                                        <input id="phone_house" type="text" placeholder="Telf. Fijo" class="campos-input-login" style="background: url('icons/phone-house.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>
                                
                            </div> 
                                                </div>  
                                    
                                    
                                
                                <div class="row">
                                    <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                            <div class="form-group">
                                            <!-- <label for="email"style="color:#005574;"><strong>Correo electrónico</strong></label> -->
                                                <input type="email" placeholder="Correo electrónico"
                                                    class="campos-input-login" name="example-email"
                                                    id="email" style="background: url('icons/email-blue.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                            <div class="form-group">
                                            <!-- <label for="email"style="color:#005574;"><strong>Confirma tu correo electrónico</strong></label> -->
                                                <input type="email" placeholder="confirmación"
                                                    class="campos-input-login" name="example-email"
                                                    id="email" style="background: url('icons/email-blue.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                            </div>
                                        </div>
                                        
                                    <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                        
                                        <div class="form-group">
                                        <!-- <label for="born_date"style="color:#005574;"><strong>Fecha de nacimiento</strong></label> -->
                                            <input type="date" placeholder="1990-01-01"
                                                class="campos-input-login"
                                                id="born_date" style="
                                                display: flex;
                                                align-items: center; padding-left: 20px;">
                                        </div>
                                    </div>
                            </div>
                        
                                
                            
                            <!-- <hr> -->
                            <div class="row">
                                
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <div class="form-group">
                                <!-- <label for="address_destiny"style="color:#005574;"><strong>Pais</strong></label> -->
                                    <select id="address_destiny" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                        <option value="0">País</option>
                                        <?php  
                                            foreach ($allcountriesRegister as $key => $value) {
                                        ?>
                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 address_state">
                                    <div class="form-group">
                                    <!-- <label for="address_state"style="color:#005574;"><strong>Estado</strong></label> -->
                                        <input id="address_state" type="text" placeholder="Estado" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 select_state" style="display: none;">
                                    <div class="form-group">
                                    <!-- <label for="select_state"style="color:#005574;"><strong>Estado</strong></label> -->
                                        <select id="select_state" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                        <option value="0">Estado</option>
                                        <?php  
                                            foreach ($allAddressStates as $key => $value) {
                                        ?>
                                                <option value="<?php echo $value['id_estado']; ?>"><?php echo $value['estado']; ?></option>
                                        <?php
                                            }
                                        ?>
                                        </select>
                                    </div>    
                                </div>
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 address_city">
                                    <div class="form-group">
                                    <!-- <label for="address_city"style="color:#005574;"><strong>Ciudad</strong></label> -->
                                        <input id="address_city" type="text" placeholder="Ciudad" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>    
                                </div>
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 select_city" style="display: none;">
                                    <div class="form-group">
                                    <!-- <label for="select_city"style="color:#005574;"><strong>Ciudad</strong></label> -->
                                        <select id="select_city" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                        </select>
                                    </div>
                                </div>
                                <!-- </div> -->
                                <!-- <div class="row"> -->
                                    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 address_address">
                                        <div class="form-group">
                                        <!-- <label for="address"style="color:#005574;"><strong>Dirección</strong></label> -->
                                            <input id="address" type="text" placeholder="Dirección" class="campos-input-login" style="background: url('icons/direccion.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 select_address" style="display: none;">
                                        <div class="form-group">
                                        <!-- <label for="select_address"style="color:#005574;"><strong>Dirección</strong></label> -->
                                            <input id="select_address" type="text" placeholder="Dirección" class="campos-input-login" style="background: url('icons/direccion.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                        </div>
                                    </div>
                                </div> 

                            <!-- <hr> -->
                            
                            <div class="row">
                                
                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    
                                    <div class="form-group">
                                    <!-- <label for="password"style="color:#005574;"><strong>Contraseña</strong></label> -->
                                        <input id="password" placeholder="contraseña" class="campos-input-login" style="background: url('icons/password.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                
                                    <div class="form-group">
                                    <!-- <label for="confirm_password"style="color:#005574;"><strong>Confirmar contraseña</strong></label> -->
                                        <input id="confirm_password" placeholder="Confirma tu contraseña" class="campos-input-login" style="background: url('icons/password.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    
                                    <div class="form-group">
                                    <!-- <label for="contact"style="color:#005574;"><strong>Como supiste de nosotros</strong></label> -->
                                        <select id="contact" class="campos-input-login"style="
                                                display: flex;
                                                align-items: center; padding-left: 20px;">
                                            <option value="0">Como supiste de nosotros</option>
                                            <option value="2">Facebook</option>
                                            <option value="3">Instagram</option>
                                            <option value="4">Twitter</option>
                                            <option value="5">Radio/Televisión</option>
                                            <option value="6">Referido</option>
                                            <option value="7">Otros</option>
                                        </select>
                                    </div>
                                
                                </div>
                            </div>  
                                
                            <!-- <hr> -->
                        
                            <!-- <div id="myMap"></div> <br> -->
                            
                            
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                                <label class="form-check-label" for="flexCheckChecked">
                                    Acepto terminos y condiciones
                                </label>
                            </div>
                           
                            
                            
                            <hr>
                            
                            <div class="form-group mb-4 text-center">
                                <div class="col-sm-12">
                                    <button onclick="registerModClient(1);" class="boton-total-envios-registro"><strong>REGISTRAR</strong></button>
                                </div>
                               
                            </div>
                            </div>

                        </div>
                    </div>

                    <!-- AREA DE REGISTRO DE EMPRESAS -->
                            <!-- ******************************************************************************* -->
                            <div id="company-register" style="display:none;"> 
                            <!-- <hr> -->
                                
                            <div class="row">
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                    <div class="form-group">
                                    <!-- <label for="company_name">Nombre de la empresa</label> -->
                                        <input id="company_name" type="text" placeholder="Empresa"
                                        class="campos-input-login" style="background: url('icons/agency.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;"> </div>
                                </div>
                                
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                        <div class="form-group">
                                        <!-- <label for="company_document"># Identificación</label> -->
                                            <input id="company_document" type="text" placeholder="# Identificación"
                                                class="campos-input-login" style="background: url('icons/identification.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;"> </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                    <div class="form-group">
                                    <!-- <label for="company_phone">Teléfono de Contacto</label> -->
                                        <input id="company_phone" type="text" placeholder="Teléfono" class="campos-input-login" style="background: url('icons/phone.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>
                                        </div>
                                        <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                        
                                            <div class="form-group">
                                                <!-- <label for="company_email">Correo electrónico</label> -->
                                                    <input type="email" placeholder="Correo electrónico"
                                                    class="campos-input-login" name="example-email"
                                                    id="company_email" style="background: url('icons/email-blue.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                            <div class="form-group">
                                                <!-- <label for="confirm_email">Confirmar correo electrónico</label> -->
                                                    <input type="email" placeholder="Confir. de correo electrónico"
                                                    class="campos-input-login" name="example-email"
                                                    id="confirm_email" style="background: url('icons/email-blue.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                            </div>
                                        </div>          
                                </div>
                            
                                
                                <div class="row">
                                      
                                </div>
                                    
            
                            
                            <div class="row">
                                
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                    <div class="form-group">
                                        <!-- <label for="address_destiny_company">Pais</label> -->
                                                <select id="address_destiny_company" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                                <option value="0">País</option>
                                                    <?php  
                                                        foreach ($allcountriesRegister as $key => $value) {
                                                    ?>
                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                    <?php
                                                        }
                                                    ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 address_state_company">
                                    <div class="form-group">
                                        <!-- <label for="address_state_company">Estado</label> -->
                                        <input id="address_state_company" type="text" placeholder="Estado" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 select_state_company" style="display: none;">
                                    <div class="form-group">
                                        <!-- <label for="select_state_company">Estado</label> -->
                                            <select id="select_state_company" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                                <option value="0">Estado</option>
                                                    <?php  
                                                        foreach ($allAddressStates as $key => $value) {
                                                    ?>
                                                <option value="<?php echo $value['id_estado']; ?>"><?php echo $value['estado']; ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                            </select>
                                    </div>    
                                </div>
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 address_city_company">
                                    <div class="form-group">
                                        <!-- <label for="address_city_company">Ciudad</label> -->
                                        <input id="address_city_company" type="text" placeholder="Ciudad" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>    
                                </div>
                                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 select_city_company" style="display: none;">
                                    <div class="form-group">
                                        <!-- <label for="select_city_company">Ciudad</label> -->
                                            <select id="select_city_company" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                            </select>
                                    </div>
                                </div>
                                
                                    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 address_address_company">
                                        <div class="form-group">
                                            <!-- <label for="address_company">Dirección</label> -->
                                            <input id="address_company" type="text" placeholder="Dirección" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 select_address_company" style="display: none;" >
                                        <div class="form-group">
                                            <!-- <label for="select_address_company">Dirección</label> -->
                                            <input id="select_address_company" type="text" placeholder="Dirección" class="campos-input-login" style="background: url('icons/pais.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                        </div>
                                    </div>
                                </div> 

                        
                            
                            <div class="row">
                                
                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    
                                    <div class="form-group">
                                    <!-- <label for="company_password">Contraseña</label> -->
                                        <input id="company_password" placeholder="Contraseña" class="campos-input-login" style="background: url('icons/password.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                
                                    <div class="form-group">
                                    <!-- <label for="company_confirm_password">Confirmar contraseña</label> -->
                                        <input id="company_confirm_password" placeholder="Confirmar contraseña" class="campos-input-login" style="background: url('icons/password.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    
                                    <div class="form-group">
                                    <!-- <label for="contact">Como supiste de nosotros</label> -->
                                        <select id="contact" class="campos-input-login"  style="
                                                display: flex;
                                                align-items: center; padding-left:20px;">
                                           <option value="0">Como supiste de nosotros</option>
                                            <option value="2">Facebook</option>
                                            <option value="3">Instagram</option>
                                            <option value="4">Twitter</option>
                                            <option value="5">Radio/Televisión</option>
                                            <option value="6">Referido</option>
                                            <option value="7">Otros</option>
                                        </select>
                                    </div>
                                
                                </div>
                            </div>  
                           
                        
                            <!-- <div id="myMap"></div> <br> -->
            
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                                <label class="form-check-label" for="flexCheckChecked">
                                    Acepto terminos y condiciones
                                </label>
                            </div>
                            <!-- <div class="alert alert-danger register-error" role="alert"></div>
                            <div class="alert alert-success register-success" role="alert"></div> -->
                                 
                            <hr><br>
                            
                            
    
                            
                            <div class="form-group mb-4 text-center">
                                <div class="col-sm-12">
                                    <button onclick="registerModClient(1);" class="boton-total-envios-registro"><strong>REGISTRAR</strong></button>
                                </div>
                        
                            </div>
                            </div>
                                                                        <!-- ******************************************************************************** -->
                                                                  
                                  
            
                                                      </div>
                                                </div>
                                                        
                                                    <!-- </div>
                                                </div>
                                            </div>
                                                                                   
                                            </div> -->
            <!-- <div class="col-sm-12 col-md-4 col-lg-2 col-xl-2"></div>
        </div>    -->
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <!-- <footer class="footer text-center"> 2021 © Total Envios <a
                    href="https://www.wrappixel.com/"></a>
            </footer> -->
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- Bootstrap tether Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="js/app-style-switcher.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.js"></script>
    <script src="js/main.js"></script>
</body>

</html>