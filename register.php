<?php  
    
    include 'includes/connection.class.php';
    include 'includes/headquarters.class.php';
    include 'includes/users.class.php';

    $headquarters = new Headquarters;
    $usersc = new Users;
    $allCountries = $usersc->findAddressee();
    $allAddressStates = $usersc->findAddressStates();
    $allAddressCities = $usersc->findAddressCities();
    $allHeadQ = $headquarters->findAll();
    $userTypes = $usersc->findRegisterTypes();
    $digits = 5;
    $fecha = new DateTime();
    $dataFecha = $fecha->getTimestamp();
    $code = 'TE -'.substr($dataFecha, -5);

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

        include 'head.php';
        

    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        #myMap {
           height: 350px;
           width: 680px;
        }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCnFc9MHexw438xRR2JF6yP044jDeZeF3U&sensor=false">
    </script>
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>

   
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3"></div>
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <div id="cardL">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12 col-xlg-12 col-md-12">
                                            <div id="main-wrapper" >
                                                <div class="page-wrapper">
                                                    <input id="countryVenezuela" type="hidden" value="0">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                                                
                                                                <div class="card"> 
                                                                    <div class="card-body">
                                                                    <!-- <div class="encabezado"><img id="imag" src="./logo-home-te.png"/></div>
                                                        
                                                                    <div class="alert alert-danger register-error" role="alert"> -->
                                                                        <div align="center"><h3>Registro de clientes</h3></div><br>
                                                                        <div class="row">
                                                                            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                                                
                                                                                    <div class="form-group">
                                                                                    <label for="user_type">Tipo de usuario</label>
                                                                                        <select id="user_type" class="campos-input-login">
                                                                                            <option value="2">Cliente</option>
                                                                                            <option value="5">Empresa</option>
                                                                                        </select>
                                                                                    </div>
                                                                                
                                                                            </div>
                                                                            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                                            
                                                                               
                                                                                    <div class="form-group">
                                                                                    <label for="headquarter">Agencia</label>
                                                                                        <select id="headquarter" class="campos-input-login">
                                                                                            <!-- <option value="0">Seleccionar...</option> -->
                                                                                            <?php  
                                                                                                foreach ($allHeadQ as $key => $value) {
                                                                                            ?>
                                                                                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                                                            <?php
                                                                                                }
                                                                                            ?>
                                                                                        </select>
                                                                                    </div>
                                                                            </div>   
                                                                            
                                                                            
                                                                        </div> 
                                                                        <hr>   
                                                                        <div class="row">
                                                                            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                                                <div class="form-group">
                                                                                <label for="name">Nombre completo</label>
                                                                                    <input id="name" type="text" placeholder="Nombre Completo"
                                                                                    class="campos-input-login"> </div>
                                                                            </div>
                                                                            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                        
                                                                                    <div class="form-group">
                                                                                    <label for="lastname">Apellido completo</label>
                                                                                        <input id="lastname" type="text" placeholder="Apellido Completo"
                                                                                            class="campos-input-login"> </div>
                                                                                </div>
                                                                            </div>
                                                                        
                                                                            
                                                                            <div class="row">
                                                                                <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                                                    
                                                                                        <div class="form-group">
                                                                                        <label for="document">Identificación</label>
                                                                                            <input id="document" type="text" placeholder="# Documento" class="campos-input-login"> 
                                                                                        </div>
                                                                                    </div>
                                                                                
                                                                                <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                                                    
                                                                                        <div class="form-group">
                                                                                        <label for="born_date">Fecha de nacimiento</label>
                                                                                            <input type="date" placeholder="1990-01-01"
                                                                                                class="campos-input-login"
                                                                                                id="born_date">
                                                                                        </div>
                                                                                    </div>


                                                                                <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                                                
                                                                                    <div class="form-group">
                                                                                    <label for="country">Pais de origen</label>
                                                                                        <select class="campos-input-login" id="country" name="country">
                                                                                        <option value="">Selecciona tu pais</option>    
                                                                                        <option value="Afganistan">Afghanistan</option>
                                                                                        <option value="Albania">Albania</option>
                                                                                        <option value="Algeria">Algeria</option>
                                                                                        <option value="American Samoa">American Samoa</option>
                                                                                        <option value="Andorra">Andorra</option>
                                                                                        <option value="Angola">Angola</option>
                                                                                        <option value="Anguilla">Anguilla</option>
                                                                                        <option value="Antigua & Barbuda">Antigua & Barbuda</option>
                                                                                        <option value="Argentina">Argentina</option>
                                                                                        <option value="Armenia">Armenia</option>
                                                                                        <option value="Aruba">Aruba</option>
                                                                                        <option value="Australia">Australia</option>
                                                                                        <option value="Austria">Austria</option>
                                                                                        <option value="Azerbaijan">Azerbaijan</option>
                                                                                        <option value="Bahamas">Bahamas</option>
                                                                                        <option value="Bahrain">Bahrain</option>
                                                                                        <option value="Bangladesh">Bangladesh</option>
                                                                                        <option value="Barbados">Barbados</option>
                                                                                        <option value="Belarus">Belarus</option>
                                                                                        <option value="Belgium">Belgium</option>
                                                                                        <option value="Belize">Belize</option>
                                                                                        <option value="Benin">Benin</option>
                                                                                        <option value="Bermuda">Bermuda</option>
                                                                                        <option value="Bhutan">Bhutan</option>
                                                                                        <option value="Bolivia">Bolivia</option>
                                                                                        <option value="Bonaire">Bonaire</option>
                                                                                        <option value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>
                                                                                        <option value="Botswana">Botswana</option>
                                                                                        <option value="Brazil">Brazil</option>
                                                                                        <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                                                                        <option value="Brunei">Brunei</option>
                                                                                        <option value="Bulgaria">Bulgaria</option>
                                                                                        <option value="Burkina Faso">Burkina Faso</option>
                                                                                        <option value="Burundi">Burundi</option>
                                                                                        <option value="Cambodia">Cambodia</option>
                                                                                        <option value="Cameroon">Cameroon</option>
                                                                                        <option value="Canada">Canada</option>
                                                                                        <option value="Canary Islands">Canary Islands</option>
                                                                                        <option value="Cape Verde">Cape Verde</option>
                                                                                        <option value="Cayman Islands">Cayman Islands</option>
                                                                                        <option value="Central African Republic">Central African Republic</option>
                                                                                        <option value="Chad">Chad</option>
                                                                                        <option value="Channel Islands">Channel Islands</option>
                                                                                        <option value="Chile">Chile</option>
                                                                                        <option value="China">China</option>
                                                                                        <option value="Christmas Island">Christmas Island</option>
                                                                                        <option value="Cocos Island">Cocos Island</option>
                                                                                        <option value="Colombia">Colombia</option>
                                                                                        <option value="Comoros">Comoros</option>
                                                                                        <option value="Congo">Congo</option>
                                                                                        <option value="Cook Islands">Cook Islands</option>
                                                                                        <option value="Costa Rica">Costa Rica</option>
                                                                                        <option value="Cote DIvoire">Cote DIvoire</option>
                                                                                        <option value="Croatia">Croatia</option>
                                                                                        <option value="Cuba">Cuba</option>
                                                                                        <option value="Curaco">Curacao</option>
                                                                                        <option value="Cyprus">Cyprus</option>
                                                                                        <option value="Czech Republic">Czech Republic</option>
                                                                                        <option value="Denmark">Denmark</option>
                                                                                        <option value="Djibouti">Djibouti</option>
                                                                                        <option value="Dominica">Dominica</option>
                                                                                        <option value="Dominican Republic">Dominican Republic</option>
                                                                                        <option value="East Timor">East Timor</option>
                                                                                        <option value="Ecuador">Ecuador</option>
                                                                                        <option value="Egypt">Egypt</option>
                                                                                        <option value="El Salvador">El Salvador</option>
                                                                                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                                        <option value="Eritrea">Eritrea</option>
                                                                                        <option value="Estonia">Estonia</option>
                                                                                        <option value="Ethiopia">Ethiopia</option>
                                                                                        <option value="Falkland Islands">Falkland Islands</option>
                                                                                        <option value="Faroe Islands">Faroe Islands</option>
                                                                                        <option value="Fiji">Fiji</option>
                                                                                        <option value="Finland">Finland</option>
                                                                                        <option value="France">France</option>
                                                                                        <option value="French Guiana">French Guiana</option>
                                                                                        <option value="French Polynesia">French Polynesia</option>
                                                                                        <option value="French Southern Ter">French Southern Ter</option>
                                                                                        <option value="Gabon">Gabon</option>
                                                                                        <option value="Gambia">Gambia</option>
                                                                                        <option value="Georgia">Georgia</option>
                                                                                        <option value="Germany">Germany</option>
                                                                                        <option value="Ghana">Ghana</option>
                                                                                        <option value="Gibraltar">Gibraltar</option>
                                                                                        <option value="Great Britain">Great Britain</option>
                                                                                        <option value="Greece">Greece</option>
                                                                                        <option value="Greenland">Greenland</option>
                                                                                        <option value="Grenada">Grenada</option>
                                                                                        <option value="Guadeloupe">Guadeloupe</option>
                                                                                        <option value="Guam">Guam</option>
                                                                                        <option value="Guatemala">Guatemala</option>
                                                                                        <option value="Guinea">Guinea</option>
                                                                                        <option value="Guyana">Guyana</option>
                                                                                        <option value="Haiti">Haiti</option>
                                                                                        <option value="Hawaii">Hawaii</option>
                                                                                        <option value="Honduras">Honduras</option>
                                                                                        <option value="Hong Kong">Hong Kong</option>
                                                                                        <option value="Hungary">Hungary</option>
                                                                                        <option value="Iceland">Iceland</option>
                                                                                        <option value="Indonesia">Indonesia</option>
                                                                                        <option value="India">India</option>
                                                                                        <option value="Iran">Iran</option>
                                                                                        <option value="Iraq">Iraq</option>
                                                                                        <option value="Ireland">Ireland</option>
                                                                                        <option value="Isle of Man">Isle of Man</option>
                                                                                        <option value="Israel">Israel</option>
                                                                                        <option value="Italy">Italy</option>
                                                                                        <option value="Jamaica">Jamaica</option>
                                                                                        <option value="Japan">Japan</option>
                                                                                        <option value="Jordan">Jordan</option>
                                                                                        <option value="Kazakhstan">Kazakhstan</option>
                                                                                        <option value="Kenya">Kenya</option>
                                                                                        <option value="Kiribati">Kiribati</option>
                                                                                        <option value="Korea North">Korea North</option>
                                                                                        <option value="Korea Sout">Korea South</option>
                                                                                        <option value="Kuwait">Kuwait</option>
                                                                                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                                        <option value="Laos">Laos</option>
                                                                                        <option value="Latvia">Latvia</option>
                                                                                        <option value="Lebanon">Lebanon</option>
                                                                                        <option value="Lesotho">Lesotho</option>
                                                                                        <option value="Liberia">Liberia</option>
                                                                                        <option value="Libya">Libya</option>
                                                                                        <option value="Liechtenstein">Liechtenstein</option>
                                                                                        <option value="Lithuania">Lithuania</option>
                                                                                        <option value="Luxembourg">Luxembourg</option>
                                                                                        <option value="Macau">Macau</option>
                                                                                        <option value="Macedonia">Macedonia</option>
                                                                                        <option value="Madagascar">Madagascar</option>
                                                                                        <option value="Malaysia">Malaysia</option>
                                                                                        <option value="Malawi">Malawi</option>
                                                                                        <option value="Maldives">Maldives</option>
                                                                                        <option value="Mali">Mali</option>
                                                                                        <option value="Malta">Malta</option>
                                                                                        <option value="Marshall Islands">Marshall Islands</option>
                                                                                        <option value="Martinique">Martinique</option>
                                                                                        <option value="Mauritania">Mauritania</option>
                                                                                        <option value="Mauritius">Mauritius</option>
                                                                                        <option value="Mayotte">Mayotte</option>
                                                                                        <option value="Mexico">Mexico</option>
                                                                                        <option value="Midway Islands">Midway Islands</option>
                                                                                        <option value="Moldova">Moldova</option>
                                                                                        <option value="Monaco">Monaco</option>
                                                                                        <option value="Mongolia">Mongolia</option>
                                                                                        <option value="Montserrat">Montserrat</option>
                                                                                        <option value="Morocco">Morocco</option>
                                                                                        <option value="Mozambique">Mozambique</option>
                                                                                        <option value="Myanmar">Myanmar</option>
                                                                                        <option value="Nambia">Nambia</option>
                                                                                        <option value="Nauru">Nauru</option>
                                                                                        <option value="Nepal">Nepal</option>
                                                                                        <option value="Netherland Antilles">Netherland Antilles</option>
                                                                                        <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                                                                        <option value="Nevis">Nevis</option>
                                                                                        <option value="New Caledonia">New Caledonia</option>
                                                                                        <option value="New Zealand">New Zealand</option>
                                                                                        <option value="Nicaragua">Nicaragua</option>
                                                                                        <option value="Niger">Niger</option>
                                                                                        <option value="Nigeria">Nigeria</option>
                                                                                        <option value="Niue">Niue</option>
                                                                                        <option value="Norfolk Island">Norfolk Island</option>
                                                                                        <option value="Norway">Norway</option>
                                                                                        <option value="Oman">Oman</option>
                                                                                        <option value="Pakistan">Pakistan</option>
                                                                                        <option value="Palau Island">Palau Island</option>
                                                                                        <option value="Palestine">Palestine</option>
                                                                                        <option value="Panama">Panama</option>
                                                                                        <option value="Papua New Guinea">Papua New Guinea</option>
                                                                                        <option value="Paraguay">Paraguay</option>
                                                                                        <option value="Peru">Peru</option>
                                                                                        <option value="Phillipines">Philippines</option>
                                                                                        <option value="Pitcairn Island">Pitcairn Island</option>
                                                                                        <option value="Poland">Poland</option>
                                                                                        <option value="Portugal">Portugal</option>
                                                                                        <option value="Puerto Rico">Puerto Rico</option>
                                                                                        <option value="Qatar">Qatar</option>
                                                                                        <option value="Republic of Montenegro">Republic of Montenegro</option>
                                                                                        <option value="Republic of Serbia">Republic of Serbia</option>
                                                                                        <option value="Reunion">Reunion</option>
                                                                                        <option value="Romania">Romania</option>
                                                                                        <option value="Russia">Russia</option>
                                                                                        <option value="Rwanda">Rwanda</option>
                                                                                        <option value="St Barthelemy">St Barthelemy</option>
                                                                                        <option value="St Eustatius">St Eustatius</option>
                                                                                        <option value="St Helena">St Helena</option>
                                                                                        <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                                                                        <option value="St Lucia">St Lucia</option>
                                                                                        <option value="St Maarten">St Maarten</option>
                                                                                        <option value="St Pierre & Miquelon">St Pierre & Miquelon</option>
                                                                                        <option value="St Vincent & Grenadines">St Vincent & Grenadines</option>
                                                                                        <option value="Saipan">Saipan</option>
                                                                                        <option value="Samoa">Samoa</option>
                                                                                        <option value="Samoa American">Samoa American</option>
                                                                                        <option value="San Marino">San Marino</option>
                                                                                        <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                                                                                        <option value="Saudi Arabia">Saudi Arabia</option>
                                                                                        <option value="Senegal">Senegal</option>
                                                                                        <option value="Seychelles">Seychelles</option>
                                                                                        <option value="Sierra Leone">Sierra Leone</option>
                                                                                        <option value="Singapore">Singapore</option>
                                                                                        <option value="Slovakia">Slovakia</option>
                                                                                        <option value="Slovenia">Slovenia</option>
                                                                                        <option value="Solomon Islands">Solomon Islands</option>
                                                                                        <option value="Somalia">Somalia</option>
                                                                                        <option value="South Africa">South Africa</option>
                                                                                        <option value="Spain">Spain</option>
                                                                                        <option value="Sri Lanka">Sri Lanka</option>
                                                                                        <option value="Sudan">Sudan</option>
                                                                                        <option value="Suriname">Suriname</option>
                                                                                        <option value="Swaziland">Swaziland</option>
                                                                                        <option value="Sweden">Sweden</option>
                                                                                        <option value="Switzerland">Switzerland</option>
                                                                                        <option value="Syria">Syria</option>
                                                                                        <option value="Tahiti">Tahiti</option>
                                                                                        <option value="Taiwan">Taiwan</option>
                                                                                        <option value="Tajikistan">Tajikistan</option>
                                                                                        <option value="Tanzania">Tanzania</option>
                                                                                        <option value="Thailand">Thailand</option>
                                                                                        <option value="Togo">Togo</option>
                                                                                        <option value="Tokelau">Tokelau</option>
                                                                                        <option value="Tonga">Tonga</option>
                                                                                        <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                                                                                        <option value="Tunisia">Tunisia</option>
                                                                                        <option value="Turkey">Turkey</option>
                                                                                        <option value="Turkmenistan">Turkmenistan</option>
                                                                                        <option value="Turks & Caicos Is">Turks & Caicos Is</option>
                                                                                        <option value="Tuvalu">Tuvalu</option>
                                                                                        <option value="Uganda">Uganda</option>
                                                                                        <option value="United Kingdom">United Kingdom</option>
                                                                                        <option value="Ukraine">Ukraine</option>
                                                                                        <option value="United Arab Erimates">United Arab Emirates</option>
                                                                                        <option value="United States of America">United States of America</option>
                                                                                        <option value="Uraguay">Uruguay</option>
                                                                                        <option value="Uzbekistan">Uzbekistan</option>
                                                                                        <option value="Vanuatu">Vanuatu</option>
                                                                                        <option value="Vatican City State">Vatican City State</option>
                                                                                        <option value="Venezuela">Venezuela</option>
                                                                                        <option value="Vietnam">Vietnam</option>
                                                                                        <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                                                                        <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                                                                        <option value="Wake Island">Wake Island</option>
                                                                                        <option value="Wallis & Futana Is">Wallis & Futana Is</option>
                                                                                        <option value="Yemen">Yemen</option>
                                                                                        <option value="Zaire">Zaire</option>
                                                                                        <option value="Zambia">Zambia</option>
                                                                                        <option value="Zimbabwe">Zimbabwe</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="row">
                                                                                
                                                                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                                                        <div class="form-group">
                                                                                        <label for="email">Correo electrónico</label>
                                                                                            <input type="email" placeholder="johnathan@admin.com"
                                                                                                class="campos-input-login" name="example-email"
                                                                                                id="email">
                                                                                        </div>
                                                                                    </div>    
                                                                                    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6"></div>
                                                                                
                                                                            </div>
                                                                        <div class="row">
                                                                           
                                                                           
                                                                            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                                                
                                                                                    <div class="form-group">
                                                                                    <label for="phone">Teléfono celular</label>
                                                                                        <input id="phone" type="text" placeholder="123 456 7890" class="campos-input-login">
                                                                                    </div>
                                                                                
                                                                            </div>
                                                                            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                                               
                                                                                    <div class="form-group">
                                                                                    <label for="phone_house">Teléfono de casa</label>
                                                                                        <input id="phone_house" type="text" placeholder="123 456 7890" class="campos-input-login">
                                                                                    </div>
                                                                               
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                        <div class="row">
                                                                            
                                                                            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                                            <div class="form-group">
                                                                            <label for="address_destiny">Pais destino</label>
                                                                                <select id="address_destiny" class="form-select campos-input-login">
                                                                                    <!-- <option value="0">Seleccionar...</option> -->
                                                                                    <?php  
                                                                                        foreach ($allCountries as $key => $value) {
                                                                                    ?>
                                                                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                                                    <?php
                                                                                        }
                                                                                    ?>
                                                                                </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 address_state">
                                                                                <div class="form-group">
                                                                                <label for="address_state">Estado</label>
                                                                                    <input id="address_state" type="text" placeholder="Estado" class="form-control campos-input-login">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 select_state" style="display: none;">
                                                                                <div class="form-group">
                                                                                <label for="select_state">Estado</label>
                                                                                    <select id="select_state" class="form-select campos-input-login">
                                                                                    <!-- <option value="0">Seleccionar...</option> -->
                                                                                    <?php  
                                                                                        foreach ($allAddressStates as $key => $value) {
                                                                                    ?>
                                                                                            <option value="<?php echo $value['id_estado']; ?>"><?php echo $value['estado']; ?></option>
                                                                                    <?php
                                                                                        }
                                                                                    ?>
                                                                                    </select>
                                                                                </div>    
                                                                            </div>
                                                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 address_city">
                                                                                <div class="form-group">
                                                                                <label for="address_city">Ciudad</label>
                                                                                    <input id="address_city" type="text" placeholder="Ciudad" class="form-control campos-input-login">
                                                                                </div>    
                                                                            </div>
                                                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 select_city" style="display: none;">
                                                                                <div class="form-group">
                                                                                <label for="select_city">Ciudad</label>
                                                                                    <select id="select_city" class="form-select campos-input-login">
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 address_address">
                                                                                    <div class="form-group">
                                                                                    <label for="address">Dirección</label>
                                                                                        <input id="address" type="text" placeholder="Dirección" class="form-control campos-input-login">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 select_address" style="display: none;">
                                                                                    <div class="form-group">
                                                                                    <label for="select_address">Dirección</label>
                                                                                        <input id="select_address" type="text" placeholder="Dirección" class="form-control campos-input-login">
                                                                                    </div>
                                                                                </div>
                                                                            </div> 
                  
                                                                        <hr>
                                                                       
                                                                        <div class="row">
                                                                           
                                                                            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                                                
                                                                                <div class="form-group">
                                                                                <label for="password">Contraseña</label>
                                                                                    <input id="password" type="password" value="password" class="campos-input-login">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                                            
                                                                                <div class="form-group">
                                                                                <label for="confirm_password">Confirmar contraseña</label>
                                                                                    <input id="confirm_password" type="password" value="password" class="campos-input-login">
                                                                                </div>
                                                                            </div>
                                                                        </div>  
                                                                            
                                                                        <hr>
                                                                    
                                                                        <!-- <div id="myMap"></div> <br> -->
                                                                        
                                                                        <div class="row">
                                                                            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                                                
                                                                                <div class="form-group">
                                                                                <label for="contact">Como supiste de nosotros</label>
                                                                                    <select id="contact" class="campos-input-login">
                                                                                        <option value="2">Facebook</option>
                                                                                        <option value="3">Instagram</option>
                                                                                        <option value="4">Twitter</option>
                                                                                        <option value="5">Radio/Televisión</option>
                                                                                        <option value="6">Referido</option>
                                                                                        <option value="7">Otros</option>
                                                                                    </select>
                                                                                </div>
                                                                            
                                                                            </div>

                                                                            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6"></div>

                                                                        </div>
                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                                                                            <label class="form-check-label" for="flexCheckChecked">
                                                                                Acepto terminos y condiciones
                                                                            </label>
                                                                        </div>
                                                                        <div class="alert alert-danger register-error" role="alert"></div>
                                                                        <div class="alert alert-success register-success" role="alert"></div>
                                                                        
                                                                        
                                                
                                                                        
                                                                        <div class="form-group mb-4">
                                                                            <div class="col-sm-12">
                                                                                <button onclick="register(1);" class="boton-register">Registrar</button>
                                                                            </div>
                                                                            <div align="center" ><a href="login.php" class="footer-text">¿Ya tienes una cuenta? Ingresa aquí</a></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
            <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3"></div>
        </div>   
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center"> 2021 © Total Envios <a
                    href="https://www.wrappixel.com/"></a>
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- Bootstrap tether Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="js/app-style-switcher.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.js"></script>
    <script src="js/main.js"></script>
</body>

</html>