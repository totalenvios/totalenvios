<?php
// error_reporting(0);
        // ini_set('display_errors', 0);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        
        include 'includes/connection.class.php';
        include 'includes/users.class.php';
        include 'includes/packages.class.php';
        include 'includes/headquarters.class.php';
        include 'includes/stock.class.php';
        include 'includes/warehouses.class.php';
        include 'includes/depart.class.php';


        $users = new Users;
        $allUsers = $users->findAll();
        $packages = new Packages;
        $allPackages = $packages->findAll();
        $depart = new Depart;
        $allDeparts = $depart->findAll();
        $headquarters = new Headquarters;
        $allHeadquarters = $headquarters->findAll();
        $allOperators = $users->findAllOperators();
        $allClients = $users->findAllClients();
        $states = $packages->findStates();
        $allAddressStates = $users->findAddressStates();
        $allAddressCities = $users->findAddressCities();
        $userTypes = $users->findRegisterTypes();
        $warehouses = new Warehouses;

        $allCities = $warehouses->findCiudades();
        
        $createWh = $warehouses->generateIdForWarehouse(0, 0, "direccion", "2021-06-16 16:14:33", 100.00, 3, "code", 10.00, 1, 1, 0, 1);
        $stock = new Stock;

        $allCountries = $users->findAddressee(); 
        $allconuntriesRegister = $users->findCountryForRegister();
        $allAddressStates = $users->findAddressStates();
       
        $warehouseStates = $warehouses->findWStates();
        $allWarehouses = $warehouses->findAll();
        $findLast = $warehouses->findLast();
        $boxes = $packages->findBoxes();
        $trips = $warehouses->findTrips();
        $categories = $warehouses->findCategories();
        $countries = $warehouses->findCountries();

        session_start();

        $regions = $users->findRegions();

        $userDestiny = $users->findByEmail($_SESSION['email'])['name'].' '.$users->findByEmail($_SESSION['email'])['lastname'];
        $userDestinyId = $users->findByEmail($_SESSION['email'])['id'];
        $dataUser = $users->findByEmail($_SESSION['email']);
        $destiny = $users->findAddressById($dataUser['country']);
        $curriers = $packages->findCurriers();
        $regionCurrier = $users->findRegionByZone($dataUser['country']);

        // CREAR EL WAREHOUSE ANTES DE ENTRAR A LA VISTA



        if ($_SESSION['state'] != 1) {
            header('Location: login.php');
        }

        $date_in = date('2020-m-d');
        $date_out = date('Y-m-d');
        $digits = 5;
        $fecha = new DateTime();
        $dataFecha = $fecha->getTimestamp();
        $lastId = ($warehouses->findLastId()['id']+1);
       $code = 'TE-WH'.$createWh;
        
        $lastIdForBox = ($warehouses->findLastIdBox()['id']+1);
        $boxCode = $lastIdForBox;
        $bulk = isset($_REQUEST['bulk']) ? $_REQUEST['bulk'] : 0;

        $nextFriday = strtotime('next friday');
        $nextFriday = date('Y-m-d', $nextFriday);

    ?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

            include 'head.php';

        ?>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body style="background:#e3e3e3ff;">


<div class="modal fade" id="confirmCreateWarehouse" tabindex="-1" role="dialog" aria-labelledby="confirmCreateWarehouseLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirmCreateWarehouseLabel">CONFIRMACIÓN DE CREACIÓN DE WAREHOUSE</h5>
                        <button onclick="$('#confirmCreateWarehouse').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body text-center">
                    <div class="container-fluid text-center">
                        <div class="row text-center">
                            <div class="col-lg-12 col-xlg-12 col-md-12 text-center">
                                <img src="icons/alert.png" width="60" height="60"><br><br>
                            </div>    
                            <h4 style="color:#005574;">Al crear el paquete con los datos suministrados estarías confirmando la creación del warehouse y el código correspondiente<br>¿Desea continuar con esta operación?</h4>
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="alert alert-danger register-error" role="alert">
                                </div>
                                <div class="alert alert-success register-success" role="alert">
                                </div>

                               <input type="text" id="user_id_modal" hidden>

                                <div class="form-group mb-4">
                                    <div class="btn-group" style="content-align:center; width:100%">
                                                           
                                                        
                                                      
                                                          
    

                                    <div><a onclick="addBox();"class="btn btn-primary" type="button" style="background-color:white; color:#005574; border:2px solid white;"><i class="fas fa-check"></i></a></div>
                                    
                                
                                    </div>
                                </div>
                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade" id="newUserModal" tabindex="-1" role="dialog" aria-labelledby="newUserModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="newUserModalLabel">Registro de nuevo cliente</h5>
                    <button onclick="$('#newUserModal').modal('hide');" type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 col-md-12">

                                <div class="alert alert-danger user-error" role="alert">
                                </div>
                                <div class="alert alert-success user-success" role="alert">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">

                                    </div>
                                    <div class="col-sm-12 col-md-5 col-lg-3 col-xl-5"></div>

                                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4"
                                        style="text-align:right; content-align:right;">
                                        <div class="btn-group" align="right">
                                            <a id="muchosDestinatarios" onclick="salirRegistro();"
                                                class="btn btn-primary"
                                                style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i
                                                    style="color:white;" class="fas fa-close"></i> Salir</a>
                                            <a id="boton-destinatario" onclick="AgregarDest();" class="btn btn-primary"
                                                style="background-color:white; color:#ff554dff; border:2px solid #ff554dff;"
                                                disabled><i style="color:#ff554dff;" class="fas fa-plus"></i> Registrar
                                                destinatario</a>
                                        </div>

                                    </div>
                                </div><br>
                                <div class="card" id="card-registro-usuarios-wh">
                                    <div class="card-body">
                                        <div align="center"
                                            style="background-color:#005574; color:white; width:100%; padding:8px;">
                                            <h4>Nuevo Usuario</h4>
                                        </div><br>
                                        <div class="row">
                                            <input id="user_id" type="text" placeholder="Nombre Completo"
                                                class="form-control" hidden>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label class="user_type_wh">Tipo de usuario *</label>

                                                    <select readonly="readonly" id="user_type_wh" class="form-select">
                                                        <option selected="selected" value="2">Persona</option>
                                                        <option value="5">Empresa</option>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="name_wh">Nombre *</label>

                                                    <input id="name_wh" type="text" placeholder="Nombre Completo"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="lastname_wh">Apellido *</label>

                                                    <input id="lastname_wh" type="text" placeholder="Apellido Completo"
                                                        class="form-control">
                                                </div>
                                            </div>


                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="document_wh">Documento *</label>

                                                    <input id="document_wh" type="text" placeholder="# Documento"
                                                        class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">

                                                <div class="form-group">
                                                    <label for="company_email_wh">Correo electrónico</label>
                                                    <input type="email" placeholder="example@email.com"
                                                        class="campos-input-login" name="example-email" id="email_wh">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="phone_wh">Teléfono *</label>

                                                    <input id="phone_wh" type="text" placeholder="123 456 7890"
                                                        class="form-control">

                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="phone_house_wh">Teléfono de casa</label>

                                                    <input id="phone_house_wh" type="text" placeholder="123 456 7890"
                                                        class="form-control">

                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                                <div class="form-group">
                                                    <label for="date_wh" class="col-md-12 p-0">Fecha de Nacimiento
                                                        *</label>
                                                    <input type="date" placeholder="1990-01-01" class="form-control"
                                                        id="born_date_wh" style="background: url('icons/cumpleaños.png');background-repeat: no-repeat;
                                                background-position: 15px center;
                                                background-size: 20px;
                                                display: flex;
                                                align-items: center; padding-left: 45px;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="address_destiny_company">Pais</label>
                                                    <select id="address_destiny_company"
                                                        class="form-select campos-input-login">
                                                        <option value="0">País</option>
                                                        <?php  
                                                            foreach ($allconuntriesRegister as $key => $value) {
                                                                                                ?>
                                                        <option value="<?php echo $value['id']; ?>">
                                                            <?php echo $value['name']; ?></option>
                                                        <?php
                                                                                                 }
                                                                                                ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 address_state_company">
                                                <div class="form-group">
                                                    <label for="address_state_company">Estado</label>
                                                    <input id="address_state_company" type="text" placeholder="Estado"
                                                        class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 select_state_company"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_state_company">Estado</label>
                                                    <select id="select_state_company"
                                                        class="form-select campos-input-login">
                                                        <option value="0">Estado</option>
                                                        <?php  
                                                                                                    foreach ($allAddressStates as $key => $value) {
                                                                                                ?>
                                                        <option value="<?php echo $value['id_estado']; ?>">
                                                            <?php echo $value['estado']; ?></option>
                                                        <?php
                                                                                                }
                                                                                                ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 address_city_company">
                                                <div class="form-group">
                                                    <label for="address_city_company">Ciudad</label>
                                                    <input id="address_city_company" type="text" placeholder="Ciudad"
                                                        class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 select_city_company"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_city_company">Ciudad</label>
                                                    <select id="select_city_company"
                                                        class="form-select campos-input-login">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div
                                                class="col-sm-10 col-md-10 col-lg-10 col-xl-10 address_address_company">
                                                <div class="form-group">
                                                    <label for="address_company">Dirección</label>
                                                    <input id="address_company" type="text" placeholder="Dirección"
                                                        class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10 select_address_company"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_address_company">Dirección</label>
                                                    <input id="select_address_company" type="text"
                                                        placeholder="Dirección" class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                                <label for="select_address_company">Registrar usuario</label>
                                                <a id="muchosDestinatarios" onclick="RegUsuarioWarehouse(1);"
                                                    class="btn btn-primary"
                                                    style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i
                                                        style="color:white;" class="fas fa-save"></i> Guardar</a>
                                            </div>

                                        </div>








                                    </div>
                                </div>
                            </div>

                            <div id="registrarDestinatario" style="display:none;">
                                <!-- registro de destinatario oculto hasta completar registro -->
                                <div class="card">
                                    <div class="card-body">
                                        <div align="center"
                                            style="background:#005574; color:white; width:100%; padding:8px;">
                                            <h4>Nuevo destinatario</h4>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="name_destiner">Nombre *</label>

                                                    <input id="name_destiner_user" type="text"
                                                        placeholder="Nombre Completo" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="lastname_destiner">Apellido *</label>

                                                    <input id="lastname_destiner_user" type="text"
                                                        placeholder="Apellido Completo" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="contact_phone_destiner">Telefono de contacto</label>

                                                    <input type="text" placeholder="123 456 7890" class="form-control"
                                                        id="contact_phone_destiner_user">

                                                </div>
                                            </div>
                                        </div>
                                        <input id="countryVenezuela" type="hidden" value="0">
                                        <div class="row">

                                            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="address_destiny_destiner">Pais destino</label>
                                                    <select id="address_destiny_destiner"
                                                        class="form-select campos-input-login">
                                                        <option value="0">¿Cuál es destino?</option>
                                                        <?php  
                                                                                        foreach ($allCountries as $key => $value) {
                                                                                    ?>
                                                        <option value="<?php echo $value['id']; ?>">
                                                            <?php echo $value['name']; ?></option>
                                                        <?php
                                                                                        }
                                                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 address_state_destiner">
                                                <div class="form-group">
                                                    <label for="address_state_destiner">Estado</label>
                                                    <input id="address_state_destiner" type="text" placeholder="Estado"
                                                        class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 select_state_destiner"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_state_destiner">Estado</label>
                                                    <select id="select_state_destiner"
                                                        class="form-select campos-input-login">
                                                        <option value="0">¿Cuál estado?</option>
                                                        <?php  
                                                                                        foreach ($allAddressStates as $key => $value) {
                                                                                    ?>
                                                        <option value="<?php echo $value['id_estado']; ?>">
                                                            <?php echo $value['estado']; ?></option>
                                                        <?php
                                                                                        }
                                                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 address_city_destiner">
                                                <div class="form-group">
                                                    <label for="address_city_destiner">Ciudad</label>
                                                    <input id="address_city_destiner" type="text" placeholder="Ciudad"
                                                        class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 select_city_destiner"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_city_destiner">Ciudad</label>
                                                    <select id="select_city_destiner"
                                                        class="form-select campos-input-login">
                                                    </select>
                                                    <input id="ciudad" type="text"
                                                        placeholder="Dirección" class="form-control campos-input-login">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div
                                                class="col-sm-12 col-md-12 col-lg-12 col-xl-12 address_address_destiner">
                                                <div class="form-group">
                                                    <label for="address_Destiner">Dirección</label>
                                                    <input id="address_destiner" type="text" placeholder="Dirección"
                                                        class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 select_address_destiner"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_address_destiner">Dirección</label>
                                                    <input id="select_address_destiner" type="text"
                                                        placeholder="Dirección" class="form-control campos-input-login">
                                                </div>
                                            </div>

                                            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                                <label for="select_address_company">Registrar nuevo destinatario</label>
                                                <a id="muchosDestinatarios" onclick="RegDestinerForUserWarehouse();"
                                                    class="btn btn-primary"
                                                    style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i
                                                        style="color:white;" class="fas fa-save"></i> Guardar</a>
                                            </div>
                                        </div>





                                    </div>
                                </div>
                            </div>
                            <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>

    <!-- MODAL DE REGISTRO DE DESTINATARIO -->
    <!-- ++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="modal fade" id="newDestinerModal" tabindex="-1" role="dialog" aria-labelledby="newDestinerModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="newDestinerModalLabel">Registro de nuevo destinatario</h5>
                    <button onclick="$('#newDestinerModal').modal('hide');" type="button" class="close"
                        data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="alert alert-danger user-error" role="alert">
                                </div>
                                <div class="alert alert-success user-success" role="alert">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                        <input id="user_id_for_destiner" type="text" class="form-control" hidden>
                                    </div>
                                    <div class="col-sm-12 col-md-5 col-lg-3 col-xl-5"></div>

                                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4"
                                        style="text-align:right; content-align:right;">
                                        <div class="btn-group" align="right">
                                            <a id="muchosDestinatarios" onclick="RegDestinatario()"
                                                class="btn btn-primary"
                                                style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i
                                                    style="color:white;" class="fas fa-save"></i> Guardar</a>
                                            <!-- <a id="boton-destinatario" onclick="AgregarDest();" class="btn btn-primary" style="background-color:white; color:#ff554dff; border:2px solid #ff554dff;"><i style="color:#ff554dff;" class="fas fa-plus"></i> Registrar destinatario</a> -->
                                        </div>

                                    </div>
                                </div><br>
                                <div class="card" id="card-registro-usuarios-wh">
                                    <div class="card-body">

                                        <div align="center"
                                            style="background:#005574; color:white; width:100%; padding:8px;">
                                            <h5>Destinatario</h5>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="name_destiner">Nombre *</label>

                                                    <input id="name_destiner" type="text" placeholder="Nombre Completo"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="lastname_destiner">Apellido *</label>

                                                    <input id="lastname_destiner" type="text"
                                                        placeholder="Apellido Completo" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="contact_phone_destiner">Telefono de contacto</label>

                                                    <input type="text" placeholder="123 456 7890" class="form-control"
                                                        id="contact_phone_destiner">

                                                </div>
                                            </div>
                                        </div>
                                        <input id="countryVenezuela" type="hidden" value="0">
                                        <div class="row">

                                            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                                <div class="form-group">
                                                    <label for="address_destiny_destiner_dos">Pais destino</label>
                                                    <select id="address_destiny_destiner_dos"
                                                        class="form-select campos-input-login">
                                                        <option value="0">¿Cuál es destino?</option>
                                                        <?php  
                                                                                        foreach ($allCountries as $key => $value) {
                                                                                    ?>
                                                        <option value="<?php echo $value['id']; ?>">
                                                            <?php echo $value['name']; ?></option>
                                                        <?php
                                                                                        }
                                                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div
                                                class="col-sm-12 col-md-4 col-lg-6 col-xl-4 address_state_destiner_dos">
                                                <div class="form-group">
                                                    <label for="address_state_destiner_dos">Estado</label>
                                                    <input id="address_state_destiner_dos" type="text"
                                                        placeholder="Estado" class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 select_state_destiner_dos"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_state_destiner_dos">Estado</label>
                                                    <select id="select_state_destiner_dos"
                                                        class="form-select campos-input-login">
                                                        <option value="0">¿Cuál estado?</option>
                                                        <?php  
                                                                                        foreach ($allAddressStates as $key => $value) {
                                                                                    ?>
                                                        <option value="<?php echo $value['id_estado']; ?>">
                                                            <?php echo $value['estado']; ?></option>
                                                        <?php
                                                                                        }
                                                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 address_city_destiner_dos">
                                                <div class="form-group">
                                                    <label for="address_city_destiner_dos">Ciudad</label>
                                                    <input id="address_city_destiner_dos" type="text"
                                                        placeholder="Ciudad" class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-6 col-xl-4 select_city_destiner_dos"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_city_destiner_dos">Ciudad</label>
                                                    <select id="select_city_destiner_dos"
                                                        class="form-select campos-input-login">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div
                                                class="col-sm-12 col-md-12 col-lg-12 col-xl-12 address_address_destiner_dos">
                                                <div class="form-group">
                                                    <label for="address_Destiner_dos">Dirección</label>
                                                    <input id="address_destiner" type="text" placeholder="Dirección"
                                                        class="form-control campos-input-login">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 select_address_destiner"
                                                style="display: none;">
                                                <div class="form-group">
                                                    <label for="select_address_destiner_dos">Dirección</label>
                                                    <input id="select_address_destiner_dos" type="text"
                                                        placeholder="Dirección" class="form-control campos-input-login">
                                                </div>
                                            </div>
                                        </div>





                                    </div>
                                </div>
                            </div>


                            <!-- registro de destinatario oculto hasta completar registro -->








                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <!-- ++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div> -->
    <div id="main-wrapper" data-boxed-layout="full">
        <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
                }

            ?>


        <div class="page-wrapper" style="background:#e3e3e3ff; margin-bottom:40px;"><br>


            <div class="row">
                <div class="alert alert-danger register-error" role="alert">
                </div>
                <div class="alert alert-success register-success" role="alert">
                </div>
                <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1">
                    <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                </div>
                <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                     <!--<input type="number" id="warehouse_id" value="<?php echo $createWh; ?>" hidden>-->
                     <input type="number" id="operator_id" value="<?php echo $users->findByEmail($_SESSION['email'])['id']; ?>" hidden>
                     <input type="number" id="warehouse_id" value="<?php echo $createWh; ?>" hidden>
                    <strong>
                       <!-- <h2 style="color:#005574;" id="code-title"><?php echo $code; ?></h2>-->
                       <h3 style="color:#005574;"> <strong>CREAR WAREHOUSE</strong> </h3>
                    </strong>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                    <small style="margin-bottom:35px; color:black;"><strong>FECHA DE REGISTRO:
                        </strong><?php echo date('m-d-Y') ?></small>
                </div>
                <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2" align="right">
                    <button id="wState" class="boton-total-envios-registro" style="color:white; width:100px;" value="2"
                        onclick="changeSateWh();"><i class="fas fa-lock"></i></button>
                </div>
            </div>


            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="user_from" style="color:black;">CLIENTE</label>
                                <div class="input-group mb-3">

                                    <input id="id_user_from" hidden>
                                    <input type="text" class="form-control" placeholder="Nombre"
                                        aria-label="Recipient's username" aria-describedby="basic-addon2"
                                        onkeyup="searchUserPackage();" id="user_from">

                                    <div class="input-group-append">
                                        <button
                                            onclick="$('#warehouseModal').modal('hide'); $('#newUserModal').modal('show');"
                                            class="btn btn-outline-secondary"
                                            style="background-color:#ff554dff; color:white;"><i style="color:white"
                                                class="fas fa-plus"></i></button>
                                    </div>
                                </div>
                                <ul id="myUS">
                                </ul>
                                <!-- <ul id="myDestiner">
                        </ul> -->
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="user_from" style="color:black;">DESTINATARIO</label>
                                <div class="input-group mb-3">
                                    <input id="id_user_to" hidden>
                                    <input type="text" class="form-control" placeholder="Nombre"
                                        aria-label="Recipient's username" aria-describedby="basic-addon2"
                                        onkeyup="searchFilter();" id="myInput">

                                    <div class="input-group-append">
                                        <button onclick="modalDestinerOpen();" class="btn btn-outline-secondary"
                                            style="background-color:#ff554dff; color:white;" disabled><i style="color:white"
                                                class="fas fa-plus"></i></button>
                                    </div>
                                </div>
                                <ul id="myDestiner">
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2 region">
                            <div class="form-group">
                                <label for="region" style="color:black;">REGIÓN</label>

                                <input id="region" placeholder="REGION" class="input-register-warehouse">

                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label for="num_guia" style="color:black;">#GUIA</label>

                                <input id="num_guia" placeholder="#guia" class="input-register-warehouse">

                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <div class="btn-group text-center">

                                <button value="1" id="trip_air" onclick="changeTripAir();" class="btn btn-primary"
                                    style="background-color:#005574; color:white; border:2px solid #005574; width:100px;"><img
                                        src="icons/air.png" alt=""></button>
                                <button value="2" id="trip_ocean" onclick="changeTripOcean();" class="btn btn-primary"
                                    style="background-color:white; color:#005574; border:2px solid #005574; width:100px;"><img
                                        src="icons/ocean.png" alt=""></button>

                            </div>
                            <input id="trip" value="1" hidden>
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2" hidden>
                            <div class="form-group">
                                <label for="trip" style="color:black;">TIPO DE ENVIO</label>

                                <select id="trip" class="input-register-warehouse">
                                    <?php  

                                                foreach ($trips as $key => $value) {
                                            ?>
                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php
                                                }

                                            ?>
                                </select>

                            </div>

                        </div>

                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="country_destiner_get">Pais destino</label>
                                <select id="country_destiner_get" class="form-select campos-input-login">
                                    <option value="0">¿Cuál es destino?</option>
                                    <?php  
                                foreach ($allCountries as $key => $value) {
                            ?>
                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php
                                }
                            ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="state_destiner_get">Estado</label>
                                <select id="state_destiner_get" class="form-select campos-input-login">
                                    <option value="0">¿Cuál estado?</option>
                                    <?php  
                            foreach ($allAddressStates as $key => $value) {
                        ?>
                                    <option value="<?php echo $value['id_estado']; ?>"><?php echo $value['estado']; ?>
                                    </option>
                                    <?php
                            }
                        ?>
                                </select>
                            </div>

                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">

                            <div class="form-group">
                                <label for="address_send">Ciudad</label>
                                <select id="address_send" class="form-select campos-input-login">
                                    <option value="0">¿Cuál ciudad?</option>
                                    <?php  
                            foreach ($allCities as $key => $value) {
                        ?>
                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php
                            }
                        ?>
                                </select>
                            </div>

                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label for="address_send">DIRECCIÓN</label>
                                <input id="address_address_destiner_get" type="text"
                                    class=" form-control input-register-warehouse" placeholder="DIRECCIÓN DE ENTREGA">
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="address_send">TELÉFONO</label>
                                <input id="contact_phone_get" type="text" class="form-control input-register-warehouse"
                                    placeholder="TELÉFONO">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <label for="nota">NOTA</label>
                                <input id="nota" type="text" class="form-control input-register-warehouse"
                                    placeholder="Nota a considerar sobre el warehouse a crear">
                        </div>
                    </div><br>

                    <hr>
                    <div class="row">
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="depart" style="color:black;">DEPARTAMENTO</label>
                                <select id="depart" class="input-register-warehouse">
                                    <?php  

                                                    foreach ($allDeparts as $key => $value) {
                                                        $opt = '';
                                                        if ($value['id'] == 1) {
                                                            $opt = 'selected';
                                                        }
                                                ?>
                                    <option <?php echo $opt; ?> value="<?php echo $value['id']; ?>">
                                        <?php echo $value['nombre']; ?></option>
                                    <?php
                                                    }

                                                ?>
                                </select>
                            </div>

                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="currier" style="color:black;">CURRIER</label>

                                <select id="currier" class="input-register-warehouse">
                                    <?php  
                                                    
                                                    foreach ($curriers as $key => $value) {
                                                        $opt = '';
                                                        if ($zoneCurrier[0]['id'] == $value['id']) {
                                                            $opt = 'selected';
                                                        }
                                                ?>
                                    <option <?php echo $opt; ?> value="<?php echo $value['id']; ?>">
                                        <?php echo $value['name']; ?></option>
                                    <?php
                                                    }

                                                ?>
                                </select>
                            </div>

                        </div>
                        <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1 button_register_new_currier">
                            <label for="currier" style="color:black;">C. INTERNO</label>
                            <button class="boton-total-envios-registro-config" onclick="newCurrierIntern();"><i
                                    class="fas fa-plus fa-lg" aria-hidden="true"></i></button>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                            <div class="currier_Intern">
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">


                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="date_send" style="color:black;">FECHA DE SALIDA</label>
                                <input type="date" placeholder="Fecha de Envio" class="input-register-warehouse"
                                    id="date_send" value="<?php echo $nextFriday; ?>">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-xlg-4 col-md-4">
                    <div class="alert alert-danger register-box-error" role="alert">
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-9 col-xlg-9 col-md-9">
                                    <h4> <strong> CAJAS</strong></h4>
                                </div>

                            </div>
                            <hr>
                            <br>
                            <div class="row">
                                <div class="col-lg-5 col-xlg-5 col-md-5" hidden>
                                    <input id="box-id" type="hidden" value="0">
                                    <div class="form-group">
                                        <!-- <label for="box-name">BOX</label> -->
                                        <input onkeyup="searchPromos();" id="promociones" type="text"
                                            placeholder="Busca una promoción" class="form-control">
                                        <ul id="myPromo">
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-xlg-5 col-md-5">
                                    <input id="box-id" type="hidden" value="0">
                                    <div class="form-group">
                                        <!-- <label for="box-name">BOX</label> -->
                                        <input onkeyup="searchBoxes();" id="box-name" type="text"
                                            placeholder="Busca un BOX" class="form-control">
                                        <ul id="myBOX">
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-xlg-1 col-md-1">
                                    <a href="settings-box.php" class="btn btn-primary"
                                        style="background-color:white; color:#005574; border:2px solid #005574;"><i
                                            class="fas fa-plus"></i></a>
                                </div>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group">
                                        <label for="box-height">Alto</label>
                                        <input id="box-height" type="number" placeholder="Alto" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group">
                                        <label for="box-width">Ancho</label>

                                        <input id="box-width" type="number" placeholder="Ancho" class="form-control">

                                    </div>
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group">
                                        <label for="box-lenght">Largo</label>

                                        <input id="box-lenght" type="number" placeholder="Largo" class="form-control">

                                    </div>
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group">
                                        <label for="box-weight">Peso</label>

                                        <input id="box-weight" type="number" placeholder="Peso" class="form-control">

                                    </div>
                                </div>
                                <div class="col-lg-4 col-xlg-4 col-md-4">
                                    <div class="form-group">
                                        <label for="category">costo</label>

                                        <input id="box-cost" type="text" placeholder="Costo de la mercancia"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-xlg-5 col-md-5">
                                    <div class="form-group">
                                        <label for="category">Categoria</label>

                                        <select id="category" class="form-control">
                                            <?php  

                                                            foreach ($categories as $key => $value) {
                                                        ?>
                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?>
                                            </option>
                                            <?php
                                                        }

                                                        ?>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-lg-5 col-xlg-5 col-md-5">
                                    <div class="form-group">
                                        <label for="category">Descripción</label>

                                        <input id="box-description" type="text" placeholder="Descripcion"
                                            class="form-control">
                                        <input id="box_id" hidden>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2" id="register-box-for-table"
                                    style="display:block;">
                                    <label for="category"></label>
                                    <button id="addBoxButton" onclick="addBox();" class="btn-cargar-cajas"
                                        style="margin-top:25px;"><i class="fas fa-check"></i></button>
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2" id="edit-box-for-table" style="display:none;">
                                    <label for="category"></label>
                                    <button id="addBoxButton" onclick="editBox();" class="btn-cargar-cajas"
                                        style="margin-top:25px;"><i class="fas fa-edit"></i></button>
                                </div>
                            </div><br>
                        </div>
                    </div>

                </div>
                <div class="col-lg-8 col-xlg-8 col-md-8">

                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-5 col-xlg-5 col-md-5">
                                    <h4> <strong> MEDIDAS </strong></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group mb-4">
                                        <label for="pieCubico">FT<sup>3</sup></label>

                                        <input id="pieCubico" type="number" placeholder="0.00" class="form-control"
                                            readonly>

                                    </div>
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group mb-4">
                                        <label for="LBVOL">LBVol</label>

                                        <input id="LBVOL" type="number" placeholder="0.00" class="form-control"
                                            readonly>

                                    </div>
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group mb-4">
                                        <label for="pesolb">PESO LB</label>

                                        <input id="pesolb" type="number" placeholder="0.00" class="form-control"
                                            readonly>

                                    </div>
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group mb-4">
                                        <label for="pesokg">PESO KG</label>

                                        <input id="pesokg" type="number" placeholder="0.00" class="form-control"
                                            readonly>

                                    </div>
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group mb-4">
                                        <label for="metrocubico">METRO<sup>3</sup></label>

                                        <input id="metrocubico" type="number" placeholder="0.00" class="form-control"
                                            readonly>

                                    </div>
                                </div>

                                <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2" align="right" id="aereo"
                                    style="display:none;">

                                    <img src="icons/air.png" alt="">
                                </div>
                                <div class="col-sm-2 col-md-2 col-lg-2 col-xl-" align="right" id="maritimo"
                                    style="display:none;">

                                    <img src="icons/ocean.png" alt="">
                                </div>

                            </div>

                        </div>
                    </div>



                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-xlg-6 col-md-6">
                                    <h4><strong>COSTOS</strong></h4>
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group mb-4">
                                        <label for="tarifa_base" style="color:#005574;"><strong>TARIFA</strong></label>

                                        <input id="tarifa_base" type="number" class="form-control" value="0.00"
                                            style="border-color:#fc3c3d;">

                                    </div>
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group mb-4">
                                        <label for="tasa_base_te" style="color:#005574;"><strong>TARIFA
                                                TE</strong></label>

                                        <input id="tasa_base_te" type="number" value="0.00" class="form-control"
                                            style="border-color:#fc3c3d;">

                                    </div>
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group mb-4">
                                        <label for="tarifaInterna_base" style="color:#005574;">T. INTERNA</label>

                                        <input id="tarifaInterna_base" type="number" value="0.00"
                                            class="form-control" style="border-color:#fc3c3d;">

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group mb-4">
                                        <label for="currier_tasa">COSTO CURRIER</label>

                                        <input id="currier_tasa" type="number" placeholder="0.00" class="form-control"
                                            readonly>

                                    </div>
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group mb-4">
                                        <label for="costo_interno">COSTO INTERNO</label>

                                        <input id="costo_interno" type="number" placeholder="0.00" class="form-control"
                                            readonly>

                                    </div>
                                </div>

                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group mb-4">
                                        <label for="material_tasa">COSTO MATERIAL</label>

                                        <input id="material_tasa" type="number" placeholder="0.00" class="form-control"
                                            value="0.00">

                                    </div>
                                </div>
                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group mb-4">
                                        <label for="costo_total">COSTO TOTAL</label>

                                        <input id="costo_total" type="number" placeholder="0.00" class="form-control"
                                            value="0.00">

                                    </div>
                                </div>

                                <div class="col-lg-2 col-xlg-2 col-md-2">
                                    <div class="form-group mb-4">
                                        <label for="total">MONTO TOTAL</label>

                                        <input id="total" type="number" placeholder="0.00" class="form-control">

                                    </div>
                                </div>
                                <div class="col-lg-1 col-xlg-1 col-md-1">
                                    <div class="form-group mb-4">
                                        <label for="ganancia">GAN</label>

                                        <input id="ganancia" type="number" placeholder="0.00" class="form-control">

                                    </div>
                                </div>
                                <div class="col-lg-1 col-xlg-1 col-md-1">
                                    <div class="form-group mb-4">
                                        <label for="porcGanancia">%GAN</label>

                                        <input id="porcGanancia" type="number" placeholder="0.00" class="form-control">

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>





            <div class="form-group" hidden>
                <label for="box-code">Nro. de caja</label>
                <input id="box-code" type="text" placeholder="Codigo" class="form-control"
                    value="<?php echo $boxCode; ?>" readonly="readonly">
            </div>








            <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1" hidden>
                <div class="form-group mb-4">
                    <label class="col-sm-12">Seguro</label>

                    <input id="seguro" type="number" placeholder="Costo" value=0 class="form-control">

                </div>
            </div>
            <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1" hidden>
                <div class="form-group mb-4">
                    <label class="col-sm-12">Logist</label>

                    <input id="logistica" type="number" value=0 placeholder="Costo" class="form-control">

                </div>
            </div>

            <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1" hidden>
                <div class="form-group mb-4">
                    <label class="col-sm-12">G Ext.</label>

                    <input id="gastosExtra" type="number" placeholder="Costo" value=0 class="form-control">

                </div>
            </div>










            <div class="row">
                <div class="col-md-12">
                    <div id="wboxes" class="card" style="display: none;">
                        <div class="card-body">
                            <h3 class="text-center">
                                Cajas
                            </h3>
                            <table class="table table-warehouse"
                                style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                <th>#</th>
                                <th>CATEGORIA</th>
                                <th>DESCRIPCION</th>
                                <th>COSTO</th>

                                <th>LARGO</th>
                                <th>ANCHO</th>
                                <th>ALTO</th>
                                <th>PESO</th>

                                <th>FT<sup>3</sup></th>
                                <th>LB VOL</th>
                                <th>T. CURRIER</th>
                                <th>T. INTERNA</th>
                                <th>MATERIAL</th>
                                <th>COSTO TOTAL</th>
                                <!-- <th>TafINT</th>
                                                <th>CostINT</th> -->
                                <!-- <th>Subtotal</th> -->
                                <!-- <th>Tasa Taf</th> -->
                                <th>TARIFA TE</th>
                                <th>MONTO TOTAL</th>
                                <th>GANANCIA</th>
                                <th>% GANANCIA</th>
                                <th>ACCIÓN</th>
                                <!-- <th>% GANAN</th> -->
                                <!-- <th>Eliminar</th> -->
                                <tbody id="boxes-table">
                                    <tr></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-1 col-xlg-1 col-md-1">
                            <div class="form-group mb-4">
                                <label class="col-sm-12">FT<sup>3</sup></label>

                                <input id="pieCubTotales" type="number" placeholder="FT3" value=0.00
                                    class="form-control">

                            </div>

                        </div>
                        <div class="col-lg-1 col-xlg-1 col-md-1">
                            <div class="form-group mb-4">
                                <label class="col-sm-12">LBVOL</label>

                                <input id="lbvolTotales" type="number" placeholder="LB VOL" value=0.00
                                    class="form-control">

                            </div>

                        </div>
                        <div class="col-lg-1 col-xlg-1 col-md-1">
                            <div class="form-group mb-4">
                                <label class="col-sm-12">PESO</label>

                                <input id="pesoTotal" type="number" placeholder="PESO LB" value=0.00
                                    class="form-control">

                            </div>
                        </div>
                        <div class="col-lg-1 col-xlg-1 col-md-1"></div>
                        <div class="col-lg-2 col-xlg-2 col-md-2">
                            <div class="form-group mb-4">
                                <label class="col-sm-12">COSTO TOTAL</label>

                                <input id="monto_total_box" type="number" placeholder="MONTO TOTAL" value=0.00
                                    class="form-control">

                            </div>
                        </div>
                        <div class="col-lg-2 col-xlg-2 col-md-2">
                            <div class="form-group mb-4">
                                <label class="col-sm-12">MONTO TOTAL</label>

                                <input id="costo_total_box" type="number" placeholder="TOTAL" value=0.00
                                    class="form-control">

                            </div>

                        </div>
                        <div class="col-lg-2 col-xlg-2 col-md-2">
                            <div class="form-group mb-4">
                                <label class="col-sm-12">GANANCIA TOTAL</label>

                                <input id="ganancia_total" type="number" placeholder="GANANCIA" value=0.00
                                    class="form-control">

                            </div>

                        </div>
                        <div class="col-lg-2 col-xlg-2 col-md-2">
                            <div class="form-group mb-4">
                                <label class="col-sm-12">% GANANCIA TOTAL</label>

                                <input id="total_porcentaje_ganancia" type="number" placeholder="%GANANCIA" value=0.00
                                    class="form-control">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 col-xlg-8 col-md-8">

                </div>
                <div class="col-lg-4 col-xlg-4 col-md-4" align="right">
                    <div class="btn-group">
                    <button onclick="deleteDataForWhCreate();" class="btn btn-primary" style="background-color:white; color:#ff554dff; border:2px solid #ff554dff;"><i style="color:#ff554dff;"
                            class="fas fa-redo"></i> CANCELAR</button>
                    <button onclick="addWarehouse();" class="btn btn-primary" style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i style="color:white"
                            class="fas fa-save"></i> CONFIRMAR</button>
                            
                    </div>
                                                
                </div>
            </div>



            <!--                                         
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div> -->
            <!-- <footer class="footer text-center"> 2021 © Total Envios
                </footer> -->
            <?php 

            include 'foot.php';

        ?>
            <?php 

            if ($bulk != '0') {
            ?>
            <input id="packagesForBulk" type="hidden" value="1">
            <?php
                echo '<script type="text/javascript">'
                . '$( document ).ready(function() {'
                . '$("#warehouseModal").modal("show");'
                . 'getBulkPackages();'
                . '});'
                . '</script>';
            }else{
            ?>
            <input id="packagesForBulk" type="hidden" value="0">
            <?php
            }
        ?>
            <script>
                $(document).ready(function () {
                    localStorage.removeItem('boxes');
                })
                
            </script>

</body>

</html>