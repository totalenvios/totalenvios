<?php  


			ini_set('display_errors', 1);
		    ini_set('display_startup_errors', 1);
		    error_reporting(E_ALL);
		    
		    include 'includes/connection.class.php';
		    include 'includes/users.class.php';
		    include 'includes/packages.class.php';
		    include 'includes/headquarters.class.php';
		    include 'includes/stock.class.php';
		    include 'includes/warehouses.class.php';
		    include 'includes/depart.class.php';

		    $users = new Users;
		    $allUsers = $users->findAll();
		    $packages = new Packages;
		    $allPackages = $packages->findAll();
		    $depart = new Depart;
		    $allDeparts = $depart->findAll();
		    $headquarters = new Headquarters;
		    $stock = new Stock;
		    $warehouses = new Warehouses;
		    $departs = new Depart;
		    $allCurriers = $users->findCurriers();
		    $allDepartments = $departs->findAll();
		    $allCategories = $warehouses->findCategories();
		    $allParameters = $warehouses->findAllParameters();
		    $allRegions = $users->findRegions();
		    $allBoxes = $warehouses->findAllBoxes();
		    $allBox = $warehouses->findAllBox();
		    $allTrips = $warehouses->findTrips();
		    $allStates = $warehouses->findWStates();
		    $lastIdForBox = ($warehouses->findLastIdBox()['id']+1);
		    $CRCost = $warehouses->findCRCost();
        	$boxCode = $lastIdForBox;


		?>	
		<html dir="ltr" lang="en">

        <head>
            <?php 

                include 'head.php';

            ?>

		    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
		    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
		    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        </head>
        <body style="background:#e3e3e3ff;">
		<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
            data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
            <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
                }

            ?>
            <?php include 'sidebar.php'; ?>

			<div class="page-wrapper" style="background:#e3e3e3ff; margin-bottom:40px;"><br>
			<div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                                <h4 style="color:#005574;">BOXES</h4>
                            </div>
                        </div><br>
						<div class="alert alert-danger register-error" role="alert">
                        </div>
                        <div class="alert alert-success register-success" role="alert">
                        </div>
						<div class="card">
							<div class="card-body">
								<div class="row">
								<div class="col-lg-3 col-xlg-3 col-md-3" hidden>
										<div class="form-group">
											<label for="name">BOX</label>
											<input style="text-transform:uppercase;" id="box_id" type="text" placeholder="Nombre del BOX" class="form-control"> 
										</div>
									</div>
									<div class="col-lg-3 col-xlg-3 col-md-3">
										<div class="form-group">
											<label for="name">BOX</label>
											<input style="text-transform:uppercase;" id="name" type="text" placeholder="Nombre del BOX" class="form-control"> 
										</div>
									</div>
									<div class="col-lg-1 col-xlg-1 col-md-1">
										<div class="form-group">
											<label for="width">Alto</label>
											<input id="width" type="number" min="0" placeholder="AL" class="form-control"> 
										</div>
									</div>
									<div class="col-lg-1 col-xlg-1 col-md-1">
										<div class="form-group">
											<label for="height">Ancho</label>
											<input id="height" type="number" min="0"  placeholder="LA" class="form-control"> 
										</div>
									</div>
									<div class="col-lg-1 col-xlg-1 col-md-1">
										<div class="form-group">
											<label for="lenght">Largo</label>
											<input id="lenght" type="number" min="0" placeholder="AN" class="form-control"> 
										</div>
									</div>
									<div class="col-lg-1 col-xlg-1 col-md-1">
										<div class="form-group">
											<label for="weigth">Peso</label>
											<input id="weigth" type="number" min="0" placeholder="LB" class="form-control"> 
										</div>
									</div>

									<div class="col-lg-1 col-xlg-1 col-md-1" id="cubic_feet_edit" style="display:none;">
										<div class="form-group">
											<label for="cubic_f">PC3</label>
											<input id="cubic_f" type="number" placeholder="LB" class="form-control"> 
										</div>
									</div>
									<div class="col-lg-1 col-xlg-1 col-md-1" id="lb_vol_edit" style="display:none;">
										<div class="form-group">
											<label for="lb_vol">LBV</label>
											<input id="lb_vol" type="number" placeholder="LB" class="form-control"> 
										</div>
									</div>
									<div class="col-lg-2 col-xlg-2 col-md-2">
										<div class="form-group">
											<label for="box_cost">Costo</label>
											<input id="box_cost" type="number" min="0" placeholder="COSTO" class="form-control"> 
										</div>
									</div>
									<div class="col-lg-1 col-xlg-1 col-md-1" id="register_box_button">
										<button class="boton-total-envios-registro-config" onclick="addBOX();" style="margin-top:30px;"><i class="fas fa-plus fa-lg" aria-hidden="true"></i></button>
										
									</div>
									<div class="col-lg-1 col-xlg-1 col-md-1" id="edit_box_button" style="display:none;">
										<button class="boton-total-envios-registro-config" onclick="editBOX();" style="margin-top:30px;"><i class="fas fa-check fa-lg" aria-hidden="true"></i></button>
										
									</div>
								</div>
								<!-- <div class="row">
									

									
									

								</div> -->
							</div>
						</div>

						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-lg-4 col-xlg-4 col-md-4">
										<input type="text" class="form-control" placeholder="BUSCA UN BOX">
									</div>
									<div class="col-lg-1 col-xlg-1 col-md-1">

									</div>
									<div class="col-lg-4 col-xlg-4 col-md-4">
										<input type="text" class="form-control" placeholder="BUSCA POR COSTOS">
									</div>
								</div><br><br>



							<table class="table table-warehouse" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                        <thead>
                                            
                                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Nombre</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Ancho</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Alto</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Peso</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Largo</th>
											<th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">costo</th>
											<th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Acción</th>
                                        </thead>
                                        <tbody>
                                            <?php  

                                                foreach ($allBox as $key => $value) {
                                            ?>
                                                    <tr>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                            <?php echo $value['name'];?>
                                                        </td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['width']; ?>
                                                        </td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['height']; ?>
                                                        </td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['weigth']; ?>
                                                        </td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['lenght']; ?>
                                                        </td>
														<td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['box_cost']; ?>
                                                        </td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
															<div class="btn-group" role="group" aria-label="Basic example">
																<button onclick="editBoxesCost(<?php echo $value['id']; ?>);" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-edit"></i></button>
																<button onclick="deleteBoxToConfig(<?php echo $value['id']; ?>);" class="btn btn-primary" style="background-color:#005574; color:white; border:2px solid #005574;"><i class="fas fa-trash"></i></button>

															</div>
                                                        </td>
                                                    </tr>
                                            <?php
                                                }

                                            ?>
                                        </tbody>
                                    </table>
							</div>
						</div>
              
		                

							
				        
        </body>
        <!-- <footer class="footer text-center"> 2021 © Total Envios
        </footer> -->
        <?php 

        include 'foot.php';

        ?>