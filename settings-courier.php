<?php  


			ini_set('display_errors', 1);
		    ini_set('display_startup_errors', 1);
		    error_reporting(E_ALL);
		    
		    include 'includes/connection.class.php';
		    include 'includes/users.class.php';
		    include 'includes/packages.class.php';
		    include 'includes/headquarters.class.php';
		    include 'includes/stock.class.php';
		    include 'includes/warehouses.class.php';
		    include 'includes/depart.class.php';

		    $users = new Users;
		    $allUsers = $users->findAll();
		    $packages = new Packages;
		    $allPackages = $packages->findAll();
		    $depart = new Depart;
		    $allDeparts = $depart->findAll();
		    $headquarters = new Headquarters;
		    $stock = new Stock;
		    $warehouses = new Warehouses;
		    $departs = new Depart;
		    $allCurriers = $users->findCurriers();
		    $allDepartments = $departs->findAll();
		    $allCategories = $warehouses->findCategories();
		    $allParameters = $warehouses->findAllParameters();
		    $allRegions = $users->findRegions();
		    $allBoxes = $warehouses->findAllBoxes();
		    $allBox = $warehouses->findAllBox();
		    $allTrips = $warehouses->findTrips();
		    $allStates = $warehouses->findWStates();
		    $lastIdForBox = ($warehouses->findLastIdBox()['id']+1);
		    $CRCost = $warehouses->findCRCost();
        	$boxCode = $lastIdForBox;


		?>	
		<html dir="ltr" lang="en">

        <head>
            <?php 

                include 'head.php';

            ?>

		    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
		    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
		    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        </head>
        <body style="background:#e3e3e3ff;">
		<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
            data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
            <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
                }

            ?>
            <?php include 'sidebar.php'; ?>

			<div class="page-wrapper" style="background:#e3e3e3ff; margin-bottom:40px;"><br>
			<div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                                <h4 style="color:#005574;">COURIERS</h4>
                            </div>
                        </div><br>
						<div class="card">
							<div class="card-body">
								<div class="row">
								<div class="col-lg-3 col-xlg-3 col-md-3" hidden>
										<div class="form-group">
											<!-- <label for="name">Nombre *</label> -->
											<input id="courier_id" type="text" placeholder="Courier" class="form-control"> 
										</div>
									</div>
									<div class="col-lg-3 col-xlg-3 col-md-3">
										<div class="form-group">
											<!-- <label for="name">Nombre *</label> -->
											<input id="name" type="text" placeholder="Courier" class="form-control"> 
										</div>
									</div>
									<div class="col-lg-3 col-xlg-3 col-md-3">
										<div class="form-group">
											<!-- <label for="name">Nombre *</label> -->
											<select id="region" class="form-control">
                                                        <?php  

                                                            foreach ($allRegions as $key => $value) {
                                                        ?>
                                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                        <?php
                                                        }

                                                        ?>
                                                    </select>
										</div>
									</div>
									<div class="col-lg-3 col-xlg-3 col-md-3">
										<div class="form-group">
											<!-- <label for="name">Nombre *</label> -->
											<select id="type" class="form-control">
                                                        <?php  

                                                            foreach ($allTrips as $key => $value) {
                                                        ?>
                                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                        <?php
                                                        }

                                                        ?>
                                                    </select> 
										</div>
									</div>

									<div class="col-lg-3 col-xlg-3 col-md-3">
										<div class="form-group">
											<!-- <label for="name">Nombre *</label> -->
											<input id="currier-cost" type="text" placeholder="Costo" class="form-control"> 
										</div>
									</div>
								</div>
								<div class="row">
								<div class="col-lg-3 col-xlg-3 col-md-3">
										<div class="form-group">
											<!-- <label for="name">Nombre *</label> -->
											<input id="cost_inter" type="text" placeholder="Costos Interno" class="form-control"> 
										</div>
									</div>
									<div class="col-lg-3 col-xlg-3 col-md-3">
										<div class="form-group">
											<!-- <label for="name">Nombre *</label> -->
											<input id="currier-tasa" type="text" placeholder="Tasa" class="form-control"> 
										</div>
									</div>
									<div class="col-lg-1 col-xlg-1 col-md-1" id="courier-button-register">
										<button class="boton-total-envios-registro-config" onclick="registerCurrier();"><i class="fas fa-plus fa-lg" aria-hidden="true"></i></button>
										
									</div>
									<div class="col-lg-1 col-xlg-1 col-md-1" id="courier-button-register" style="display:none;">
										<button class="boton-total-envios-registro-config" onclick="registerCurrier();"><i class="fas fa-plus fa-lg" aria-hidden="true"></i></button>
										
									</div>

								</div>
							</div>
						</div>

						<div class="card">
							<div class="card-body">
							<table class="table table-warehouse" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                    <thead>
                                        
                                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">Nombre</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Estado</th>
										<th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Acción</th>
                                    </thead>
                                    <tbody>
                                        <?php  

                                            foreach ($allCurriers as $key => $value) {
                                        ?>
                                                <tr>
                                                    
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php echo $value['name'];?>
                                                    </td>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                    	
                                                    	<?php
                                                    		if ($value['active'] == 1) {
                                                    			echo 'HABILITADO';
                                                    		 }else{
                                                    		 	echo "INHABILITADO";
                                                    		 }
                                                    	?>
                                                    		
                                                    </td>
													<td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
														<div class="btn-group" role="group" aria-label="Basic example">
																<button onclick="editCourier();" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-edit"></i></button>
																<button onclick="deleteCourier(<?php echo $value['id']; ?>);" class="btn btn-primary" style="background-color:#005574; color:white; border:2px solid #005574;"><i class="fas fa-trash"></i></button>
														</div>
									
													</td>
													
                                                    
                                                </tr>
                                        <?php
                                            }

                                        ?>
                                    </tbody>
                                </table>
							</div>
						</div>
              
		                
						  
        </body>
        <!-- <footer class="footer text-center"> 2021 © Total Envios
        </footer> -->
        <?php 

        include 'foot.php';

        ?>