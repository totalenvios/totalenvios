<?php  


			ini_set('display_errors', 1);
		    ini_set('display_startup_errors', 1);
		    error_reporting(E_ALL);
		    
		    include 'includes/connection.class.php';
		    include 'includes/users.class.php';
		    include 'includes/packages.class.php';
		    include 'includes/headquarters.class.php';
		    include 'includes/stock.class.php';
		    include 'includes/warehouses.class.php';
		    include 'includes/depart.class.php';

		    $users = new Users;
		    $allUsers = $users->findAll();
		    $packages = new Packages;
		    $allPackages = $packages->findAll();
		    $depart = new Depart;
		    $allDeparts = $depart->findAll();
		    $headquarters = new Headquarters;
		    $stock = new Stock;
		    $warehouses = new Warehouses;
		    $departs = new Depart;
            $allGeneralStatus = $packages->findGeneralStatus();
		    $allCurriers = $users->findCurriers();

            $allInternCurrierCost = $packages->findInternCouriersCost();
            $allCityForCurrier = $packages->findCityForCurrier();
            $allInternCurrier = $packages->findInternCouriers();

            $allCountries= $warehouses->findCountries();
            $allEstados = $warehouses->findEstados();

		    $allDepartments = $departs->findAll();
		    $allCategories = $warehouses->findCategories();
		    $allParameters = $warehouses->findAllParameters();
		    $allRegions = $users->findRegions();
		    $allBoxes = $warehouses->findAllBoxes();
		    $allBox = $warehouses->findAllBox();
		    $allTrips = $warehouses->findTrips();
		    $allStates = $warehouses->findWStates();
		    $lastIdForBox = ($warehouses->findLastIdBox()['id']+1);
		    $CRCost = $warehouses->findCRCost();
        	$boxCode = $lastIdForBox;


		?>	
		<html dir="ltr" lang="en">

        <head>
            <?php 

                include 'head.php';

            ?>

		    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
		    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
		    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        </head>
        <body style="background:#e3e3e3ff;">
		<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
            data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
            <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
                }

            ?>
            <?php include 'sidebar.php'; ?>

			<div class="page-wrapper" style="background:#e3e3e3ff; margin-bottom:40px;"><br>
			<div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                                <h4 style="color:#005574;">TARIFAS</h4>
                            </div>
                        </div><br>
						<div class="alert alert-danger register-error" role="alert">
                        </div>
                        <div class="alert alert-success register-success" role="alert">
                        </div>
						<div class="card">
							<div class="card-body">
								<div class="row">
								    <div class="col-lg-3 col-xlg-5 col-md-5" hidden>
										<div class="form-group">
											<!-- <label for="name">Nombre *</label> -->
											<input id="general_status_id" type="text" placeholder="Categoría" class="form-control"> 
										</div>
									</div>
									<div class="col-lg-3 col-xlg-3 col-md-3 name-currier-hide">
										<div class="form-group">
											<!-- <label for="name">Nombre *</label> -->
											
											<label for="name">Courier</label>
											<select id="currier_id_tarifa" class="form-control">
                                                        <?php  

                                                            foreach ($allCurriers as $key => $value) {
                                                        ?>
                                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                        <?php
                                                        }

                                                        ?>
                                                    </select> 
										
										</div>
									</div>
                                    <div class="col-lg-1 col-xlg-1 col-md-1 name-currier-hide">
                                        <button class="boton-total-envios-courier" style="margin-left:-30px; margin-top:30px;" onclick="openRegisterCurrier();"><i class="fas fa-plus fa-lg" aria-hidden="true"></i></button>
                                    </div>
                                    <div class="col-lg-3 col-xlg-3 col-md-3 name-currier-hide">
                                        <div class="form-group">
											<label for="name">Ciudad</label>
											<select id="city_id_tarifa" class="form-control">
                                                        <?php  

                                                            foreach ($allCityForCurrier as $key => $value) {
                                                        ?>
                                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                        <?php
                                                        }

                                                        ?>
                                                    </select> 
										</div>
									</div>
                                    <div class="col-lg-1 col-xlg-1 col-md-1 name-currier-hide">
                                        <button class="boton-total-envios-courier" style="margin-left:-30px; margin-top:30px;" onclick="openRegisterCity();"><i class="fas fa-plus fa-lg" aria-hidden="true"></i></button>
                                    </div>
                                    <div class="col-lg-4 col-xlg-4 col-md-4 name-currier-hide">
                                        <div class="form-group">
											<label for="trip_id">Tipo de envio</label>
											<select id="trip_id_tarifa" class="form-control">
                                                        <?php  

                                                            foreach ($allTrips as $key => $value) {
                                                        ?>
                                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                        <?php
                                                        }

                                                        ?>
                                                    </select> 
										</div>
									</div>

								</div>
                                <div class="row">
									<div class="col-lg-2 col-xlg-2 col-md-2">
										<div class="form-group">
											<!-- <label for="name">Nombre *</label> -->
											<input style="text-transform:uppercase;" id="tarifa_currier" type="number" min="0" placeholder="0.00" class="form-control"> 
										</div>
									</div>
                                    <div class="col-lg-2 col-xlg-2 col-md-2">
										<div class="form-group">
											<!-- <label for="name">Nombre *</label> -->
											<input style="text-transform:uppercase;" id="tarifaTE_currier" type="number" min="0" placeholder="0.00" class="form-control"> 
										</div>
									</div>
									
									<div class="col-lg-1 col-xlg-1 col-md-1" id="register-general-status-button">
                                    <button class="boton-total-envios-registro-config" onclick="addTarifaCurrier();"><i class="fas fa-plus fa-lg" aria-hidden="true"></i></button>
										
									</div>
									<div class="col-lg-1 col-xlg-1 col-md-1" id="edit-general-status-button" style="display:none;">
										<button class="boton-total-envios-registro-config" onclick="editForCurrier();"><i class="fas fa-check fa-lg" aria-hidden="true"></i></button>
										
									</div>

								</div>
							</div>
						</div>

                        <div class="row">
                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                <div class="card" id="register-currier-intern-open" style="display:none;">
                                    <div class="card-body">
                                        <h6 style="color:#005574; margin-bottom:20px;"><strong>REGISTRA UN COURIER</strong></h6>
                                        <div class="row">
                                            <div class="col-lg-3 col-xlg-5 col-md-5" hidden>
                                                <div class="form-group">
                                                    <!-- <label for="name">Nombre *</label> -->
                                                    <input id="general_status_id" type="text" placeholder="Categoría" class="form-control"> 
                                                </div>
                                            </div>
                                            <div class="col-lg-9 col-xlg-9 col-md-9">
                                                <div class="form-group">
                                                    <!-- <label for="name">Nombre *</label> -->
                                                    <input style="text-transform:uppercase;" id="currier_name" type="text" placeholder="COURIER" class="form-control"> 
                                                </div>
                                            </div>   
                                            <div class="col-lg-3 col-xlg-3 col-md-3" id="register-general-status-button">
                                                <button class="boton-total-envios-registro-config" onclick="addNewCurrier();"><i class="fas fa-check fa-lg" aria-hidden="true"></i></button>
                                                
                                            </div>
                                            <div class="col-lg-4 col-xlg-4 col-md-4" id="edit-general-status-button" style="display:none;">
                                                <button class="boton-total-envios-registro-config" onclick="editStatus();"><i class="fas fa-check fa-lg" aria-hidden="true"></i></button>
                                                
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9 col-xlg-9 col-md-9">
                                <div class="card" id="register-city-intern-open"  style="display:none;">
                                    <div class="card-body">
                                        <h6 style="color:#005574; margin-bottom:20px;"><strong>REGISTRA UNA CIUDAD<strong></h6>
                                        <div class="row">
                                        <div class="col-lg-3 col-xlg-5 col-md-5" hidden>
                                                <div class="form-group">
                                                    <!-- <label for="name">Nombre *</label> -->
                                                    <input id="general_status_id" type="text" placeholder="Categoría" class="form-control"> 
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <!-- <label for="name">Nombre *</label> -->
                                                    <select id="country_currier" class="form-control">
                                                                <?php  

                                                                    foreach ($allCountries as $key => $value) {
                                                                ?>
                                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                                <?php
                                                                }

                                                                ?>
                                                            </select>  
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <!-- <label for="name">Nombre *</label> -->
                                                    <select id="state_id_currier" class="form-control">
                                                                <?php  

                                                                    foreach ($allEstados as $key => $value) {
                                                                ?>
                                                                <option value="<?php echo $value['id_estado']; ?>"><?php echo $value['estado']; ?></option>
                                                                <?php
                                                                }

                                                                ?>
                                                    </select>  
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3">
                                                <div class="form-group">
                                                    <!-- <label for="name">Nombre *</label> -->
                                                    <input style="text-transform:uppercase;" id="city_name_currier" type="text" placeholder="CIUDAD" class="form-control"> 
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-xlg-2 col-md-2">
                                                <div class="form-group">
                                                    <!-- <label for="name">Nombre *</label> -->
                                                    <select id="region_id_currier" class="form-control">
                                                                <?php  

                                                                    foreach ($allRegions as $key => $value) {
                                                                ?>
                                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                                <?php
                                                                }

                                                                ?>
                                                            </select> 
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-1 col-xlg-1 col-md-1" id="register-general-status-button">
                                                <button class="boton-total-envios-registro-config" onclick="addCityCurrier();"><i class="fas fa-check fa-lg" aria-hidden="true"></i></button>
                                                
                                            </div>
                                            <div class="col-lg-3 col-xlg-3 col-md-3" id="edit-general-status-button" style="display:none;">
                                                <button class="boton-total-envios-registro-config" onclick="editStatus();"><i class="fas fa-check fa-lg" aria-hidden="true"></i></button>
                                                
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        

						<div class="card">
							<div class="card-body">
							<table class="table table-warehouse" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                        <thead>
                                            
                                            <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">CURRIER</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">PAIS</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">ESTADO</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">CIUDAD</th>
											<th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">REGION</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">TIPO DE ENVIO</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">TARIFA</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">TARIFA TE</th>
											<th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">ACCIÓN</th>
                                        </thead>
                                        <tbody>
                                            <?php  

                                                foreach ($CRCost as $key => $value) {
                                            ?>
                                                    <tr>
                                                        
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                            <?php echo $warehouses->findCurriersById($value['currier_id'])['name']; ?>
                                                        </td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php 
                                                            $pais = $packages->findCityCurrierById($value['city_id'])[0]['country'];
                                                            echo $packages->findCountryCurrierById($pais)[0]['name'];?>
                                                        </td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php
                                                             $estado = $packages->findCityCurrierById($value['city_id'])[0]['state_id'];
                                                             echo $packages->findStateCurrierById($estado)[0]['estado'];
                                                        ?>
                                                        </td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                            <?php echo $packages->findCityCurrierById($value['city_id'])[0]['name'];?>
                                                        </td>
														<td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                            <?php echo $warehouses->findRegionById($value['region_id'])['name'];?>
                                                        </td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                            <?php 
                                                                if( $value['trip_id'] == 2){
                                                            ?> 
                                                                <img src="icons/ocean.png" style="height: 20px; width: 20px;"/>
                                                            <?php 
                                                                }else{
                                                            ?>
                                                                <img src="icons/air.png" style="height: 30px; width: 30px;"/>
                                                            <?php  
                                                                };
                                                            ?>    




                                                            
                                                        </td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                            <?php echo $value['tarifa'];?>
                                                        </td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                            <?php echo $value['tarifaTE'];?>
                                                        </td>
                                            
                                                
														<td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
														<div class="btn-group" role="group" aria-label="Basic example">
																<button onclick="editCurrier(<?php echo $value['id']; ?>);" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-edit"></i></button>
																<button onclick="deleteCurrier(<?php echo $value['id']; ?>);" class="btn btn-primary" style="background-color:#005574; color:white; border:2px solid #005574;"><i class="fas fa-trash"></i></button>

															</div>
                                                        </td>
                                                        
                                                    </tr>
                                            <?php
                                                }

                                            ?>
                                        </tbody>
                                    </table>
							</div>
						</div>
              
		             

							
				        
        </body>
        <!-- <footer class="footer text-center"> 2021 © Total Envios
        </footer> -->
        <?php 

        include 'foot.php';

        ?>