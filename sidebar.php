<aside class="left-sidebar" data-sidebarbg="skin6" style="margin-top:40px; background:#005574; color:white;">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav" style="background:#005574; color:white;">
            <ul id="sidebarnav">
                <li class="sidebar-item pt-2" style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings.php"
                        aria-expanded="false">
                        <img src="icons/parametros.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <!-- <i class="fas fa-home fa-2x fa-lg" aria-hidden="true" style="color:#fc3c3d;"></i> -->
                        <span class="hide-menu" style="color:white;"><strong>PARÁMETROS<strong></span>
                    </a>
                </li>
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-category.php"
                        aria-expanded="false">
                        <img src="icons/CATEGORIAS.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>CATEGORIAS </strong></span>
                    </a>
                </li>
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-depart.php"
                        aria-expanded="false">
                        <img src="icons/DEPARTAMENTOS.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>DEPARTAMENTOS </strong></span>
                    </a>
                </li>
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-countries.php"
                        aria-expanded="false">
                        <img src="icons/country.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>PAISES </strong></span>
                    </a>
                </li>
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-region.php"
                        aria-expanded="false">
                        <img src="icons/REGIONES.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>REGIONES </strong></span>
                    </a>
                </li>
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-cities.php"
                        aria-expanded="false">
                        <img src="icons/CIUDADES.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>CIUDADES</strong></span>
                    </a>
                </li>
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-new-courier.php"
                        aria-expanded="false">
                        <img src="icons/COURIER.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>COURIERS</strong></span>
                    </a>
                </li>
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-new-intern-courier.php"
                        aria-expanded="false">
                        <img src="icons/intern-currier.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>COURIERS INTERNOS</strong></span>
                    </a>
                </li>
                
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-status.php"
                        aria-expanded="false">
                        <img src="icons/TASAS.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>TARIFAS </strong></span>
                    </a>
                </li>

                <li class="sidebar-item dropdown"  style="background:#005574; color:white;">
                        
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-courier-intern.php"
                        aria-expanded="false">
                        <img src="icons/TASAS.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>TARIFAS INTERNAS</strong></span>
                    </a>
                       
                      
                    
                </li>
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-cost.php"
                        aria-expanded="false">
                        <img src="icons/ESTADOS.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>ESTADOS WH</strong></span>
                    </a>
                </li>
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-shipping-status.php"
                        aria-expanded="false">
                        <img src="icons/send.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>ESTADOS DEL ENVIO</strong></span>
                    </a>
                </li>
                <!--  <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-cost-concept.php"
                        aria-expanded="false">
                        <img src="icons/TASAS.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>COSTOS</strong></span>
                    </a>
                </li>
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-status.php"
                        aria-expanded="false">
                        <img src="icons/TASAS.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>TASAS Y COSTOS </strong></span>
                    </a>
                </li>-->
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-box.php"
                        aria-expanded="false">
                        <img src="icons/BOX.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>BOX</strong></span>
                    </a>
                </li>

                <!-- <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-promociones.php"
                        aria-expanded="false">
                        <img src="icons/promociones.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>PROMOCIONES</strong></span>
                    </a>
                </li>

                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="settings-promociones.php"
                        aria-expanded="false">
                        <img src="icons/contactos.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>CONTACTOS</strong></span>
                    </a>
                </li>-->
                
                
               
               <!--  <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="headquarters.php"
                        aria-expanded="false">
                       <i class="fas fa-warehouse"></i>
                        <span class="hide-menu">Agencias</span>
                    </a>
                </li>
                
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="addressee.php"
                        aria-expanded="false">
                        <i class="fa fa-users" aria-hidden="true"></i>
                        <span class="hide-menu">Destinatarios</span>
                    </a>
                </li>
                
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="states.php"
                        aria-expanded="false">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                        <span class="hide-menu">Configuracion</span>
                    </a>
                </li> -->
            </ul>

        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>