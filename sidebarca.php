<aside class="left-sidebar" data-sidebarbg="skin6" style="margin-top:40px; background:#005574; color:white;">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav" style="background:#005574; color:white;">
            <ul id="sidebarnav">
                <li class="sidebar-item pt-2" style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="casillero.php"
                        aria-expanded="false">
                        <img src="icons/alert.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>&nbsp;&nbsp;PREALERTAS<strong></span>
                    </a>
                </li>
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="casillero-pack.php"
                        aria-expanded="false">
                        <img src="icons/BOX.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>&nbsp;&nbsp;PAQUETES</strong></span>
                    </a>
                </li>
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="casillero-wh.php"
                        aria-expanded="false">
                        <img src="icons/warehouse.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>&nbsp;&nbsp;WAREHOUSES</strong></span>
                    </a>
                </li>
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="casillero-fact.php"
                        aria-expanded="false">
                        <img src="icons/facturacion.png" style="height: 40px; width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>&nbsp;&nbsp;FACTURACIÓN</strong></span>
                    </a>
                </li>
                <li class="sidebar-item"  style="background:#005574; color:white;">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="destinatario.php"
                        aria-expanded="false">
                        <img src="icons/contactos.png" style=" width: 40px;"/> &nbsp;
                        <span class="hide-menu" style="color:white;"><strong>&nbsp;&nbsp;DESTINATARIOS</strong></span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>