
        <?php

            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
            
            include 'includes/connection.class.php';
            include 'includes/users.class.php';
            include 'includes/packages.class.php';
            include 'includes/headquarters.class.php';
            include 'includes/stock.class.php';
            include 'includes/packing.class.php';
            include 'includes/warehouses.class.php';

            $users = new Users;
            $allUsers = $users->findAll();
            $packages = new Packages;
            $headquarters = new Headquarters;
            $allHeadquarters = $headquarters->findAll();
            $allOperators = $users->findAllOperators();
            $allClients = $users->findAllClients();
            $states = $packages->findStates();
            $stock = new Stock;
            $packing = new Packing;
            $allPacking = $packing->findAll();
            $curriers = $packing->findCurriers();
            $warehouses = new Warehouses;
            $allWarehouses = $warehouses->findAll();
            $warehouseStates = $warehouses->findWStates();
            $trips = $warehouses->findTrips();
            $countries = $warehouses->findCountries();
            $packing_states = $packing->findStates();

            session_start();

            if ($_SESSION['state'] != 1) {
                header('Location: login.php');
            }

            $date_in = date('2020-m-d');
            $date_out = date('Y-m-d');
            $bulk = isset($_REQUEST['bulk']) ? $_REQUEST['bulk'] : 0;

        ?>

        <!DOCTYPE html>
        <html dir="ltr" lang="en">

        <head>
            <?php 

                include 'head.php';

            ?>
        </head>
        
        <body>
        
            <div class="preloader">
                <div class="lds-ripple">
                    <div class="lds-pos"></div>
                    <div class="lds-pos"></div>
                </div>
            </div>
            <div id="main-wrapper" data-header-position="absolute" data-boxed-layout="full">
                <?php 

                    include 'navbar.php'; 
                    if ($_SESSION['state'] != 1) {
                        header('Location: login.php');
                    }

                ?>
                
                <div class="page-wrapper">
                    <div class="modal fade" id="addPackingState" tabindex="-1" role="dialog" aria-labelledby="addPackingStateLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="addPackingStateLabel">Packing:</h5>
                            <button onclick="$('#addPackingState').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-12 col-xlg-12 col-md-12">
                                        <div class="alert alert-danger register-error" role="alert">
                                        </div>
                                        <div class="alert alert-success register-success" role="alert">
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group mb-4">
                                                    <label for="example-email" class="col-md-12 p-0">Fecha Creacion *</label>
                                                    <div class="col-md-12 border-bottom p-0">
                                                        <input type="date" placeholder="Fecha Entrada" readonly="readonly"
                                                            class="form-control p-0 border-0" id="date_in" value="<?php echo $date_out; ?>">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group mb-4">
                                                    <label class="col-md-12 p-0">Nombre *</label>
                                                    <div class="col-md-12 border-bottom p-0">
                                                        <input id="name" type="text" placeholder="Nombre"
                                                            class="form-control p-0 border-0"> </div>
                                                </div>
                                                <div class="form-group mb-4">
                                                    <div class="col-sm-12">
                                                        <button onclick="addPState();" class="btn btn-success">Registrar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="page-breadcrumb bg-white">
                        <div class="row align-items-center">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                <!-- <h4 class="page-title"><a href="#" class="btn btn-primary" onclick="$('#packageModal').modal('show');" class="fw-normal">Nuevo Packing</a></h4> -->
                            </div>
                            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                                <div class="d-md-flex">
                                    <ol class="breadcrumb ms-auto">
                                        <li><a href="logout.php" class="fw-normal">Logout</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                       <ul class="nav nav-tabs" id="myTab" role="tablist">
                          <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="states-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Estados</button>
                          </li>
                          <li class="nav-item" role="presentation">
                            <button class="nav-link" id="invoices-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Facturacion</button>
                          </li>
                        </ul><br>
                        <div class="tab-content" id="myTabContent">
                          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="states-tab">
                            <div class="row justify-content-center">
                                <br>
                                <div class="alert alert-danger state-error" role="alert">
                                </div>
                                <div class="alert alert-success state-success" role="alert">
                                </div>
                            <h1>Paquetes: </h1>
                            <?php  

                                foreach ($states as $key => $value) {
                            ?>
                                <div class="row">
                                    <div class="col-md-9">
                                        <input readonly="readonly" type="text" class="form-control" value="<?php echo $value['name']; ?>">
                                    </div>
                                    <div class="col-md-3">
                                        <button onclick="deletePState(<?php echo $value['id']; ?>)" class="btn btn-primary delete-button"><i class="fas fa-trash"></i></button>
                                    </div>
                                    
                                </div>
                            <?php
                                }

                            ?>
                        </div><br>
                        <div class="row">
                            <div class="text-center">
                                <button class="btn btn-primary">Agregar</button>
                            </div>
                        </div>
                        <hr>
                         <div class="row justify-content-center">
                            <h1>Warehouse: </h1>
                            <?php  

                                foreach ($warehouseStates as $key => $value) {
                            ?>
                                <div class="row">
                                    <div class="col-md-9">
                                        <input readonly="readonly" type="text" class="form-control" value="<?php echo $value['name']; ?>">
                                    </div>
                                    <div class="col-md-3">
                                        <button onclick="deletePState(<?php echo $value['id']; ?>)" class="btn btn-primary delete-button"><i class="fas fa-trash"></i></button>
                                    </div>
                                    
                                </div>
                            <?php
                                }

                            ?>
                        </div><br>
                        <div class="row">
                            <div class="text-center">
                                <button class="btn btn-primary">Agregar</button>
                            </div>
                        </div>
                        <hr>
                         <div class="row justify-content-center">
                            <h1>Packing List: </h1>
                            <?php  

                                foreach ($packing_states as $key => $value) {
                            ?>
                                <div class="row">
                                    <div class="col-md-9">
                                        <input readonly="readonly" type="text" class="form-control" value="<?php echo $value['name']; ?>">
                                    </div>
                                    <div class="col-md-3">
                                        <button onclick="deletePState(<?php echo $value['id']; ?>)" class="btn btn-primary delete-button"><i class="fas fa-trash"></i></button>
                                    </div>
                                    
                                </div>
                            <?php
                                }

                            ?>
                        </div><br>
                        <div class="row">
                            <div class="text-center">
                                <button onclick="$('#addPackingState').modal('show');" class="btn btn-primary">Agregar</button>
                            </div>
                        </div>
                        <hr>
                         <div class="row justify-content-center">
                            <h1>Usuarios: </h1>
                            <?php  

                                foreach ($states as $key => $value) {
                            ?>
                                <div class="row">
                                    <div class="col-md-9">
                                        <input readonly="readonly" type="text" class="form-control" value="<?php echo $value['name']; ?>">
                                    </div>
                                    <div class="col-md-3">
                                        <button onclick="deletePState(<?php echo $value['id']; ?>)" class="btn btn-primary delete-button"><i class="fas fa-trash"></i></button>
                                    </div>
                                    
                                </div>
                            <?php
                                }

                            ?>
                        </div><br>
                        <div class="row">
                            <div class="text-center">
                                <button class="btn btn-primary">Agregar</button>
                            </div>
                        </div>
                    </div>
                          <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="invoices-tab">Facturacion y pagos</div>
                        </div>
                        
                    </div>
                    <footer class="footer text-center"> 2021 © Total Envios
                    </footer>
                </div>
                
            </div>

            <?php 

                include 'foot.php';

            ?>

        </body>

        </html>