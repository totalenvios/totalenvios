
<?php  

    include 'includes/connection.class.php';
    include 'includes/stock.class.php';
    include 'includes/headquarters.class.php';

    $stock = new Stock;
    $allStock = $stock->findAll();

    $headquarters = new Headquarters;
    $allHeadquarters = $headquarters->findAll();

    session_start();
    if ($_SESSION['state'] != 1) {
        header('Location: login.php');
    }
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

        include 'head.php';

    ?>
</head>

<body>
    <!-- Modal -->
    <div class="modal fade" id="addStock" tabindex="-1" role="dialog" aria-labelledby="addStockLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="addStockLabel">Datos:</h5>
            <button onclick="$('#addStock').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="alert alert-danger addStock-error" role="alert">
                        </div>
                        <div class="alert alert-success addStock-success" role="alert">
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Nombre *</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input id="name" type="text" placeholder="Nombre" class="form-control p-0 border-0"> 
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                <label class="col-md-12 p-0">Agencia</label>
                                <div class="col-md-12 border-bottom p-0">
                                    <select id="headquarter" class="form-select shadow-none p-0 border-0 form-control-line">
                                        <option value="0">Seleccionar...</option>
                                        <?php  
                                        foreach ($allHeadquarters as $key => $value) {
                                            ?>
                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                                <div class="form-group mb-4">
                                    <div class="col-sm-12">
                                        <button onclick="addStock();" class="btn btn-success">Agregar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-header-position="absolute" data-boxed-layout="full">
        <?php 
            include 'navbar.php'; 
        ?>
        <div class="page-wrapper">
            <div class="page-breadcrumb bg-white">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><a href="#" class="btn btn-primary" onclick="$('#addStock').modal('show');" class="fw-normal">Nuevo Almacen</a></h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <div class="d-md-flex">
                            <ol class="breadcrumb ms-auto">
                                <li><a href="logout.php" class="fw-normal">Logout</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>Name</th>
                            <th>Activa</th>
                        </thead>
                        <tbody>
                            <?php  

                                foreach ($allStock as $key => $value) {
                            ?>
                                    <tr>
                                        <td><?php echo $value['id']; ?></td>
                                        <td><?php echo $value['name']; ?></td>
                                        <td><?php echo $value['active']; ?></td>
                                    </tr>
                            <?php
                                }

                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <footer class="footer text-center"> 2021 © Total Envios
            </footer>
        </div>
        
    </div>
    <?php 

        include 'foot.php';

    ?>
</body>

</html>