-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-05-2021 a las 05:47:57
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `total_envios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alerts`
--

CREATE TABLE `alerts` (
  `id` int(11) NOT NULL,
  `tracking` varchar(100) NOT NULL,
  `date_r` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_client` int(11) NOT NULL,
  `transp` text DEFAULT NULL,
  `date_in` timestamp NULL DEFAULT NULL,
  `content` text NOT NULL,
  `last` tinyint(1) DEFAULT 0,
  `package_cost` float NOT NULL DEFAULT 0,
  `transp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `alerts`
--

INSERT INTO `alerts` (`id`, `tracking`, `date_r`, `id_client`, `transp`, `date_in`, `content`, `last`, `package_cost`, `transp_id`) VALUES
(1, '131321', '2021-05-03 03:11:39', 19, 'Amazon', '2021-04-24 12:34:07', '1 paqueta', 0, 0, 2),
(2, '213213', '2021-05-03 03:11:32', 5, 'Pruebas', '2021-04-23 04:00:00', '12312sasad', 0, 0, 3),
(3, '213213', '2021-05-03 03:11:16', 5, 'pruebas', '2021-04-23 04:00:00', '123213', 0, 0, 1),
(4, '2131', '2021-05-03 03:11:14', 5, '12313', '2021-04-23 04:00:00', '123123', 0, 0, 2),
(5, '13123', '2021-05-03 03:11:11', 5, 'Amazon', '2021-04-24 04:00:00', '2 Paquetes', 0, 0, 3),
(6, '213213', '2021-05-03 03:11:04', 5, 'amazon', '2021-04-24 04:00:00', 'asdad', 0, 0, 4),
(7, '213213', '2021-05-03 03:11:02', 5, 'amazom', '2021-04-28 04:00:00', 'pruebas contenido', 0, 0, 1),
(8, 'ADVBRBRB', '2021-05-03 03:10:57', 50005, '2', '2021-05-18 04:00:00', 'Medicinas', 125, 0, 2),
(9, 'WEB85596', '2021-05-27 04:00:00', 50005, NULL, '2021-05-27 04:00:00', 'SDASDSADSD', 0, 8596.2, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `box`
--

CREATE TABLE `box` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `box`
--

INSERT INTO `box` (`id`, `name`, `active`) VALUES
(1, 'Small', 1),
(2, 'Medium', 1),
(3, 'Big', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `box_types`
--

CREATE TABLE `box_types` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(120) DEFAULT NULL,
  `large` float DEFAULT NULL,
  `wi` float DEFAULT NULL,
  `he` float DEFAULT NULL,
  `vol_lb` float DEFAULT NULL,
  `feet_cub` float DEFAULT NULL,
  `lb_p` float DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `box_types`
--

INSERT INTO `box_types` (`id`, `name`, `description`, `large`, `wi`, `he`, `vol_lb`, `feet_cub`, `lb_p`, `created_at`) VALUES
(1, 'A3', 'Caja tipo A3', 10, 7, 5, 2.11, 0.2, 20, '2021-05-02 21:31:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `name`, `active`) VALUES
(1, 'Afganistan', 1),
(2, 'Albania', 1),
(3, 'Alemania', 1),
(4, 'Andorra', 1),
(5, 'Angola', 1),
(6, 'Antigua y Barbuda', 1),
(7, 'Arabia Saudita', 1),
(8, 'Argelia', 1),
(9, 'Argentina', 1),
(10, 'Armenia', 1),
(11, 'Australia', 1),
(12, 'Austria', 1),
(13, 'Azerbaiyán', 1),
(14, 'Bahamas', 1),
(15, 'Bahrein', 1),
(16, 'Bangladesh', 1),
(17, 'Barbados', 1),
(18, 'Belarús', 1),
(19, 'Belice', 1),
(20, 'Benin', 1),
(21, 'Bhután', 1),
(22, 'Bolivia', 1),
(23, 'Bosnia', 1),
(24, 'Botswana', 1),
(25, 'Brasil', 1),
(26, 'Brunei Darussalam', 1),
(27, 'Bulgaria', 1),
(28, 'Burkina Faso', 1),
(29, 'Burundi', 1),
(30, 'Bélgica', 1),
(31, 'Cabo Verde', 1),
(32, 'Camboya', 1),
(33, 'Camerún', 1),
(34, 'Canadá', 1),
(35, 'Chad', 1),
(36, 'Chequia', 1),
(37, 'Chile', 1),
(38, 'China', 1),
(39, 'Chipre', 1),
(40, 'Colombia', 1),
(41, 'Comoras', 1),
(42, 'Congo', 1),
(43, 'Costa Rica', 1),
(44, 'Croacia', 1),
(45, 'Cuba', 1),
(46, 'Côte d\'Ivoire', 1),
(47, 'Dinamarca', 1),
(48, 'Djibouti', 1),
(49, 'Dominica', 1),
(50, 'Ecuador', 1),
(51, 'Egipto', 1),
(52, 'El Salvador', 1),
(53, 'Emiratos Arabes', 1),
(54, 'Eritrea', 1),
(55, 'Eslovaquia', 1),
(56, 'Eslovenia', 1),
(57, 'España', 1),
(58, 'Estados Unidos', 1),
(59, 'Estonia', 1),
(60, 'Eswatini', 1),
(61, 'Etiopía', 1),
(62, 'Fiji', 1),
(63, 'Filipinas', 1),
(64, 'Finlandia', 1),
(65, 'Francia', 1),
(66, 'Gabón', 1),
(67, 'Gambia', 1),
(68, 'Georgia', 1),
(69, 'Ghana', 1),
(70, 'Granada', 1),
(71, 'Grecia', 1),
(72, 'Guatemala', 1),
(73, 'Guinea', 1),
(74, 'Guyana', 1),
(75, 'Haití', 1),
(76, 'Honduras', 1),
(77, 'Hungría', 1),
(78, 'India', 1),
(79, 'Indonesia', 1),
(80, 'Iraq', 1),
(81, 'Irlanda', 1),
(82, 'Irán', 1),
(83, 'Islandia', 1),
(84, 'Israel', 1),
(85, 'Italia', 1),
(86, 'Jamaica', 1),
(87, 'Japón', 1),
(88, 'Jordania', 1),
(89, 'Kazajstán', 1),
(90, 'Kenya', 1),
(91, 'Kirguistán', 1),
(92, 'Kiribati', 1),
(93, 'Kuwait', 1),
(94, 'Lesotho', 1),
(95, 'Letonia', 1),
(96, 'Liberia', 1),
(97, 'Libia', 1),
(98, 'Lituania', 1),
(99, 'Luxemburgo', 1),
(100, 'Líbano', 1),
(101, 'Macedonia', 1),
(102, 'Madagascar', 1),
(103, 'Malasia', 1),
(104, 'Malawi', 1),
(105, 'Maldivas', 1),
(106, 'Malta', 1),
(107, 'Malí', 1),
(108, 'Marruecos', 1),
(109, 'Mauricio', 1),
(110, 'Mauritania', 1),
(111, 'Micronesia', 1),
(112, 'Mongolia', 1),
(113, 'Montenegro', 1),
(114, 'Mozambique', 1),
(115, 'Myanmar', 1),
(116, 'México', 1),
(117, 'Mónaco', 1),
(118, 'Namibia', 1),
(119, 'Nauru', 1),
(120, 'Nepal', 1),
(121, 'Nicaragua', 1),
(122, 'Nigeria', 1),
(123, 'Niue', 1),
(124, 'Noruega', 1),
(125, 'Nueva Zelanda', 1),
(126, 'Nigeria', 1),
(127, 'Pakistán', 1),
(128, 'Palau', 1),
(129, 'Panamá', 1),
(130, 'Papua', 1),
(131, 'Paraguay', 1),
(132, 'Paises Bajos', 1),
(133, 'Perú', 1),
(134, 'Polonia', 1),
(135, 'Portugal', 1),
(136, 'Qatar', 1),
(137, 'Reino Unido', 1),
(138, 'Republica Centroafricana', 1),
(139, 'Lao', 1),
(140, 'Congo', 1),
(141, 'Republica Dominicana', 1),
(142, 'Republica democrática de Corea', 1),
(143, 'Tanzánia', 1),
(144, 'Republica de Coreo', 1),
(145, 'Moldova', 1),
(146, 'Siria', 1),
(147, 'Rumania', 1),
(148, 'Rwanda', 1),
(149, 'Saint Kitts y Nevis', 1),
(150, '', 1),
(151, 'Samoa', 1),
(152, 'San Marino', 1),
(153, 'San vicente', 1),
(154, 'Santa Lucia', 1),
(155, 'San Tomé y Príncipe', 1),
(156, 'Senegal', 1),
(157, 'Serbia', 1),
(158, 'Seychelles', 1),
(159, 'Sierra Leona', 1),
(160, 'Singapur', 1),
(161, 'Somalia', 1),
(162, 'Sri Lanka', 1),
(163, 'Sudáfrica', 1),
(164, 'Sudán', 1),
(165, 'Sudán del Sur', 1),
(166, 'Suecia', 1),
(167, 'Suiza', 1),
(168, 'Suriname', 1),
(169, 'Tailandia', 1),
(170, 'Tayikistán', 1),
(171, 'Timor-Leste', 1),
(172, 'Togo', 1),
(173, 'Tokelau', 1),
(174, 'Tonga', 1),
(175, 'Trinidad y Tabago', 1),
(176, 'Turkmenistán', 1),
(177, 'Turquía', 1),
(178, 'Tuvalu', 1),
(179, 'Túnez', 1),
(180, 'Ucrania', 1),
(181, 'Uganda', 1),
(182, 'Uruguay', 1),
(183, 'Uzbekistán', 1),
(184, 'Vanuatu', 1),
(185, 'Venezuela', 1),
(186, 'Vietnam', 1),
(187, 'Yemen', 1),
(188, 'Zambia', 1),
(189, 'Zimbabwe', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curriers`
--

CREATE TABLE `curriers` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `curriers`
--

INSERT INTO `curriers` (`id`, `name`, `active`) VALUES
(1, 'Pruebas', 1),
(2, 'Pruebas 1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `depart`
--

CREATE TABLE `depart` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `headquarters`
--

CREATE TABLE `headquarters` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `headquarters`
--

INSERT INTO `headquarters` (`id`, `name`, `active`) VALUES
(1, 'MIAMI', 1),
(2, 'TULSA', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `number` varchar(200) DEFAULT NULL,
  `id_client` int(11) NOT NULL,
  `date_i` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `value` int(11) NOT NULL,
  `id_warehouse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `invoices`
--

INSERT INTO `invoices` (`id`, `number`, `id_client`, `date_i`, `value`, `id_warehouse`) VALUES
(1, NULL, 5, '2021-04-02 18:06:54', 20, 5),
(2, NULL, 5, '2021-04-02 18:20:02', 30, 6),
(3, NULL, 5, '2021-04-02 18:23:06', 20, 6),
(4, NULL, 5, '2021-04-02 04:00:00', 20, 5),
(5, NULL, 5, '2021-04-02 04:00:00', 20, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `code` varchar(45) NOT NULL,
  `user_from` int(11) DEFAULT NULL,
  `user_to` int(11) DEFAULT NULL,
  `date_in` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_out` timestamp NULL DEFAULT NULL,
  `alias` text DEFAULT NULL,
  `operator` int(11) DEFAULT NULL,
  `tracking` varchar(45) NOT NULL,
  `description` text DEFAULT NULL,
  `state` int(11) DEFAULT 1,
  `height` double(10,2) DEFAULT NULL,
  `width` double(10,2) DEFAULT NULL,
  `lenght` double(10,2) DEFAULT NULL,
  `weight` double(10,2) DEFAULT NULL,
  `id_headquarter` int(11) DEFAULT NULL,
  `id_stock` int(11) DEFAULT NULL,
  `id_transp` int(11) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packages_states`
--

CREATE TABLE `packages_states` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `packages_states`
--

INSERT INTO `packages_states` (`id`, `name`, `active`) VALUES
(1, 'Recibido en almacen', 1),
(2, 'Perdido', 1),
(3, 'Por asignar', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packing`
--

CREATE TABLE `packing` (
  `id` int(11) NOT NULL,
  `date_in` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `name` varchar(45) NOT NULL,
  `note` text NOT NULL,
  `id_currier` int(11) NOT NULL,
  `date_out` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_trip` int(11) NOT NULL,
  `id_destiny` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `packing`
--

INSERT INTO `packing` (`id`, `date_in`, `name`, `note`, `id_currier`, `date_out`, `id_trip`, `id_destiny`, `state`) VALUES
(3, '2021-04-13 04:00:00', 'Ramon', 'pruebas', 2, '2021-04-13 04:00:00', 1, 5, 1),
(4, '2021-04-13 04:00:00', 'Pruebas', 'Probando', 2, '2021-04-13 04:00:00', 1, 3, 1),
(6, '2021-04-16 04:00:00', 'Ramon Briceno', 'pruebas', 2, '2021-04-16 04:00:00', 1, 7, 1),
(7, '2021-04-16 04:00:00', 'Ramon Briceno', 'pruebas', 2, '2021-04-16 04:00:00', 2, 7, 2),
(8, '2021-04-16 04:00:00', 'Ramon Briceno', 'pruebas', 2, '2021-04-16 04:00:00', 2, 7, 2),
(9, '2021-04-16 04:00:00', 'Ramon Briceno', 'pruebas', 2, '2021-04-16 04:00:00', 2, 7, 2),
(10, '2021-04-16 04:00:00', 'Ramon Briceno', 'pruebas', 2, '2021-04-16 04:00:00', 2, 7, 2),
(12, '2021-04-16 04:00:00', 'Ramon Briceno', 'pruebas', 2, '2021-04-16 04:00:00', 1, 7, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packing_states`
--

CREATE TABLE `packing_states` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `packing_states`
--

INSERT INTO `packing_states` (`id`, `name`, `active`) VALUES
(3, 'Activo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packing_temp`
--

CREATE TABLE `packing_temp` (
  `id` int(11) NOT NULL,
  `json` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `packing_temp`
--

INSERT INTO `packing_temp` (`id`, `json`) VALUES
(1, '4,5,6,7'),
(2, '4,5,6'),
(3, '4,5'),
(4, '4,5,6,7'),
(5, '4,5,6,7');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regions`
--

CREATE TABLE `regions` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shipping_sites`
--

CREATE TABLE `shipping_sites` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `country_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `id_headquarter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `stock`
--

INSERT INTO `stock` (`id`, `name`, `active`, `id_headquarter`) VALUES
(1, 'Pruebas alma', 1, 1),
(2, 'Pruebas', 1, 2),
(3, 'Pruebas 111', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transp`
--

CREATE TABLE `transp` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `transp`
--

INSERT INTO `transp` (`id`, `name`, `active`) VALUES
(1, 'FEDEX', 1),
(2, 'UPS', 1),
(3, 'DHL', 1),
(4, 'AMAZON', 1),
(5, 'USPS', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trip`
--

CREATE TABLE `trip` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `trip`
--

INSERT INTO `trip` (`id`, `name`, `active`) VALUES
(1, 'Aereo', 1),
(2, 'Maritimo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `document` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `country` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `phone_house` varchar(45) DEFAULT NULL,
  `user_type` int(11) NOT NULL,
  `addressee` tinyint(1) NOT NULL DEFAULT 0,
  `password` varchar(100) NOT NULL,
  `id_headquarter` int(11) NOT NULL DEFAULT 0,
  `code` varchar(10) NOT NULL,
  `born_date` timestamp NOT NULL DEFAULT '1990-01-01 04:00:00',
  `temp_token` varchar(300) NOT NULL,
  `note` varchar(145) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `email`, `document`, `address`, `country`, `phone`, `phone_house`, `user_type`, `addressee`, `password`, `id_headquarter`, `code`, `born_date`, `temp_token`, `note`) VALUES
(0, 'Default', 'Default', 'default@totalenvios.com', '123456789', 'Default TimeZone', 'Default', '123456789', NULL, 2, 0, '123456789', 0, 'TE-0000', '1990-01-01 04:00:00', '', NULL),
(5, 'Ramon', 'Briceno', 'ramonbriceno12@gmail.com', '1232131', 'El cafetal', 'Venezuela', '12345', '', 1, 0, '$2y$10$ANkODArzYWOTN/sE61t/T.Zxf6QaaW8sip69RIR2C7DVsiszk53D.', 0, '12312232', '1990-01-01 04:00:00', '', ''),
(6, 'Pruebas', 'Pruebas', 'pruebas@gmail.com', '12345', 'El cafetal', '3', '12345', '', 2, 0, '$2y$10$Kfn6DABy72HYTar3cauTuORG4uM.d2Gwu6x54iwu5iGiDySvRNCrS', 0, '12312232', '1990-01-01 04:00:00', '6913f5ebf26afd4e23312ba7f1ec642c63bd2c5c48b1d2c70884071817d428d366d3a08686be653cdea591581fe429119ac16d780f93ef68c3ce3eda3b5fcd0c', ''),
(7, 'Operador', 'Operador', 'operador@totalenvios.com', '12345', 'Miami', '6', '12345', '', 3, 0, '$2y$10$gGCbVWuVmT45nZ3D7SSo6.yiquqPg2SfExW.LTSs0gkcTvfOyEbpi', 0, '12312232', '1990-01-01 04:00:00', '', ''),
(8, 'Ramon', '1', 'ramon@gmail.com', '8782832', 'sdsadsaa', '6', '4545345', '', 1, 0, '$2y$10$UwahcIgQq7p5VlRJ4w9aBufrBUZlM3TRUDY/SHio6B1pfZjEYEizO', 0, '12312232', '1990-01-01 04:00:00', '', ''),
(9, 'Ramon', 'Pruebas', 'ramonpr@gmail.com', '1312312', 'Miami', '6', '123213', '', 1, 0, '$2y$10$rXOfnTfPQVs0/QdV6b.K6.jhXasK/J2WlRyFmVgQxVmszjDEzFa6u', 0, '12312232', '1990-01-01 04:00:00', '', ''),
(10, 'Ramon', 'Probandor', 'rapro@gmail.com', '2312321', 'Caracas', '7', '1312321', '', 1, 0, '$2y$10$yfsxGSde.SxAvoewXX79UOGyCoX7B2F9ELl7WOvwzouBcrFFJXIjm', 0, '12312232', '1990-01-01 04:00:00', '', ''),
(11, 'Ramon', 'Briceno', 'ramonbriceno1212@gmail.com', '213213', 'El cafetal', '7', '12343', '', 1, 0, '$2y$10$eAyk/ZCfZwDTkXb7RXarhe3KrZsPAUYOF5WxsWd8TXFQaS0UFQWJm', 1, '12312232', '1990-01-01 04:00:00', '', ''),
(12, 'Ramon', 'Briceno', 'ramonbriceno12111@gmail.com', '1121111', 'El cafetal', '7', '132123', '', 1, 0, '$2y$10$2Xs3Xt2nTAk0dYEn99vkte/QCirJAIO2V2BVvFvgw07cxGJJCjWVW', 1, '12312232', '1990-01-01 04:00:00', '', ''),
(13, 'Ramon', 'Briceno', 'ramonbriceno12111111@gmail.com', '12sadsadadqe21e', 'El cafetal', '7', '312321312321', '', 1, 0, '$2y$10$Cf7BDOQkbbEhZ0ov.oxNgeiI/BIUNPVWSz9bhU4qlYOEW.2PZMani', 1, '12312232', '1994-08-31 04:00:00', '', ''),
(14, 'Ramon', 'Briceno', 'ramonbriceno121212121212@gmail.com', '123213', 'El cafetal', '7', '3123213', '', 1, 0, '$2y$10$QYOFo5fn1tqfxyUt/NkCkeGJT5r9uklvvbgI9XUQK7qhf/SaSWy32', 2, '12312232', '2021-03-01 04:00:00', '', ''),
(15, 'Ramon', 'Briceno', 'ramonbriceno14@gmail.com', '12321312', 'El cafetal', '7', '04122434077', '', 1, 0, '$2y$10$HvmickkedBKzXUDGZ60ieOGZvi8Agc4Z.DDgR6ZF5uf6QQuI72n5C', 1, '12312232', '2021-03-17 04:00:00', '', ''),
(16, 'Ramon', 'Briceno', 'ramonbriceno16@gmail.com', '123213', 'El cafetal', '7', '04122434077', '', 3, 0, '$2y$10$YPgtrjoMsCiveBpjrP/99O0Di/J9PC0hO2ocQu1OLFWtkzMEll00q', 2, '12312232', '2021-02-04 04:00:00', '', ''),
(17, 'Ramon', 'Briceno', 'ramonbriceno17@gmail.com', '13123', 'El cafetal', '7', '04122434077', '', 2, 0, '$2y$10$G3gnmnemfUCO9QEOIGBubelylB0inoeb5/oe17UuhOPpVqKVWbFEC', 1, '12312232', '2021-03-13 04:00:00', '', ''),
(18, 'Ramon', 'Briceno', 'ramonbriceno1215@gmail.com', '21321', 'El cafetal', '6', '04122434077', '', 3, 0, '$2y$10$oIt59m6YGTDjBQ4zlUua7u/IxmuCTXpT9pq.2c055BNzTBt0G.YWq', 2, '12312232', '2021-03-01 04:00:00', '', ''),
(19, 'Juan', 'Pablo', 'juan@gmail.com', '123123', 'El cafetal', '7', '1223', '', 1, 0, '$2y$10$LzkZ.oX4BoYRp/rscdSpceN/.OLBE1aUx9PEmmP5Ddx6KzOhIAIy6', 1, '12312232', '2021-04-22 04:00:00', '', ''),
(50005, 'Reissy José', 'Torres Emanuelli', 'rjte.1993@gmail.com', '20504717', 'Unnamed Road, Ciudad Guayana 8050, Bolívar, Venezuela', '185', '04148986671', '02869520525', 2, 0, '$2y$10$tzWx4gozsTe8zK3phKWPYuFhb.luOj60y9AGdJYQxcOVXcS5NU9bm', 1, 'TE-20991', '1993-01-01 04:00:00', '', 'Registro Web');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_states`
--

CREATE TABLE `user_states` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_types`
--

CREATE TABLE `user_types` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_types`
--

INSERT INTO `user_types` (`id`, `name`) VALUES
(1, 'Destinatario'),
(2, 'Cliente'),
(3, 'Operador'),
(4, 'Admin'),
(5, 'Empresa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `warehouses`
--

CREATE TABLE `warehouses` (
  `id` int(11) NOT NULL,
  `id_from` int(11) NOT NULL,
  `id_to` int(11) NOT NULL,
  `address` text NOT NULL,
  `date_in` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `height` double(10,2) NOT NULL,
  `width` double(10,2) NOT NULL,
  `weight` double(10,2) NOT NULL,
  `lenght` double(10,2) NOT NULL,
  `description` text DEFAULT NULL,
  `price` double(10,2) NOT NULL,
  `id_currier` int(11) NOT NULL,
  `code` varchar(45) NOT NULL,
  `state` int(11) DEFAULT 1,
  `cost` double(10,2) NOT NULL,
  `trip_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `date_send` timestamp NOT NULL DEFAULT current_timestamp(),
  `box` int(11) NOT NULL DEFAULT 0,
  `casillero` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `warehouses`
--

INSERT INTO `warehouses` (`id`, `id_from`, `id_to`, `address`, `date_in`, `height`, `width`, `weight`, `lenght`, `description`, `price`, `id_currier`, `code`, `state`, `cost`, `trip_id`, `country_id`, `date_send`, `box`, `casillero`) VALUES
(4, 5, 6, '', '2021-04-06 04:00:00', 10.00, 10.00, 10.00, 10.00, 'pruebas', 10.00, 10, '0', 1, 10.00, 2, 4, '2021-03-26 04:00:00', 3, NULL),
(5, 5, 8, '', '2021-03-26 04:00:00', 10.00, 10.00, 10.00, 10.00, 'Pruebas', 10.00, 1, 'TW-5', 1, 10.00, 2, 7, '2021-03-26 04:00:00', 1, NULL),
(6, 5, 8, '', '2021-03-26 04:00:00', 10.00, 10.00, 10.00, 10.00, 'Pruebas', 10.00, 1, 'TW-6', 1, 10.00, 1, 7, '2021-03-26 04:00:00', 2, NULL),
(7, 7, 8, '', '2021-03-26 04:00:00', 10.00, 10.00, 10.00, 10.00, 'Pruebas', 10.00, 1, 'TW-7', 1, 10.00, 2, 7, '2021-03-26 04:00:00', 2, NULL),
(8, 7, 8, '', '2021-03-26 04:00:00', 10.00, 10.00, 10.00, 10.00, 'Pruebas', 10.00, 1, 'TW-8', 1, 10.00, 2, 7, '2021-03-26 04:00:00', 2, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `warehouses_states`
--

CREATE TABLE `warehouses_states` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `warehouse_packages`
--

CREATE TABLE `warehouse_packages` (
  `id` int(11) NOT NULL,
  `id_warehouse` int(11) NOT NULL,
  `id_package` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `warehouse_packages`
--

INSERT INTO `warehouse_packages` (`id`, `id_warehouse`, `id_package`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 2),
(4, 1, 1),
(5, 1, 1),
(11, 4, 10),
(12, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `warehouse_packing`
--

CREATE TABLE `warehouse_packing` (
  `id` int(11) NOT NULL,
  `id_packing` int(11) NOT NULL,
  `id_warehouse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `warehouse_packing`
--

INSERT INTO `warehouse_packing` (`id`, `id_packing`, `id_warehouse`) VALUES
(1, 3, 5),
(2, 4, 6),
(4, 7, 0),
(5, 10, 4),
(6, 10, 5),
(7, 10, 6),
(10, 12, 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alerts`
--
ALTER TABLE `alerts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `box`
--
ALTER TABLE `box`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `box_types`
--
ALTER TABLE `box_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `curriers`
--
ALTER TABLE `curriers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `depart`
--
ALTER TABLE `depart`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `headquarters`
--
ALTER TABLE `headquarters`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `packages_states`
--
ALTER TABLE `packages_states`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `packing`
--
ALTER TABLE `packing`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `packing_states`
--
ALTER TABLE `packing_states`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `packing_temp`
--
ALTER TABLE `packing_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `shipping_sites`
--
ALTER TABLE `shipping_sites`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `transp`
--
ALTER TABLE `transp`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `trip`
--
ALTER TABLE `trip`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_states`
--
ALTER TABLE `user_states`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `warehouses`
--
ALTER TABLE `warehouses`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `warehouses_states`
--
ALTER TABLE `warehouses_states`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `warehouse_packages`
--
ALTER TABLE `warehouse_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `warehouse_packing`
--
ALTER TABLE `warehouse_packing`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alerts`
--
ALTER TABLE `alerts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `box`
--
ALTER TABLE `box`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `box_types`
--
ALTER TABLE `box_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;

--
-- AUTO_INCREMENT de la tabla `curriers`
--
ALTER TABLE `curriers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `depart`
--
ALTER TABLE `depart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `headquarters`
--
ALTER TABLE `headquarters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `packages_states`
--
ALTER TABLE `packages_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `packing`
--
ALTER TABLE `packing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `packing_states`
--
ALTER TABLE `packing_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `packing_temp`
--
ALTER TABLE `packing_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `shipping_sites`
--
ALTER TABLE `shipping_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `transp`
--
ALTER TABLE `transp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `trip`
--
ALTER TABLE `trip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50006;

--
-- AUTO_INCREMENT de la tabla `user_states`
--
ALTER TABLE `user_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `warehouses`
--
ALTER TABLE `warehouses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `warehouses_states`
--
ALTER TABLE `warehouses_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `warehouse_packages`
--
ALTER TABLE `warehouse_packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `warehouse_packing`
--
ALTER TABLE `warehouse_packing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
