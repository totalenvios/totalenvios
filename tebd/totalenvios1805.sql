-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 18, 2021 at 10:01 AM
-- Server version: 8.0.25-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `totalenvios`
--

-- --------------------------------------------------------

--
-- Table structure for table `alerts`
--

CREATE TABLE `alerts` (
  `id` int NOT NULL,
  `tracking` varchar(100) NOT NULL,
  `date_r` timestamp NOT NULL,
  `id_client` int NOT NULL,
  `width` double(10,2) DEFAULT NULL,
  `height` double(10,2) DEFAULT NULL,
  `weight` double(10,2) DEFAULT NULL,
  `lenght` double(10,2) DEFAULT NULL,
  `value` double(10,2) NOT NULL DEFAULT '0.00',
  `transp` text NOT NULL,
  `date_in` timestamp NOT NULL,
  `content` text NOT NULL,
  `img` varchar(365) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `last` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `alerts`
--

INSERT INTO `alerts` (`id`, `tracking`, `date_r`, `id_client`, `width`, `height`, `weight`, `lenght`, `value`, `transp`, `date_in`, `content`, `img`, `last`) VALUES
(1, '131321', '2021-04-23 12:34:07', 19, NULL, NULL, NULL, NULL, 0.00, 'Amazon', '2021-04-24 12:34:07', '1 paqueta', '', 0),
(2, '213213', '2021-04-23 04:00:00', 5, NULL, NULL, NULL, NULL, 0.00, 'Pruebas', '2021-04-23 04:00:00', '12312sasad', '', 0),
(3, '213213', '2021-04-23 04:00:00', 5, NULL, NULL, NULL, NULL, 0.00, 'pruebas', '2021-04-23 04:00:00', '123213', '', 0),
(4, '2131', '2021-04-23 04:00:00', 5, NULL, NULL, NULL, NULL, 0.00, '12313', '2021-04-23 04:00:00', '123123', '', 0),
(5, '13123', '2021-04-24 04:00:00', 5, NULL, NULL, NULL, NULL, 0.00, 'Amazon', '2021-04-24 04:00:00', '2 Paquetes', '', 0),
(6, '213213', '2021-04-24 04:00:00', 5, NULL, NULL, NULL, NULL, 0.00, 'amazon', '2021-04-24 04:00:00', 'asdad', '', 0),
(7, '213213', '2021-04-28 04:00:00', 5, NULL, NULL, NULL, NULL, 0.00, 'amazom', '2021-04-28 04:00:00', 'pruebas contenido', '', 0),
(8, '111222333', '2021-05-06 04:00:00', 5, 10.00, 10.00, 10.00, 10.00, 0.00, 'AMAZON', '2021-05-06 04:00:00', 'Pruebas', '', 0),
(9, '', '2021-05-15 04:00:00', 50013, 11.00, 24.00, 11.00, 11.00, 11.00, '4', '2021-05-15 04:00:00', 'asdasd', '', 0),
(10, '213213', '2021-05-19 04:00:00', 50021, 11.00, 11.00, 11.00, 11.00, 11.00, '4', '2021-05-19 04:00:00', 'sdas', NULL, 0),
(11, '213213', '2021-05-17 04:00:00', 50021, 10.00, 10.00, 10.00, 10.00, 10.00, '4', '2021-05-17 04:00:00', 'asdsad', '213081061-Screenshot from 2021-05-12 18-18-17.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `box`
--

CREATE TABLE `box` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `width` double(10,2) NOT NULL DEFAULT '0.00',
  `height` double(10,2) NOT NULL DEFAULT '0.00',
  `weigth` double(10,2) NOT NULL DEFAULT '0.00',
  `lenght` double(10,2) NOT NULL DEFAULT '0.00',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `box`
--

INSERT INTO `box` (`id`, `name`, `width`, `height`, `weigth`, `lenght`, `active`) VALUES
(1, 'Small', 0.00, 0.00, 0.00, 0.00, 1),
(2, 'Medium', 0.00, 0.00, 0.00, 0.00, 1),
(3, 'Big', 0.00, 0.00, 0.00, 0.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `boxes`
--

CREATE TABLE `boxes` (
  `id` int NOT NULL,
  `code` varchar(45) NOT NULL,
  `height` double(10,2) NOT NULL,
  `width` double(10,2) NOT NULL,
  `weight` double(10,2) NOT NULL,
  `lenght` double(10,2) NOT NULL,
  `id_warehouse` int NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `id_region` int NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `id_region`, `active`) VALUES
(1, 'Argentina', 1, 1),
(2, 'Chile', 1, 1),
(3, 'Colombia', 1, 1),
(4, 'Mexico', 1, 1),
(5, 'Panama', 1, 1),
(6, 'USA', 1, 1),
(7, 'Venezuela', 1, 1),
(8, 'canada', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `curriers`
--

CREATE TABLE `curriers` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `id_destiny` int NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `curriers`
--

INSERT INTO `curriers` (`id`, `name`, `id_destiny`, `active`) VALUES
(1, 'Pruebas', 1, 1),
(2, 'Pruebas 1', 1, 1),
(3, 'Ramon', 1, 1),
(4, 'Pruebas', 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `headquarters`
--

CREATE TABLE `headquarters` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `headquarters`
--

INSERT INTO `headquarters` (`id`, `name`, `active`) VALUES
(1, 'Miami', 1),
(2, 'CA', 1);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int NOT NULL,
  `number` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_client` int NOT NULL,
  `date_i` timestamp NOT NULL,
  `value` int NOT NULL,
  `id_warehouse` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `number`, `id_client`, `date_i`, `value`, `id_warehouse`) VALUES
(1, NULL, 5, '2021-04-02 18:06:54', 20, 5),
(2, NULL, 5, '2021-04-02 18:20:02', 30, 6),
(3, NULL, 5, '2021-04-02 18:23:06', 20, 6),
(4, NULL, 5, '2021-04-02 04:00:00', 20, 5),
(5, NULL, 5, '2021-04-02 04:00:00', 20, 4);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int NOT NULL,
  `code` varchar(45) NOT NULL,
  `user_from` int DEFAULT NULL,
  `user_to` int DEFAULT NULL,
  `date_in` timestamp NOT NULL,
  `date_out` timestamp NULL DEFAULT NULL,
  `alias` text,
  `operator` int DEFAULT NULL,
  `tracking` varchar(45) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `state` int DEFAULT '1',
  `height` double(10,2) DEFAULT NULL,
  `width` double(10,2) DEFAULT NULL,
  `lenght` double(10,2) DEFAULT NULL,
  `weight` double(10,2) DEFAULT NULL,
  `id_headquarter` int DEFAULT NULL,
  `id_stock` int DEFAULT NULL,
  `id_transp` int DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `code`, `user_from`, `user_to`, `date_in`, `date_out`, `alias`, `operator`, `tracking`, `description`, `state`, `height`, `width`, `lenght`, `weight`, `id_headquarter`, `id_stock`, `id_transp`, `img`) VALUES
(1, '12345', 5, 6, '2021-03-17 12:18:44', '2021-03-19 12:18:44', NULL, 5, 'asdsa23122', 'Probando', 1, 10.00, 10.00, 10.00, 10.00, 0, 0, NULL, NULL),
(2, '1231', 6, 6, '2021-03-19 04:00:00', NULL, NULL, 7, '123123', 'sadas', 1, 1.00, 1.00, 1.00, 1.00, 0, 0, NULL, NULL),
(3, '1232', 6, 6, '2021-03-18 04:00:00', NULL, NULL, 7, '1312', 'dfdsfs', 1, 1.00, 1.00, 1.00, 1.00, 0, 0, NULL, NULL),
(4, '222333', 6, 6, '2021-03-17 04:00:00', NULL, NULL, 7, '3212312', 'Pruebas', 1, 10.00, 10.00, 10.00, 20.00, 0, 0, NULL, NULL),
(5, '13123', 6, 6, '2021-03-23 04:00:00', NULL, NULL, 7, '123213', 'sdsadasdsada', 1, 24.00, 10.00, 22.00, 15.00, 1, 1, NULL, NULL),
(6, '0', 6, 6, '2021-03-22 04:00:00', NULL, NULL, 7, '21331', 'sfsfsd', 1, 22.00, 10.00, 33.00, 15.00, 1, 1, NULL, NULL),
(7, '0', 6, 6, '2021-03-22 04:00:00', NULL, NULL, 7, '111', 'Paquete 10', 1, 10.00, 10.00, 10.00, 10.00, 2, 3, NULL, NULL),
(8, '0', 5, 0, '2021-03-23 04:00:00', '1999-01-01 04:00:00', NULL, 7, '12323', 'asdasd', 1, 11.00, 11.00, 11.00, 11.00, 2, 2, NULL, NULL),
(9, '0', 5, 0, '2021-03-09 04:00:00', '1999-01-01 04:00:00', NULL, NULL, '213213', 'adasd', NULL, 10.00, 10.00, 10.00, 1.00, NULL, NULL, NULL, NULL),
(10, '0', 5, 0, '2021-03-23 04:00:00', '1999-01-01 04:00:00', NULL, 0, '123', 'asdas', 1, 1.00, 10.00, 10.00, 10.00, 0, 0, NULL, NULL),
(11, '0', 10, 0, '2021-03-28 04:00:00', '1999-01-01 04:00:00', NULL, 0, '12321', 'pruebas', 1, 10.00, 10.00, 10.00, 10.00, 0, 0, NULL, NULL),
(12, '0', 0, 0, '2021-04-09 04:00:00', '1999-01-01 04:00:00', NULL, 0, '213213', 'sdadsa', 1, 10.00, 10.00, 10.00, 10.00, 0, 0, NULL, NULL),
(13, '0', 10, 0, '2021-04-24 04:00:00', '1999-01-01 04:00:00', NULL, 0, '213213', 'sdasdsa', 1, 24.00, 22.00, 22.00, 22.00, 0, 0, NULL, NULL),
(14, '0', 10, 0, '2021-04-24 04:00:00', '1999-01-01 04:00:00', NULL, 0, '213213', 'sdasdsa', 1, 24.00, 22.00, 22.00, 22.00, 0, 0, NULL, NULL),
(15, '0', 10, 0, '2021-04-24 04:00:00', '1999-01-01 04:00:00', NULL, 0, '213213', 'sdasdsa', 1, 24.00, 22.00, 22.00, 22.00, 0, 0, NULL, NULL),
(16, '0', 10, 0, '2021-04-24 04:00:00', '1999-01-01 04:00:00', NULL, 0, '213213', 'sdasdsa', 1, 24.00, 22.00, 22.00, 22.00, 0, 0, NULL, NULL),
(17, '0', 10, 0, '2021-04-24 04:00:00', '1999-01-01 04:00:00', NULL, 0, '213213', 'sdasdsa', 1, 24.00, 22.00, 22.00, 22.00, 0, 0, NULL, NULL),
(18, '0', 10, 0, '2021-04-24 04:00:00', '1999-01-01 04:00:00', NULL, 0, '213213', 'sdasdsa', 1, 24.00, 22.00, 22.00, 22.00, 0, 0, NULL, NULL),
(19, '0', 10, 0, '2021-04-24 04:00:00', '1999-01-01 04:00:00', NULL, 0, '213213', 'sdasdsa', 1, 24.00, 22.00, 22.00, 22.00, 0, 0, NULL, NULL),
(20, '0', 10, 0, '2021-04-24 04:00:00', '1999-01-01 04:00:00', NULL, 0, '213213', 'sdasdsa', 1, 24.00, 22.00, 22.00, 22.00, 0, 0, NULL, '1017341492-user.png'),
(21, '0', 8, 0, '2021-04-24 04:00:00', '1999-01-01 04:00:00', NULL, 0, '213213', 'adasd', 1, 24.00, 11.00, 11.00, 22.00, 0, 0, NULL, NULL),
(22, '0', 11, 0, '2021-04-16 04:00:00', '1999-01-01 04:00:00', NULL, 0, '213213', 'adasd', 1, 24.00, 22.00, 22.00, 22.00, 0, 0, NULL, '1185088467-user.png'),
(23, '0', 5, 0, '2021-04-24 04:00:00', '1999-01-01 04:00:00', NULL, 0, '213213', 'adasd', 1, 24.00, 11.00, 11.00, 11.00, 0, 0, NULL, NULL),
(24, '0', 5, 0, '2021-04-24 04:00:00', '1999-01-01 04:00:00', NULL, 0, '213213', 'adasd', 1, 24.00, 11.00, 11.00, 11.00, 0, 0, NULL, '438020323-user.png'),
(25, '', 50000, 50000, '2021-02-01 04:00:00', NULL, NULL, 0, '12345', 'Optional', 1, 20.00, 20.00, 20.00, 20.00, 0, 0, NULL, NULL),
(26, '', 50000, 50000, '2021-02-01 04:00:00', NULL, NULL, 0, '12345', 'Optional', 1, 20.00, 20.00, 20.00, 20.00, 0, 0, NULL, NULL),
(27, '', 50000, 50000, '2021-02-01 04:00:00', NULL, NULL, 0, '12345', 'Optional', 1, 20.00, 20.00, 20.00, 20.00, 0, 0, NULL, NULL),
(28, '', 50000, 50000, '2021-02-01 04:00:00', NULL, NULL, 0, '12345', 'Optional', 1, 20.00, 20.00, 20.00, 20.00, 0, 0, NULL, NULL),
(29, '', 50000, 50000, '2021-02-01 04:00:00', NULL, NULL, 0, '12345', 'Optional', 1, 20.00, 20.00, 20.00, 20.00, 0, 0, NULL, NULL),
(30, '', 50000, 50000, '2021-02-01 04:00:00', NULL, NULL, 0, '12345', 'Optional', 1, 20.00, 20.00, 20.00, 20.00, 0, 0, NULL, NULL),
(31, '12345', 0, 0, '2021-05-06 04:00:00', '1999-01-01 04:00:00', 'Ramon', 0, '111222333', 'adasd', 1, 10.00, 10.00, 10.00, 10.00, 0, 0, 5, '892509594-favicon.png'),
(32, '12131', 50007, 0, '2021-05-06 04:00:00', '1999-01-01 04:00:00', 'Ramon', 0, '111222333', 'saada', 1, 10.00, 10.00, 10.00, 10.00, 0, 0, 5, NULL),
(33, 'saad', 0, 0, '2021-04-23 04:00:00', '1999-01-01 04:00:00', 'Ramon', 0, '213213', '', 1, 0.00, 0.00, 0.00, 0.00, 0, 0, 5, NULL),
(34, '1234', 50008, 0, '2021-05-06 04:00:00', '1999-01-01 04:00:00', 'Ramon', 0, '111222333', '', 1, 10.00, 10.00, 10.00, 10.00, 0, 0, 4, NULL),
(35, '12312232', 9, 0, '2021-04-23 04:00:00', '1999-01-01 04:00:00', 'Ramon ', 0, '213213', 'adasd', 1, 10.00, 10.00, 10.00, 10.00, 0, 0, 4, NULL),
(36, '12312232', 8, 0, '2021-04-24 04:00:00', '1999-01-01 04:00:00', 'Ramon ', 16, '213213', 'adasd', 1, 10.00, 10.00, 10.00, 10.00, 0, 0, 5, NULL),
(37, '12312232', 8, 0, '2021-05-13 04:00:00', '1999-01-01 04:00:00', 'Ramon ', 0, '', 'adasd', 1, 1.00, 1.00, 1.00, 1.00, 0, 0, 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `packages_states`
--

CREATE TABLE `packages_states` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `packages_states`
--

INSERT INTO `packages_states` (`id`, `name`, `active`) VALUES
(1, 'Activo', 1),
(2, 'Cancelado', 1),
(3, 'Entregado', 1);

-- --------------------------------------------------------

--
-- Table structure for table `packing`
--

CREATE TABLE `packing` (
  `id` int NOT NULL,
  `date_in` timestamp NOT NULL,
  `name` varchar(45) NOT NULL,
  `note` text NOT NULL,
  `id_currier` int NOT NULL,
  `date_out` timestamp NOT NULL,
  `id_trip` int NOT NULL,
  `id_destiny` int NOT NULL,
  `state` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `packing`
--

INSERT INTO `packing` (`id`, `date_in`, `name`, `note`, `id_currier`, `date_out`, `id_trip`, `id_destiny`, `state`) VALUES
(3, '2021-04-13 04:00:00', 'Ramon', 'pruebas', 2, '2021-04-13 04:00:00', 1, 5, 1),
(4, '2021-04-13 04:00:00', 'Pruebas', 'Probando', 2, '2021-04-13 04:00:00', 1, 3, 1),
(6, '2021-04-16 04:00:00', 'Ramon Briceno', 'pruebas', 2, '2021-04-16 04:00:00', 1, 7, 1),
(7, '2021-04-16 04:00:00', 'Ramon Briceno', 'pruebas', 2, '2021-04-16 04:00:00', 2, 7, 2),
(8, '2021-04-16 04:00:00', 'Ramon Briceno', 'pruebas', 2, '2021-04-16 04:00:00', 2, 7, 2),
(9, '2021-04-16 04:00:00', 'Ramon Briceno', 'pruebas', 2, '2021-04-16 04:00:00', 2, 7, 2),
(10, '2021-04-16 04:00:00', 'Ramon Briceno', 'pruebas', 2, '2021-04-16 04:00:00', 2, 7, 2),
(12, '2021-04-16 04:00:00', 'Ramon Briceno', 'pruebas', 2, '2021-04-16 04:00:00', 1, 7, 2);

-- --------------------------------------------------------

--
-- Table structure for table `packing_states`
--

CREATE TABLE `packing_states` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `packing_states`
--

INSERT INTO `packing_states` (`id`, `name`, `active`) VALUES
(3, 'Activo', 1);

-- --------------------------------------------------------

--
-- Table structure for table `packing_temp`
--

CREATE TABLE `packing_temp` (
  `id` int NOT NULL,
  `json` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `packing_temp`
--

INSERT INTO `packing_temp` (`id`, `json`) VALUES
(1, '4,5,6,7'),
(2, '4,5,6'),
(3, '4,5'),
(4, '4,5,6,7'),
(5, '4,5,6,7'),
(6, '4,5');

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `name`, `active`) VALUES
(1, 'Norte', 1),
(2, 'Sur', 1),
(3, 'Este', 1),
(4, 'Oeste', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `id_headquarter` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `name`, `active`, `id_headquarter`) VALUES
(1, 'Pruebas alma', 1, 1),
(2, 'Pruebas', 1, 2),
(3, 'Pruebas 111', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `transp`
--

CREATE TABLE `transp` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `transp`
--

INSERT INTO `transp` (`id`, `name`, `active`) VALUES
(1, 'FEDEX', 1),
(2, 'UPS', 1),
(3, 'DHL', 1),
(4, 'AMAZON', 1),
(5, 'USPS', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trip`
--

CREATE TABLE `trip` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `trip`
--

INSERT INTO `trip` (`id`, `name`, `active`) VALUES
(1, 'Aereo', 1),
(2, 'Maritimo', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `document` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `address_send` text,
  `country` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `phone_house` varchar(45) NOT NULL,
  `user_type` int NOT NULL,
  `addressee` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(100) NOT NULL,
  `id_headquarter` int NOT NULL DEFAULT '0',
  `code` text,
  `born_date` timestamp NOT NULL DEFAULT '1990-01-01 04:00:00',
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `temp_token` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `email`, `document`, `address`, `address_send`, `country`, `phone`, `phone_house`, `user_type`, `addressee`, `password`, `id_headquarter`, `code`, `born_date`, `confirmed`, `temp_token`) VALUES
(6, 'Pruebas', 'Pruebas', 'pruebas@gmail.com', '12345', 'El cafetal', NULL, '3', '12345', '', 2, 0, '$2y$10$Kfn6DABy72HYTar3cauTuORG4uM.d2Gwu6x54iwu5iGiDySvRNCrS', 0, '12312232', '1990-01-01 04:00:00', 0, '6913f5ebf26afd4e23312ba7f1ec642c63bd2c5c48b1d2c70884071817d428d366d3a08686be653cdea591581fe429119ac16d780f93ef68c3ce3eda3b5fcd0c'),
(7, 'Operador', 'Operador', 'operador@totalenvios.com', '12345', 'Miami', NULL, '6', '12345', '', 3, 0, '$2y$10$gGCbVWuVmT45nZ3D7SSo6.yiquqPg2SfExW.LTSs0gkcTvfOyEbpi', 0, '12312232', '1990-01-01 04:00:00', 0, ''),
(8, 'Ramon', '1', 'ramon@gmail.com', '8782832', 'sdsadsaa', NULL, '6', '4545345', '', 1, 0, '$2y$10$UwahcIgQq7p5VlRJ4w9aBufrBUZlM3TRUDY/SHio6B1pfZjEYEizO', 0, '12312232', '1990-01-01 04:00:00', 0, ''),
(9, 'Ramon', 'Pruebas', 'ramonpr@gmail.com', '1312312', 'Miami', NULL, '6', '123213', '', 1, 0, '$2y$10$rXOfnTfPQVs0/QdV6b.K6.jhXasK/J2WlRyFmVgQxVmszjDEzFa6u', 0, '12312232', '1990-01-01 04:00:00', 0, ''),
(10, 'Ramon', 'Probandor', 'rapro@gmail.com', '2312321', 'Caracas', NULL, '7', '1312321', '', 1, 0, '$2y$10$yfsxGSde.SxAvoewXX79UOGyCoX7B2F9ELl7WOvwzouBcrFFJXIjm', 0, '12312232', '1990-01-01 04:00:00', 0, ''),
(11, 'Ramon', 'Briceno', 'ramonbriceno1212@gmail.com', '213213', 'El cafetal', NULL, '7', '12343', '', 1, 0, '$2y$10$eAyk/ZCfZwDTkXb7RXarhe3KrZsPAUYOF5WxsWd8TXFQaS0UFQWJm', 1, '12312232', '1990-01-01 04:00:00', 0, ''),
(12, 'Ramon', 'Briceno', 'ramonbriceno12111@gmail.com', '1121111', 'El cafetal', NULL, '7', '132123', '', 1, 0, '$2y$10$2Xs3Xt2nTAk0dYEn99vkte/QCirJAIO2V2BVvFvgw07cxGJJCjWVW', 1, '12312232', '1990-01-01 04:00:00', 0, ''),
(13, 'Ramon', 'Briceno', 'ramonbriceno12111111@gmail.com', '12sadsadadqe21e', 'El cafetal', NULL, '7', '312321312321', '', 1, 0, '$2y$10$Cf7BDOQkbbEhZ0ov.oxNgeiI/BIUNPVWSz9bhU4qlYOEW.2PZMani', 1, '12312232', '1994-08-31 04:00:00', 0, ''),
(14, 'Ramon', 'Briceno', 'ramonbriceno121212121212@gmail.com', '123213', 'El cafetal', NULL, '7', '3123213', '', 1, 0, '$2y$10$QYOFo5fn1tqfxyUt/NkCkeGJT5r9uklvvbgI9XUQK7qhf/SaSWy32', 2, '12312232', '2021-03-01 04:00:00', 0, ''),
(15, 'Ramon', 'Briceno', 'ramonbriceno14@gmail.com', '12321312', 'El cafetal', NULL, '7', '04122434077', '', 1, 0, '$2y$10$HvmickkedBKzXUDGZ60ieOGZvi8Agc4Z.DDgR6ZF5uf6QQuI72n5C', 1, '12312232', '2021-03-17 04:00:00', 0, ''),
(16, 'Ramon', 'Briceno', 'ramonbriceno16@gmail.com', '123213', 'El cafetal', NULL, '7', '04122434077', '', 3, 0, '$2y$10$YPgtrjoMsCiveBpjrP/99O0Di/J9PC0hO2ocQu1OLFWtkzMEll00q', 2, '12312232', '2021-02-04 04:00:00', 0, ''),
(17, 'Ramon', 'Briceno', 'ramonbriceno17@gmail.com', '13123', 'El cafetal', NULL, '7', '04122434077', '', 2, 0, '$2y$10$G3gnmnemfUCO9QEOIGBubelylB0inoeb5/oe17UuhOPpVqKVWbFEC', 1, '12312232', '2021-03-13 04:00:00', 0, ''),
(18, 'Ramon', 'Briceno', 'ramonbriceno1215@gmail.com', '21321', 'El cafetal', NULL, '6', '04122434077', '', 3, 0, '$2y$10$oIt59m6YGTDjBQ4zlUua7u/IxmuCTXpT9pq.2c055BNzTBt0G.YWq', 2, '12312232', '2021-03-01 04:00:00', 0, ''),
(19, 'Juan', 'Pablo', 'juan@gmail.com', '123123', 'El cafetal', NULL, '7', '1223', '', 1, 0, '$2y$10$LzkZ.oX4BoYRp/rscdSpceN/.OLBE1aUx9PEmmP5Ddx6KzOhIAIy6', 1, '12312232', '2021-04-22 04:00:00', 0, ''),
(50000, 'Default', 'Default', 'default@totalenvios.com', '123456789', 'Default Timezone', NULL, 'Default', '123456789', '', 2, 0, '1234567891', 0, '12312232', '1990-01-01 04:00:00', 0, ''),
(50001, 'Ramon', 'Briceno', 'ramonbriceno15@gmail.com', '12345', 'NONE', NULL, '7', '04122434077', '', 2, 0, '$2y$10$m/UdCWYAUEZtv0ykJS87Ae385LO26j5mf6EspaSAQWusMCyYY0Nse', 1, 'TW-30357', '2021-05-06 04:00:00', 0, NULL),
(50002, 'Ramon', 'Briceno', 'ramonbriceno199@gmail.com', '12345', 'NONE', NULL, '7', '04122434077', '', 2, 0, '$2y$10$nc7CP1alTz3gzdB60.9/cO/mTwWkwPd.gRd/cfb8aU26YS6OhSYt.', 1, 'TW-71045', '2021-05-06 04:00:00', 0, NULL),
(50003, 'Ramon', 'Briceno', 'ramonbriceno1221@gmail.com', '12345', 'NONE', NULL, '7', '04122434077', '', 2, 0, '$2y$10$Dp0JXU5iAnlAyvFu9DkSieHRgMz.DpxmfJBe1xJ7cffJ3sVNiwtFq', 1, 'TW-74857', '2021-05-04 04:00:00', 0, NULL),
(50004, 'Ramon', 'Briceno', 'ramonbriceno22@gmail.com', '12345', 'NONE', NULL, '7', '04122434077', '', 2, 0, '$2y$10$VrNPoe9vj708HmNdj5A15esM6RnstMzVL70tdq.uec0swrJul5ciK', 1, 'TW-36953', '2021-05-06 04:00:00', 0, NULL),
(50005, 'Ramon', 'Briceno', 'ramonbriceno3000@gmail.com', '12345', 'NONE', NULL, '7', '04122434077', '', 2, 0, '$2y$10$zaR32Rw8QYOf4yYRHn4MaOA8JQbTNh02v1g56NaSj8kH5TjJi54JW', 1, 'TW-67445', '2021-05-06 04:00:00', 0, NULL),
(50006, 'Ramon', 'Briceno', 'ramonbricen12@gmail.com', '12345', 'NONE', NULL, '7', '04122434077', '', 2, 0, '$2y$10$yonVYvYf00BDyvOs5/zb3eXO1ZZiQXPP0qh6qw5qB8DD/kFwKXjJS', 1, 'TW-41175', '2021-05-07 04:00:00', 0, NULL),
(50007, 'Ramon', 'Briceno', 'ramonbriceno50@gmail.com', '12345', 'NONE', NULL, '7', '04122434077', '', 2, 0, '$2y$10$PA.UVgEPmXmSYbk2mmU89OhopJhrCEpxG7YcSi41U0O3kq/ENcWzW', 1, 'TW-29580', '2021-05-05 04:00:00', 0, NULL),
(50008, 'Ramon', 'Briceno', 'ramonbriceno60@gmail.com', '12345', 'NONE', NULL, '7', '04122434077', '', 2, 0, '$2y$10$9OTaAyfStJ3PhozyW2FZCumUDLtOHjPfKK1iRK6wmdRjL09FLCqxG', 1, 'TW-58715', '2021-05-06 04:00:00', 0, NULL),
(50009, 'Ramon', 'Briceno', 'ramonbriceno28@gmail.com', '12345', 'Calle Cumana, Caracas 1061, Distrito Capital, Venezuela', NULL, '7', '04122434077', '', 2, 0, '$2y$10$aeyPZ6k5yLQETM9BxZj0G.t9MXWFLHX9axIAfe1qSrKkjD2vCgEh.', 1, 'TW-20234', '2021-05-11 04:00:00', 1, NULL),
(50014, 'Ramon', 'Briceno', 'ramonbriceno43@gmail.com', '12345', 'El cafetal', NULL, 'Romania', '04122434077', '', 2, 0, '$2y$10$6qMLFVKBIQF.Map3Y1B7OectZdT/97dwQ9gqrIkjAY/vFb67BqZDS', 1, 'TW-62508', '2021-05-12 04:00:00', 0, NULL),
(50015, 'Ramon', 'Briceno', 'ramonbriceno121511@gmail.com', '12345', 'NONE', NULL, '7', '04122434077', '', 2, 0, '$2y$10$9vkd8/Bipf/HnerzkeDbNuj1/Qgdt.V/5VMReoCp8nWi4ypZo2Dt6', 1, 'TW-80583', '2021-05-13 04:00:00', 0, NULL),
(50016, 'Ramon', 'Briceno', 'ramonbriceno121111@gmail.com', '12345', 'NONE', NULL, '7', '04122434077', '', 2, 0, '$2y$10$zw4PR6/W41whxgiba4xY5ONsX11nSajs1GXm7gayl/pgVQUHSNkBa', 1, 'TW-15390', '2021-05-13 04:00:00', 0, NULL),
(50017, 'Ramon', 'Briceno', 'ramonbriceno122122@gmail.com', '12345', 'NONE', NULL, '7', '04122434077', '', 2, 0, '$2y$10$gvpIc.FWxG6CoUG2HyBdOeudmWsbR1XLGai7JByKjO0tF0uUA.YXq', 1, 'TW-97241', '2021-05-06 04:00:00', 0, NULL),
(50018, 'daniel', 'da', 'daniel@gmail.com', '12345', 'NONE', NULL, '1', '1111', '', 2, 0, '$2y$10$iVwCk8w8WitkteC8kaW/2.0r0uEMEiErQoUtZLDN79xBYO1O83FkS', 1, 'TW-15671', '2021-05-08 04:00:00', 0, NULL),
(50019, 'jose', 'jose', 'josed@gmail.com', '12345', 'NONE', NULL, '7', '04122434077', '', 2, 0, '$2y$10$Fk0t8iRRugBLgK5LIeGwk.ohKaBlvD8EOSq6KIgq88M6Vjib6wrMm', 1, 'TW-27837', '2021-05-13 04:00:00', 0, NULL),
(50020, 'Carlos', 'Pruebas', 'carlospruebas@gmail.com', '12345', 'Calle Cumana, Caracas 1061, Distrito Capital, Venezuela', NULL, 'Venezuela', '12345', '', 2, 0, '$2y$10$bE6he2lQiHfp8UCYfI74leJLD2zH5WceoWc9D.9bhMHt1fVdPtHPi', 1, 'TW-36492', '2021-05-19 04:00:00', 0, NULL),
(50021, 'ramon', 'briceno', 'ramonbriceno12@gmail.com', '12345', 'Calle Cumana, Caracas 1061, Distrito Capital, Venezuela', 'Venezuela', 'Afganistan', '12345', '', 2, 0, '$2y$10$pQPp8.OOvuItl/Uj3IgBAOVzTtxoY7txhT4d92Q7fSEC2VI7TaMOO', 1, 'TW-36675', '2021-05-18 04:00:00', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_states`
--

CREATE TABLE `user_states` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `name`) VALUES
(1, 'Destinatario'),
(2, 'Cliente'),
(3, 'Operador'),
(4, 'Admin'),
(5, 'Empresa');

-- --------------------------------------------------------

--
-- Table structure for table `warehouses`
--

CREATE TABLE `warehouses` (
  `id` int NOT NULL,
  `id_from` int NOT NULL,
  `id_to` int NOT NULL,
  `address` text NOT NULL,
  `date_in` timestamp NOT NULL,
  `height` double(10,2) NOT NULL,
  `width` double(10,2) NOT NULL,
  `weight` double(10,2) NOT NULL,
  `lenght` double(10,2) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `price` double(10,2) NOT NULL,
  `id_currier` int NOT NULL,
  `code` varchar(45) NOT NULL,
  `state` int DEFAULT '1',
  `cost` double(10,2) NOT NULL,
  `trip_id` int NOT NULL,
  `country_id` int NOT NULL,
  `date_send` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `box` int NOT NULL DEFAULT '0',
  `casillero` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `warehouses`
--

INSERT INTO `warehouses` (`id`, `id_from`, `id_to`, `address`, `date_in`, `height`, `width`, `weight`, `lenght`, `description`, `price`, `id_currier`, `code`, `state`, `cost`, `trip_id`, `country_id`, `date_send`, `box`, `casillero`) VALUES
(4, 5, 6, '', '2021-04-06 04:00:00', 10.00, 10.00, 10.00, 10.00, 'pruebas', 10.00, 10, '0', 1, 10.00, 2, 4, '2021-03-26 04:00:00', 3, NULL),
(5, 5, 8, '', '2021-03-26 04:00:00', 10.00, 10.00, 10.00, 10.00, 'Pruebas', 10.00, 1, 'TW-5', 1, 10.00, 2, 7, '2021-03-26 04:00:00', 1, NULL),
(6, 5, 8, '', '2021-03-26 04:00:00', 10.00, 10.00, 10.00, 10.00, 'Pruebas', 10.00, 1, 'TW-6', 1, 10.00, 1, 7, '2021-03-26 04:00:00', 2, NULL),
(7, 7, 8, '', '2021-03-26 04:00:00', 10.00, 10.00, 10.00, 10.00, 'Pruebas', 10.00, 1, 'TW-7', 1, 10.00, 2, 7, '2021-03-26 04:00:00', 2, NULL),
(8, 7, 8, '', '2021-03-26 04:00:00', 10.00, 10.00, 10.00, 10.00, 'Pruebas', 10.00, 1, 'TW-8', 1, 10.00, 2, 7, '2021-03-26 04:00:00', 2, NULL),
(9, 8, 11, '', '2021-05-11 04:00:00', 10.00, 1.00, 10.00, 10.00, 'adasd', 10.00, 1, 'TW-53600', 1, 10.00, 1, 1, '2021-05-11 04:00:00', 1, 11312),
(10, 8, 11, '', '2021-05-11 04:00:00', 10.00, 10.00, 10.00, 10.00, 'adasd', 10.00, 1, 'TW-36010', 1, 10.00, 1, 1, '2021-05-11 04:00:00', 1, 11),
(11, 8, 12, '', '2021-05-11 04:00:00', 24.00, 24.00, 1.00, 1.00, '', 1.00, 2, 'TW-61984', 1, 1.00, 1, 1, '2021-05-11 04:00:00', 1, 11),
(12, 8, 11, '', '2021-05-12 04:00:00', 10.00, 10.00, 10.00, 10.00, 'adasd', 20.00, 1, 'TW-69845', 1, 20.00, 1, 7, '2021-05-12 04:00:00', 1, 1),
(13, 0, 50021, '', '2021-05-18 04:00:00', 10.00, 10.00, 10.00, 10.00, 'adasd', 22.00, 1, 'TE-WH37732', 2, 22.00, 1, 1, '2021-05-21 04:00:00', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `warehouses_states`
--

CREATE TABLE `warehouses_states` (
  `id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `warehouses_states`
--

INSERT INTO `warehouses_states` (`id`, `name`, `active`) VALUES
(1, 'Completo', 1),
(2, 'Pendiente', 1);

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_packages`
--

CREATE TABLE `warehouse_packages` (
  `id` int NOT NULL,
  `id_warehouse` int NOT NULL,
  `id_package` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `warehouse_packages`
--

INSERT INTO `warehouse_packages` (`id`, `id_warehouse`, `id_package`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 2),
(4, 1, 1),
(5, 1, 1),
(11, 4, 10),
(12, 4, 1),
(13, 9, 1),
(14, 9, 2),
(15, 10, 1),
(16, 10, 2),
(17, 11, 1),
(18, 11, 2),
(19, 12, 1),
(20, 12, 2),
(21, 12, 3);

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_packing`
--

CREATE TABLE `warehouse_packing` (
  `id` int NOT NULL,
  `id_packing` int NOT NULL,
  `id_warehouse` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `warehouse_packing`
--

INSERT INTO `warehouse_packing` (`id`, `id_packing`, `id_warehouse`) VALUES
(1, 3, 5),
(2, 4, 6),
(4, 7, 0),
(5, 10, 4),
(6, 10, 5),
(7, 10, 6),
(10, 12, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alerts`
--
ALTER TABLE `alerts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `box`
--
ALTER TABLE `box`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `boxes`
--
ALTER TABLE `boxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curriers`
--
ALTER TABLE `curriers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `headquarters`
--
ALTER TABLE `headquarters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages_states`
--
ALTER TABLE `packages_states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packing`
--
ALTER TABLE `packing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packing_states`
--
ALTER TABLE `packing_states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packing_temp`
--
ALTER TABLE `packing_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transp`
--
ALTER TABLE `transp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trip`
--
ALTER TABLE `trip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_states`
--
ALTER TABLE `user_states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouses`
--
ALTER TABLE `warehouses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouses_states`
--
ALTER TABLE `warehouses_states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse_packages`
--
ALTER TABLE `warehouse_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse_packing`
--
ALTER TABLE `warehouse_packing`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alerts`
--
ALTER TABLE `alerts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `box`
--
ALTER TABLE `box`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `boxes`
--
ALTER TABLE `boxes`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `curriers`
--
ALTER TABLE `curriers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `headquarters`
--
ALTER TABLE `headquarters`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `packages_states`
--
ALTER TABLE `packages_states`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `packing`
--
ALTER TABLE `packing`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `packing_states`
--
ALTER TABLE `packing_states`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `packing_temp`
--
ALTER TABLE `packing_temp`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transp`
--
ALTER TABLE `transp`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `trip`
--
ALTER TABLE `trip`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50022;

--
-- AUTO_INCREMENT for table `user_states`
--
ALTER TABLE `user_states`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `warehouses`
--
ALTER TABLE `warehouses`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `warehouses_states`
--
ALTER TABLE `warehouses_states`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `warehouse_packages`
--
ALTER TABLE `warehouse_packages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `warehouse_packing`
--
ALTER TABLE `warehouse_packing`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
