
<?php  

    include 'includes/connection.class.php';
    include 'includes/users.class.php';
    include 'includes/headquarters.class.php';
    include 'includes/packages.class.php';

    $headquarters = new Headquarters;
    $allHeadQ = $headquarters->findAll();

    $packages = new Packages;

    $users = new Users;
    $allUsers = $users->findAll();
    $userTypes = $users->findUserTypes();

    $allDestiner = $users->findDestiner();

    $usersRegistered = $users->countClients()['cuantos'];
    $userForConfirmed = $users->countClientsForConfirmed()['forConfirmed'];

    $allUserType = $users->findUserTypes();




    session_start();
    if ($_SESSION['state'] != 1) {
        header('Location: login.php');
    }
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

        include 'head.php';

    ?>
</head>

<body style="background:#e3e3e3ff;">
    <!-- Modal -->
    <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="registerModalLabel">Datos:</h5>
            <button onclick="$('#registerModal').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="alert alert-danger register-error" role="alert">
                        </div>
                        <div class="alert alert-success register-success" role="alert">
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Nombre *</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input id="name" type="text" placeholder="Nombre"
                                            class="form-control p-0 border-0"> </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Apellido *</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input id="lastname" type="text" placeholder="Apellido"
                                            class="form-control p-0 border-0"> </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label for="example-email" class="col-md-12 p-0">Fecha de Nacimiento *</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input type="date" placeholder="1990-01-01"
                                            class="form-control p-0 border-0"
                                            id="born_date">
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label for="example-email" class="col-md-12 p-0">Email *</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input type="email" placeholder="johnathan@admin.com"
                                            class="form-control p-0 border-0" name="example-email"
                                            id="email">
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-sm-12">Tipo de usuario *</label>
                                    <div class="col-sm-12 border-bottom">
                                        <select id="user_type" class="form-select shadow-none p-0 border-0 form-control-line">
                                            <option value="0">Seleccionar</option>
                                            <?php  

                                                foreach ($userTypes as $key => $value) {
                                            ?>
                                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                            <?php
                                                }

                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Password *</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input id="password" type="password" value="password" class="form-control p-0 border-0">
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Teléfono *</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input id="phone" type="text" placeholder="123 456 7890" class="form-control p-0 border-0">
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Documento *</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input id="document" type="text" placeholder="# Documento" class="form-control p-0 border-0"> </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-sm-12">País *</label>
                                    <div class="col-sm-12 border-bottom">
                                        <select id="country" class="form-select shadow-none p-0 border-0 form-control-line">
                                            <option value="1">Argentina</option>
                                            <option value="2">Chile</option>
                                            <option value="3">Colombia</option>
                                            <option value="4">Mexico</option>
                                            <option value="5">Panama</option>
                                            <option value="6">USA</option>
                                            <option value="7">Venezuela</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-sm-12">Dirección *</label>
                                    <div class="col-sm-12 border-bottom">
                                        <input id="address" type="text" placeholder="Dirección" class="form-control p-0 border-0">
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-sm-12">Agencia *</label>
                                    <div class="col-sm-12 border-bottom">
                                        <select id="headquarter" class="form-select shadow-none p-0 border-0 form-control-line">
                                            <option value="0">Seleccionar...</option>
                                            <?php  
                                                foreach ($allHeadQ as $key => $value) {
                                            ?>
                                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                            <?php
                                                }


                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                                <div class="form-group mb-4">
                                    <div class="col-sm-12">
                                        <button onclick="registerNew();" class="btn btn-success">Registrar</button>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>




    <div class="modal fade" id="deleteClientForModal" tabindex="-1" role="dialog" aria-labelledby="deleteClientForModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteClientForModalLabel">ELIMINAR CLIENTE</h5>
                        <button onclick="$('#deleteClientForModal').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body text-center">
                    <div class="container-fluid text-center">
                        <div class="row text-center">
                            <div class="col-lg-12 col-xlg-12 col-md-12 text-center">
                                <img src="icons/alert.png" width="60" height="60"><br><br>
                            </div>    
                            <h4 style="color:#005574;">Al eliminar un cliente, eliminará toda su lista de destinatarios <br>¿Desea continuar con esta operación?</h4>
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="alert alert-danger register-error" role="alert">
                                </div>
                                <div class="alert alert-success register-success" role="alert">
                                </div>

                               <input type="text" id="user_id_modal" hidden>

                                <div class="form-group mb-4">
                                    <div class="btn-group" style="content-align:center; width:100%">
                                                           
                                                        
                                                      
                                                          
    

                                    <div><a onclick="DeleteClientAndDestiner();"class="btn btn-primary" type="button" style="background-color:white; color:#005574; border:2px solid white;"><i class="fas fa-check"></i></a></div>
                                    
                                
                                    </div>
                                </div>
                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="userTypeModal" tabindex="-1" role="dialog" aria-labelledby="userTypeModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="userTypeModalLabel">TIPO DE USUARIO</h5>
                        <button onclick="$('#userTypeModal').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body text-center">
                    <div class="container-fluid text-center">
                        <div class="row text-center">
                            <h4 style="color:#005574;">Cambiar el tipo de usuario</h4>
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="form-group">
                                        <select id="user_type_client" class="form-select campos-input-login">
                                           
                                            <?php  
                                                foreach ($allUserType as $key => $value) {
                                            ?>
                                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                </div>
                               <input type="text" id="user_id_modal_type" hidden>

                                <div class="form-group mb-4">
                                    <div class="btn-group" style="content-align:center; width:100%">
                                                    

                                    <div><a onclick="cambiarUserType();"class="btn btn-primary" type="button" style="background-color:white; color:#005574; border:2px solid white;"><i class="fas fa-check"></i></a></div>
                                    
                                
                                    </div>
                                </div>
                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper"  data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
        <?php 

            include 'navbar.php'; 
            

        ?>
        <div class="page-wrapper" style="background:#e3e3e3ff;">

        <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <div class= "card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                        <img src="icons/usuarios-confirmados.png" alt="" width="100" height="100">
                                    </div> 
                                    <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                        <h5 style="color:#005574;"><strong>USUARIOS</strong></h5>
                                        <h1 style="color:#005574;"><?php echo $usersRegistered; ?></h1>
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <div class= "card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                        <img src="icons/usuarios-por-confirmar.png" alt="" width="100" height="100">
                                    </div> 
                                    <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                        <h5 style="color:#005574;"><strong>POR CONFIRMAR</strong></h5>
                                        <h1 style="color:#005574;"><?php echo $userForConfirmed; ?></h1>
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <div class= "card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                        <img src="icons/cumpleaños.png" alt="" width="100" height="100">
                                    </div> 
                                    <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                        <h5 style="color:#005574;"><strong>CUMPLEAÑOS</strong></h5>
                                        <h1 style="color:#005574;"><?php echo $userForConfirmed; ?></h1>
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 
                
              
               
                <div class="container-fluid" style="background:#e3e3e3ff;">
                             
                
                    <!-- <div class="row">
                        <div class="col-sm-10 col-md-10 col-lg-10">
                            <h4  align="center" style="background:white; color:#005574;">Listado de warehouse</h4> -->
                            <!-- <h4 align="right"><a href="registerWarehouse.php" style="background-color:#ff554dff; color:white;"><i style="color:white;" class="fas fa-plus"></i></a></h4> -->
                        <!-- </div> -->

                        <!-- <div class="col-sm-2 col-md-2 col-lg-2">
                            <a href="registerWarehouse.php" class="btn" style="background-color:#ff554dff; color:white;"><i style="color:white;" class="fas fa-plus"></i></a>
                        </div>
                    </div> -->
                    <div class="card" id="cliente-table" style="display:block;">
                        <div class="card-body">
                            <div class="row">
                                
                                <div class_="col-lg-4 col-xlg-4 col-md-4">
                                    <div class="row">
                                        <div class="input-group col-lg-3 col-xlg-3 col-md-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text search-span" id="basic-addon1"><i class="fas fa-search"></i></span>
                                            </div>
                                            <input id="search-user" onkeyup="searchUser();" type="text" class="form-control" placeholder="NOMBRE DEL CLIENTE" aria-label="Username" aria-describedby="basic-addon1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                                    <hr style="background-color:#005574;">
                                
                               
                                <div class="row">
                           
                                    <div class="col-sm-9 col-md-9 col-lg-9">
                                        <div align="left" style="background:white; color:#005574; width:100%; padding:8px;"><h4>CLIENTES</h4></div>
                                    </div><br>
                                    <div class="col-sm-3 col-md-3 col-lg-3" style="text-align:right; content-align:right;">
                                        
                                    
                                        <a align="right" class="btn" style="background-color:#ff554dff; color:white;" href="register-user.php"><i style="color:white;" class="fas fa-plus"></i> <strong>REGISTRAR CLIENTE</strong></a>
                                        
                                    </div>
                                </div>
                                
                                
                            
                            <br>
                            <div class="row">
                                <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                                <div class_="col-lg-10 col-xlg-10 col-md-10">
                                    <table class="table table-warehouse" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                    <thead>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">CASILLERO</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">NOMBRE</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574; witdh:90px;">CÉDULA</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574; witdh:200px;">EMAIL</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">MÓVIL</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">FIJO</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">PAIS</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">ESTADO</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">CIUDAD</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">DIRECCIÓN</th>
                                            
                                            <th style="border: 2px solid #e3e3e3ff; background-color:#white;"></th>
                                        </thead>
                                        <tbody class="users-body">
                                            <?php  

                                                foreach ($allUsers as $key => $value) {
                                            ?>
                                                    <tr>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['code']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['name'].' '.$value['lastname']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white; witdh:90px;"><?php echo $value['document']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white; witdh:200px;"><?php echo $value['email']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['phone']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['phone_house']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['country'];?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['state_name'];?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['city_name'];?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['address'];?></td>
                                                       
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                            <div class="btn-group" style="content-align:center; width:100%">
                                                           
                                                             <div><a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Ver destinatarios" href="destiner-list.php?id=<?php echo $value['id']; ?>" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-address-book"></i></a></div>
                                                        
                                                            
                                                            <div><a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Editar cliente" href="edit-user.php?id=<?php echo $value['id']; ?>" style="background-color:#005574; color:white; border:2px solid #005574;"><i class="fas fa-edit"></i></a></div>

                                                            <div><a class="btn btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Eliminar cliente y destinatarios" onclick="deleteModalForClient(<?php echo $value['id']; ?>); " style="background-color:white; color:#fc3c3d; border:2px solid #fc3c3d;"><i class="fas fa-trash"></i></a></div>
                                                            
                                                        
                                                            </div>
                                                        </td>
                                                    </tr>
                                            <?php
                                                }

                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class_="col-lg-1 col-xlg-12 col-md-1"></div>  
                            </div>
                        </div>
                    </div>
                            
                    <div class="card" id="destinatario-table" style="display:none;">
                        <div class="card-body">
                            <div class="row">
                                
                                <div class_="col-lg-10 col-xlg-10 col-md-10">
                                    <div class="row">
                                        <div class="input-group col-lg-3 col-xlg-3 col-md-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text search-span" id="basic-addon1"><i class="fas fa-search"></i></span>
                                            </div>
                                            <input id="search-user" onkeyup="searchUser();" type="text" class="form-control" placeholder="NOMBRE DEL CLIENTE" aria-label="Username" aria-describedby="basic-addon1">
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <hr style="background-color:#005574;">
                                
                               
                                <div class="row">
                           
                                    <div class="col-sm-9 col-md-9 col-lg-9">
                                        <div align="left" style="background:white; color:#005574; width:100%; padding:8px;"><h4>DESTINATARIOS</h4></div>
                                    </div><br>
                                    <div class="col-sm-3 col-md-3 col-lg-3" style="text-align:right; content-align:right;">
                                        
                                    
                                        <a align="right" class="btn" style="background-color:#ff554dff; color:white;" href="register-destiner.php"><i style="color:white;" class="fas fa-plus"></i> <strong>REGISTRAR DESTINATARIO</strong></a>
                                        
                                    </div>
                                </div>
                                
                                
                            
                            <br>
                            <div class="row">
                                <div class_="col-lg-1 col-xlg-12 col-md-1"></div>
                                <div class_="col-lg-10 col-xlg-10 col-md-10">
                                    <table class="table table-warehouse" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                    <thead>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">NOMBRE</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">PAIS</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574; witdh:90px;">ESTADO</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574; witdh:200px;">CIUDAD</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">CONTACTO</th>
                                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">CLIENTE</th>
                                            
                                            <th style="border: 2px solid #e3e3e3ff; background-color:#white;"></th>
                                        </thead>
                                        <tbody class="users-body">
                                            <?php  

                                                foreach ($allDestiner  as $key => $value) {
                                            ?>
                                                    <tr>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['name'].' '.$value['lastname']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white; witdh:90px;"><?php echo $value['country_name']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white; witdh:200px;"><?php echo $value['state_name']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['city_name']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['contact_phone'];?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php
                                                        
                                                        echo $value['user_id'];
                                                        
                                                        ?>
                                                        
                                                        </td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white; content-align:center;">
                                                            <div class="btn-group" style="content-align:center; width:100%" align="center">
                                                           
                                                             <div><a class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-user"></i></a></div>
                                                        
                                                            
                                                            <div><button class="btn btn-primary" style="background-color:#005574; color:white; border:2px solid #005574;"><i class="fas fa-edit"></i></button></div>

                                                            <div><a class="btn btn-primary" style="background-color:white; color:#fc3c3d; border:2px solid #fc3c3d;"><i class="fas fa-trash"></i></a></div>
                                                            
                                                        
                                                            </div>
                                                        </td>
                                                    </tr>
                                            <?php
                                                }

                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class_="col-lg-1 col-xlg-12 col-md-1"></div>  
                            </div>
                        </div>
                    </div>



                <footer class="footer text-center" style="background:#e3e3e3ff;"> 2021 © Total Envios
                </footer>
            </div>
            
        </div>    
            
    <?php 

        include 'foot.php';

    ?>
</body>

</html>