
    <?php  
        // error_reporting(0);
        // ini_set('display_errors', 0);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        
        include 'includes/connection.class.php';
        include 'includes/users.class.php';
        include 'includes/packages.class.php';
        include 'includes/headquarters.class.php';
        include 'includes/stock.class.php';
        include 'includes/warehouses.class.php';

        $id = $_REQUEST['id'];
        $users = new Users;
        $allUsers = $users->findAll();
        $packages = new Packages;
        $allPackagesByW = $packages->findAllByW($id);
        $headquarters = new Headquarters;
        $allHeadquarters = $headquarters->findAll();
        $allOperators = $users->findAllOperators();
        $allClients = $users->findAllClients();
        $states = $packages->findStates();

        $stock = new Stock;
        $warehouses = new Warehouses;
        $allBoxesByW = $warehouses->findAllBoxesW($id)[0];
        $allWarehouses = $warehouses->findAll();
        $findLast = $warehouses->findLast();
        $boxes = $packages->findBoxes();
        $trips = $warehouses->findTrips();
        $countries = $warehouses->findCountries();

        $warehouseData = $warehouses->findById($id);
        $clientData = $users->findById($warehouseData['id_from']);
        $clientToData = $users->findById($warehouseData['id_to']);

        session_start();
        if ($_SESSION['state'] != 1) {
            header('Location: login.php');
        }
        $date_in = date('2020-m-d');
        $date_out = date('Y-m-d');    
    ?>

    <!DOCTYPE html>
    <html dir="ltr" lang="en">

    <head>
        <?php 

            include 'head.php';

        ?>
    </head>

    <body>
    <input id="idWarehouse" type="hidden" value="<?php echo $id; ?>">
    <input id="wDelete" type="hidden" value="0">    
    <input id="warehouseId" type="hidden" value="0">
    <input id="idPackage" type="hidden" value="0">
    <input id="idPackageM" type="hidden" value="0">
    <input id="idUser" type="hidden" value="<?php echo $clientData['id']; ?>">
    <input id="idUserFor" type="hidden" value="<?php echo $clientToData['id']; ?>">
    <input id="idUserW" type="hidden" value="0">
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-header-position="absolute" data-boxed-layout="full">
        <?php 

            include 'navbar.php'; 
            if ($_SESSION['state'] != 1) {
                header('Location: login.php');
            }

        ?>
    
        <div class="page-wrapper">
            <div class="page-breadcrumb bg-white">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <!-- <h4 class="page-title"><a href="#" class="btn btn-primary" onclick="$('#warehouseModal').modal('show');" class="fw-normal">Nuevo Warehouse</a></h4> -->
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <div class="d-md-flex">
                            <ol class="breadcrumb ms-auto">
                                <li><a href="logout.php" class="fw-normal">Logout</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="alert alert-danger register-error" role="alert">
                </div>
                <div class="alert alert-success register-success" role="alert">
                </div>
                <div class="row justify-content-center">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 id="code-title">TW-<?php echo $warehouseData['id']; ?></h3>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                        <label for="example-email" class="col-md-12 p-0">Fecha de Registro *</label>
                                        <div class="row">
                                            <div class="col-md-6 border-bottom p-0">
                                                <input style="background-color: white !important;" type="text"class="form-control p-0 border-0" value="<?php echo $warehouseData['date_in']; ?>" placeholder="<?php echo $warehouseData['date_in']; ?>" readonly="readonly">
                                            </div>
                                            <div class="col-md-6 border-bottom p-0">
                                                <input type="date" class="form-control p-0 border-0" id="date_in" value="<?php echo $warehouseData['date_in']; ?>" placeholder="<?php echo $warehouseData['date_in']; ?>">
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Cliente</label>
                                        <div class="col-md-12 border-bottom p-0">
                                            <input onkeyup="searchUserPackage();" id="user_from" class="form-control" type="text" value="<?php echo $clientData['name'].' '.$clientData['lastname']; ?>">
                                            <ul id="myUS">
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Destinatario</label>
                                        <div class="col-md-12 border-bottom p-0">
                                            <input onkeyup="searchFilter();" id="myInput" class="form-control" type="text" value="<?php echo $clientToData['name'].' '.$clientToData['lastname']; ?>">
                                            <ul id="myUL">
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Tipo de viaje</label>
                                        <div class="col-md-12 border-bottom p-0">
                                            <select id="trip" class="form-select shadow-none p-0 border-0 form-control-line">
                                                <option value="0">Seleccionar</option>
                                                <?php  
                                                    $selected = '';

                                                    foreach ($trips as $key => $value) {
                                                        if ($value['id'] == $warehouseData['trip_id']) {
                                                            $selected = 'selected';
                                                        }else{
                                                            $selected = '';
                                                        }
                                                ?>
                                                        <option <?php echo $selected;?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                <?php
                                                    }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 border-bottom p-0">
                                    <div class="form-group mb-4">
                                        <label class="col-md-12 p-0">Destino *</label>
                                        <div class="col-md-12 border-bottom p-0">
                                            <select id="country" class="form-select shadow-none p-0 border-0 form-control-line">
                                                <option value="0">Seleccionar</option>
                                                <?php  
                                                    $selected = '';
                                                    foreach ($countries as $key => $value) {
                                                        if ($value['id'] == $warehouseData['country_id']) {
                                                            $selected = 'selected';
                                                        }else{
                                                            $selected = '';
                                                        }
                                                ?>
                                                        <option <?php echo $selected; ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                <?php
                                                    }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6 border-bottom p-0">
                                            <label for="example-email" class="col-md-12 p-0">Fecha de Envio *</label>
                                            <input style="background-color: white !important;" type="text"class="form-control p-0 border-0" value="<?php echo $warehouseData['date_in']; ?>" placeholder="<?php echo $warehouseData['date_in']; ?>" readonly="readonly">
                                        </div>
                                        <div class="col-md-6 border-bottom p-0">
                                            <label for="example-email" class="col-md-12 p-0">Fecha de Envio *</label>
                                            <input type="date" class="form-control p-0 border-0" id="date_send" value="<?php echo $warehouseData['date_send']; ?>" placeholder="<?php echo $warehouseData['date_send']; ?>">
                                        </div>
                                        

                                        
                                    </div>
                                </div>
                            </div><br>
                            <div class="row">
                                <!-- <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Descripcion</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input id="description" type="text" placeholder="Descripcion" class="form-control p-0 border-0" value="<?php echo $warehouseData['description']; ?>"> 
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Costo Mercancia</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input readonly="readonly" id="cost" type="text" placeholder="Costo" class="form-control p-0 border-0" value="<?php echo $warehouseData['cost']; ?>"> 
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0">Tipo</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <select id="box" class="form-select shadow-none p-0 border-0 form-control-line">
                                                    <option value="0">Seleccionar</option>
                                                    <?php  
                                                        $selected = '';
                                                        foreach ($boxes as $key => $value) {
                                                            if ($value['id'] == $warehouseData['box']) {
                                                                $selected = 'selected';
                                                            }else{
                                                                $selected = '';
                                                            }
                                                    ?>
                                                            <option <?php echo $selected; ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                    <?php
                                                        }

                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label class="col-md-12 p-0">Currier</label>
                                            <div class="col-md-12 border-bottom p-0">
                                                <input id="currier" type="text" placeholder="Currier" class="form-control p-0 border-0" value="<?php echo $warehouseData['id_currier']; ?>"> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="text-center">
                                        <h1>Cajas</h1>
                                    </div>
                                    <table class="table packages-table">
                                        <thead>
                                            <th>#</th>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Ancho</th>
                                            <th>Alto</th>
                                            <th>Largo</th>
                                            <th>Peso</th>
                                        </thead>
                                        <tbody>
                                            <?php  

                                                foreach ($allBoxesByW as $key => $value) {
                                            ?>
                                                    <tr>
                                                        <td><?php echo $warehouses->findBoxesById($value['id_box'])['code']; ?></td>
                                                        <td><?php echo $warehouses->findBoxesById($value['id_box'])['name']; ?></td>
                                                        <td><?php echo $warehouses->findBoxesById($value['id_box'])['width']; ?></td>
                                                        <td><?php echo $warehouses->findBoxesById($value['id_box'])['height']; ?></td>
                                                        <td><?php echo $warehouses->findBoxesById($value['id_box'])['lenght']; ?></td>
                                                        <td><?php echo $warehouses->findBoxesById($value['id_box'])['weight']; ?></td>
                                                    </tr>
                                            <?php
                                                }

                                            ?>
                                        </tbody>
                                    </table>
                                    
                                    <div class="form-group mb-4">
                                        <div class="col-sm-12">
                                            <button onclick="editWarehouse();" class="btn btn-success">Editar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer text-center"> 2021 © Total Envios
            </footer>
        </div>
        
    </div>
    <?php 

        include 'foot.php';

    ?>
    </body>

    </html>