<?php  
    
    include 'includes/connection.class.php';
    include 'includes/headquarters.class.php';
    include 'includes/users.class.php';
    include 'includes/warehouses.class.php';
    
    
    $users = new Users;
    $headquarters = new Headquarters;
    $warehouses = new Warehouses;
    $warehouse_details = $_REQUEST['id'];
    //buscar usuario para ese id
    
    $warehouseById = $warehouses->findById($warehouse_details);
    $allBoxesForWarehouse = $warehouses->findBoxesWarehouse($warehouse_details);
    $destinyWarehouse = $warehouses->findWarehouseDestiny($warehouse_details);

    //DATA DEL WAREHOUSE

    $USER_FROM_NAME= $users->findById($warehouseById['id_from'])['name'];
    $USER_FROM_LASTNAME = $users->findById($warehouseById['id_from'])['lastname'];
    $USER_FROM= $users->findById($warehouseById['id_from'])['id'];
    

    $USER_TO_NAME= $users->findAddresseeById($warehouseById['id_to'])[0]['name'];
    $USER_TO_LASTNAME = $users->findAddresseeById($warehouseById['id_to'])[0]['lastname'];
    $USER_TO = $users->findAddresseeById($warehouseById['id_to'])[0]['id'];

    $COUNTRY = $warehouses->findCountryById($destinyWarehouse[0]['country_id']);
    $STATE = $warehouses->findEstadosById($destinyWarehouse[0]['state_id']);
    $CITY = $warehouses->findcityNameForwh($destinyWarehouse[0]['city_id']);
    //$ADDRESS = $warehouses->findCountryById($destinyWarehouse['country_id']);

    $DEPART = $warehouses->findDepartById($warehouseById['id_depart']);
    $CURRIER = $warehouses->findCurriersById($warehouseById['id_currier']);
    $TIPO_ENVIO = $warehouses->findTripById($warehouseById['trip_id']);
    /********** */
    $STATUS_WH = $warehouses->findStatusForId($warehouseById['ship_state']);
    $STATUS_ENVIO = $warehouses->findShipStateForId($warehouseById['state']);

    $ALLSTATUSWH = $warehouses->findStatusFor();
    $ALLSTATUSENVIO = $warehouses->findShipStateFor();
    /** */
    $FECHA_SALIDA = $warehouseById['date_send'];



    $allcountriesRegister = $users->findCountryForRegister();
    $allCountries = $users->findAddressee();
    $allAddressStates = $users->findAddressStates();
    $allAddressCities = $users->findCities();

    $allDepart = $warehouses->findDepart();
    $allCurrier = $users->findCurriers();
    $allTrip = $warehouses->findTrips();

    $allHeadQ = $headquarters->findAll();
    $userTypes = $users->findRegisterTypes();
    $digits = 5;
    $fecha = new DateTime();
    $dataFecha = $fecha->getTimestamp();
    $code = 'TE -'.substr($dataFecha, -5);
    $categories = $warehouses->findCategories();
    $lastIdForBox = ($warehouses->findLastIdBox()['id']+1);
    $boxCode = $lastIdForBox;


?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php 

        include 'head.php';
        

    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        #myMap {
            height: 350px;
            width: 680px;
        }
    </style>
    <script
        src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCnFc9MHexw438xRR2JF6yP044jDeZeF3U&sensor=false">
    </script>
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>


</head>

<body style="background:#e3e3e3ff;">

    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
        <?php 

            include 'navbar.php'; 
            

        ?>

        <div class="page-wrapper" style="background:#e3e3e3ff;">
            <div class="alert alert-danger register-error" role="alert">
            </div>
            <div class="alert alert-success register-success" role="alert">
            </div>

            <div class="modal fade bd-example-modal-xl" tabindex="-1" id="addBoxModal" role="dialog"
                aria-labelledby="addBoxModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">

                        <div class="modal-body">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-9 col-xlg-9 col-md-9">
                                            <h4> <strong> AGREGAR CAJAS A UN WAREHOUSE</strong></h4>
                                        </div>

                                    </div>
                                    <hr>
                                    <br>
                                    <div class="form-group" hidden>
                                        <label for="box-code">Nro. de caja</label>
                                        <input id="box-code" type="text" placeholder="Codigo" class="form-control"
                                            value="<?php echo $boxCode; ?>" readonly="readonly">
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12 col-xlg-12 col-md-12">
                                            <div class="alert alert-danger register-box-error" role="alert">
                                            </div>
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-9 col-xlg-9 col-md-9">
                                                            <h4> <strong> CAJAS</strong></h4>
                                                        </div>

                                                    </div>
                                                    <hr>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-lg-5 col-xlg-5 col-md-5">
                                                            <input id="box-id" value="0" hidden>
                                                            <input id="warehouse_id" value="0" hidden>
                                                            
                                                        </div>
                                                        <div class="col-lg-5 col-xlg-5 col-md-5">
                                                            <input id="box-id" type="hidden" value="0">
                                                            <div class="form-group">
                                                                <!-- <label for="box-name">BOX</label> -->
                                                                <input onkeyup="searchBoxes();" id="box-name"
                                                                    type="text" placeholder="Busca un BOX"
                                                                    class="form-control">
                                                                <ul id="myBOX">
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-1 col-xlg-1 col-md-1">
                                                            <a href="settings-box.php" class="btn btn-primary"
                                                                style="background-color:white; color:#005574; border:2px solid #005574;"><i
                                                                    class="fas fa-plus"></i></a>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group">
                                                                <label for="box-height">Alto</label>
                                                                <input id="box-height" type="number" placeholder="Alto"
                                                                    class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group">
                                                                <label for="box-width">Ancho</label>

                                                                <input id="box-width" type="number" placeholder="Ancho"
                                                                    class="form-control">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group">
                                                                <label for="box-lenght">Largo</label>

                                                                <input id="box-lenght" type="number" placeholder="Largo"
                                                                    class="form-control">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group">
                                                                <label for="box-weight">Peso</label>

                                                                <input id="box-weight" type="number" placeholder="Peso"
                                                                    class="form-control">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-xlg-4 col-md-4">
                                                            <div class="form-group">
                                                                <label for="category">costo</label>

                                                                <input id="box-cost" type="text"
                                                                    placeholder="Costo de la mercancia"
                                                                    class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-5 col-xlg-5 col-md-5">
                                                            <div class="form-group">
                                                                <label for="category">Categoria</label>

                                                                <select id="category" class="form-control">
                                                                    <?php  

                                                            foreach ($categories as $key => $value) {
                                                        ?>
                                                                    <option value="<?php echo $value['id']; ?>">
                                                                        <?php echo $value['name']; ?></option>
                                                                    <?php
                                                        }

                                                        ?>
                                                                </select>

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-5 col-xlg-5 col-md-5">
                                                            <div class="form-group">
                                                                <label for="category">Descripción</label>

                                                                <input id="box-description" type="text"
                                                                    placeholder="Descripcion" class="form-control">
                                                                <input id="box_id" hidden>
                                                            </div>
                                                        </div>

                                                    </div><br>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-lg-12 col-xlg-12 col-md-12">

                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-5 col-xlg-5 col-md-5">
                                                            <h4> <strong> MEDIDAS </strong></h4>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="pieCubico">FT<sup>3</sup></label>

                                                                <input id="pieCubico" type="number" placeholder="0.00"
                                                                    class="form-control" readonly>

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="LBVOL">LBVol</label>

                                                                <input id="LBVOL" type="number" placeholder="0.00"
                                                                    class="form-control" readonly>

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="pesolb">PESO LB</label>

                                                                <input id="pesolb" type="number" placeholder="0.00"
                                                                    class="form-control" readonly>

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="pesokg">PESO KG</label>

                                                                <input id="pesokg" type="number" placeholder="0.00"
                                                                    class="form-control" readonly>

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="metrocubico">METRO<sup>3</sup></label>

                                                                <input id="metrocubico" type="number" placeholder="0.00"
                                                                    class="form-control" readonly>

                                                            </div>
                                                        </div>

                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2" align="right"
                                                            id="aereo" style="display:none;">

                                                            <img src="icons/air.png" alt="">
                                                        </div>
                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-" align="right"
                                                            id="maritimo" style="display:none;">

                                                            <img src="icons/ocean.png" alt="">
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>



                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-xlg-6 col-md-6">
                                                            <h4><strong>COSTOS</strong></h4>
                                                        </div>
                                                        <input id="trip" value=" <?php echo $warehouseById['trip_id'];?>">
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="tarifa_base"
                                                                    style="color:#005574;"><strong>TARIFA</strong></label>

                                                                <input id="tarifa_base" type="number" placeholder="0.00"
                                                                    class="form-control" style="border-color:#fc3c3d;">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="tasa_base_te"
                                                                    style="color:#005574;"><strong>TARIFA
                                                                        TE</strong></label>

                                                                <input id="tasa_base_te" type="number"
                                                                    placeholder="0.00" class="form-control"
                                                                    style="border-color:#fc3c3d;">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="tarifaInterna_base"
                                                                    style="color:#005574;">T. INTERNA</label>

                                                                <input id="tarifaInterna_base" type="number"
                                                                    placeholder="0.00" class="form-control"
                                                                    style="border-color:#fc3c3d;">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="currier_tasa">COSTO CURRIER</label>

                                                                <input id="currier_tasa" type="number"
                                                                    placeholder="0.00" class="form-control" readonly>

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="costo_interno">COSTO INTERNO</label>

                                                                <input id="costo_interno" type="number"
                                                                    placeholder="0.00" class="form-control" readonly>

                                                            </div>
                                                        </div>

                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="material_tasa">COSTO MATERIAL</label>

                                                                <input id="material_tasa" type="number"
                                                                    placeholder="0.00" class="form-control"
                                                                    value="0.00">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="costo_total">COSTO TOTAL</label>

                                                                <input id="costo_total" type="number" placeholder="0.00"
                                                                    class="form-control" value="0.00">

                                                            </div>
                                                        </div>

                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="total">MONTO TOTAL</label>

                                                                <input id="total" type="number" placeholder="0.00"
                                                                    class="form-control">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-1 col-xlg-1 col-md-1">
                                                            <div class="form-group mb-4">
                                                                <label for="ganancia">GAN</label>

                                                                <input id="ganancia" type="number" placeholder="0.00"
                                                                    class="form-control">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-1 col-xlg-1 col-md-1">
                                                            <div class="form-group mb-4">
                                                                <label for="porcGanancia">%GAN</label>

                                                                <input id="porcGanancia" type="number"
                                                                    placeholder="0.00" class="form-control">

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-xlg-2 col-md-2" id="edit-box-for-table">
                                                    <label for="category"></label>
                                                    <button id="addBoxButtonForModal" onclick="addBoxForModal();"
                                                        class="btn-cargar-cajas" style="margin-top:25px;"><i
                                                            class="fas fa-check"></i>AGREGAR</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><br><br>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div class="modal fade bd-example-modal-xl" tabindex="-1" id="editBoxModal" role="dialog"
                aria-labelledby="editBoxModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">

                        <div class="modal-body">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-9 col-xlg-9 col-md-9">
                                            <h4> <strong> EDITAR CAJAS DE UN WAREHOUSE</strong></h4>
                                        </div>

                                    </div>
                                    <hr>
                                    <br>


                                    <div class="row">
                                        <div class="col-lg-12 col-xlg-12 col-md-12">
                                            <div class="alert alert-danger register-box-error" role="alert">
                                            </div>
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-9 col-xlg-9 col-md-9">
                                                            <h4> <strong> CAJAS</strong></h4>
                                                        </div>

                                                    </div>
                                                    <hr>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-lg-5 col-xlg-5 col-md-5">
                                                            <input id="box-id" value="0" hidden>
                                                            <input id="warehouse_id" value="0" hidden>
                                                            <div class="form-group">
                                                                <!-- <label for="box-name">BOX</label> -->
                                                                <input onkeyup="searchPromos();" id="promociones"
                                                                    type="text" placeholder="Busca una promoción"
                                                                    class="form-control">
                                                                <ul id="myPromo">
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-5 col-xlg-5 col-md-5">
                                                            <input id="box-id" type="hidden" value="0">
                                                            <div class="form-group">
                                                                <!-- <label for="box-name">BOX</label> -->
                                                                <input onkeyup="searchBoxes();" id="box-name"
                                                                    type="text" placeholder="Busca un BOX"
                                                                    class="form-control">
                                                                <ul id="myBOX">
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-1 col-xlg-1 col-md-1">
                                                            <a href="settings-box.php" class="btn btn-primary"
                                                                style="background-color:white; color:#005574; border:2px solid #005574;"><i
                                                                    class="fas fa-plus"></i></a>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group">
                                                                <label for="box-height">Alto</label>
                                                                <input id="box-height" type="number" placeholder="Alto"
                                                                    class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group">
                                                                <label for="box-width">Ancho</label>

                                                                <input id="box-width" type="number" placeholder="Ancho"
                                                                    class="form-control">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group">
                                                                <label for="box-lenght">Largo</label>

                                                                <input id="box-lenght" type="number" placeholder="Largo"
                                                                    class="form-control">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group">
                                                                <label for="box-weight">Peso</label>

                                                                <input id="box-weight" type="number" placeholder="Peso"
                                                                    class="form-control">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-xlg-4 col-md-4">
                                                            <div class="form-group">
                                                                <label for="category">costo</label>

                                                                <input id="box-cost" type="text"
                                                                    placeholder="Costo de la mercancia"
                                                                    class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-5 col-xlg-5 col-md-5">
                                                            <div class="form-group">
                                                                <label for="category">Categoria</label>

                                                                <select id="category" class="form-control">
                                                                    <?php  

                                                            foreach ($categories as $key => $value) {
                                                        ?>
                                                                    <option value="<?php echo $value['id']; ?>">
                                                                        <?php echo $value['name']; ?></option>
                                                                    <?php
                                                        }

                                                        ?>
                                                                </select>

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-5 col-xlg-5 col-md-5">
                                                            <div class="form-group">
                                                                <label for="category">Descripción</label>

                                                                <input id="box-description" type="text"
                                                                    placeholder="Descripcion" class="form-control">
                                                                <input id="box_id" hidden>
                                                            </div>
                                                        </div>

                                                    </div><br>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-lg-12 col-xlg-12 col-md-12">

                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-5 col-xlg-5 col-md-5">
                                                            <h4> <strong> MEDIDAS </strong></h4>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="pieCubico">FT<sup>3</sup></label>

                                                                <input id="pieCubico" type="number" placeholder="0.00"
                                                                    class="form-control" readonly>

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="LBVOL">LBVol</label>

                                                                <input id="LBVOL" type="number" placeholder="0.00"
                                                                    class="form-control" readonly>

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="pesolb">PESO LB</label>

                                                                <input id="pesolb" type="number" placeholder="0.00"
                                                                    class="form-control" readonly>

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="pesokg">PESO KG</label>

                                                                <input id="pesokg" type="number" placeholder="0.00"
                                                                    class="form-control" readonly>

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="metrocubico">METRO<sup>3</sup></label>

                                                                <input id="metrocubico" type="number" placeholder="0.00"
                                                                    class="form-control" readonly>

                                                            </div>
                                                        </div>

                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2" align="right"
                                                            id="aereo" style="display:none;">

                                                            <img src="icons/air.png" alt="">
                                                        </div>
                                                        <div class="col-sm-2 col-md-2 col-lg-2 col-xl-" align="right"
                                                            id="maritimo" style="display:none;">

                                                            <img src="icons/ocean.png" alt="">
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>



                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-xlg-6 col-md-6">
                                                            <h4><strong>COSTOS</strong></h4>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="tarifa_base"
                                                                    style="color:#005574;"><strong>TARIFA</strong></label>

                                                                <input id="tarifa_base" type="number" placeholder="0.00"
                                                                    class="form-control" style="border-color:#fc3c3d;">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="tasa_base_te"
                                                                    style="color:#005574;"><strong>TARIFA
                                                                        TE</strong></label>

                                                                <input id="tasa_base_te" type="number"
                                                                    placeholder="0.00" class="form-control"
                                                                    style="border-color:#fc3c3d;">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="tarifaInterna_base"
                                                                    style="color:#005574;">T. INTERNA</label>

                                                                <input id="tarifaInterna_base" type="number"
                                                                    placeholder="0.00" class="form-control"
                                                                    style="border-color:#fc3c3d;">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="currier_tasa">COSTO CURRIER</label>

                                                                <input id="currier_tasa" type="number"
                                                                    placeholder="0.00" class="form-control" readonly>

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="costo_interno">COSTO INTERNO</label>

                                                                <input id="costo_interno" type="number"
                                                                    placeholder="0.00" class="form-control" readonly>

                                                            </div>
                                                        </div>

                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="material_tasa">COSTO MATERIAL</label>

                                                                <input id="material_tasa" type="number"
                                                                    placeholder="0.00" class="form-control"
                                                                    value="0.00">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="costo_total">COSTO TOTAL</label>

                                                                <input id="costo_total" type="number" placeholder="0.00"
                                                                    class="form-control" value="0.00">

                                                            </div>
                                                        </div>

                                                        <div class="col-lg-2 col-xlg-2 col-md-2">
                                                            <div class="form-group mb-4">
                                                                <label for="total">MONTO TOTAL</label>

                                                                <input id="total" type="number" placeholder="0.00"
                                                                    class="form-control">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-1 col-xlg-1 col-md-1">
                                                            <div class="form-group mb-4">
                                                                <label for="ganancia">GAN</label>

                                                                <input id="ganancia" type="number" placeholder="0.00"
                                                                    class="form-control">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-1 col-xlg-1 col-md-1">
                                                            <div class="form-group mb-4">
                                                                <label for="porcGanancia">%GAN</label>

                                                                <input id="porcGanancia" type="number"
                                                                    placeholder="0.00" class="form-control">

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-xlg-2 col-md-2" id="edit-box-for-table">
                                                    <label for="category"></label>
                                                    <button id="addBoxButton" onclick="editBoxForMod();"
                                                        class="btn-cargar-cajas" style="margin-top:25px;"><i
                                                            class="fas fa-edit"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><br><br>
                            </div>
                        </div>
                    </div>

                </div>
            </div>








            <br>
            <h3 style="color:#005574;"><strong> <?php echo $warehouseById['code']?></strong></h3><br>
            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <input id="countryVenezuela" type="hidden" value="0">
                        <input id="wh_id_update" type="hidden" value="<?php echo $warehouseById['id'];?>">
                        <input id="destiner_id" type="hidden">
                        <div class="col-lg-2 col-xlg-2 col-md-2">
                            <div class="form-group">
                                <label for="user_from" style="color:black;">CLIENTE</label>
                                <div class="input-group mb-3">

                                    <input id="id_user_from" value="<?php echo $warehouseById['id_from'];?>" hidden>
                                    <input type="text" class="form-control" placeholder="Nombre"
                                        aria-label="Recipient's username" aria-describedby="basic-addon2"
                                        onkeyup="searchUserPackage();" id="user_from"
                                        value="<?php echo $USER_FROM_NAME.' '.$USER_FROM_LASTNAME; ?>" readonly>


                                </div>
                                <ul id="myUS">
                                </ul>
                                <!-- <ul id="myDestiner">
                                    </ul> -->
                            </div>
                        </div>
                        <div class="col-lg-2 col-xlg-2 col-md-2">
                            <div class="form-group">
                                <label for="user_from" style="color:black;">DESTINATARIO</label>
                                <div class="input-group mb-3">
                                    <input id="id_user_to" value="<?php echo $warehouseById['id_to'];?>" hidden>
                                    <input type="text" class="form-control" placeholder="Nombre"
                                        aria-label="Recipient's username" aria-describedby="basic-addon2"
                                        onkeyup="searchFilter();" id="myInput"
                                        value="<?php echo $USER_TO_NAME.' '.$USER_TO_LASTNAME; ?>">


                                </div>
                                <ul id="myDestiner">
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-2 col-xlg-2 col-md-2">
                            <input id="id_destiny_wh" value="<?php echo $destinyWarehouse[0]['id']; ?>" hidden>
                            <div class="form-group">
                                <label for="contact_phone_destiner">PAIS</label>



                                <select id="country_id_wh" class="form-select campos-input-login">
                                    <option value="<?php echo $COUNTRY['id'];?>"><?php echo $COUNTRY['name']; ?>
                                    </option>
                                    <?php  
                                                                                        foreach ($allCountries as $key => $value) {
                                                                                    ?>
                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php
                                                                                        }
                                                                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-lg-2 col-xlg-2 col-md-2">
                            <div class="form-group">
                                <label for="contact_phone_destiner">ESTADO</label>

                                <select id="state_id_wh" class="form-select campos-input-login">
                                    <option value="<?php echo $STATE[0]['id_estado'];?>">
                                        <?php echo $STATE[0]['estado']; ?></option>
                                    <?php  
                                                                                        foreach ( $allAddressStates as $key => $value) {
                                                                                    ?>
                                    <option value="<?php echo $value['id_estado']; ?>"><?php echo $value['estado']; ?>
                                    </option>
                                    <?php
                                                                                        }
                                                                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-lg-2 col-xlg-2 col-md-2">
                            <div class="form-group">
                                <label for="contact_phone_destiner">CIUDAD</label>

                                <select id="city_id_wh" class="form-select campos-input-login">
                                    <option value="<?php echo $CITY[0]['id'];?>"><?php echo $CITY[0]['name']; ?>
                                    </option>
                                    <?php  
                                                                                        foreach ( $allAddressCities as $key => $value) {
                                                                                    ?>
                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php
                                                                                        }
                                                                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-lg-2 col-xlg-2 col-md-2">
                            <div class="form-group">
                                <label for="contact_phone_destiner">DIRECCIÓN</label>
                                <input id="address_edit_wh"
                                    value="<?php echo $destinyWarehouse[0]['address_destiner'];?>" class="form-control">

                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-xlg-2 col-md-2">
                            <div class="form-group">
                                <label for="contact_phone_destiner">DEPARTAMENTO</label>

                                <select id="depart_id_wh" class="form-select campos-input-login">

                                    <option value="<?php echo $DEPART['id'];?>"><?php echo $DEPART['nombre']; ?>
                                    </option>
                                    <?php  
                                foreach ( $allDepart as $key => $value) {
                                    if($value['id'] == $DEPART['id']){
                                        $key = $key+1;
                                    }else{
                                    
                            ?>
                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['nombre']; ?></option>
                                    <?php
                                }
                                 }
                            ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-lg-2 col-xlg-2 col-md-2">
                            <div class="form-group">
                                <label for="contact_phone_destiner">COURIER</label>

                                <select id="currier_id_wh" class="form-select campos-input-login">
                                    <option value="<?php echo $CURRIER['id'];?>"><?php echo $CURRIER['name']; ?>
                                    </option>
                                    <?php  
                                    foreach ( $allCurrier as $key => $value) {
                                        if($value['id'] == $CURRIER['id']){
                                            $key = $key+1;
                                        }else{
                                ?>
                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php
                                    }
                                     }
                                ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-lg-2 col-xlg-2 col-md-2" style="display:none;">
                            <div class="form-group">
                                <label for="contact_phone_destiner">TIPO DE ENVIO</label>

                                <select id="trip_id_wh" class="form-select campos-input-login">
                                    <option value="<?php echo $TIPO_ENVIO['id'];?>"><?php echo $TIPO_ENVIO['name']; ?>
                                    </option>
                                    <?php  
                                                                                        foreach ( $allTrip as $key => $value) {
                                                                                            if($value['id'] == $TIPO_ENVIO['id']){
                                                                                                $key = $key+1;
                                                                                            }else{
                                                                                    ?>
                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php
                                                                                        }
                                                                                         }
                                                                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-lg-2 col-xlg-2 col-md-2">
                            <div class="form-group">
                                <label for="contact_phone_destiner">FECHA DE ENVIO</label>

                                <input value="<?php echo date('Y-m-d', strtotime($FECHA_SALIDA)) ?>" type="date"
                                    class="form-control" id="date_send_wh">

                            </div>
                        </div>

                        <div class="col-lg-2 col-xlg-2 col-md-2">
                            <div class="form-group">
                                <label for="contact_phone_destiner">STATUS WH</label>
                                <h1> <?php echo $STATUS_ENVIO['id'];?> </h1>
                                <select id="status_wh" class="form-select campos-input-login">
                                    <option value="<?php echo $STATUS_ENVIO[0]['id'];?>">
                                        <?php echo $STATUS_ENVIO[0]['name']; ?></option>
                                    <?php  
                                                                                        foreach ( $ALLSTATUSENVIO as $key => $value) {
                                                                                            if($value['id'] == $STATUS_ENVIO[0]['id']){
                                                                                                $key = $key+1;
                                                                                            }else{
                                                                                    ?>
                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php
                                                                                        }
                                                                                         }
                                                                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-lg-3 col-xlg-3 col-md-3">
                            <div class="form-group">
                                <label for="contact_phone_destiner">STATUS ENVIO</label>

                                <h1> <?php echo $STATUS_WH['id'];?> </h1>

                                <select id="ship_status" class="form-select campos-input-login">
                                    <option value="<?php echo $STATUS_WH[0]['id'];?>">
                                        <?php echo $STATUS_WH[0]['name']; ?></option>
                                    <?php  
                                                                                        foreach ( $ALLSTATUSWH as $key => $value) {
                                                                                            if($value['id'] == $STATUS_WH[0]['id']){
                                                                                                $key = $key+1;
                                                                                            }else{
                                                                                    ?>
                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php
                                                                                        }
                                                                                         }
                                                                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-lg-1 col-xlg-1 col-md-1">
                        <label for="num_guia">#GUIA</label>
                        <input id="num_guia"
                                    value="<?php echo $warehouseById['num_guia'];?>" class="form-control">

                        </div>


                        
                    </div>

                    <div class="row">
                        <div class="col-lg-10 col-xlg-10 col-md-10">
                            <label for="nota_wh">NOTA</label>
                            <input id="nota_wh"
                                    value="<?php echo $warehouseById['nota_wh'];?>" class="form-control">
                        </div>
                        <div class="col-lg-2 col-xlg-2 col-md-2">
                        
                            <button id="edit_warehouse" onclick="updateWarehouseData();" class="btn btn-primary"
                                style="margin-top:30px; background:#fc3c3d; color:white;"><strong>EDITAR</strong></button>
                            </div>
                    </div>

                



                </div>
            </div>
            <hr>




            <div class="row">






                <div class="card">
                    <div class="card-body">
                        <h3 style="color:#005574;">
                            <center> <strong>CAJAS</strong></center>
                        </h3><br>
                        <div class="col-lg-1 col-xlg-1 col-md-1">
                            <button id="modal_add_box" onclick="modalBoxAddtOpen();" class="btn btn-primary"
                                style="margin-top:30px; background:#fc3c3d; color:white;"><strong>Agregar Caja</strong></button>

                        </div>
                        <br>
                        <table class="table table-warehouse"
                            style="text-align:center; table-layout: fixed; text-layout:fixed;">
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">#</th>
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">CAT</th>
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">DESC</th>   
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">COSTO</th>

                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">LARGO</th>
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">ANCHO</th>
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">ALTO</th>
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">PESO</th>

                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">
                                FT<sup>3</sup></th>
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">LB VOL</th>
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">T. CURRIER</th>
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">T. INTERNA
                            </th>
                            <!-- <th>TafINT</th>
                                                <th>CostINT</th> -->
                            <!-- <th>Subtotal</th> -->
                            <!-- <th>Tasa Taf</th> -->
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">MATERIAL
                            </th>
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">COSTO TOTAL
                            </th>
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">T. TE</th>
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">MONTO TOTAL</th>
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">GANAN.</th>
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">%GANAN.</th>
                            <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;"></th>
                            <!-- <th>% GANAN</th> -->
                            <!-- <th>Eliminar</th> -->
                            <tbody id="boxes-table">
                                <?php  
                                                        foreach ($allBoxesForWarehouse as $key => $value) {
                                                            $box = $warehouses->findBoxesById($value['id_box']);
                                                        ?>
                                <input id="box-id-read" value="<?php echo $value['id_box']; ?>" hidden>
                                <tr>
                                <td><?php echo $warehouses->findBoxesById($value['id_box'])['id']; ?></td>
			<td><?php echo $warehouses->findCategoryById($warehouses->findBoxesById($value['id_box'])['category_id'])['name']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['description']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cost']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['lenght']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['width']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['height']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['weight']; ?></td>
			
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cubic_feet']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['lb_vol']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cost_taf_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cost_inter_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['gastosExtra']; ?></td>
			
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['monto_calculo_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['tasa_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['cost_total']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['ganancia_cost_type_trip']; ?></td>
			<td><?php echo $warehouses->findBoxesById($value['id_box'])['porcentaje_ganancia_cost_type_trip']; ?></td>

                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                        <div class="btn-group" style="content-align:center; width:100%">
                                            <button onclick="deleteBoxDetails(<?php echo $box['id'];?>);"
                                                class="btn btn-primary" style="background:#fc3c3d; color:white;"><i
                                                    class="fas fa-trash"></i></button>
                                            <button onclick="modalBoxEditOpen();" class="btn btn-primary"
                                                style="background:#fc3c3d; color:white;"><i
                                                    class="fas fa-edit"></i></button>
                                        </div>
                                    </td>
                                </tr>
                                <?php  
                                                                    }
                                                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>




            </div>


        </div>
    </div>
    <script src="bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="js/app-style-switcher.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.js"></script>
    <script src="js/main.js"></script>
</body>

</html>