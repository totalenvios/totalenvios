<?php  
        // error_reporting(0);
        // ini_set('display_errors', 0);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        
        include 'includes/connection.class.php';
        include 'includes/users.class.php';
        include 'includes/packages.class.php';
        include 'includes/headquarters.class.php';
        include 'includes/stock.class.php';
        include 'includes/warehouses.class.php';
        include 'includes/depart.class.php';

        $users = new Users;
        $allUsers = $users->findAll();
        $packages = new Packages;
        $allPackages = $packages->findAll();
        $depart = new Depart;
        $allDeparts = $depart->findAll();
        $headquarters = new Headquarters;
        $allHeadquarters = $headquarters->findAll(); 
        $allOperators = $users->findAllOperators();
        $allClients = $users->findAllClients();
        $states = $packages->findStates();

        $countCloseWH = $packages->countCloseWarehouse()['warehouse'];
        $countOpenWH = $packages->countOpenWarehouse()['warehouse'];
        $countOceanWH = $packages->countOceanWarehouse()['warehouse'];
        $countAirWH = $packages->countAirWarehouse()['warehouse'];

        
        $listClientForSendWH = $packages->whListForSend();

        $allstatusShip = $packages->findAllStatusShip();


        $stock = new Stock;
        $warehouses = new Warehouses;
        $warehouseStates = $warehouses->findWStates();
        $allWarehousesOnHand = $warehouses->findAll();
        $allWarehousesComplete = $warehouses->findAllComplete();
        $findLast = $warehouses->findLast();
        $boxes = $packages->findBoxes();
        $trips = $warehouses->findTrips();
        $categories = $warehouses->findCategories();
        $countries = $warehouses->findCountries();
        session_start();

        $regions = $users->findRegions();

        $userDestiny = $users->findByEmail($_SESSION['email'])['name'].' '.$users->findByEmail($_SESSION['email'])['lastname'];
        $userDestinyId = $users->findByEmail($_SESSION['email'])['id'];
        $dataUser = $users->findByEmail($_SESSION['email']);
        $destiny = $users->findAddressById($dataUser['country']);
        $curriers = $packages->findCurriers();
        // $zoneCurrier = $users->findCurrierByZone($dataUser['country']);
        $regionCurrier = $users->findRegionByZone($dataUser['country']);

        if ($_SESSION['state'] != 1) {
            header('Location: login.php');
        }

        $date_in = date('2020-m-d');
        $date_out = date('Y-m-d');
        $digits = 5;
        $fecha = new DateTime();
        $dataFecha = $fecha->getTimestamp();
        // $lastId = $warehouses->findLastId()['id'];
        // $code = 'TE-WH'.$lastId;
        $boxCode = substr($dataFecha, -5);
        $bulk = isset($_REQUEST['bulk']) ? $_REQUEST['bulk'] : 0;

        $nextFriday = strtotime('next friday');
        $nextFriday = date('Y-m-d', $nextFriday);

    ?>

    <!DOCTYPE html>
    <html dir="ltr" lang="en">

    <head>
        <?php 

            include 'head.php';

        ?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    </head>

    <body style="background:#e3e3e3ff;">
    <input id="date_in" type="hidden" value="<?php echo $date_in; ?>">
    <input id="hiddenBoxCode" type="hidden" value="<?php echo $boxCode; ?>">
    <input id="wDelete" type="hidden" value="0">    
    <input id="warehouseId" type="hidden" value="0">
    <input id="idPackage" type="hidden" value="0">
    <input id="idPackageM" type="hidden" value="0">
    <input id="idUser" type="hidden" value="0">
    <input id="idUserFor" type="hidden" value="<?php echo $userDestinyId; ?>">
    <input id="idUserW" type="hidden" value="0">
    <!-- <input id="bulkPackages" type="hidden" value="0"> -->
        <!-- Modal -->
    

    <div class="modal fade" id="addPackageModal" tabindex="-1" role="dialog" aria-labelledby="warehouseModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="warehouseModalLabel">Warehouse:</h5>
                <button onclick="$('#addPackageModal').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-xlg-12 col-md-12">
                            <div class="alert alert-danger register-errorM" role="alert">
                            </div>
                            <div class="alert alert-success register-successM" role="alert">
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group mb-4">
                                                <label class="col-md-12 p-0">Paquete #</label>
                                                <div class="col-md-12 border-bottom p-0">
                                                    <input onkeyup="searchWPackageM();" id="packageM" class="form-control" type="text" value="">
                                                    <ul id="myUWM">
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group mb-4">
                                                        <label class="col-sm-12">Alto *</label>
                                                        <div class="col-sm-12 border-bottom">
                                                            <input id="heightM" type="number" readonly="readonly" placeholder="Alto" class="form-control p-0 border-0">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group mb-4">
                                                        <label class="col-sm-12">Ancho *</label>
                                                        <div class="col-sm-12 border-bottom">
                                                            <input id="widthM" type="number" readonly="readonly" placeholder="Ancho" class="form-control p-0 border-0">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group mb-4">
                                                        <label class="col-sm-12">Largo *</label>
                                                        <div class="col-sm-12 border-bottom">
                                                            <input id="lenghtM" type="number" readonly="readonly" placeholder="Largo" class="form-control p-0 border-0">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group mb-4">
                                                        <label class="col-sm-12">Peso *</label>
                                                        <div class="col-sm-12 border-bottom">
                                                            <input id="weightM" type="number" readonly="readonly" placeholder="Peso" class="form-control p-0 border-0">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mb-4">
                                                <div class="col-sm-12">
                                                    <button onclick="addPackageToWarehouse();" class="btn btn-success">Agregar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="warehouseModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="warehouseModalLabel">Warehouse:</h5>
                <button onclick="$('#deleteModal').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-xlg-12 col-md-12">
                            <div class="alert alert-danger register-errorD" role="alert">
                            </div>
                            <div class="alert alert-success register-successD" role="alert">
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 id="deleteH3"></h5>
                                        </div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <div class="col-sm-12">
                                            <div class="text-center">
                                                <button onclick="deleteWarehouse();" class="btn btn-danger">Eliminar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>

    <div class="modal fade" id="shipStatusModal" tabindex="-1" role="dialog" aria-labelledby="shipStatusModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="shipStatusModalLabel">STATUS DE ENVIO</h5>
                        <button onclick="$('#userTypeModal').modal('hide');" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body text-center">
                    <div class="container-fluid text-center">
                        <div class="row text-center">
                            <h4 style="color:#005574;">Cambiar el status del envio</h4>
                            <div class="col-lg-12 col-xlg-12 col-md-12">
                                <div class="form-group">
                                        <select id="status_ship_id" class="form-select campos-input-login">
                                           
                                            <?php  
                                                foreach ($allstatusShip as $key => $value) {
                                            ?>
                                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                </div>
                               <input type="text" id="wh_id_to_change" hidden>

                                <div class="form-group mb-4">
                                    <div class="btn-group" style="content-align:center; width:100%">
                                                    

                                    <div><a onclick="cambiarShipStatus();"class="btn btn-primary" type="button" style="background-color:white; color:#005574; border:2px solid white;"><i class="fas fa-check"></i></a></div>
                                    
                                
                                    </div>
                                </div>
                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <div id="main-wrapper" data-header-position="absolute" data-boxed-layout="full" style="background:#e3e3e3ff;">
            <?php 

                include 'navbar.php'; 
                if ($_SESSION['state'] != 1) {
                    header('Location: login.php');
                }

            ?>
            <div class="page-wrapper" style="background:#e3e3e3ff;">
            <div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                                <hr style="background-color:#ff554dff; height: 4px; opacity: 1.0;">
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
                                <h3 style="color:#005574;"> <strong>WAREHOUSE</strong> </h3>
                            </div>
                        </div><br>
            <div class="row">
                                
                                <div class="col-lg-3 col-xlg-3 col-md-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                                    <img src="icons/ABIERTO.png" alt="" width="100" height="100">
                                                </div> 
                                                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                                    <h5 style="color:#005574;"><strong>ON HAND</strong></h5>
                                                    <h1 style="color:#005574;"><?php echo $countOpenWH; ?></h1>
                                                </div>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xlg-3 col-md-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                                    <img src="icons/CERRADO.png" alt="" width="100" height="100">
                                                </div> 
                                                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                                    <h5 style="color:#005574;"><strong>WH CERRADOS</strong></h5>
                                                    <h1 style="color:#005574;"><?php echo $countCloseWH; ?></h1>
                                                </div>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xlg-3 col-md-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">   
                                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                                    <img src="icons/ocean.png" alt="" width="100" height="100">
                                                 </div> 
                                                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                                    <h5 style="color:#005574;"><strong>ENVIOS MARÍTIMOS</strong></h5>
                                                    <h1 style="color:#005574;"><?php echo $countOceanWH; ?></h1>
                                                </div>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xlg-3 col-md-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                                     <img src="icons/air.png" alt="" width="100" height="100">
                                                </div> 
                                                <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                                    <h5 style="color:#005574;"><strong>ENVÍOS AÉREOS</strong></h5>
                                                    <h1 style="color:#005574;"><?php echo $countAirWH; ?></h1>
                                                </div>   
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                           <div class="card-body"><center>
                                    <div class="btn-group text-center" style="content-align:center; width:100%, padding:100px;" align="center">
                                                           
                                        
                                    
                                        
                                        <button class="btn btn-primary" type="button" id="whComplete" onclick="showCompleteWh();" style="background-color:#005574; color:white; border:2px solid white; width:300px;">WH LISTOS PARA ENVIAR</button>
                                        <button class="btn btn-primary" type="button" id="whOpen" onclick="showOnHand();" style="background-color:white; color:#005574; border:2px solid #005574; width:300px;">ON HAND</button>
                                        

                                        
                                                          
                                        </center>             
                                    </div>
                           </div>
                       </div>
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class_="col-lg-10 col-xlg-10 col-md-10">
                            <div class="row">
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <div class="input-group mb-3">
                                        <input id="myInputS" onkeyup="searchFilterW();" type="text" placeholder="Nombre del cliente" class="form-control mt-0 group-delivery">
                                            <ul id="myULS">
                                            </ul>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-4 col-lg-4"></div>
                                <div class="col-sm-2 col-md-2 col-lg-2">
                                    <input id="date_in_s" type="date" class="form-control" value="<?php echo $date_in; ?>">
                                </div>
                                <div class="col-sm-2 col-md-2 col-lg-2">
                                    <input id="date_out" type="date" class="form-control" value="<?php echo $date_out; ?>">
                                </div>
                                <div class="col-sm-1 col-md-1 col-lg-1" align="right">
                                    <button onclick="searchWarehouse();" class="btn btn-primary" style="background-color:white; color:#ff554dff; border:2px solid #ff554dff; width:100%;"><i style="color:#ff554dff;" class="fas fa-check"></i> Filtrar</button>
                                </div>
                            </div>
                                <hr style="background-color:#005574;">
                        </div>
                        <div class="row">
                            <div class="col-sm-9 col-md-9 col-lg-9">
                                <div align="left" style="background:white; color:#005574; width:100%; padding:8px;"><h4>Warehouses</h4>
                                </div>
                            </div><br>
                            <div class="col-sm-3 col-md-3 col-lg-3" style="text-align:right; content-align:right;">
                                <div class="btn-group" align="right">
                                    <a href="registerWarehouse.php" class="btn btn-primary" style="background-color:#ff554dff; color:white; border:2px solid #ff554dff;"><i style="color:white;" class="fas fa-plus"></i> Registrar</a>
                                    
                                </div>
                            </div>
                        </div>
                                        
                    </div>
                    <br>
                    <div id="whOnHand" style="display:none;">
                        <div class="row">
                            <div class_="col-lg-1 col-xlg-12 col-md-1">
                            </div>
                            <div class_="col-lg-10 col-xlg-10 col-md-10">
                                <table class="table table-warehouse" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                    <thead>
                                        
                                        <th style="border: 2px solid #e3e3e3ff;">STATUS</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">NRO. WAREHOUSE</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">CASILLERO</th>
                                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">CLIENTE</th>
                                        <!-- <th>Casillero</th> -->
                                        
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">DESTINATARO</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">DESTINO</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">TIPO DE ENVÍO</th>
            
                                        
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">F. DE SALIDA</th>
                                        <!-- <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Cliente</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Nro. Casillero</th> -->
                                        <!-- <th>Descripcion</th> -->
                                        
                                        <!-- <th>Ancho</th>
                                        <th>Largo</th>
                                        <th>Alto</th>
                                        <th>Peso</th> -->
                                        <!-- <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Conf. usuario</th> -->
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">STATUS DE ENVIO</th>
                                        <th style="width:12%; border: 2px solid #e3e3e3ff; background-color:#white;"></th>
                                        <th style="border: 2px solid #e3e3e3ff; background-color:#white;"></th>
                                    </thead>
                                    <tbody>
                                            <?php  

                                                foreach ($allWarehousesOnHand as $key => $value) {
                                            ?>
                                            <tr>
                                        
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php 
                                                if( $value['state'] == 2){
                                            ?> 
                                            <i class="fas fa-lock fa-2x fa-lg" style="margin-top:10px;"> </i>
                                            <?php 
                                                }else{
                                            ?>
                                            <i class="fas fa-unlock-alt fa-2x fa-lg" style="margin-top:10px;"> </i>
                                            <?php  
                                                };
                                            ?>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['code']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">TE-WH<?php echo $value['id_from']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['client_name']; ?></td>
                                        
                                        
                                        <!-- <td><?php echo $value['casillero']; ?></td> -->
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['destiner_name']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $warehouses->findcityNameForwh($warehouses->findcityDestiny($value['id'])[0]['city_id'])[0]['name']; ?></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"> <?php 
                                                                if( $value['trip_id'] == 2){
                                                            ?> 
                                                                <img src="icons/ocean.png" style="height: 20px; width: 20px;"/>
                                                            <?php 
                                                                }else{
                                                            ?>
                                                                <img src="icons/air.png" style="height: 30px; width: 30px;"/>
                                                            <?php  
                                                                };
                                                            ?>    </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['date_send']; ?></td>
                                    
                                    
                                        <!-- <td><?php echo $value['width']; ?></td>
                                        <td ><?php echo $value['lenght']; ?></td>
                                        <td><?php echo $value['height']; ?></td>
                                        <td><?php echo $value['weight']; ?></td> -->
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                            <?php 
                                                echo $warehouses->findGeneralStateById($value['ship_state'])['name'];
                                            ?>
                                            <button onclick="changeStatusShip(<?php echo $value['id']; ?>);" class="btn btn-primary" style="background-color:#005574; color:white; border:2px solid #005574;"><i class="fas fa-edit"></i></button></div>
                                        </td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><a target="_parent" href="warehouseDetails.php?id=<?php echo $value['id']; ?>" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-edit"></i></a></td>
                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white; text-layout:fixed;">
                                            <div class="btn-group" style="content-align:center; width:100%">
                                            
                                                <div><a target="_blank" href="closeTicketFormat.php?id=<?php echo $value['id']; ?>" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-tags"></i></a></div>
                                        
                                            
                                            <div><button onclick="$('#deleteModal').modal('show'); addDeleteText(<?php echo $value['id']; ?>)" class="btn btn-primary" style="background-color:#005574; color:white; border:2px solid #005574;"><i class="fas fa-trash"></i></button></div>
                                            <div><a target="_blank" href="invoceReceive.php?id=<?php echo $value['id'];?>" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-file"></i></a></div>
                                            
                                            </div>
                                        </td>
                                    </tr>
                                            <?php 

                                
                                    
                                         }

                                            ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class_="col-lg-1 col-xlg-12 col-md-1"></div>  
                        </div>
                    </div>
                    <div id="whComplete_wh">
                        <div class="row">
                            <div class_="col-lg-1 col-xlg-12 col-md-1">
                            </div>
                            <div class_="col-lg-10 col-xlg-10 col-md-10">
                                <table class="table table-warehouse" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                    <thead>
                                        <th style="border: 2px solid #e3e3e3ff;"></th>
                                        <th style="border: 2px solid #e3e3e3ff;">STATUS</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">NRO. WAREHOUSE</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">CASILLERO</th>
                                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white; ">CLIENTE</th>
                                        <!-- <th>Casillero</th> -->
                                        
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">DESTINATARO</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">DESTINO</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">TIPO DE ENVÍO</th>
            
                                        
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">F. DE SALIDA</th>
                                        <!-- <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Cliente</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Nro. Casillero</th> -->
                                        <!-- <th>Descripcion</th> -->
                                        
                                        <!-- <th>Ancho</th>
                                        <th>Largo</th>
                                        <th>Alto</th>
                                        <th>Peso</th> -->
                                        <!-- <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">Conf. usuario</th> -->
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">STATUS DE ENVIO</th>
                                        <th style="width:12%; border: 2px solid #e3e3e3ff; background-color:#white;"></th>
                                        <th style="border: 2px solid #e3e3e3ff; background-color:#white;"></th>
                                    </thead>
                                    <tbody>
                                        <?php  

                                            foreach ($allWarehousesComplete as $key => $value) {
                                        ?>
                                                <tr>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><input onclick="countChecks();" type="checkbox" class="form-check-input wcheck wcheck<?php echo $value['id'];?>" id="wcheck<?php echo $value['id'];?>" warehouse="<?php echo $value['id'];?>"></td>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php 
                                                            if( $value['state'] == 2){
                                                        ?> 
                                                        <i class="fas fa-lock fa-2x fa-lg" style="margin-top:10px;"> </i>
                                                        <?php 
                                                            }else{
                                                        ?>
                                                        <i class="fas fa-unlock-alt fa-2x fa-lg" style="margin-top:10px;"> </i>
                                                        <?php  
                                                            };
                                                        ?>
                                                    </td>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['code']; ?></td>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">TE-WH<?php echo $value['id_from']; ?></td>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['client_name']; ?></td>
                                                    
                                                    
                                                    <!-- <td><?php echo $value['casillero']; ?></td> -->
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['destiner_name']; ?></td>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $warehouses->findcityNameForwh($warehouses->findcityDestiny($value['id'])[0]['city_id'])[0]['name']; ?></td>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php 
                                                                if( $value['trip_id'] == 2){
                                                            ?> 
                                                                <img src="icons/ocean.png" style="height: 20px; width: 20px;"/>
                                                            <?php 
                                                                }else{
                                                            ?>
                                                                <img src="icons/air.png" style="height: 30px; width: 30px;"/>
                                                            <?php  
                                                                };
                                                            ?>  </td>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['date_send']; ?></td>
                                                
                                                
                                                    <!-- <td><?php echo $value['width']; ?></td>
                                                    <td ><?php echo $value['lenght']; ?></td>
                                                    <td><?php echo $value['height']; ?></td>
                                                    <td><?php echo $value['weight']; ?></td> -->
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;">
                                                        <?php 
                                                            echo $warehouses->findGeneralStateById($value['ship_state'])['name'];
                                                        ?>
                                                        <button onclick="changeStatusShip(<?php echo $value['id']; ?>);" class="btn btn-primary" style="background-color:#005574; color:white; border:2px solid #005574;"><i class="fas fa-edit"></i></button></div>
                                                    </td>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><a href="warehouseDetails.php?id=<?php echo $value['id']; ?>" target="_self" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-edit"></i></a></td>
                                                    <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white; text-layout:fixed;">
                                                        <div class="btn-group" style="content-align:center; width:100%">
                                                        
                                                            <div><a target="_blank" href="closeTicketFormat.php?id=<?php echo $value['id']; ?>" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-tags"></i></a></div>
                                                    
                                                        
                                                        <div><button onclick="$('#deleteModal').modal('show'); addDeleteText(<?php echo $value['id']; ?>)" class="btn btn-primary" style="background-color:#005574; color:white; border:2px solid #005574;"><i class="fas fa-trash"></i></button></div>
                                                        <div><a target="_blank" href="invoceReceive.php?id=<?php echo $value['id'];?>" class="btn btn-primary" style="background-color:white; color:#005574; border:2px solid #005574;"><i class="fas fa-file"></i></a></div>
                                                        
                                                        </div>
                                                    </td>
                                                </tr>
                                        <?php
                                            }

                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class_="col-lg-1 col-xlg-12 col-md-1"></div>  
                        </div>
                    </div>
                    <div id="whClientList_wh" style="display:none;">
                        <div class="row">
                            <div class_="col-lg-1 col-xlg-12 col-md-1">
                            </div>
                            <div class_="col-lg-10 col-xlg-10 col-md-10">
                                <table class="table table-warehouse" style="text-align:center; table-layout: fixed; text-layout:fixed;">
                                    <thead>
                                        
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">CASILLERO</th>
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">NOMBRE</th>
                                        <th style="border: 2px solid #e3e3e3ff; background-color:#005574; color:white;">APELLIDO</th>
                                        <!-- <th>Casillero</th> -->
                    
                                        <th style="border: 2px solid #e3e3e3ff; color:white; background-color:#005574;">WH POR ENVIAR</th>
                                       
                                    </thead>
                                        <tbody>
                                            <?php  

                                                foreach ($listClientForSendWH as $key => $value) {
                                            ?>
                                                    <tr>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['code']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['name']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['lastname']; ?></td>
                                                        <td style="border: 2px solid #e3e3e3ff; color:#005574; background-color:white;"><?php echo $value['cantidad_wh']; ?></td>     
                                                    </tr>
                                            <?php
                                                }

                                            ?>
                                        </tbody>
                                </table>
                            </div>
                            <div class_="col-lg-1 col-xlg-12 col-md-1"></div>  
                        </div>
                    </div>
                            
                </div>

                <footer class="footer text-center"> 2021 © Total Envios
                </footer>
            </div>
            
        </div>
        <?php 

            include 'foot.php';

        ?>
        <?php 

            if ($bulk != '0') {
            ?>
                <input id="packagesForBulk" type="hidden" value="1">
            <?php
                echo '<script type="text/javascript">'
                . '$( document ).ready(function() {'
                . '$("#warehouseModal").modal("show");'
                . 'getBulkPackages();'
                . '});'
                . '</script>';
            }else{
            ?>
                <input id="packagesForBulk" type="hidden" value="0">
            <?php
            }
        ?>
        <script>
            $(document).ready(function () {
                localStorage.removeItem('boxes');
            })
        </script>
    </body>

    </html>